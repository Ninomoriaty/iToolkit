Guideline Level

<table><tr><td  style="background:orange !important;"><center><font style="font-size:45px; color:black !important;"><b>
Computer Network
</b></font></center></td></tr></table>
# Reference

**Course**

- [ ] MSc 4 Introduction to web site and database design for drug discovery

**Textbook**

- [ ] 王道 408 计算机网络
- [ ] 0 计算机网络 (第8版）- 谢希仁
  - 图书在版编目(CIP)数据: 计算机网络/谢希仁编著 . 第8版 - 北京: 电子工业出版社, 2021.6 ISBN 978-7-121-41174-8

- [ ] Computer Networks
  - Computer Networks, 6th Edition by Andrew S. Tanenbaum, Nick Feamster, and David Wetherall, published by Pearson Education © 2020.
    - 6th
    - 5th
- [ ] Computer Networking A Top Down Approach
  - Computer Networking: A Top-Down Approach, Seventh Edition, ISBN 978-0-13-359414-0, by James F. Kurose and Keith W. Ross published by Pearson Education© 2017
    - 7th
    - 6th







# Pcp

- The programming languages used in the development of web service is placed in the "0 Programming Languages" folder in Wiznote since these programming language can do things more about web applications.【现在不用Wiznote了，但是比较糟糕的是并未进行整理。】
- This notebook and folder is more about the concepts and strategies used in web application development and you should realise the underlying principles of the networks.
- 





# ---------- 计算机网络体系结构 ----------

# ----- 概念 -----

# Soc-计算机网络的发展与影响

**第一阶段**

- 美国国防部高级研究计划局(ARPA)设计一个分散的指挥系统，**ARPAnet 阿帕网**
- **互联网**诞生，**TCP/IP协议**保证了其不同网络的互联，因特网是世界上最大的互联网【其实这个说法挺奇怪的，毕竟Internet就是Interconnected Network的简称】

**第二阶段**

- 1985年起,美国国家科学基金会NSF围绕6个大型计算机中心建设计算机网络, 即**国家科学基金网NSFNET**，具有**三级结构**

**第三阶段**

-  **多层次ISP结构**的因特网：因特网服务提供者/因特网服务提供商(ISP),是一个向广大用户综合提供互联网接入业务、信息业务、和增值业务的公司,如中国电信、中国联动、中国移动等。分为主干ISP、地区ISP和本地ISP。







# Cc-信息获取方式 Accessing information Model

## Client-server model

### Cc

- **Def:** a client-server model, where a client explicitly requests information from a server that hosts that information
- In this model, the data are stored on powerful computers called servers. Often, these are centrally housed and maintained by a system administrator. In contrast, the employees have simpler machines, called clients, on their desks, with which they access remote data, for example, to include in spreadsheets they are constructing.

### Ab

- remote

### Mt

![Fig.1-2 client-server](.\img\Fig.1-2 client-server.png)

- If we look at the client-server model, to a first approximation we see that two
  processes (running programs) are involved, one on the client machine and one on the server machine. 
- Communication takes the form of the client process sending a message over the network to the server process. The client process then waits for a reply message.
- When the server process gets the request, it performs the requested work or looks up the requested data and sends back a reply.



## Peer-to-peer communication(P2P)

### Cc

In this form, individuals who form a loose group can communicate with others in the group. Every person can,in principle, communicate with one or more other people; there is no fixed division into clients and servers

### e.g.

- BitTorrent
- Email





## Person-to-person communication

### Cc

### e.g.

- Instant Messaging
  - talk from UNIX
  - Twitter/X
  - Social network applications
- Media
  - Music/Radio
  - Video
  - Facebook
- Group
  - Wikipedia



## Cat-Other Online Events

### Electronic Commerce

#### Cat

![FIg.1-4 e-commerce](.\img\FIg.1-4 e-commerce.png)

#### e.g.

- Online shopping
- Access to financial institutions
- Online auctions



### Entertainment

#### e.g.

- IPTV (IP Television) systems
- 



### The Internet of Things (IoT)

#### Cc

- **Ubiquitous computing**: Ubiquitous computing entails computing that is embedded in everyday life.
- **IoT (Internet of Things)**
  - IoT (Internet of Things), is poised to connect just about every electronic device we purchase to the Internet







# Cc-计算机网络

**Def**

- a collection of interconnected, autonomous computing devices
  
- 一般认为,计算机网络是一个将分散的、具有独立功能的计算机系统,通过通信设备与线路连接起来,由功能完善的软件实现资源共享和信息传递的系统。
  - 计算机系统是各种终端设备的操作系统
  - 通信设备包括路由器、集线器、中继器、交换机等
  - 线路是逻辑线路，包括各种有线无线的连接

- 简而言之,计算机网络就是一些互连的、自治的计算机系统的集合。
  - 互连是指通过通信链路互联互通
  - 自治是指无主从关系


**Def-广义观点**

- 只要是能实现远程信息处理的系统或能进一步达到资源共享的系统,都是计算机网络。
- 广义的观点定义了一个计算机通信网络,它在物理结构上具有计算机网络的雏形,但资源共享能力弱,是计算机网络发展的低级阶段。

**Def-资源共享观点**

- 计算机网络是“以能够相互共享资源的方式互连起来的自治计算机系统的集合"
  - 目的一一资源共享
  - 组成单元—一分布在不同地理位置的多台独立的“自治计算机”
  - 网络中的计算机必须遵循的统一 规则 一 网络协议
- 该定义符合目前计算机网络的基本特征。

**Def-用户透明性观点**

- 存在一个能为用户自动管理资源的网络操作系统,它能够调用用户所需要的资源,而整个网络就像一个大的计算机系统一样对用户是透明的。用户使用网络就像使用一台单一的超级计算机,无须了解网络的存在、资源的位置信息。
- 用户透明性观点的定义描述了一个分布式系统,它是网络未来发展追求的目标。

 





# Cpn-计算机网络的组成

从不同的角度,可以将计算机网络的组成分为如下几类。

### Cpn+Cat-组成部分

一个完整的计算机网络主要由硬件、软件、协议三大部分组成

**硬件**

- 主要由主机(也称端系统)、通信链路 ( 如双绞线、光纤)、交换设备(如路
  由器、交换机等)和通信处理机(如网卡)等组成
- <u>客户机</u>是客户访问网络的出入口, <u>服务器</u>是提供服务、存储信息的设备,当然是必不可少的

**软件**

- 主要包括各种实现资源共享的软件和方便用户使用的各种工具软件(如网络操作系统、邮件收发程序、 FTP 程序、聊夭程序等)。
- 软件部分多属于应用层
- <u>操作系统</u>是最基本的软件

**协议**

- 计算机网络的核心，协议规定了网络传输数据时所遵循的规范。

### Cpn+Cat-工作方式

计算机网络(这里主要指 Internet, 即因特网)可分为边缘部分和核心部分。

**边缘部分**

- 边缘部分由所有连接到因特网上、供用户直接使用的主机组成,用来进行通信(如传输数据、音频或视频)和资源共享。
  - 边缘部分用户可以直接使用C/S模型和P2P模型，**Lk:** 应用层 > 网络应用模型


**核心部分**

- 核心部分由大量的网络和连接这些网络的路由器组成,它为边缘部分提供连通性和交换服务。

### Cpn+Cat-功能组成

计算机网络由通信子网和资源子网组成。

**通信子网**

- 通信子网由各种传输介质、通信设备和相应的网络协议组成,它使网络具有数据传输、交换、控制和存储的能力, 实现联网计算机之间的数据通信。
- 一般来说是各种硬件设备

**资源子网**

- 资源子网是实现资源共享功能的设备及其软件的集合, 向网络用户提供共享其他计算机上的硬件资源、软件资源和数据资源的服务。
- 一般来说是各种软件







# Mt-网络技术 NETWORK TECHNOLOGY

**Lk:** 由于这一部分的内容也是按照作用范围从小到大分类的，因此也可以参考：Cat-计算机网络分类 Type of Computer Network > 按分布范围分类

## PAN (Personal Area Network) 

**Cc**

- PANs (Personal Area Networks) let devices communicate over the range of a
  person

**e.g.**

-  A common example is a wireless network that connects a computer with
  its peripherals. 
  - Other examples include the network that connects your wireless headphones and your watch to your smartphone.
- Bluetooth
  - In the simplest form, Bluetooth networks use the **master-slave paradigm**.
  -  The system unit (the PC) is normally the master, talking to the mouse
    or keyboard as slaves.
  - The master tells the slaves what addresses to use, when they can transmit, how long they can transmit, what frequencies they can use, and so on.



## LAN (Local Area Network)

**Cc**

- A LAN (Local Area Network) is a private network that operates within and
nearby a single building such as a home, office, or factory.

**Ab**

- Both wireless and wired broadcast LANs can allocate resources statically or
  dynamically.
  - static allocation
    - A typical static allocation would be to divide time into discrete inter-
      vals and use a round-robin algorithm, allowing each machine to broadcast only when its time slot comes up.
    - Static allocation wastes channel capacity when a machine has nothing to transmit or receive during its allocated slot, so most systems attempt to allocate the channel dynamically (i.e., on demand).
  - Dynamic allocation methods
    -  In a centralized channel allocation method, there is a single entity,
      for example, the base station in cellular networks, which determines who goes next. It might do so by accepting multiple packets and prioritizing them according to some internal algorithm.
    -  In a decentralized channel allocation method, there is no central entity; each machine must decide for itself whether to transmit. 

### Wireless LANs

**Cc**

- LANs with wireless medium.

**Ab**

- It runs at speeds from 11 Mbps (802.11b) to 7 Gbps (802.11ad).

**e.g.**

- In most cases, each computer talks to a device called an AP (Access Point), wireless router, or base station.
  - This device relays packets between the wireless computers and also between them and the Internet.

**Mt-mesh network configuration**

-  In some cases, the relays are the same nodes as the endpoints; more commonly, however, a mesh network will include a separate collection of nodes whose sole responsibility is relaying traffic.

### Wired LANs

**Cc**

- LANs with physical connections.

**Ab**

- Typically, wired LANs can run at speeds ranging from 100 Mbps to 40 Gbps.
- They also have low latency (never more than tens of milliseconds, and often muchless) and transmission errors are infrequent.
- Wired LANs typically have lower latency, lower packet loss, and higher throughput than wireless LANs, but over time this performance gap has narrowed
- 

**Pcp-Standard/Protocol**

- There is a popular standard for wireless LANs called **IEEE 802.11**, commonly
  called **WiFi** .
- **IEEE 802.3**, popularly called **Ethernet**, is by far the most common type of wired LAN
- 

**F**

- LANs are widely used to connect personal computers and consumer electronics to let them share resources (e.g., printers) and exchange information.
- Many wired LANs comprise point-to-point wired links.

**e.g.**

- common physical modes of transmission are copper, coaxial cable, and optical fiber.

**Mt-Switched Ethernet topology**

- Each computer speaks the Ethernet protocol and connects to a device called a **switch** with a point-to-point link.
  - The job of the switch is to relay packets between computers that are attached to it, using the address in each packet to determine which computer to send it to.
- A switch has multiple **ports**, each of which can connect to one other device,
  such as a computer or even another switch.
  - A switch has multiple ports, each of which can connect to one other device, such as a computer or even another switch. 

### VLAN (Virtual LAN)

- It is also possible to divide one large **physical LAN** into two smaller **logical**
**LANs**.
-  In this design, each port is tagged with a color, say green for engineering and red for finance. 
  - The switch then forwards packets so that computers attached to the green ports are separated from the computers attached to the red ports.
  - Broadcast packets sent on a red port, for example, will not be received on a green port, just as though there were two separate physical LANs



### Home Networks

**Cc**

- LANs in the home

**Ab**

- Home networks are a type of LAN; they may have a broad, diverse range of Internet-connected devices, and must be particularly easy to manage, dependable, and secure, especially in the hands of nontechnical users.
- First, the devices that people connect to their home network need to be easy to install and maintain.
- Second, security and reliability have higher stakes because insecurity of the devices may introduce direct threats to consumer health and safety.
- Third, home networks evolve organically, as people buy various consumer
  electronics devices and connect them to the network.
  - In contrast to a more homogeneous enterprise LAN, the set of technologies connected to the home network may be significantly more diverse.
  - Interactive, interoperability

- Finally, profit margins are small in consumer electronics, so many devices aim
  to be as inexpensive as possible. 



### Power-line networks

**Ab**

- These networks carry both power and data signals at the same time
  - part of the solution is to run these two functions on different frequency bands.



## MAN (Metropolitan Area Network)

**Cc**

- A MAN (Metropolitan Area Network) covers a city.

**e.g.**

- cable television networks
- Recent developments in high-speed wireless Internet access have resulted in another MAN, which has been standardized as **IEEE 802.16** and is popularly known as **WiMAX**.
- LTE (Long Term Evolution)
- 5G



## WAN (Wide Area Network)

**Cc**

- A WAN (Wide Area Network) spans a large geographical area, often a country, a continent, or even multiple continents. 

### Wired WANs

**Cc**

- The WAN connected with cables

**Ab**

- Separation of the pure communication aspects of the network (the subnet) from the application aspects (the hosts) greatly simplifies the overall network design.

**St**

- **Hosts**: computers intended for running user (i.e., application) programs
- **Communication subnet**： The rest of the network that connects these
  hosts is then called the communication subnet, or just subnet for short.
  - **Ab**
    - Originally, its only meaning was the collection of routers and communication lines that moved packets from the source host to the destination host.
    - Readers should be aware that it has acquired a second, more recent meaning in conjunction with network addressing.
    - Internetworks or inter-networks or composite networks: comprise more than one network.
  - **Cpn**
    - **Transmission lines**
      - Transmission lines move bits between machines
      - They can be made of copper wire, coaxial cable, optical fiber, or radio links.
      - In most WANs, the network contains many transmission lines, each connecting a pair of routers. Two routers that do not share a transmission line must do so via other routers. 
        - 端点问题，而且毕竟不会有空线路
      - 
    - **Switching elements** or **Routers**
      - Switching elements, or switches or routers, are specialized devices that connect two or more transmission lines
      - When data arrive on an incoming line, the switching element must choose an outgoing line on which to forward them.
      - How the network makes the decision as to which path to use is called a **routing algorithm**. 
      - How each router makes the decision as to where to send a packet
        next is called a **forwarding algorithm**.
  - **F:** The subnet carries messages from host to host.

**Pcp-Difference between WAN and LAN**

- Separation of the pure communication aspects of the network (the subnet) from the application aspects (the hosts). Usually in a WAN, the hosts and subnet are owned and operated by different people.
- A second difference is that the routers will usually connect different kinds of
  networking technology. 
- A final difference is in what is connected to the subnet. it could be computers or smaller networks like LANs.



### Virtual Private Networks (VPNs)

**Cc**

- **Virtual links**: Use wireless Internet connectivity to connect hosts.
- **VPNs**：Networks called VPNs (Virtual Private Networks) connect the individual networks at different sites into one logical network by virtual links.

**Ab**

- a VPN has the usual advantage of virtualization, which is that it provides flexible reuse of a resource (Internet connectivity)
- A VPN also has the usual disadvantage of virtualization, which is a lack of control over the underlying resources.
- With a VPN, performance may vary with that of the underlying Internet connectivity. 
- The network itself may also be operated by a commercial Internet service provider (ISP).



### Satellite networks

**Cc**

- Wireless networks based on satellite systems.

**Ab**

-  In satellite systems, each computer on the ground has an antenna through which it can exchange data with a satellite in orbit.
  - Output
    - All computers can hear the output from the satellite
  - Input
    - in some cases, they can also hear the upward transmissions of their fellow computers to the satellite as well

**F**

- Satellite networks are inherently broadcast and are most useful when broadcast is important or no ground-based infrastructure is present

### The Cellular Telephone Network

**Cc**

-  wireless technology.
- 1G 到 5G

**Ab**

- Each cellular base station covers a distance much larger than a wireless LAN,
  with a range measured in kilometers rather than tens of meters. 
- The base stations are connected to each other by a backbone network that is usually wired.
- The data rates of cellular networks are often on the order of 100 Mbps, much smaller than a wireless LAN that can range up to on the order of 7 Gbps

### Software-Defined WANs (SD-WANs)

**Cc**

- SD-WANs are WANs which use different, complementary technologies to connect disjoint sites but provide a single SLA (Service-Level Agreement) across the network

**Ab**

- SD-WANs are one example of an SDN (Software-Defined Network), a technology that has gained momentum over the last decade and generally describes network architectures that control the network using a combination of programmable switches with control logic implemented as a separate software program.



## Internetwork or internet

**Cc**

- **internet**: A collection of interconnected networks is called an internetwork or internet.
  - **Att:** We will use these terms in a generic sense, in contrast to the global Internet (which is one specific internet), which we will always capitalize.
- **gateway 网关**: The device that makes a connection between two or more networks and provides the necessary translation, both in terms of hardware and software, is a gateway.
  - Generally speaking, an internetwork will be connected by network-layer gateways, or routers; however, even a single large network often contains many routers.

**Ab**

- An internet entails the interconnection of distinct, independently operated networks.
- Generally speaking, if two or more <u>independently operated</u> networks pay
  to interconnect, or if two or more networks <u>use fundamentally different underlying technology</u> (e.g., broadcast versus point-to-point and wired versus wireless), we probably have an internetwork.

**Web application** 

Web application, where a server generates Web pages based on its database in response to client requests that may update the database.









































# Cat-计算机网络分类 Type of Computer Network

### 按分布范围分类

**Lk:** Mt-网络技术 NETWORK TECHNOLOGY

- **广域网 (WAN)** 。广域网的任务是提供长距离通信,运送主机所发送的数据,其覆盖范围通常是直径为几十千米到几千千米的区域,因而有时也称远程网。广域网是因特网的核心部分。连接广域网的各结点交换机的链路一般都是高速链路,具有较大的通信容蜇。
  - 广域网一般使用交换技术

- **城域网 (MAN)** 。城域网的覆盖范围可以跨越几个街区甚至整个城市,覆盖区域的直径范围是 5~50km。城域网大多采用以太网技术,因此有时也常并入局域网的范围讨论。
- **局域网 (LAN)** 。局域网一般用微机或工作站通过高速线路相连,覆盖范围较小,通常是直径为几十米到几干米的区域。局域网在计算机配置的数量上没有太多的限制,少的可以只有两台,多的可达几百台。
  - 局域网使用广播技术。

- **个人区域网 (PAN)** 。个人区域网是指在个人工作的地方将消费电子设备(如平板电脑、智能手机等)用无线技术连接起来的网络,也常称为无线个人区域网 (WPAN) ,覆盖区域的直径约为 10m 。
- 注意:若中央处理器之间的距离非常近(如仅 1m 的数量级或甚至更小),则一般称为多处理器系统,而不称为计算机网络。
- 广域网和局域网之间的差异不仅在于它们所覆盖范围的不同,<u>关键还在于它们所采用的协议和网络技术的不同</u>,广域网使用点对点等技术,局域网使用广播技术。
- 目前局域网接入广域网主要是通过称为<u>路由器</u>的互连设备来实现的。

### 按传输技术分类

- **广播式网络**。所有联网计算机都共享一个公共通信信道。当一台计算机利用共享通信信道发送报文分组时,所有其他的计算机都会“收听”到这个分组。接收到该分组的计算
  机将通过检查目的地址来决定是否接收该分组。局域网基本上都采用广播式通信技术,广域网中的无线、卫星通信网络也采用广播式通信技术。
  - 广播式网络共享广播信道(如总线),通常是局域网的一种通信方式(局域网工作在数据链路层),因<u>此不需要网络层,因而也不存在路由选择问题</u>。但数据链路层使用物理层的服务必<u>须通过服务访问点实现</u>。

- **点对点网络**。每条物理线路连接一对计算机。 若通信的两台主机之间没有直接连接的线路,则它们之间的分组传输就要通过中间结点进行接收、存储和转发,直至目的结点 。
- 是否采用<u>分组存储转发与路由选择机制</u>是点对点式网络与广播式网络的重要区别,广域网基本都属于点对点网络。

### 按拓扑结构分类

- 网络拓扑结构是指由**网中结点(路由器、主机等)**与**通信线路(网线)**之间的几何关系(如
  总线形、环形)表示的网络结构, 主要指<u>通信子网</u>的拓扑结构。
  - 计算机网络拓扑结构主要取决于它的通信子网

- 星形、总线形和环形网络多用于局域网，网状网络多用于广域网。
- **总线形网络**。用单根传输线把计算机连接起来。
  - **Ab**
  - 总线形网络的优点是建网容易、增/减结点方便、节省线路。
  - 缺点是重负载时通信效率不高、总线任意一处对故障敏感。
- **星形网络**。每个终端或计算机都以单独的线路与中央设备相连。中央设备早期是计算机, 现在一般是交换机或路由器。
  - **Ab**
  - 星形网络便于集中控制和管理,因为<u>端用户之间的通信必须经过中央设备</u>。
  - 缺点是成本高、中央设备对故障敏感。
- **环形网络**。所有计算机接口设备连接成一个环。环形网络最典型的例子是令牌环局域网。环可以是单环,也可以是双环,环中信号是<u>单向传输</u>的。
- **网状网络**。一般情况下,每个结点至少有两条路径与其他结点相连,多用在广域网中。
  其有规则型和非规则型两种。
  - **Ab**
  - 其优点是可靠性高,
  - 缺点是控制复杂、线路成本高。
- 以上 4 种基本的网络拓扑结构可以互连为更复杂的网络。

### 按使用者分类

- **公用网 (Public Network)** 。指电信公司出资建造的大型网络。”公用”的意思是指所有愿意按电信公司的规定交纳费用的人都可以使用这种网络,因此也称公众网。
- **专用网 (Private Network)** 。指某个部门为满足本单位特殊业务的需要而建造的网络。这种网络不向本单位以外的人提供服务。例如铁路、电力、军队等部门的专用网。

### 按交换技术分类

- **交换技术**是指各台主机之间、各通信设备之间或主机与通信设备之间为交换信息所采用的数据格式和交换装置的方式。按交换技术可将网络分为如下几种。
- **电路交换网络**。在源结点和目的结点之间建立一条专用的通路用于传送数据,包括建立
  连接、传输数据和断开连接三个阶段。
  - **Ab**
  - 该类网络的主要特点是整个报文的比特流连续地从源点直达终点,好像是在一条管道中传送。
  - 优点是数据直接传送、时延小。
  - 缺点是线路利用率低、不能充分利用线路容量、不便于进行差错控制。
  - **e.g.:** 最典型的电路交换网是传统电话网络。
- **报文交换网络**。用户数据加上源地址、目的地址、校验码等辅助信息,然后封装成报文。整个报文传送到相邻结点,全部存储后,再转发给下一个结点,重复这一过程直到到达
  目的结点 。 
  - **Ab**
  - 每个报文可以单独选择到达目的结点的路径。
  - 报文交换网络也称**存储-转发网络**,主要特点是整个报文先传送到相邻结点,全部存储后查找转发表,转发到下一个结点。
  - 优点是可以较为充分地利用线路容量,可以实现不同链路之间不同数据传输速率的转换,可以实现格式转换,可以实现 一对多、多对一的访问,可以实现差错控制。
  - 缺点是增大了资源开销(如辅助信息导致处理时间和存储资源的开销),增加了缓冲时延,需要额外的控制机制来保证多个报文的顺序不乱序,缓冲区难以管理(因为报文的大小不确定,接收方在接收到报文之前不能预知报文的大小) 。
- **分组交换网络**,也称**包交换网络**。其原理是,将数据分成较短的固定长度的数据块,在
  每个数据块中加上目的地址、源地址等辅助信息组成分组(包),以存储-转发方式传输。
  - **Ab**
  - 其主要特点是单个分组(它只是整个报文的一部分)传送到相邻结点,存储后查找转发
    表,转发到下一个结点。同样使用了存储-转发技术。
  - 除具备报文交换网络的优点外,分组交换网络还具有自身的优点:缓冲易于管理;包的平均时延更小,网络占用的平均缓冲区更少;更易于标准化;更适合应用。现在的主流网络基本上都可视为分组交换网络。
  - 缺点是分组交换要求把数据分成大小相当的小数据片,每片都要加上控制信息(如目的地址),因而传送数据的总开销较多。
  - 报文相当于是分组的数据块合而为一进行转发，两种技术其实非常接近。
- 对各种交换方式,不同规格的终端都很难相互通信

### 按传输介质分类

- **传输介质**可分为有线和无线两大类,因此网络可以分为有线网络和无线网络。
- **有线网络**又分为双绞线网络、同轴电缆网络等。
- **无线网络**又可分为蓝牙、微波、无线电等类型。

### 按现实应用分类

#### Broadband Access Networks

**Cc**

- Open Network

**Pcp-Metcalfe's law**

-  the value of a network is proportional to the square of the number of users because this is roughly the number of different connections that may be made (Gilder, 1993).
-  It helps to explain how the tremendous popularity of the Internet comes from its size.

**F**

- Internet access provides home users with connectivity to remote computers.

**e.g.**

- Internet

#### Mobile and Wireless Access Networks

**Cc**

- Mobile and wireless devices

**Pcp**

- Although wireless networking and mobile computing are often related, they
  are not identical, the key point of "wireless" is whether the connectivity or connection media of the Internet is wired and the mobility of devices depends on whether the machine is portable.

**e.g.**

- Cellular networks 
- Wireless hotspots
  - 802.11 standard
- GPS (Global Positioning System）
- m-commerce (mobile-commerce)
- NFC (Near Field Communication)
- Sensor networks

#### Content Provider Networks

**Cc**

- Database

**e.g.**

- 'The cloud',  cloud computing, or a data-center network. 
- CDN (Content Delivery Network)
  - A CDN is a large collection of servers that are geographically distributed in such a way that content is placed as close as possible to the users that are requesting it

#### Transit Networks

**Cc**

- Connection
- Typically, content and applications are hosted in data-center networks, and you may be accessing that content from an access network.
- When the content provider and your ISP (Internet Service Provider) are not
  directly connected, they often rely on a **transit network** to carry the traffic between them.
- Also called backbone network.

**Ab**

- As the size (and negotiating power) of the access networks and the content provider networks continues to increase, the larger networks have come to rely less on transit networks to deliver their traffic, preferring often to directly interconnect and rely on the transit network only as a backup.

**e.g.**

- ISP (Internet Service Provider) 

#### Enterprise Networks

**Cc**

- Region

**e.g.**

- Resource sharing
- VPNs (Virtual Private Networks)
  - Networks called VPNs (Virtual Private Networks) connect the individual networks at different sites into one logical network.
- Communication medium
  - feishu
  - email
  - IP telephony or VoIP (Voice over IP)
  - Team Viewer or Microsoft Team
- Desktop sharing
- e-business





# e.g.-EXAMPLES OF NETWORKS

## The Internet

### Cc

- The Internet is a vast collection of different networks that use certain common protocols and provide certain common services. 

### Soc

The ARPANET

- **Lk:** Computer Network > 1.4.1
  - Related Cc:
    - ARPA, the Advanced Research Projects Agency
    - digital packet-switching technology or packet-switched subnet
    - IMPs (Interface Message Processors)
    - TCP/IP
    - sockets


NSFNET

- **Lk:** Computer Network > 1.4.1
  - Related Cc:
    - NSF (the U.S. National Science Foundation) 
    - CSNET (Computer Science Network) 
    - fuzzball
    - NSFNET (National Science Foundation Network)
    - ANS (Advanced Networks and Services) and ANSNET
    - NAP (Network Access Point)
    -  World Wide Web (WWW


### St-The Internet Architecture

![Figure 1-16. Overview of the Internet architecture](./img/Figure 1-16. Overview of the Internet architecture.png)

- Host: Starting with a computer at home (at the right edges of the figure).
- Host to POP: Move packets between the home and the ISP.
  - To join the Internet, the computer is connected to **an internet service provider (ISP)** from whom the user purchases Internet access. 
  - We call the location at which customer packets enter the ISP network for service the ISP's **POP (Point of Presence)**.
  - From this point on, the system is fully digital and packet switched.
- POP to ISP: packets are moved between the POPs of different ISPs.
  - The **backbone** of the ISP: The long-distance transmission lines that interconnect routers at POPs in the different cities that the ISPs serve.
- ISP to ISP: If a packet is destined for a host served directly by the ISP, that packet is routed over the backbone and delivered to the host. Otherwise, it must be handed over to another ISP.
  - ISPs connect their networks to exchange traffic at **IXPs (Internet eXchange**
    **Points)**.
    - Basically, an IXP is a building full of routers, at least one per ISP.
    - IXPs are drawn vertically because ISP networks overlap geographically. 
  - The connected ISPs are said to **peer** with each other.
    - Peering at IXPs depends on the business relationships between ISPs.
      - **Transit**: the traffic or Internet connectivity between ISPs.
      - A small handful of **transit providers**, including AT&T and Level 3, operate large international backbone networks with thousands of routers connected by high-bandwidth fiber-optic links
        - These ISPs do not pay for transit. They are usually called tier-1 ISPs and are said to form the backbone of the Internet, since
          everyone else must connect to them to be able to reach the entire Internet.
    - The path a packet takes through the Internet depends on the peering choices of the ISPs.
      - Often, the path a packet takes will not be the shortest path through the Internet. It could be the least congested or the cheapest for the ISPs.
-  Data centers
  - These data centers are designed for computers, not humans, and may be filled with rack upon rack of machines. Such an installation is called a server farm.
  -  Colocation or hosting data centers let customers put equipment such as servers at ISP POPs so that short, fast connections can be made between the servers and the ISP backbones. 
- 之后就是反向从ISP和Data Centers中传输数据到Hosts中，其实也是同理的逆过程。

### Cpn-internet services

#### Cc-internet services

**Def** 

- To join the Internet, the computer is connected to **an internet service**
  **provider** from whom the user purchases Internet access. 

**F**

- This lets the computer exchange packets with all of the other accessible hosts on the Internet. 

**Ab:**

- The most important attribute is connectivity.
- Access networks are limited by the bandwidth of the last mile or last leg of transmission.

#### The cable network

**Cc**

- The cable network, sometimes called an **HFC (Hybrid Fiber-Coaxial) network,** is a single integrated infrastructure that uses a packet-based transport called **DOCSIS (Data Over Cable Service Interface Specification)** to transmit a variety of data services, including television channels, high-speed data, and voice.
- The device at the home end is called a **cable modem**, and the device at the cable headend is called the **CMTS (Cable Modem Termination System).** 
- The word **modem** is short for ''*mo*dulator *dem*odulator'' and refers to any device that converts between digital bits and analog signals.

#### FTTH

**Cc**

- Another option for last-mile deployment involves running optical fiber to residences using a technology called FTTH (Fiber to the Home).



## Mobile Phone Network

### Cc

- **UMTS (Universal Mobile Telecommunications System)** is the formal name for the cellular phone network. 

- **SIM card (Subscriber Identity Module)**: a removable chip containing the subscriber's identity and account information

### Ab

- Another difference between mobile phone networks and the conventional Internet is **mobility**.
  - When a user moves out of the range of one cellular base station and
    into the range of another one, the flow of data must be re-routed from the old to the new cell base station. This technique is known as **handover** or **handoff**.
    -  based on CDMA technology
  - **Soft handover**: the mobile is actually connected to two base stations for a short while before handover. 
  - **Hard handover**: the mobile disconnects from the old base station before connecting to the new one.
- **Security**
  - With UMTS, the mobile also uses the information on the SIM card to check
    that it is talking to a legitimate network.
- **Privacy**
  - cryptographic keys on the SIM card are used to encrypt transmissions.

### St-UMTS

![Figure 1-19. Simplified 4G LTE network architecture](./img/Figure 1-19. Simplified 4G LTE network architecture.png)

- First, there is the **E-UTRAN (Evolved UMTS Terrestrial Radio Access Network)** which is a fancy name for the <u>radio communication protocol</u> that is used over the air between the mobile device (e.g., the cell phone).

  This part is the wireless side of the mobile phone network. 

  - The cellular base station, which is now called an **eNodeB.**，
    - The base station implements the air interface.
  -  The air interface is based on **CDMA (Code Division Multiple Access)**.
  - The cellular base station together with its controller forms the **radio access network**.
    - The controller node or **RNC (Radio Network Controller)** controls how the spectrum is used. 

- The rest of the mobile phone network carries the traffic for the radio access
  network. It is called the **core network**. 

  - both packet- and circuit-switched equipment in the core network
  - In 4G networks, the core network became packet-switched, and is now called the **EPC (Evolved Packet Core)**.
    - The 3G UMTS core network evolved from the core network used for the 2G GSM system that came before it; the 4G EPC completed the transition to a fully packet-switched core network. The 5G system is also fully digital, too. No Analog anymore.
  - To carry all of this data, the UMTS core network nodes connect directly to a packet-switched network.
  - The **S-GW (Serving Network Gateway)** and the **P-GW (Packet Data Network Gateway)** deliver data packets to and from mobiles and interface to external packet networks such as the Internet.
    - This transition is set to continue in future mobile phone networks. 
    - Internet protocols are even used on mobiles to set up connections for voice calls over a packet data network, in the manner of voice over IP. IP and packets are used all the way from the radio access through to the core network. 
  - Each mobile phone network has a **HSS (Home Subscriber Server)** in
    the core network that knows the location of each subscriber, as well as other profile information that is used for authentication and  authorization.
    - In this way, each mobile can be found by contacting the HSS.

- Data Services

  - Early packet data services: GPRS (General Packet Radio Service) in the GSM system

### Pcp-Packet Switching and Circuit Switching

#### Packet Switching

**Cc**

- packet-switched networks (which are connectionless)

**Soc**

-  Internet community

**Ab**

- In a connectionless design, every packet is routed independently of every other packet.
- In a packet-switched network, if too many packets arrive at the a router during a particular time interval, the router will choke and probably lose packets. 
  - The sender will eventually notice this and resend the data, but the quality of service may be poor unless the applications account for this variability.

#### Circuit Switching

**Cc**

- circuit-switched networks (which are connection-oriented)

**Soc**

-  telephone companies

**Ab**

- Circuit switching can support quality of service more easily.
- In the telephone system, a caller must dial the called party's number and wait for a connection before talking or sending data. This connection setup establishes a route through the telephone system that is maintained until the call is terminated.
- All words or packets follow the same route. If a line or switch on the path goes
  down, the call is aborted, making it less fault tolerant than a connectionless design.

**e.g.**

-  **PSTN (Public Switched Telephone Network).**
  - This legacy is seen in the UMTS network with the MSC (Mobile Switching Center), GMSC (Gateway Mobile Switching Center), and MGW (Media Gateway) elements that set up connections over a circuit-switched core network such as the PSTN (Public Switched Telephone Network).

### Cc-Early Generation Mobile Networks: 1G, 2G, and 3G

#### 1G

**Cc**

- First-generation mobile phone systems transmitted voice calls as continuously varying (analog) signals rather than sequences of (digital) bits.

**e.g.**

- AMPS (Advanced Mobile Phone System), which was deployed in the United States in 1982, was a widely used first-generation system.

#### 2G

**Cc**

- Second-generation mobile phone systems switched to transmitting voice calls in digital form to increase capacity, improve security, and offer text messaging.

**e.g.**

- GSM (Global System for Mobile communications), which was deployed starting in 1991 and has become widely used worldwide. It is a 2G system

#### 3G

**Cc**

- The third generation, or 3G, systems were initially deployed in 2001 and offer
  both digital voice and broadband digital data services.
- 3G is loosely defined by the ITU standards.

**e.g.**

- UMTS is the main 3G system that is deployed worldwide. It is also the basis for its various successors. 

#### Ab

- The scarce resource in 3G systems, as in 2G and 1G systems before them, is radio spectrum. 
  - It is the scarcity of spectrum that led to the cellular network design

#### Mt-cellular network design

**Cc**

- To manage the radio interference between users, the coverage area is divided into cells.
- Within a cell, users are assigned channels that do not interfere with each other and do not cause too much interference for adjacent cells.
- This allows for good reuse of the spectrum, or frequency reuse, in the neighboring cells, which increases the capacity of the network.



### Cc-Modern Mobile Networks: 4G and 5G

#### 4G

**Cc**

- The 4G, later 4G (LTE (Long Term Evolution)) technologies offer faster speeds, emerged in the late 2000s.

**e.g.**

- 

#### 5G

**Cc**

- 5G technologies are promising faster speeds—up to 10 Gbps—and are now set for large-scale deployment in the early 2020s.

**e.g.**

- 

#### Ab

- One of the main distinctions between these technologies is the frequency spectrum that they rely on. 5G's frequency is generally higher  than 4G's.
- Higher frequency also brings more technical problems like signal attenuation, interference, and errors using newer algorithms and technologies etc.





## Wireless Network (WiFi)

### Cc

- A common slang name for it is **WiFi**, but it is an important standard and deserves respect, so we will call it by its more formal name, **802.11**

### Ab

- Instead of expensive, licensed spectrum, 802.11 systems operate in unlicensed bands such as the **ISM (Industrial, Scientific, and Medical)** bands defined by ITU-R (e.g. 902-928 MHz, 2.4-2.5 GHz, 5.725-5.825 GHz).

### St

**Cpn**

- **Client**
  - **Def:** Device connected to the WiFi.

- **AP (Access Point)**
  - **Def:** Infrastructure called APs (access points) or base stations connect to the wired network, and all communication between clients goes through an access point.

**St-Wireless network with an access point**

The access points connect to the wired network, and all communication between clients goes through an access point. 

**St-ad hoc network**

It is also possible for clients that are in radio range to talk directly, such as two computers in an office without an access point.

### Pcp-802.11 Transmission

**Soc-Problem**

- multipath fading
  - At the frequencies used for 802.11, radio signals can be reflected off solid objects so that multiple echoes of a transmission may reach a receiver along different paths. 
  - The echoes can cancel or reinforce each other, causing the received signal to fluctuate greatly.
- The problem that multiple transmissions that are sent at the same time
  will collide, which may interfere with reception
- Mobility devices linkage requirement
- the problem of security. Since wireless transmissions are broadcast, it is easy for nearby computers to receive packets of information that were not intended for them. 

**Cc-path diversity**

- The key idea for overcoming variable wireless conditions is **path diversity**, or
  the sending of information along multiple, independent paths

- OFDM (Orthogonal Frequency Division Multiplexing)
  - It divides a wide band of spectrum into many narrow slices over which different bits are sent in parallel.

**Cc-CSMA (Carrier Sense Multiple Access)**

- CSMA (Carrier Sense Multiple Access) scheme: Computers wait for a short random interval before transmitting and defer their transmissions if they hear that someone else is already transmitting.
- This scheme makes it less likely that two computers will send at the same time. 
- It does not work as well as in the case of wired networks, because of the limited transmission range.

**Cc-Mobility Solution**

- The solution is that an 802.11 network can consist of multiple cells, each with its own access point, and a distribution system that connects the cells.
- That said, mobility in 802.11 has been of limited value so far compared to mobility in the mobile phone network. Typically, 802.11 is used by nomadic clients that go from one fixed location to another, rather than being used on-the-go Mobility is not really needed for nomadic usage. 

**Cc-Security Solution**

- To prevent this, the 802.11 standard included an encryption scheme known as **WEP (Wired Equivalent Privacy)**. 
  - The idea was to make wireless security like that of wired security.
  - It is a good idea, but unfortunately, the scheme was flawed and soon broken (Borisov et al., 2001). 
- It has since been replaced with newer schemes that have different cryptographic details in the **802.11i** standard, called **WiFi Protected Access**, initially called **WPA (WiFi Protected Access)** but now replaced by **WPA2**, and even more sophisticated protocols such as **802.1X**, which allows certificated-based authentication of the access point to the client, as well as a variety of different ways for the client to authenticate itself to the access point.









# Ab-网络性能指标

## 速率相关

**速率 Speed**

- **Def:** 网络中的速率是指连接到计算机网络上的主机在数字信道上传送数据的速率,也称**数据传输速率、传输速率、数据率或比特率**,单位为b/s (比特/秒) (或 bit/s, 有时也写为bps ) 。
  - 注意不是字节(byte, B)而是比特(bit, b)，1 Byte = 8 bits，而且倍率并不相同
  - 计算机网络的速率单位和计算机组成原理中的存储容量或文件大小不同
    - 表示存储容量时：k, M, G, T每一级为$2^{10} = 1024$，即$1 KB = 2^{10} B = 1024 B = 1024* 8 b$
    - 表示速率时：k, M, G, T每一级为$10^3 = 1000$，即$1 kb/s = 10^3 b/s$
- 数据率较高时,可用kb/s、 Mb/s 或 Gb/s表示。
- 从定义可以看出实际上速率是指每秒发送的比特数量，而非实际在链路上的传输速度，传输速度一般以电磁波传输速度$2 * 10^8 m/s$为准。
- 在计算机网络中 ,通常把**最高数据传输速率**称为**带宽**。
- 在一段时间内,链路中有多少比特取决于带宽 (或传输速率)，而1比特跑了多远取决于传播速率。

**带宽 Bandwidth**

- 本来表示通信线路允许通过的信号频带范围,单位是赫兹 (Hz) 。
- **Def:** 在计算机网络中,带宽表示网络的通信线路所能传送数据的能力,是数字信道所能传送的<u>最高数据传输速率</u>的同义语,单位是比特/秒 (bit/s或者b/s) 。

**传播速率**

- **Def:** 传播速率是指电磁波在信道中传播的速率,单位是米/秒 (m/s) ,更常用的单位是千米/秒(km/s)
- 传播速率一般以电磁波传输速度$2 * 10^8 m/s$为准

**吞吐量 Throughput**

- **Def:** 指单位时间内实际通过某个网络(或信道、接口)的数据量，单位为b/s (比特/秒) (或 bit/s, 有时也写为bps ) 。 
- **Ab:** 吞吐量受<u>网络带宽</u>、网络额定<u>速率</u>的限制 。

### Att

- 当带宽或发送速率提高后,比特在链路上向前传播的速率并未提高,只是每秒注入链路的比特数增加了。



## 时间相关

**时延 延迟 Delay**

- **Def:** 指数据(一个报文或分组)从网络(或链路)的一端传送到另一端所需要的总时间,它由 4 部分构成:发送时延、传播时延、处理时延和排队时延。单位一般为s

- **发送时延** 。结点将分组的<u>所有比特</u>推向 (传输) 链路所需的时间,即从发送分组的第一个比特算起,到该分组的最后一个比特发送完毕所需的时间,因此也称**传输时延**。计算公式为
  $$
  发送时延 = 发送分组数据大小/信道宽度
  $$

  > 其中数据的大小一般是指发送的数据的容量or数据帧长度，单位一般是kb, Mb之类的。
  >
  > 信道宽度一般就是<u>带宽</u>或者实际数据所需的最大传输速率

- **传播时延**。电磁波在信道中传播一定的距离需要花费的时间,即一个比特从链路的一端传播到另一端所需的时间。计算公式为
  $$
  传播时延=信道长度/电磁波在信道上的传播速率
  $$

  > 传输速度一般以电磁波传输速度$2 * 10^8 m/s$为准

- **排队时延** 。分组在进入路由器后要先在输入队列中排队等待处理 。路由器确定转发端口后,还要在输出队列中排队等待转发,这就产生了排队时延。

- **处理时延**。数据在交换结点为存储转发而进行的一些必要的处理所花费的时间。 

  - 例如,分析分组的首部、从分组中提取数据部分、进行差错检验或查找适当的路由等。

- 因此,数据在网络中经历的总时延就是以上 4 部分时延之和:
  $$
  总时延 = 发送时延 + 传播时延 + 排队时延 + 处理时延
  $$

  > 注意 : 做题时,排队时延和处理时延一般可忽略不计(除非题目另有说明)。另外 , 对于高速链路,提高的仅是数据发送速率而非比特在链路上的传播速率。提高数据的发送速率只是为了减少数据的发送时延。

**时延带宽积** 

- **Def:** 指发送端发送的第一个比特即将到达终点时,发送端已经发出了多少个比特,因此又称<u>以比特为单位</u>的链路长度, 即
  $$
  时延带宽积=传播时延 \times 信道带宽
  $$
  考虑一个代表链路的圆柱形管道,其长度表示链路的传播时延, 横截面积表示链路带宽 ,则时延带宽积表示该管道可以容纳的比特数量。

**往返时延 Round-Trip Time, RTT**

- **Def:** 指从发送端发出一个短分组（发送的第一个比特），随后接收端收到数据后立即发送确认，到发送端收到来自接收端的确认（收到确认的第一个比特），这个过程总共经历的时延。在互联网中,往返时延还包括各中间结点的处理时延、排队时延及转发数据时的发送时延。

- `ping`的结果就有rtt这一项，艇有意思的

- RTT越大，在收到确认前可以发送的数据越多是事实，虽然一般不会有人用这个时间来追求发送的数据量就是了。。

- RTT的计算
  $$
  RTT = 往返传播时延 + 处理时延 = 单向传播时延*2 + 处理时延
  $$
  注意RTT不包括发送时延

## 其他

**信道利用率/信道效率**

- **Def:** 指出某一信道有百分之多少的时间是有数据通过的。
- **Def-流量控制ver**：信道效率是对发送方而言的,是指发送方在一个发送周期的时间内,有效地发送数据所需要的时间占整个发送周期的比率。

$$
信道利用率 = 有数据通过时间/(有+无)数据通过时间 \\
= 有效数据通过时间/发送周期 \\
= \frac{数据量/数据传输率}{发送周期}
$$

- **Ab**

  - 信道利用率越大或者说越接近1，时延越来越单调递增接近正无穷。简单来说就是信道使用的人越多延迟越高。


**信道吞吐率**

- **Def**
  $$
  信道吞吐率 = 信道利用率*发送方的发送速率
  $$





# F-计算机网络的功能

主要的五大功能

**数据通信**

- 数据通信是计算机网络最基本和最重要的功能
- 用来实现联网计算机之间各种信息的传输,并将分散在不同地理位置的计算机联系起来,进行统 一 的调配、控制和管理
- e.g.: 例如,文件传输、电子邮件等应用,离开了计算机网络将无法实现。

**资源共享**

- 资源共享可以是软件共享、数据共享,也可以是硬件共享。
- 它使计算机网络中的资源互通有无、分工协作,从而极大地提高硬件资源、软件资源和数据资源的利用率。

**分布式处理**

- 当计算机网络中的某个计算机系统负荷过重时,可以将其处理的某个复杂任务分配给网络中的其他计算机系统,从而利用空闲计算机资源以提高整个系统的利用率。

**提高可靠性**

- 计算机网络中的各台计算机可以通过网络互为替代机。

**负载均衡**、

- 将工作任务均衡地分配给计算机网络中的各台计算机。







# ----- 架构 -----

# Pcp-网络协议 NETWORK PROTOCOLS

## Ab/Obj2-Design Goals

### Reliability 

**Cc-Reliability**

- the ability to recover from errors, faults, or failures
- Reliability is the design issue of making a network that operates correctly even though it is comprised of a collection of components that are themselves unreliable. 
- Another reliability issue is finding a working path through a network.

**Cc-Error Detection 检错**

- One mechanism for finding errors in received information uses codes for error
  detection.

**Cc-Error Correction 纠错**

- More powerful codes allow for error correction, where the correct message is recovered from the possibly incorrect bits that were originally received.

**Cc-Routing**

- The network would automatically make a decision to find a working path through a network

### Resource Allocation

**Cc-Resource Allocation**

- The mechanisms to allocate or share access to a common, limited resource

**Cc-Scalable 可拓展性**

- Designs that continue to work well when the network gets large are said to be scalable.

**Cc-Statistical Multiplexing**

- This design is called statistical multiplexing, meaning sharing based on the statistics of demand.

**Cc-Flow Control 流量控制**

- Flow control keep a fast sender from swamping a slow receiver with data.
  - Feedback from the receiver to the sender is often used.
  - Flow Control and corresponding allocation problem occur at every level.
- This overloading of the network is called **congestion**. 
  - One strategy is for each computer to reduce its demand for resources (e.g., bandwidth) when it experiences congestion. 
  - It, too, can be used in all layers.

**Cc-Timeliness 即时性**

- Most networks must provide service to applications that want this **real-time delivery** at the same time that they provide service to applications that want high throughput. 
- **Quality of service** is the name given to mechanisms that reconcile these competing demands.

### Evolvability 

**Cc-Evolvability**

- allowing for incremental deployment of protocol improvements over time
- concerns the evolution of the network

**Cc-Protocol Layering**

- the key structuring mechanism used to support change by dividing the overall problem and hiding implementation details: **protocol layering**.

**Cc-Addressing**

- **Addressing or naming**, in the low and high layers, respectively is a mechanism for identifying the senders and receivers that are involved in a particular message.

**Cc-Internetworking**

- **Internetworking** is the overall topic of those mechanisms for disassembling, transmitting, and then reassembling messages

### Security 

**Cc-Security**

- defending the network against various types of attacks.
- secure the network by defending it against different kinds of threats.

**Cc-Confidentiality**

- Mechanisms that provide confidentiality defend against eavesdropping on communications, and they are used in multiple layers.

**Cc-Authentication**

- Mechanisms for authentication prevent someone from impersonating someone else.

**Cc-Integrity**

-  Other mechanisms for integrity prevent surreptitious changes to messages,



## Cc-Protocol Layering

### 实体 Entity

**Def** 

- 具体来说,实体指任何可发送或接收信息的硬件或软件进程,通常是一个特定的软件模块

**Ab**
- 在计算机网络的分层结构中,第$n$层中的活动元素通常称为第$n$层实体
- **对等实体 Peers**: The entities comprising the corresponding layers on different machines are called **peers**.
- The peers may be software processes, hardware devices, or even human beings. 

**F**
- It is the peers that communicate by using the protocol to talk to each other.
- Entities use protocols in order to implement their service definitions.



### 层 Layer

**Def**

- To reduce their design complexity, most networks are organized as a stack of layers or levels, each one built upon the one below it.

**Ab**

- The number of layers, the name of each layer, the contents of each layer, and the function of each layer <u>differ from network to network</u>.
- The purpose of each layer is to offer certain services to the higher layers while shielding those layers from the details of how the offered services are actually implemented. 
- In reality, no data are directly transferred from layer $n$ on one machine to layer $n$ on another machine. Instead, each layer passes data and control information to the layer immediately below it, until the lowest layer is reached. 
  - Below layer 1 is the **physical medium** through which <u>actual/physical communication</u> occurs.
  - The <u>virtual communication</u> between peers of layer $n$ is the layer $n$ protocol.

### 协议 Protocol

**Def**

- Basically, a protocol is an agreement between the communicating parties on how communication is to proceed.
- A protocol is a set of rules governing the format and meaning of the packets, or messages that are exchanged by the <u>peer entities</u> within a layer.
- 协议,就是规则的集合。这些规则明确规定了所交换的数据的格式及有关的同步问题。
- 这些为进行网络中的数据交换而建立的规则、标准或约定称为网络协议 (Network Protocol)

**Ab**

- Protocols are invisible to the users.
  - 注意这里的可见是指封装那种状态。
  - 类似于封装的implementation of ADT in object-oriented programming

- When layer $n$ on one machine carries on a conversation with layer $n$ on another machine, the rules and conventions used in this conversation are collectively known as the **layer $n$ protocol**. 
  - protocols relate to the packets sent between peer entities on different machines
  - 协议是控制两个(或多个)<u>对等实体</u>进行通信的规则的集合, 是水平的【注意只能是对等实体之间】
  - 不对等实体之间是没有协议的

- In fact, the protocol itself can change in some layer without the layers above and below it even noticing.
  - Each protocol is completely independent of the other ones as long as the interfaces are not changed.
  - Each process may add some information <u>intended only for its peer</u>. This information is not passed up to the layer above.
  - Entities are free to change their protocols at will, provided they do not change the service visible to their users.

**Cpn**

- 协议由语法、语义和同步三部分组成
- **语法**
  - 语法规定了传输数据的格式
- **语义**
  - 语义规定了所要完成的功能,即需要发出何种控制信息、完成何种动作及做出何种应答
- **同步**或**时序**
  - 同步规定了执行各种操作的条件、时序关系等,即事件实现顺序的详细说明。

**F**

- 一个完整的协议通常应具有线路管理 (建立、释放连接)、差错控制、数据转换等功能。

- **Entities** use **protocols** in order to implement their **service** definitions.

### 接口 Interface

**Def** 

- The interface defines which primitive operations and services the lower layer makes available to the upper one. 
- 接口是同一结点内相邻两层间交换信息的连接点,是一个系统内部的规定 。 

**Ab**

- Between each pair of adjacent layers is an interface.
- 每层只能为紧邻的两个层次之间定义接口,不能跨层定义接口，是垂直的。
- 在典型的接口上,同一结点相邻两层的实体通过**服务访问点 (Service Access Point, SAP)** 进行交互。
  - 每个 SAP 都有一个能够标识它的地址。 
  - SAP 是一个抽象的概念,它实际上是一个逻辑接口(类似于邮政信箱),但和通常所说的两个设备之间的硬件接口是很不一样的。
  - 服务是通过 SAP 提供给上层使用的,第 n 层的SAP 就是第 n+1 层可以访问第 n 层服务的地方

**F**

- When network designers decide how many layers to include in a network and what each one should do, one of the most important considerations is
  defining clean interfaces between the layers. 

  - Doing so, in turn, requires that each layer performs a specific collection of well-understood functions.

  - In addition to minimizing the amount of information that must be passed between layers, clear interfaces also make it simpler to replace one layer with a completely different protocol or implementation.


### 网络结构 Network Architecture

**Def**

- A set of layers and protocols is called a network architecture。
- 我们把<u>计算机网络的各层及其协议的集合</u>称为网络的**体系结构( Aichitecture)** 。换言之,计算机网络的体系结构就是这个计算机网络及其所应完成的功能的精确定义,它是计算机网络中的层次、各层的协议及层间接口的集合。

**Ab**

- 跟Layer类似，需要强调的是,这些功能究竟是用何种硬件或软件完成的, 则是一个遵循这种体系结构的实现 ( Implementation) 问题。<u>体系结构是抽象的,而实现是具体的</u>, 是真正在运行的计算机硬件和软件。
- <u>计算机网络的体系结构</u>通常都具有<u>可分层</u>的特性,它将复杂的大系统分成若干较容易实现的层次 。 

### 协议栈 Protocol Stack

**Def**

- A list of the protocols used by a certain system, <u>one protocol per layer</u>, is called a protocol stack.
- 协议栈是由网络层级结构中每层取一个协议构成的一系列协议，这些协议往往统一为一个功能系统而发挥作用。

## Cc-服务 Services

### Cc+F-服务 Services

**Def**

- 服务是指下层为紧邻的上层提供的功能调用。
- A service is a set of primitives (operations) that a layer provides to the layer above it.
- The service defines what operations the layer is able to perform on behalf of its users, but it says nothing at all about how these operations are implemented.

**Ab**

- Services are visible to the users, though the implementation details are not specific.
  - 类似于abstract data type in object-oriented programming

- A service relates to an interface between two layers, with the lower layer being the service provider and the upper layer being the service user. The service uses the lower layer to allow the upper layer to do its work.：
  - 服务是指下层通过接口为紧邻的上层提供的功能调用，它是垂直的。对等实体在协议的控制下,使得本层能为上一层提供服务,但要实现本层协议还需要使用下一层所提供的服务。
  - services relate to the interfaces between layers

- 服务是通过 SAP 提供给上层使用的,第 n 层的SAP 就是第 n+1 层可以访问第 n 层服务的地方

**Cpn**

- 服务原语 Service Primitives

**Cat**

本条目具体内容Lk下述Ab即可

- 面向连接服务与无连接服务

- 可靠服务和不可靠服务

- 有应答服务和无应答服务 request-reply interaction

  - 有应答服务是指接收方在 收到数据后向发送方给出相应的应答,该应答由传输系统内部自动实现,而不由用户实现。所发送的应答既可以是肯定应答,也可以是否定应答,通常在接收到的数据有错误时发送否定应答。例如,文件传输服务就是一种有应答服务。

  - 无应答服务是指接收方收到数据后不自动给出应答。若需要应答,则由高层实现。例如,对于 WWW 服务,客户端收到服务器发送的页面文件后不给出应答。




### Ab-Connections

Layers offer two types of service to <u>the layers above them</u>: connection-oriented and connectionless.

#### 面向连接服务 Connection-Oriented Service

**Soc**

- Connection-oriented service is modeled after the telephone system.

**Cc**

- **Connection-Oriented Service**: To use a connection-oriented network service, the service user first establishes a connection, uses the connection, and then releases the connection.
  - 在面向连接服务中,通信前双方必须先建立连接,分配相应的资源(如缓冲区),以保证通信能正常进行,传输结束后释放连接和所占用的资源。因此这种服务可以分为连接建立、数据传输和连接释放三个阶段。
- **Connection**: The essential aspect of a connection is that it acts like a tube: the sender pushes objects (bits) in at one end, and the receiver takes them out at the other end. 
  - A **circuit** is another name for a connection with associated resources, such as a fixed bandwidth. 
- **Negotiation**: In some cases when a connection is established, <u>the sender, receiver, and subnet</u> conduct <u>a negotiation about the parameters to be used</u>, such as maximum message size, quality of service required, and other issues.

**Ab**

- **Order**: In most cases, the order is preserved so that the bits arrive in the order they were sent.

**e.g.**

- 例如 TCP 就是一种面向连接服务的协议。

#### 无连接服务 Connectionless service

**Soc**

- Connectionless service is modeled after the postal system

**Cc**

- **Connectionless Service**: Each message (letter/packet) carries <u>the full destination address</u>, and each one is routed through the intermediate nodes inside the system <u>independent</u> of all the subsequent messages.
  - 在无连接服务中,通信前双方不需要先建立连接,需要发送数据时可直接发送,把每个带有目的地址的包(报文分组)传送到线路上,由系统选定路线进行传输。
    - 这是一种不可靠的服务。这种服务常被描述为“尽最大努力交付 “(Best-Effort-Delivery), 它并不保证通信的可靠性。
- **Store-and-forward switching**: When the intermediate nodes receive a message <u>in full</u> before sending it on to the next node, this is called store-and-forward switching. 
- **Cut-through switching**: The alternative, in which the onward transmission of a message at a node starts <u>before it is completely received</u> by the node, is called cut-through switching. 
  - The receiving sequence of these messages may not be ordered.

**Ab**

- Unreliable (meaning not acknowledged) connectionless service is often called **datagram service**, in analogy with telegram service, which also does not return an acknowledgement to the sender.
  - 注意这里强调的是无需acknowledgement即确认，而不是指不可信。

**e.g.**

- IP 、 UDP 就是一种无连接服务的协议。

### Ab-可靠性 Reliability

**Cc**

- **Def:** Some services are reliable in the sense that they never lose data.
- 可靠服务是指网络具有纠错、检错、应答机制,能保证数据正确、可靠地传送到目的地。
- 不可靠服务是指网络只是尽量正确、可靠地传送,而不能保证数据正确、可靠地传送到目的地,是一种尽力而为的服务。
  - 对于提供不可靠服务的网络,其网络的正确性、可靠性要由应用或用户来保障。例如,用户收到信息后要判断信息的正确性,如果不正确,那么用户要把出错信息报告给信息的发送者,以便发送者采取纠正措施。通过用户的这些措施,可以把不可靠的服务变成可靠的服务。

**Mt**

- Usually, a reliable service is implemented by having the receiver acknowledge the receipt of each message so the sender is sure that it arrived. 
  - The acknowledgement process introduces overhead and delays, which are often worth it but sometimes the price that has to be paid for reliability is too high.

**Ab**

- Connection-oriented and connectionless services can each be characterized by their reliability. 

**Pcp-Reasons for using unreliable communication**

- Reliable communication (in our sense, that is, acknowledged) may not be available in a given layer.
  - In particular, many reliable services are built on top of an unreliable datagram service. 
- The delays inherent in providing a reliable service may be unacceptable, especially in real-time applications such as multimedia.

**Cat-Reliable service**

Cc-Reliable

- Reliable message stream
- Reliable byte stream
- The acknowledged datagram service
- Request-reply service
  - In this service, the sender transmits a single datagram containing a request; the reply contains the answer.
  - Request-reply is commonly used to implement communication in the client-server model

Ab

- Reliable connection-oriented service has two minor variations: message sequences and byte streams

**Cat-Unreliable service**

Cc-Unreliable

- Unreliable datagram
- Unreliable connection

**Cat-Mixed**

通过结合Connection和Reliability中的特性，可以得到不同的service类型，部分如下：

![Figure 1-28. Six different types of service](./img/Figure 1-28. Six different types of service.png)

### Cpn-服务原语 Service Primitives

#### Cc

**Service Primitives 服务原语**

- Service Primitives/Operations/（原语）通常指协议或服务提供的最底层、不可再分割的基本操作或功能。这些操作是构建更复杂网络通信的基础，由协议或服务直接提供给上层使用。
- 上层使用下层所提供的服务时必须与下层交换 一 些命令,这些命令在 OSI 参考模型中称为**服务原语** 
- A service is formally specified by a set of primitives (operations) available to
  user processes to access the service. 
- These primitives tell the service to perform some action or report on an action taken by a peer entity.

#### Ab-Primitives

- System and service primitives: If the protocol stack is located in the operating system, as it often is, the primitives are normally system calls.
  - These calls cause a trap to kernel mode, which then turns control of the machine over to the operating system to send the necessary packets.
- The set of primitives available depends on the nature of the service being provided.
  - The primitives for connection-oriented service are different from those of
    connectionless service.

#### e.g.-Primitives

Six service primitives that provide a simple connection-oriented service.

- LISTEN
  - Block waiting for an incoming connection

- CONNECT
  - Establish a connection with a waiting peer

- ACCEPT
  - Accept an incoming connection from a peer

- RECEIVE
  - Block waiting for an incoming message

- SEND
  - Send a message to the peer

- DISCONNECT
  - Terminate a connection


另一种常用的原语

- **请求 (Request)** 。由服务用户发往服务提供者,请求完成某项工作。
- **指示 (Indication)** 。由服务提供者发往服务用户,指 示用户做某件事情。
- **响应 (Response)** 。由服务用户发往服务提供者,作为对指示的响应。
- **证实 (Confirmation)** 。由服务提供者发往服务用户,作为对请求的证实 。

- 有应答服务包括全部4类原语,而无应答服务则只有请求和指示两类原语 。

#### Mt-The Acknowledged Datagram Service

**Mt-How client-server communication/connection-oriented communication might work with acknowledged datagrams**

![Figure 1-30. A simple client-server interaction using acknowledged datagrams](./img/Figure 1-30. A simple client-server interaction using acknowledged datagrams.png)

- Tips: 由于篇幅较长所以此处记录并不完整，如果真的需要详细说明Lk: Computer Networks > 1.5.4 Service Primitives
- First, the server executes LISTEN to indicate that it is prepared to accept incoming connections.
- Next, the client process executes CONNECT to establish a connection with the
  server. 
  - The CONNECT call needs to specify who to connect to, so it might have a
    parameter giving the server's address.
  - The client process is suspended until there is a response.
  - When the packet arrives at the server, the operating system sees that the packet is requesting a connection.
  - The server process can then establish the connection with the ACCEPT call.
  - At this point, the client and server are both running and they have a connection established.
- The next step is for the server to execute RECEIVE to prepare to accept the first request.
  - The RECEIVE call blocks the server.
- Then the client executes SEND to transmit its request (3) followed by the execution of RECEIVE to get the reply
- After it has done the work, the server uses SEND to return the answer to the client (4).
- When the client is done, it executes DISCONNECT to terminate the connection
  (5). 
- When the server gets the packet, it also issues a DISCONNECT of its own, acknowledging the client and releasing the connection (6).
- In a nutshell, this is how connection-oriented communication works.

**Ab**

- In short, in the real world, a simple request-reply protocol over an unreliable network is often inadequate.

## Ab-Protocol Layering

- Protocol layering is a central concept in network protocol design for evolvability.
- The **peer process abstraction** is crucial to all network design.
  - Using it, the unmanageable task of designing the complete network can be broken into several smaller, manageable design problems, namely, the design of the individual layers.
  - As a consequence, all real networks use layering.
- **上下层关系**：第n层实体实现的服务为第 n+1 层所利用
  - 在这种情况下,第n层称为服务提供者,第 n+l 层则服务于用户。
  - 第n层的实体不仅要使用第 n-1 层的服务来实现自身定义的功能,还要向第 n+1 层提
    供本层的服务,该服务是第 n 层及其下面各层提供的服务总和。
  - 最低层只提供服务,是整个层次结构的基础;中间各层既是下一层的服务使用者,又是
    上一层的服务提供者;最高层面向用户提供服务。
  - 上一层只能通过相邻层间的接口使用下一层的服务,而不能调用其他层的服务;下一层所提供服务的实现细节对上一层透明【也就是说第n层提供的服务的具体实现细节对第n+1层完全屏蔽，上层是看不到下层服务的实现细节的，也就是所谓的透明】。
- 每层都实现一种<u>相对独立的功能</u>,降低大系统的复杂度 。
- 各层之间界面自然清晰,易于理解,相互交流尽可能少 。
- 各层功能的精确定义 独立于 具体的实现方法【就是说不涉及具体实现方法】, 可以采用最合适的技术来实现。
- 保持下层对上层的独立性, <u>上层单向使用下层提供的服务</u> 
- 整个分层结构应能促进标准化工作，包括提供标准语言
- 由于分层后各层之间相对独立, 灵活性好, 因而分层的体系结构易于更新(替换单个模块),易于调试, 易于交流,易于抽象,易于标准化
- 层次越多,有些功能在不同层中难免重复出现,产生额外的开销,导致整体运行效率越低。层次越少,就会使每层 的协议太复杂 。 因此, 在分层时应考虑 层次的清晰程度 与 运行效率、层次数量等属性间的折中。


## St-Protocol Layering

![Figure 1-25. Layers, protocols, and interfaces](./img/Figure 1-25. Layers, protocols, and interfaces.png)

额外补充

- 在Layer内Primitives构成Services，通过Interface将功能提供给上层
- 在各个Layer中取出的功能相关且构成一个System的一系列Protocol称为一个Protocol Stack。

- 

## Mt-Multilayer Communication

**Cc-数据单位**

- **协议数据单元**

  - **Def:** 在计算机网络体系结构的各个层次中,每个报文都分为两部分:一是数据部分,即 SDU ;二是控制信息部分,即 PCI, 它们共同组成 PDU 。

  - **协议数据单元 (PDU, Protocol Data Unit)**

    - **Def:** 对等层之间传送的数据单位称为该层的 PDU
      - 第n层的协议数据单元记为n-PDU
      - 在实际的网络中,每层的协议数据单元都有一个通俗的名称
        - 如物理层的 PDU 称为比特,数据链路层的 PDU 称为帧,网络层的 PDU 称为分组,传输层的 PDU 称为报文段。
    - **Cpn**
      - **服务数据单元 (SDU, Service Data Unit)** : 为完成用户所要求的功能而应传送的数据。
        - 第n层的服务数据单元记为n-SDV 。
      - **协议控制信息 (PCI, Protocol Control Information)** : 控制协议操作的信息。
        - 第n层的协议控制信息记为n-PCI 。

  - **Mt-各层数据单元的转换关系**

    - 在各层间传输数据时,把从第n+1层收到的PDU作为第n层的SDU,加上第n层的PCI,就变成了第n层的PDU, 交给第n-1层后作为SDU发送,接收方接收时做相反的处理,因此可知关系为【该层的PDU相当于其下一层的SDU】
      $$
      n-SDU + n-PCI = n-PDU \\
      = (n-1)-SDU
      $$

  

**Mt-How to provide communication to the top layer of the five-layer network（Protocol Stack）**

![Figure 1-27. Example information flow supporting virtual communication in layer 5](./img/Figure 1-27. Example information flow supporting virtual communication in layer 5.png)

- A message, $M$, is produced by an application process running in layer 5 and given to layer 4 for transmission.
- Layer 4 puts a **header** in front of the message to identify the message and then passes the result to layer 3.
  - The header includes <u>control information</u>, such as addresses, to allow layer 4 on the destination machine to deliver the message. 
  - Other examples of control information used in some layers are sequence numbers (in case the lower layer does not preserve message order), sizes, and times.
  - Other layer $n$ header provide similar information.

- In many networks, no limit is placed on the size of messages transmitted in the layer 4 protocol, but there is nearly always a limit imposed by the layer 3 protocol.
  Consequently, layer 3 must break up the incoming messages into smaller  units, **packets**, <u>prepending a layer 3 header to each packet</u>. In this example, $M$ is split into two parts, $M_1$ and $M_2$ , that will be transmitted separately.
- Layer 3 decides which of the outgoing lines to use and passes the packets to
  layer 2.
- Layer 2 adds to each piece <u>not only a header but also a trailer</u> and gives the resulting unit to layer 1 for physical transmission. 
- At the receiving machine, the message moves upward, from layer to layer, w<u>ith headers being stripped off as it progresses</u>. <u>None of the headers for layers below n are passed up to layer n</u>.

SDU版本

- 每个协议栈的最顶端都是一个面向用户的接口,下面各层是为通信服务的协议。
- 用户传输一个数据报时,通常给出用户能够理解的自然语言,然后通过应用层,将自然语言会转化为用于通信的通信数据。
- 通信数据到达传输层,作为传输层的数据部分(传输层 SDU) ,加上传输层的控制信息(传输层 PCI) ,组成传输层的 PDU, 然后交到网络层,传输层的 PDU 下放到网络层后,就成为网络层的 SDU, 然后加上网络层的 PCI, 又组成了网络层的 PDU, 下放到数据链路层,就这样层层下放,层层包裹,最后形成的数据报通过通信线路传输,到达接收方结点协议栈
- 接收方再逆向地逐层把“包裹"拆开,然后把收到的数据提交给用户

**Pcp- The relation between the virtual and actual communication and the difference between protocols and interfaces.**

- The peer processes in layer 4, for example, conceptually think of their communication as being 'horizontal', using the layer 4 protocol.
- Each one is likely to have procedures called something like *SendToOtherSide* and *GetFromOtherSide*, even though these procedures actually communicate with lower layers across the interface, and not with the other side.
- 即virtual communication代表了同层之间的信息交流，因为layer的功能封装而保证其两侧的交流可以视作是protocol直接在同层中进行的。而actual communication则是该层实际通过interface到另一侧的同层之间的信息交流。这个体现了peer process abstraction的Ab。
  - **不同终端的同层对等**：不同机器上的同一层称为**对等层**,同一层的实体称为**对等实体**。
  - 两台主机通信时,对等层在逻辑上有一条直接信道,表现为不经过下层就把信息传送到对方。

**Ab**

- It is worth pointing out that the lower layers of a protocol hierarchy are frequently <u>implemented in hardware or firmware.</u> 
  - Nevertheless, complex protocol algorithms are involved, even if they are embedded (in whole or in part) in hardware.

- 





## Pcp-The Relationship of Services to Protocols

![Figure 1-31. The relationship between a service and a protocol](./img/Figure 1-31. The relationship between a service and a protocol.png)

区别：

- Services and protocols are distinct concepts.
- In this way, the service and the protocol are completely decoupled.
- **Entities** use **protocols** in order to implement their **service** definitions.
- Def对比，Ab对比
- 只有那些能够被传递到上层的功能才能算是服务，并非所有功能都会被传递到上一层















# 参考模型 REFERENCE MODELS

**Lk:** 分层结构每一层的具体内容请参考对应的后续的层级章节，参考模型的内容会在之后查漏补缺补充到后续的层级章节中，因此将这个部分的内容当作是一个提纲即可。

## The OSI Reference Model

### Soc

- This model is based on a proposal developed by the **International Standards Organization (ISO)** as a first step toward international standardization of the protocols used in the various layers (Day and Zimmermann, 1983). It was revised in 1995 (Day, 1995).

### Cc-OSI Model

- 国际标准化组织 (ISO) 提出的网络体系结构模型,称为开放系统互连参考模型 (OSI/RM),通常简称为 OSI 参考模型 。
- **ISO OSI (Open Systems Interconnection) Reference Model** because it deals with connecting open systems—that is, systems that are open for communication with other systems.
- OSI model for short.



### Ab

- OSI参考模型是法定标准



### Pcp-OSI Model

**The principles that were applied to arrive at the seven layers**

- A layer should be created where a different abstraction is needed.
- Each layer should perform a well-defined function.
- The function of each layer should be chosen with an eye toward defining internationally standardized protocols.
- The layer boundaries should be chosen to minimize the information flow across the interfaces.
- The number of layers should be large enough that distinct functions need not be thrown together in the same layer out of necessity and small enough that the architecture does not become unwieldy.



### ---------- St-OSI 参考模型 ----------

![Figure 1-32. The OSI reference model](./img/Figure 1-32. The OSI reference model.png)

### Pcp-排列说明

- 下面的层级排序为由低到高的排序，可以记作“物联网书会实用”
- 其中主机一般认为全部层级都有，而中间系统一般最多只有物理层、数据链路层、网络层
- 从本质上说,由物理层、数据链路层和网络层组成的通信子网为网络环境中的主机提供**点到点的服务**,而传输层为网络中的主机提供**端到端的通信**。
  - 直接相连的结点之间的通信称为**点到点通信**,它只提供一台机器到另一台机器之间的通信,不涉及程序或进程的概念。同时,点到点通信并不能保证数据传输的可靠性,也不能说明源主机与目的主机之间是哪两个进程在通信,这些工作都是由传输层来完成的。
  - **端到端通信**建立在点到点通信的基础上,它是由一段段的点到点通信信道构成的,是比点到点通信更高一级的通信方式,以完成应用程序(进程)之间的通信。“端”是指用户程序的端口,端口号标识了应用层中不同的进程。
  - 通俗地说,点到点可以理解为主机到主机之间的通信,一个点是指一个硬件地址或 IP 地址,网络中参与通信的主机是通过硬件地址或 IP 地址标识的;端到端的通信是指运行在不同主机内的两个进程之间的通信,一个进程由一个端口来标识,所以称为端到端通信。
  - 端到端需要经历完整的七层结构，点到点一般只需要经过通信子网即可。


### 物理层 Physical Layer

**Cc**

- 物理层主要定义数据终端设备 (DTE) 和数据通信设备 (DCE) 的物理与逻辑连接方法,所
  以**物理层协议**也称**物理层接口标准**。由于在通信技术的早期阶段,通信规则称为规程 (Procedure),因此物理层协议也称**物理层规程**。

**Ab**

- 通信子网
- 物理层的传输单位是比特或比特流
- 物理层的服务访问点是“网卡接口”
- 注意,传输信息所利用的一些物理媒体,如双绞线、光缆、无线信道等,并不在物理层协议
  之内而在物理层协议下面。因此,有人把物理媒体当作第 0 层。
- 物理层不会继续添加PCI，一般只是直接将之前的SDU转化为比特流的形式

**F**

- 功能是在物理媒体上为数据端设备透明地传输原始比特流。
  - **透明传输**：指不管所传数据是何种比特组合，都应该能在链路上传输
  - 处理信号通过介质的传输

- 通信链路与通信结点的连接需要一些电路接口, 物理层规定了这些接口的一些参数
- 物理层也规定了通信链路上传输的信号的意义和电气特征。
  - 物理层定义对应的传输模式
    - 例如单工、半双工、双工

  - 物理层定义传输速率

- 物理层能保证比特同步
- 物理层能进行比特编码

**e.g.**

- 物理层接口标准很多,如 EIA-232C 、 EIA/TIARS-449 、 CCITT 的 X.21 等
- 物理层本身的协议有Rj45，802.3等

### 数据链路层 Data Link Layer

**Cc**

- 

**Ab**

- 通信子网
- 数据链路层的传输单位是帧
- 数据链路层的服务访问点是 “MAC 地址(网卡地址)“

**F**

- 任务是将网络层传来的 IP 数据报组装成帧。数据链路层的功能可以概括为<u>成帧、差错控制、流量控制和传输管理</u>等。
  - **成帧**：定义帧的开始和结束
  - **差错控制**：由于外界噪声的干扰,原始的物理连接在传输比特流时可能发生错误。两个结点之间如果规定了数据链路层协议,那么可以检测出这些差错,然后把收到的错误信息丢弃或者纠错，这就是差错控制功能。
    - 主要检查帧错和位错。
    - 可以进行数据重发
  - **流量控制**：在两个相邻结点之间传送数据时,由于两个结点性能的不同,可能结点 A 发送数据的速率会比结点 B 接收数据的速率快,如果不加控制,那么结点 B 就会丢弃很多来不及接收的正确数据,造成传输线路效率的下降。流量控制可以协调两个结点的速率,使结点 A 发送数据的速率刚好是结点 B 可以接收的速率。
    - 数据链路层是相邻结点之间的流量控制
  - **传输管理**
    - **访问控制**：控制对信道的访问，进行链路连接的建立、拆除、分离
    - **物理寻址**
- 广播式网络在数据链路层还要处理新的问题,即如何控制对共享信道的访问。数据链路层的一个特殊的子层——介质访问子层,就是专门处理这个问题的。

**e.g.**

- 典型的数据链路层协议有 SDLC 、 HDLC 、 PPP 、 STP、Ethernet 和帧中继等

### 网络层 Network Layer

**Cc**

- 因特网的主要网络层协议是**无连接的网际协议（lntemet Protocol, IP)** 和许多**路由选择协议**,因此因特网的网络层也称网际层或 IP 层。

**Ab**

- 通信子网
- 网络层的传输单位是数据报或分组
- 网络层的服务访问点是 “IP 地址(网络地址)“

**F**

- 网络层关心的是通信子网的运行控制,主要任务是把网络层的协议数据单元(分组)从源端传到目的端,为分组交换网上的不同主机提供通信服务。
- 关键问题是对分组进行路由选择,并实现流量控制、拥塞控制、差错控制和网际互连等功能。
  - **路由选择**：结点 A 向结点 B 传输一个分组时,有很多条可以选择的路由,而网络层的作用就是根据网络的情况,利用相应的路由算法计算出一条合适的路径,使这个分组A可以顺利到达结点 B 。
    - 其实多数路由管理的工作都属于网络层
  - **流量控制**：流量控制与数据链路层的流量控制含义一样,都是协调 A 的发送速率和 B 的接收速率。
    - 网络层是整个网络中的流量控制
  - **差错控制**：差错控制是通信两结点之间约定的特定检错规则,如奇偶校验码,接收方根据这个规则检查接收到的分组是否出现差错,如果出现了差错,那么能纠错就纠错,不能纠错就丢弃,确保向上层提交的数据都是无误的。
  - **拥塞控制**：在所有结点都处于来不及接收分组而要丢弃大量分组的情况,那么网络就处于拥塞状态,拥塞状态使得网络中的两个结点无法正常通信。网络层要采取一定的措施来缓解这种拥塞。

**e.g.**

- 网络层的协议有 IP 、 IPX 、 ICMP 、 IGMP 、 ARP 、 RARP 和 OSPF 等。

### 传输层 Transport Layer

**Cc**

- 传输层也称运输层

**Ab**

- 不属于分类里的子网
- 传输单位是报文段 (TCP) 或用户数据报 (UDP)或段
- 传输层的服务访问点是“端口号“

**F**

- 传输层负责主机中<u>两个进程之间</u>的通信,功能是为端到端连接提供可靠的传输服务,为端到端连接提供<u>流量控制、差错控制、服务质量、数据传输管理、拥塞控制</u>等服务。
  - **流量控制**：流量控制与数据链路层的流量控制含义一样,都是协调 A 的发送速率和 B 的接收速率。
    - 传输层是端到端的流量控制

  - **差错控制和校正**：差错控制是通信两结点之间约定的特定检错规则,如奇偶校验码,接收方根据这个规则检查接收到的分组是否出现差错,如果出现了差错,那么能纠错就纠错,不能纠错就丢弃,确保向上层提交的数据都是无误的。
  - **服务质量**或**可靠性**
    - **可靠传输**：对报文段的信息进行确认，从而保证多个报文段的信息能准确、有序地发送到另一端。
    - **不可靠传输**：对于简单的、单个报文段的信息不进行确认，若出错则重复发送

  - **拥塞控制**：同上

- 由于一台主机可同时运行多个进程,因此传输层具有<u>复用和分用</u>的功能。
  - 复用是指多个应用层进程可同时使用下面传输层的服务
  - 分用是指传输层把收到的信息分别交付给上面应用层中相应的进程。
- 使用传输层的服务,高层用户可以直接进行端到端的数据传输,从而忽略通信子网的存在。

**e.g.**

- 传输层的协议有 TCP、UDP。

### 会话层 Session Layer

**Ab**

- 资源子网
- 传输单位是报文

**F**

- 
- 会话层负责管理主机间的会话进程,包括建立、管理及终止进程间的会话。会话层可以使用校验点使通信会话在通信失效时从校验点继续恢复通信,实现数据同步。
- 会话层允许不同主机上的各个进程之间进行会话。会话层利用传输层提供的端到端的服务,向表示层提供它的增值服务。
  - 这种服务主要为表示层实体或用户进程建立连接并在连接上有序地传输数据,这就是会话,也称建立同步 (SYN) 。
- 建立对应的校验点/同步点，进行数据检验或同步暂存，使用校验点可使通信失效时从校验点继续恢复通信，实现数据同步

### 表示层 Presentation Layer

**Ab**

- 资源子网
- 传输单位是报文

**F**

- 
- 表示层主要处理在两个通信系统中交换信息的表示方式。
  - 不同机器采用的编码和表示方法不同,使用的数据结构也不同。为了使不同表示方法的数据和信息之间能互相交换,表示层采用抽象的标准方法定义数据结构,并采用标准的编码形式。
  - 通常是语法和语义的处理
- 数据压缩和恢复
- 数据加密和解密也是表示层可提供的数据表示变换功能。

**e.g.**

- 各种编码和显示方案的协议，例如:ASCII，Unicode之类的，例如JPEG、PNG等格式。

### 应用层 Application Layer

**Cc**

- 应用层是 OSI 参考模型的最高层,是用户与网络的界面

**Ab**

- 资源子网
- 传输单位是报文
- 应用层提供的服务访问点是“用户界面”
- 应用层是最复杂的一层,使用的协议也最多

**F**

- 应用层为特定类型的网络应用提供访问 OSI 参考模型环境的手段。因为用户的实际应用多种多样,这就要求应用层采用不同的应用协议来解决不同类型的应用要求,因此应用层是最复杂的一层,使用的协议也最多。

**e.g.**

- 典型的协议有用于文件传送的 FTP 、用于电子邮件的 SMTP 、用于万维网的 HTTP 等。

### --------------------

### Problems-A Critique of the OSI

**Tips:** A Critique of the OSI Model and Protocols

**Bad Timing**

- The time at which a standard is established is absolutely critical to its success. 
- If the interval between the two elephants is very short (because everyone is in a hurry to get started), the people developing the standards may get crushed. It now appears that the standard OSI protocols got crushed.

**Bad Design**

- Both the model and the protocols are flawed. 
  - The choice of seven layers was more political than technical, and two of the layers (session and presentation) are nearly empty, whereas two other ones (data link and network) are overfull.
- The OSI model, along with its associated service definitions and protocols, is extraordinarily complex.
  - They are also difficult to implement and inefficient in operation.
- In addition to being incomprehensible, another problem with OSI is that some
  functions, such as addressing, flow control, and error control, reappear again and again in each layer. 
  - 还真是，408里全都是这些。

**Bad Implementations**

- Given the enormous complexity of the model and the protocols, it will come as no surprise that the initial implementations were huge, unwieldy, and slow. 

**Bad Politics**

- OSI, on the other hand, was widely thought to be the creature of the European
  telecommunication ministries, the European Community, and later the U.S. Government. This belief was only partly true





## The TCP/IP Reference Model

### Soc

- 参考Internet的Soc，ARPANET的后续发展中涉及了TCP和IP协议的产生。

### Cc

### Ab

- TCP/IP参考模型是事实标准



### ---------- St-TCP/IP 参考模型 ----------

![Figure 1-33. The TCP IP reference model](./img/Figure 1-33. The TCP IP reference model.png)

### Pcp-排列说明

- 下面的层级排序为由低到高的排序，相较于OSI Model主要是整合了一些层次。

### 网络接口层 The Link Layer

**Cc**

- The link layer describes what links such as serial lines and classic Ethernet
  must do to meet the needs of this connectionless internet layer.
- It is not really a layer at all, in the normal sense of the term, but rather an interface between hosts and transmission links (or between the link layer and the internet layer). Early material on the TCP/IP model ignored it.

**Ab**

- the choice of a packet-switching network based on a connectionless layer that runs across different networks
- 对应 OSI参考模型中的物理层和数据链路层

**F**

- 网络接口层的功能类似于 OSI 参考模型的物理层和数据链路层。
- 网络接口层表示与物理网络的接口, 但实际上 TCP/IP 本身并未真正描述这一部分,只是指出主机必须使用某种协议与网络连接,以便在其上传递 IP 分组。
  - 具体的物理网络既可以是各种类型的局域网,如以太网、令牌环网、令牌总线网等,
  - 也可以是诸如电话网、 SDH 、 X.25 、帧中继和 ATM 等公共数据网络。
- 网络接口层的作用是从主机或结点接收 IP 分组,并把它们发送到指定的物理网络上。

**e.g.**

- DSL
- SONET
- 802.11
- Ethernet

### 网际层 The Internet Layer

**Cc**

- The internet layer

**Ab**

- The internet layer is the linchpin that holds the whole architecture together. 
- 对应 OSI 参考模型中的网络层
- 网际层(主机-主机)是 TCP/IP 体系结构的关键部分

**F**

- Its job is to permit hosts to inject packets into any network and have them travel independently to the destination (potentially on a different network).
  - Packet routing
    - The routing problem has largely been solved, but congestion can only be handled with help from higher layers.

  - 

- 网际层和 OSI 参考模型的网络层在功能上非常相似。
- 网际层将分组发往任何网络,并为之独立地选择合适的路由,但它不保证各个分组有
  序地到达,各个分组的有序交付由高层负责。
- 网际层定义了标准的分组格式和协议,即 IP 。 IP 协议是因特网中的核心协议;

**e.g.-IP协议**

- IP (Internet Protocol): The job of the internet layer is to deliver IP packets where they are supposed to go.
  - IPv4, IPv6

- ICMP (Internet Control Message Protocol): companion protocol of IP.

### 传输层 The Transport Layer

**Ab**

- 对应 OSI 参考模型中的传输层

**F**

- It is designed to allow peer entities on the source and destination hosts to carry on a conversation, just as in the OSI transport layer.
- 传输层和 OSI 参考模型的传输层在功能上非常相似。
- 使得发送端和目的端主机上的对等实体进行会话。

**e.g.-协议**

- Two end-to-end transport protocols have been defined here
- TCP (Transmis-sion Control Protocol), is a <u>reliable connection-oriented protocol</u> that allows a byte stream originating on one machine to be delivered without error on any other machine in the internet.
  - 传输控制协议 (Transmission Control Protocol, TCP) 。它是面向连接的,数据传输的单位是报文段,能够提供可靠的交付。
  - It segments the incoming byte stream into discrete messages and passes each one on to the internet layer. 
  - At the destination, the receiving TCP process reassembles the received messages into the output stream.
  - TCP also handles flow control to make sure a fast sender cannot swamp a slow receiver with more messages than it can handle.

- UDP (User Datagram Protocol), is an <u>unreliable, connectionless protocol</u> for applications that do not want TCP's sequencing or flow control and wish to provide their own (if any). 
  - 用户数据报协议 (User Datagram Protocol, UDP) 。它是无连接的,数据传输的单位是用户数据报,不保证提供可靠的交付,只能提供“尽最大努力交付"。
  - It is also widely used for <u>one-shot, client-server-type request-reply queries</u> and applications in which <u>prompt delivery is more important than accurate delivery</u>, such as transmitting speech or video.


### 应用层 The Application Layer

**Ab**

- 对应 OSI 参考模型中的会话层、表示层和应用层。

**F**

- 应用层(用户-用户)包含所有的高层协议 It contains all the higher-level protocols

**e.g.-协议**

- The early ones included virtual terminal (TELNET), file transfer (FTP), and electronic mail (SMTP).
  - 虚拟终端协议 (Telnet) 、文件传输协议 (FTP)、电子邮件协议 (SMTP)

- The Domain Name System (DNS), for mapping host names onto their network addresses.
  - 域名解析服务 (DNS)

- HTTP, the protocol for fetching pages on the World Wide Web
  - 超文本传输协议 (HTTP) 

- RTP, the protocol for delivering real-time media such as voice or movies.

### --------------------

### Problems-A Critique of the TCP/IP

**Tips:** A Critique of the TCP/IP Model and Protocols

- First, the model does not clearly distinguish the concepts of services, interfaces, and protocols.
  - Consequently, the TCP/IP model is not much of a guide for designing new networks using new technologies
- Second, the TCP/IP model is not at all general and is poorly suited to describ-
  ing any protocol stack other than TCP/IP. 
- Third, the link layer is not really a layer at all in the normal sense of the term
  as used in the context of layered protocols.
  - It is an interface (between the network and data link layers).
- Fourth, the TCP/IP model does not distinguish between the physical and data link layers. 
- Finally, although the IP and TCP protocols were carefully thought out and well implemented, many of the other early protocols were ad hoc, generally produced by a couple of graduate students hacking away until they got tired.



## Pcp-TCP/IP 模型与 OSI 参考模型的比较

### 相似

- 二者都采取分层的体系结构,而且分层的功能也大体相似。
- 二者都是基于独立的协议栈的概念。
- 二者都可以解决异构网络的互连,实现世界上不同厂家生产的计算机之间或不同网络之间的通信。



### 不同

- OSI参考模型比TCP/IP参考模型先出现，OSI参考模型主要是作为一个理论模型，而TCP/IP模型是先有协议栈的实现之后才补充理论模型的。
- OSI 参考模型的最大贡献就是精确地定义了三个主要概念:服务、协议和接口,这与现代的面向对象程序设计思想非常吻合。而 TCP/IP 模型在这三个概念上却没有明确区分,不符合软件工程的思想。
-  OSI 参考模型产生在协议发明之前,没有偏向于任何特定的协议,通用性良好。但设计者在协议方面没有太多经验,不知道把哪些功能放到哪一层更好。 TCP/IP 模型正好相反,首先出现的是协议,模型实际上是对已有协议的描述,因此不会出现协议不能匹配模型的情况,但该模型不适合于任何其他非 TCP/IP 的协议栈。
-  TCP/IP 模型在设计之初就考虑到了多种异构网的互连问题,并将网际协议 (IP) 作为
  一个单独的重要层次。 OSI 参考模型最初只考虑到用一种标准的公用数据网将各种不同的系统互连。 OSI 参考模型认识到 IP 的重要性后,只好在网络层中划分出一个子层来完成类似于 TCP/IP模型中的 IP 的功能。
-  OSI 参考模型在网络层支持无连接和面向连接的通信,但在传输层仅有面向连接的通
  信。而 TCP/IP 模型认为可靠性是端到端的问题,因此它在网际层仅有一种无连接的通信模式, 但传输层支持无连接和面向连接两种模式。【这个不同点恰反，常常作为考查点。】



# The Model Used in this Note

**Tips:** The Model Used in This Book

![Figure 1-36. The reference model used in this book](./img/Figure 1-36. The reference model used in this book.png)

**St-Hybrid Model of OSI Model and TCP/IP Model**

- This model has five layers, running from the physical layer up through the link,
  network and transport layers to the application layer.
-  The physical layer specifies how to transmit bits across different kinds of media as electrical (or other analog) signals. 
- The link layer is concerned with how to send finite-length messages between directly connected computers with specified levels of reliability. 
  - Ethernet and 802.11 are examples of link layer protocols.

- The network layer deals with how to combine multiple links into networks,
  and networks of networks, into internetworks so that we can send packets between distant computers.
  - This includes the task of finding the path along which to send the packets. 
  - IP is the main example protocol we will study for this layer.

- The transport layer strengthens the delivery guarantees of the Network layer, usually with increased reliability, and provide delivery abstractions, such as a reliable byte stream, that match the needs of different applications.
  - TCP is an important example of a transport layer protocol.

- The application layer contains programs that make use of the network.
  - Many, but not all, networked applications have user interfaces, such as a Web browser.
  - Our concern, however, is with the portion of the program that uses the network.
  - This is the HTTP protocol in the case of the Web browser. There are also important support programs in the application layer, such as the DNS, that are used by many applications. 
  - These form the glue that makes the network function.


**Pcp+F**

- As mentioned earlier, the strength of the OSI reference model is the model itself (minus the presentation and session layers), which has proven to be ex-
  ceptionally useful for discussing computer networks.
- In contrast, the strength of the TCP/IP reference model is the protocols, which have been widely used for many years.
- In this way, we retain the value of the OSI model for understanding network architectures, but concentrate primarily on protocols that are important in practice, from TCP/IP and related protocols to newer ones such as 802.11, SONET, and Bluetooth.







# ----- 其他 -----

# STANDARDIZATION

**Cc**

- Standards define what is needed for **interoperability**.
- Often getting to interoperability this way is difficult, since there are many implementation choices and standards that usually define many options.

- A protocol standard defines the protocol over the wire but not the service interface inside the box, except to help explain the protocol. 
  - That said, good service interfaces, like good **APIs (Application Programming Interfaces)**. are valuable for getting protocols used

**Pcp**

一般而言，标准分为法定标准和事实标准

- **法定标准**：由法律权威机构制定的正式的、合法的标准，例如OSI
  - **De jure (Latin for ''by law'') standards**, in contrast, are adopted through the rules of some formal standardization body. 
  - International standardization authorities are generally divided into two classes: 
    - those established by treaty among national governments
    - those comprising voluntary, non-treaty organizations
  - **e.g.-Organization**
    - In the area of computer network standards, there are several organizations of each type, notably ITU, ISO, IETF, and IEEE, all of which we will discuss below.
- **事实标准**：由于该公司/技术/产品/协议等长期占据市场主导地位，因而被广泛使用的标准。
  - **De facto (Latin for ''from the fact'') standards** are those that have just happened, without any formal plan.
  - **e.g.-Standard**
    - HTTP, the protocol on which the Web runs, started life as a de facto standard.
    - Bluetooth is another example. It was originally developed by Ericsson but now everyone is using it.
    - TCP/IP
- De facto standards often evolve into de jure standards, especially if they are successful. 
- Standards bodies often ratify each others' standards, in what looks like patting one another on the back, to increase the market for a technology.
  - HTTP

- These days, many ad hoc business alliances that are formed around particular technologies also play a significant role in developing and refining network standards. 
  - For example, **3GPP (Third Generation Partnership Project)** was a collaboration among telecommunications associations that drives
    the UMTS 3G mobile phone standards.


**Cat-Who's Who in the Telecommunications World**

- In some cases, the telecommunication authority is a nationalized company like **AT&T**, and in others it is simply a branch of the government, usually known as the **PTT (Post, Telegraph & Telephone administration).**
- **ITU (International Telecommunication Union)**. Its job was to standardize international telecommunications, which in those days meant telegraphy.
  - ITU-T, the Telecommunications Standardization Sector
  - ITU-R, the Radiocommunications Sector
  - ITU-D, the Development Sector.

**Cat-Who's Who in International Standards World**

- International standards are produced and published by **ISO (International Standards Organization)**, a voluntary non-treaty organization founded in 1946.
  - These members include ANSI (U.S.), BSI (Great Britain), AFNOR (France), DIN (Germany), and 157 others.
  - On issues of telecommunication standards, ISO and ITU-T often cooperate (ISO is a member of ITU-T) to avoid the irony of two official and mutually incompatible international standards.
  - Process
    -  A working group is then formed to come up with a CD (Committee Draft). The CD is then circulated to all the member bodies, which get 6 months to criticize it. 
    - If a substantial majority approves, a revised document, called a DIS (Draft International Standard), is produced and circulated for comments and voting.
    - Based on the results of this round, the final text of the IS (International Standard) is prepared, approved, and published.
  - NIST (National Institute of Standards and Technology) is part of the U.S.
    Department of Commerce. It used to be called the National Bureau of Standards.
  - Another major player in the standards world is IEEE (Institute of Electrical and Electronics Engineers), the largest professional organization in the world.

**Cat-Who's Who in Internet Standards World**

- IAB (Internet Activities Board/Internet Architecture Board)
  - Communication was done by a series of technical reports called **RFCs (Request For Comments)**. RFCs are stored online and can be fetched by anyone interested in them from www.ietf.org/rfc.

- The researchers were moved to the IRTF (Internet Research Task Force), which was made subsidiary to IAB, along with the IETF (Internet Engineering Task Force). 
- Later, the Internet Society was created, populated by people interested in the Internet. The Internet Society is thus in a sense comparable to ACM or IEEE. It is governed by elected trustees who appoint the IAB's members.
- The idea of this split was to have the IRTF concentrate on long-term research while the IETF dealt with short-term engineering issues. 
- For Web standards, the **World Wide Web Consortium (W3C)** develops pro-
  tocols and guidelines to facilitate the long-term growth of the Web. 

**Pcp-formal standardization process**

因特网的所有标准都以RFC (Request For Comments) 的形式在因特网上发布,但并非每个
RFC 都是因特网标准, RFC 要上升为因特网的正式标准需经过以下 4 个阶段。

- 因特网草案 (Internet Draft )。这个阶段还不是 RFC 文档。【不一定要有】
- 建议标准 (Proposed Standard) 。从这个阶段开始就成为 RFC 文档。
  - To become a Proposed Standard, the basic idea must be explained in an
    RFC and have sufficient interest in the community to warrant consideration.  

- 草案标准 (Draft Standard) 。
  - To advance to the Draft Standard stage, a working implementation must have been rigorously tested by at least two independent sites for at least 4 months.

- 因特网标准 (Internet Standard) 。
  - If the IAB is convinced that the idea is sound and the software works, it can declare the RFC to be an Internet Standard. 

- DoD standards 【不一定要用】
  - Some Internet Standards have become DoD standards (MIL-STD), making them mandatory for DoD suppliers.


此外,还有试验的 RFC 和提供信息的 RFC 。

在国际上,负责制定、实施相关网络标准的标准化组织众多, 主要有如下几个:

- 国际标准化组织 CISO) 。 其制定的主要网络标准或规范有 OSI 参考模型、 HDLC 等。
- 国际电信联盟 (ITU) 。其前身为 国际电话电报咨询委员会 (CCITT) ,其下属机构 ITU-T制定了大量有关远程通信 的标准。
- 国际电气电子工程师协会 (IEEE) 。 世界上最大的专业技术团体,由计算机和工程学专业人士组成。 IEEE 在通信领域最著名的研究成果是 802 标准。
- Internet工程任务组(IETF): 负责因特网相关标准制定，例如RFC



# POLICY, LEGAL, AND SOCIAL ISSUES

**Tips:** 并非详细内容，建议直接Lk: Computer Networks > 1.8 POLICY, LEGAL, AND SOCIAL ISSUES，另外由于这是强时效性内容，其实也不需要太关注

**Online Speech**

- The trouble comes with topics that people actually care about, like politics,
  religion, or sex.
- DMCA takedown notices (after the Digital Millennium Copyright Act)

**Net Neutrality**

- **Def**
  - The notion that ISPs should provide equal quality of service to a given
    type of application traffic, regardless of who is sending that content, is often referred to as network neutrality.
- On June 11, 2018, net neutrality was abolished in the entire United States by order of the FCC. 
- zero rating

**Security**

- DDoS (Distributed Denial of Service) attack
- Spam email (or unwanted electronic mail)
- Phishing messages

**Privacy**

- profiling and tracking users by collecting data about their network behavior over time.
- One way that advertisers track users is by placing small files called **cookies** that Web browsers store on users' computers. Cookies allow advertisers and tracking companies to track users' browsing behavior and activities from
  one site to another.
- **browser fingerprinting**: it turns out that the configuration of your browser is unique enough to you that a company can use code on its Web page to extract your browser settings and determine your unique identity with high probability. 
  - web services are also used to collected users' personal information.
- The rise of mobile services has also made location privacy a growing concern.
- Various technologies, ranging from VPNs to anonymous browsing software
  such as the Tor browser, aim to improve user privacy by obfuscating the source of user traffic. 

**Disinformation**







# METRIC UNITS

![Figure 1-38. The principal metric prefixes](./img/Figure 1-38. The principal metric prefixes.png)

**Pcp**

- The principal metric prefixes are listed in Fig. 1-38.
- The prefixes are typically abbreviated by their first letters, with the units greater than 1 capitalized (KB, MB, etc.). 
  - One exception (for historical reasons) is kbps for kilobits/sec. 
  - Since milli and micro both begin with the letter 'm', a choice had to be made. Normally, 'm' is used for milli and 'μ' (the Greek letter mu) is used for micro.
- It is also worth pointing out that for measuring memory, disk, file, and database sizes, in common industry practice, the units have slightly different meanings.
  - There, kilo means $2^{10}$ (1024) rather than $10^3$ (1000) because memories are always a power of two. 
  - Similarly, a 1-MB memory contains $2^{20}$(1,048,576) bytes, a 1-GB memory contains $2^{30}$ (1,073,741,824) bytes, and a 1-TB database contains $2^{40}$ (1,099,511,627,776) bytes.
  - However, a 1-kbps communication line transmits 1000 bits per second and a 10-Mbps LAN runs at 10,000,000 bits/sec because these speeds are not powers of two
- Note also the capital ''B'' in that usage to mean 'bytes''  (units of eight bits), instead of a lowercase ''b'' that means 'bits.''
  - $1 \mathrm{B} = 8 \mathrm{b}$









# ---------- 物理层 ----------

# 物理层

## Cc

- 物理层主要定义数据终端设备 (DTE) 和数据通信设备 (DCE) 的物理与逻辑连接方法,所
  以**物理层协议**也称**物理层接口标准**。由于在通信技术的早期阶段,通信规则称为规程 (Procedure),因此物理层协议也称**物理层规程**。



## Ab

- 通信子网
- 物理层的传输单位是比特(Bits)或比特流(Bit stream)
- 物理层的服务访问点是“网卡接口”
- 注意,传输信息所利用的一些物理媒体,如双绞线、光缆、无线信道等,并不在物理层协议
  之内而在物理层协议下面。因此,有人把有时称传输媒体为 0 层
- 物理层不会继续添加PCI，一般只是直接将之前的SDU转化为比特流的形式
- 物理层考虑的是怎样才能在连接各台计算机的传输媒体上传输数据比特流,而不是指具体的传输媒体



## Ab-物理层接口的特性

物理层的主要任务可以描述为确定与传输媒体的接口有关的一些特性:

- **机械特性**：指明接口所用接线器的形状和尺寸、引脚数目和排列、固定和锁定装置等。定义物理连接的特性,规定物理连接时所采用的规格、接口形状、引线数目、引脚数量和排列情况
- **电气特性**：指明在接口电缆的各条线上出现的电压的范围，规定传输二进制位时,线路上信号的电压范围、阻抗匹配、传输速率和距离限制等。【更强调电压的范围和条件】
- **功能特性**：指明某条线上出现的某一电平的电压表示何种意义，指明某条线上出现的<u>某一电平表示何种意义</u>,接口部件的信号线的用途。【更强调电平的意义和引脚，也就是编码方式】
- **过程特性**：或称规程特性。指明对于不同功能的各种可能事件的出现顺序，定义各条物理线路的工作规程和时序关系。



## F

- It defines the electrical, timing, and other interfaces by which bits are sent as
  signals over channels.
- The properties of different kinds of physical channels determine the per-
  formance (e.g., throughput, latency, and error rate)
- The purpose of the physical layer is to transport bits from one machine to another. 
  - 物理层的功能是在物理媒体上为数据端设备透明地传输原始比特流。
  - **透明传输**：指不管所传数据是何种比特组合，都应该能在链路上传输
  - 物理层考虑的是如何在连接到各种计算机的传输媒体上传输数据比特流,而不指具体的传输媒体。网络中的硬件设备和传输介质的种类繁多,通信方式也各不相同。物理层应尽可能屏蔽这些差异,让数据链路层感觉不到这些差异,使数据链路层只需考虑如何完成本层的协议和服务。
  
- 物理层可以处理信号通过介质的传输
- 物理层规定了通信链路、通信结点的连接的电路接口的各项参数，即各种<u>电路接口标准</u>；同时物理层也规定了通信链路上<u>传输的信号的意义和电气特征</u>。

  - 包括 Ab-物理层接口的特性 中的各项特性
  - 物理层定义对应的传输模式
    - 例如单工、半双工、双工
  - 物理层定义传输速率
- 物理层能保证比特同步
- 物理层能进行比特编码



## 协议和接口

- 物理层接口标准
  - EIA-232C or  EIA RS-232-C
  - EIA/TIARS-449
  - ADSL
  - SONET/SDH
  - CCITT的X.21

- 物理层本身的协议有Rj45，802.3等











# 通信基础

## Cc-基本概念

### 数据通信系统概念

**通信**

- 通信的目的是传送信息,如文字、图像和视频等

**数据**

- **Def:** 数据是指传送通信信息的实体
- **Ab**
  - 可用“模拟的”或“数字 的"来修饰: 
    - 连续变化的数据称为模拟数据
    - 取值仅允许为有限的几个离散数值的数据称为数字数据。
- **Mt-数据传输方式**
  - 串行传输是指1比特1比特地按照时间顺序传输
    - 远距离通信通常采用串行传输
  - 并行传输是指若干比特通过多条通信信道同时传输。

**信号**

- **Def:** 信号是数据的电气或电磁表现,是数据在传输过程中的存在形式

- **Cat**
  
  - **模拟信号**：连续变化的信号称为模拟信号/连续信号
  - **数字信号**：取值仅允许为有限的几个离散数值的信号称为数字信号/离散信号
  
- **Cat**
  
  - 信道上传送的信号有基带信号和宽带信号
  
  - **基带信号**
    - **Def:** 在计算机内部或在相邻设备之间近距离传输时,可以不经过调制就在信道上直接进行的传输
      - 基带信号将<u>数字信号</u> 1 和 0 直接用两种不同的电压表示,然后送到数字信道上传输(称为基带传输)
    - **Ab**
      - 基带信号就是发出的直接表达了要传输的信息的数字信号,比如我们说话的声波
    
      - 来自信源的信号,像计算机输出的代表各种文字或图像文件的数据信号都属于基带信号。
      - 在传输距离较近时，计算机网络采用基带传输方式（近距离衰减小，从而信号内容不易发生变化）
      - 它通常用于局域网。
      - 数字基带传输就是在信道中直接传输数字信号,且传输媒体的整个带宽都被基带信号占用,双向地传输信息。
      - 最简单的方法是用两个高低电平来表示二进制数字,常用的编码方法有不归零编码和曼彻斯特编码。
    
  - **频带传输**
    - **Def:** 用数字信号对特定频率的载波进行调制(数字调制),将其变成适合千传送的信号后再进行传输
  
    - **Ab**
      - 远距离传输或无线传输时,数字信号必须用频带传输技术进行传输。
  
      - 利用频带传输,不仅解决了电话系统传输数字信号的问题,而且可以实现多路复用,进而提高传输信道的利用率。
  
  - **宽带信号**
    - **Def:** 宽带信号将基带信号进行调制后形成<u>频分复用模拟信号</u>,然后送到模拟信道上传输(称为宽带传输)。
      - 借助频带传输,可将链路容量分解成两个或多个信道,每个信道可以携带不同的信号,这就是宽带传输
    - **Ab**
      - 把基带信号经过载波调制后，把信号的频率范围搬移到较高的频段以便在信道中传输（即仅在一段频率范围内能够通过信道)
      - 在传输距离较远时，计算机网络采用宽带传输方式（远距离衰减大，即使信号变化大也能最后过滤出来基带信号
      - 宽带传输中所有的信道能同时互不干扰地发送信号,链路容量大大增加
  

**数据通信**

- 数据通信是指数字计算机或其他数字终端之间的通信。
- **Cat-通信方式**
  - **单向通信**：只有一个方向的通信而没有反方向的交互,仅需要一条信道。
    - 例如,无线电广播、电视广播就属千这种类型。
    - A third category consists of links that allow traffic in only one direction, like a one-way street. They are called **simplex links**.
  - **半双工通信**：通信的双方都可以发送或接收信息,但任何一方都不能同时发送和接收信息,此时需要两条信道。
    - In contrast, links that can be used in either direction, but only one way at a time, like a single-track railroad line, are called **half-duplex links**.
  - **全双工通信**：通信双方可以同时发送和接收信息,也需要两条信道。
    - Links that can be used in both directions at the same time, like a two-lane road, are called **full-duplex links**.
- **Cat-数据传输方式**
  - **串行传输**：将表示一个字符的8位二进制数按由低位到高位的顺序依次发送
    - 速度慢,费用低,适合远距离

  - **并行传输**：将表示一个字符的8位二进制数同时通过8条信道发送
    - 速度快,费用高,适合近距离
    - 用于计算机内部数据传输

- **Cat-同步和异步通信**
  - **同步传输**：在同步传输的模式下，数据的传送是以一个数据区块为单位，因此同步传输又称为区块传输。
    - **Ab**
    - 在传送数据时，需先送出1个或多个同步字符，再送出整批的数据。
    - 同步依靠同步字符实现，让另一端开始接收一定量的字符
    - 同步通信的通信双方必须先建立同步,即双方的时钟要调整到同一个频率。收发双方不停地发送和接收连续的同步比特流。
    - 同步通信数据率较高,但实现的代价也较高。
    - **Cat**
      - 全网同步：即用一个非常精确的主时钟对全网所有结点上的时钟进行同步
      - 准同步：即各结点的时钟之间允许有微小的误差,然后采用其他措施实现同步传输。
    
  - **异步传输**：异步传输将比特分成小组进行传送，小组可以是8位的1个字符或更长。发送方可以在任何时刻发送这些比特组，而接收方不知道它们会在什么时候到达。
    - 在发送字符时,所发送的字符之间的时间间隔可以是任意的,但接收端必须时刻做好接收的准备 。 
    - 发送端可以在任意时刻开始发送字符,因此必须在每个字符开始和结束的地方加
      上标志,即开始位和停止位,以便使接收端能够正确地将每个字符接收下来。
    - 异步通信也可以用帧作为发送的单位。这时,帧的首部和尾部必须设有一 些特殊的比特组合,使得接收端能够找出 一帧的开始(即帧定界) 。
    - 同步依靠字符起始位和字符终止位实现，让另一端开始接收二者之间的字符
    - 异步通信的通信设备简单、便宜,但传输效率较低(因为标志的开销所占比例较大)
    


**数据通信系统**

- **Cpn: **一个数据通信系统主要划分为信源、信道和信宿三部分。
- **Mt:** 发送端信源发出的信息需要通过<u>变换器</u>转换成适合于在信道上传输的信号,而通过 信道/传输系统 传输到接收端的信号先由<u>反变换器</u>转换成原始信息,再发送给信宿 。
  - 发送端/源系统一般指信源和变换器/发送器
  - 接收端/目的系统一般指信宿和反变换器/接收器

**信源**

- **Def:** 信源是产生和发送数据的源头
- **Ab**
  - 通常是计算机或其他数字终端装置

**信道**

- **Def:** 信道是信号的传输媒介，一个信道可视为一条线路的逻辑部件,一般用来表示向某个方向传送信息的介质。
- **Ab**
  - 信道与电路并不等同
  - 一条通信线路往往包含一条发送信道和一条接收信道。
  - 信道的极限容量是指信道的最高码元传输速率或信道的极限信息传输速率。
- **Cat**
  - 按传输信号形式
    - 传送模拟信号的模拟信道
    - 传送数字信号的数字信道
  - 传输介质的不同
    - 无线信道
    - 有线信道

**信宿**

- **Def:** 信宿是接收数据的终点
- **Ab**
  - 通常是计算机或其他数字终端装置

**失真**

- 由于带宽受限、噪声、干扰等原因，发送信号波形与接收信号波形之间的差距变大的情况，导致接收器难以识别信号
- **Ab-影响因素**
  - 码元传输速率越快，失真越严重
  - 信号传输距离越远，衰减就越久，干扰就越久，失真越严重
  - 噪声干扰越多，失真越严重
  - 传输媒体质量越差，失真越严重
  - 码间串扰越严重，失真越严重

**噪声源**

- **Def:** 噪声源是信道上的噪声(即对信号的干扰)及分散在通信系统其他各处的噪声的集中表示 。
- **Cat**
  - 全局性噪声/热噪声：由于线路本身的电气特性所产生的随机噪声/热噪声，是信道固有的，随机存在的
    - 通常通过改良传感器，提高信噪比解决

  - 局部性噪声/冲击噪声：由于外界原因导致的短暂的冲击噪声，是数据链路层的各种差错产生的原因
    - 通常通过编码方式解决，各种差错控制主要就是为了纠正这些局部性噪声导致的差错




### 数据传输速率概念

**码元**

- **Def:** 码元是指用一个固定时长的信号波形(数字脉冲)表示一位k进制数字,代表不同离散数值的基本波形有k种，这个时长内的信号称为k进制码元，而该时长称为码元宽度。
- **Ab**
  - 码元是数字通信中数字信号的计量单位
- **F**
  -  1 码元可以携带若干比特的信息量。准确来说，1个k进制码元可以携带n个比特，$2^n = k$或$n = \log_2 k$，这个在位数和比特数转换的时候很常用
  -  相位、振幅不同的调制技术和进制的关系：$n$个相位和$m$种振幅能产生$nm$种波形，因此为$nm$进制码元，需要$\log_2(nm)$比特来表示这些波形，

**波特**

-  **Def:** 波特表示数字通信系统每秒传输的码元个数。
-  **Ab**
  - 1波特代表的码元可以是多进制的,也可以是二进制的,码元速率与进制数无关。

**有效离散值**

- **Def:** 1个波特/1个码元/n个比特 最大可以表示的 k进制数/k种数字/k种码元

- **Ab**

  - 别称：离散电平数目，信号状态数，
  - 相关概念：$k^m$个有效离散值 or m个k进制码元 or $k^m$个量级 or $k^m$相位 or $i$相位$j$振幅且$ij = k^m$ 在有效离散值个数上是相等的。

- **Pcp**

  - $m$位$k$进制数的有效离散值为
    $$
    有效离散值 = k^m
    $$
    $m$位$k$进制数的有效离散值所需的比特数为
    $$
    k^m 个有效离散值 \rightarrow m \log_2 (k) \ \mathrm{b}
    $$
    

**Pcp-波特、码元、有效离散值、比特数的转换**

- m波特相当于每秒传输m个码元

- m个k进制码元相当于$k^m$个有效离散值

- $k^m$个有效离散值需要$\log_2 (k)$个2进制比特表示，对于$k^m$个有效离散值有
  $$
  k^m \  有效离散值 \rightarrow m \log_2 (k) \  \mathrm{b}
  $$

- 最终可以得出在k进制码元下m波特的码元传输速率对应的比特传输速率
  $$
  m \ \mathrm{Baud} = m \ 码元/s \\
  = k^m \ 有效离散值/s \\
  \rightarrow m \log_2(k) \ \mathrm{b/s}
  $$
  

**速率**

- **Def:** 速率也称数据率,指的是数据传输速率,表示单位时间内传输/发送的数据量。 
- **Ab-影响因素**

  - 波特率和比特率是正相关的关系
  - 频率带宽（奈奎斯特定理）
  - 信噪比（香农定理）
  - 信号传播速度【电磁波速度】与信道的传输/发送速率【即此处的两种速率】是无关的

- **Cat**
  - **码元传输速率**：码元传输速率表示单位时间内数字通信系统所传输的码元个数(也可称为脉冲个数或信号变化的次数)
    - **Ab**
      - 单位是波特 (Baud) 。
      - 码元传输速率也称波特率、调制速率、波形速率或符号速率。
      - 码元传输速率与进制数无关，是码元宽度的倒数
  - **信息传输速率**：信息传输速率表示单位时间内数字通信系统传输的二进制码元个数(即比特数), 。
    - **Ab**
      - 单位是比特/秒 (b/s)
      - 信息传输速率也称比特率、信息速率、数据率
      - 其实就是之前的网络性能指标中的速率
  - **Pcp-两种速率的转化**
    - 若 1 个码元携带 n 比特的信息量, 则 M 波特率的<u>码元传输速率</u>所对应的<u>信息传输速率</u>为 $M \times n$​ b/s。
      $$
      1 \  码元 = n \  b \\
      M \  Baud  = n M  \  b/s
      $$
      
    - 以太网的转换参考曼彻斯特编码

**带宽**

- **Def-计算机网络背景下:** 带宽表示网络的通信线路所能传输数据的能力。因此,带宽表示单位时间内从网络中的某一点到另一点所能通过的“最高数据率”。
- **Def-通信工程背景下:**在模拟信号系统中，信号的最高震荡频率和最低震荡频率的差值就代表了系统的通频带宽/信道带宽，单位为Hz
- **Ab**
  - 单位：b/s 





## Pcp

### 奈奎斯特定理

**Cc**

- **码间串扰**：具体的信道所能通过的频率范围总是有限的。信号中的许多高频分量往往不能通过信道, 否则在传输中会衰减,导致接收端收到的信号波形失去码元之间的清晰界限,这种现象称为码间串扰。

- **奈奎斯特定理**：奈奎斯特定理规定:在<u>理想低通(没有噪声、带宽有限)的信道</u>中,为了避免码间串扰,<u>极限码元传输速率</u>为 $2W \  Baud$ ,

  $$
  理想低通信道下的极限数据传输速率= 2W \log_2 V \  b/s
  $$

  其中 W 是理想低通信道的信道带宽【注意，这是Hz的那一个带宽。

  而 V 表示每个码元离散电平的数目(**码元的离散电平数目**是指有多少种不同的码元。

  - 比如有16 种不同的码元,则需要 4 个二进制位,因此数据传输速率的值是码元传输速率的值的 4 倍，即相差的倍数为$\log_2 V = \log_2 16 =4$,则**极限数据率**为$8 W$
  - $\log_2 V$本质上是在求一个码元所需要的比特数，从而将波特率转换为比特率，如果直接知道就不必转化为有效离散值

**Ab**

- 奈奎斯特定理又称奈氏准则

**Pcp**

- 在任何信道中,码元传输速率是有上限的。若传输速率超过此上限,就会出现严重的码
  间串扰问题,使得接收端不可能完全正确识别码元。
- 奈氏准则是在理想条件下推导出来的。在实际条件下,最高码元传输速率要比理想条件下得出的数值小很多。
- 信道的频带越宽(即通过的信号高频分量越多),就可用更高的速率进行码元的有效传输。
- 奈氏准则给出了码元传输速率的限制,但并未对信息传输速率给出限制,即未对一个码
  元可以对应多少个二进制位给出限制。
- 由于码元传输速率受奈氏准则的制约,所以要提高数据传输速率,就必须设法使每个码元携带更多比特的信息量,此时就需要采用多元制的调制方法。

### 香农定理

**Cc**

- **香农定理**：香农 (Shannon) 定理给出了<u>带宽受限且有高斯白噪声干扰的信道</u>的<u>极限数据传输速率</u>【比特的那一个】, 当用此速率进行传输时,可以做到不产生误差。
  $$
  信道的极限数据传输速率= W \log_2 (1 + S/N) \  b/s
  $$
  其中 W 为信道的带宽【注意，是赫兹那一个带宽】, S 为信道所传输信号的平均功率, N 为信道内部的高斯噪声功率。 SNR 为信噪比,即信号的平均功率与噪声的平均功率之比,
  $$
  SNR = 10 \log_{10}(S/N) \  dB
  $$
  
  > 注意SNR和S/N的区别，计算极限数据传输速率用的是S/N
  >
  > 而往往题目所说的单位为分贝的信噪比是SNR，需要转换为S/N再计算
  >
  > 如果是比例则一般是指S/N

 **Pcp**

- 信道的带宽或信道中的信噪比越大,信息的极限传输速率越高。
- 对一定的传输带宽和一定的信噪比,信息传输速率的上限是确定的。
- 只要信息传输速率低于信道的极限传输速率,就能找到某种方法来实现无差错的传输。
- 香衣定理得出的是极限信息传输速率 ,实际信道能达到的传输速率要比它低不少。

### Pcp-两个定理的比较

- 奈氏准则只考虑了带宽与极限码元传输速率的关系,而香农定理不仅考虑到了带宽,也考虑到了信噪比 。这从另一个侧面表明, 一个码元对应的二进制位数是有限的。
- 若两个定理都可以使用，则考虑二者中较小的作为极限数据传输速率



## Mt-编码与调制

### Cc

- 数据无论是数字的还是模拟的,为 了传输的目的都必须转变成信号 。

#### 调制

**Def:** 把数据变换为模拟信号的过程称为调制

#### 编码

**Def:** 把数据变换为数字信号的过程称为编码。

- 具体用什么样的数字信号表示 0 及用什么样的数字信号表示 1 就是所谓的编码。



### Mt-数据信号转换

**Cc**

- 数字数据可以通过数字发送器转换为数字信号传输,也可以通过调制器转换成模拟信号传输;同样,模拟数据可以通过 PCM 编码器转换成数字信号传输,也可以通过放大器调制器转换成模拟信号传输。因此一共有四种组合。

#### Mt-数字数据编码为数字信号

**Cc**

- **数字数据编码**：即在基本不改变数字数据信号频率的情况下,直接传输数字信号。

**Ab**

- 使用的仪器是数字发送器

**F**

- 数字数据编码用于基带传输中

**Cat-归零编码 (RZ)**

- 在归零编码 (RZ) 中用高电平代表 1 、低电平代表 0 (或者相反)
- 每个时钟周期的中间均跳变到低电平(归零),接收方根据该跳变调整本方的时钟基准,这就为传输双方提供了自同步机制。
- 由于归零需要占用一部分带宽,因此传输效率受到了一定的影响。

**Cat-非归零编码 (NRZ)** 

- 非归零编码 (NRZ) 与 RZ 编码的区别是不用归零,依然用高电平代表 1 、低电平代表 0 (或者相反)
- 一个周期可以全部用来传输数据。但 NRZ 编码无法传递时钟信号,双方难以同步,因此若想传输高速同步数据,则需要都带有时钟线。

**Cat-反向非归零编码 (NRZI)** 

- 反向非归零编码 (NRZI) 与 NRZ 编码的区别是用信号的翻转代表 0 、信号保持不变代表 1 。
- 翻转的信号本身可以作为一种通知机制。这种编码方式集成了前两种编码的优点,既能传输时钟信号,又能尽量不损失系统带宽。 
- **e.g.:** USB2.0 通信的编码方式就是 NRZI 编码。

**Cat-曼彻斯特编码 (Manchester Encoding)** 

- 曼彻斯特编码 (Manchester Encoding) 将一个码元分成两个相等的间隔
  - 前一个间隔为高电平而后一个间隔为低电平表示码元 1; 码元 0 的表示方法则正好相反。当然,也可采用相反的规定。【高低相反】
  - 一个时钟周期中比特传输了一个，但是码元传输了两个，即曼彻斯特编码的码元传输速率是数据比特传输速率的两倍
- 该编码的特点是,在每个码元的中间出现电平跳变,位中间的跳变既作为时钟信号(可用于同步),又作为数据信号,但它所占的频带宽度是原始基带宽度的两倍 。
- **e.g.:** 以太网使用的编码方式就是曼彻斯特编码。
  - 10BaseT 即 I0Mb/s 的基带传输的双绞线以太网


**Cat-差分曼彻斯特编码**

- 差分曼彻斯特编码的规则是
  - 若码元为 1, 则前半个码元的电平与上一码元的后半个码元的电平相同
  - 若码元为 0 ,则前半个码元的电平与上一码元的后半个码元的电平不同。
  - 【一个比特的前后码元的是否相同】

- 该编码的特点是,在每个码元的中间都有一次电平的跳转,可以实现自同步,且抗干扰性较好 。
- **e.g.:** 差分曼彻斯特编码常用于局域网传输,

**Cat-4B/5B 编码**

- 将欲发送数据流的每 4 位作为一组,然后按照 4B/5B 编码规则将其转换成相应的 5 位码。 5 位码共 32 种组合,但只采用其中的 16 种对应 16 种不同的 4 位码,其他 16 种作为控制码(帧的开始和结束、线路的状态信息等)或保留。
  - 编码效率为80%


#### Mt-数字数据调制为模拟信号

**Cc**

- 数字数据调制技术在发送端将数字信号转换为模拟信号,而在接收端将模拟信号还原为数字信号,分别对应于调制解调器的调制和解调过程。

**Ab**

- 使用的仪器是调制器

**Cat-幅移键控 (ASK)** 

- 通过改变载波信号的<u>振幅</u>来表示数字信号 1 和 0, 而载波的频率和相位都不改变。
- 比较容易实现,但抗干扰能力差。

**Cat-频移键控 (FSK)** 

- 通过改变载波信号的<u>频率</u>来表示数字信号 1 和 0, 而载波的振幅和相位都不改变。
- 容易实现,抗干扰能力强,目前应用较为广泛。

**Cat-相移键控 (PSK)** 

- 通过改变载波信号的<u>相位</u>来表示数字信号 1 和 0, 而载波的振幅和频率都不改变。它又分为绝对调相和相对调相。

**Cat-正交振幅调制 (QAM)** 

- 在频率相同的前提下,将 ASK 与 PSK 结合起来,形成叠加信号。

- 设波特率为 B, 采用 m 个相位,每个相位有 n 种振幅,则该 QAM 技术的数据传输速率 R 为
  $$
  R = B \log_2 (mn) \  b/s
  $$
  

#### Mt-模拟数据编码为数字信号

**Cc**

- 这种编码方式最典型的例子是常用于对音频信号进行编码的**脉码编码调制 (PCM)** 。它主要包括三个步骤,即采样、量化和编码。

**Ab**

- 使用的仪器是PCM编码器

**Pcp-Nyquist–Shannon (奈奎斯特-香农)采样定理**

- 在通信领域,带宽是指信号最高频率 与 最低频率之差,单位为 Hz 。因此,将模拟信号转换成数字信号时,假设原始信号中的最大频率为 $f$,那么采样频率 $f_{采样}$必须大于或等于最大频率 $f$ 的两倍,才能保证采样后的数字信号完整保留原始模拟信号的信息。
- 一般简称为采样定理

**Mt-步骤**

- **采样**是指对模拟信号进行周期性扫描,把时间上连续的信号变成时间上离散的信号 。根据采样定理,当采样的频率大于或等于模拟数据的频带带宽(最高变化频率)的两倍时,
  所得的离散信号可以无失真地代表被采样的模拟数据。
  - 采样频率单位为Hz or 次/s
  - 采样频率若低于带宽，则在使用奈奎斯特/香农定理时使用采样频率作为$W$使用。
  
- **量化**是把采样取得的电平幅值按照一定的分级标度转化为对应的数字值并取整数,这样
  就把连续的电平幅值转换为了离散的数字量。采样和量化的实质就是分割和转换。
- **编码**是把量化的结果转换为与之对应的二进制编码。

#### Mt-模拟数据调制为模拟信号

**Cc**

- 为了实现传输的有效性,可能需要较高的频率。这种调制方式还可以使用**频分复用 (FDM)技术**,充分利用带宽资源。

**Ab**

- 使用的仪器是放大器和调制器

**e.g.:** 电话机和本地局交换机采用模拟信号传输模拟数据的编码方式,模拟的声音数据是加载到模拟的载波信号中传输的。



## Mt-交换

### 电路交换

**Cc**

- **Def:** 在进行数据传输前,两个结点之间必须先建立一条专用(双方独占)的物理通信路径(由通信双方之间的交换设备和链路逐段连接而成),该路径可能经过许多中间结点。这一路径在整个数据传输期间一直被独占,直到通信结束后才被释放。
- 从通信资源的分配角度来看,“交换”就是按照某种方式动态地分配传输线路的资源

**Ab**

- 电路交换的关键点是,在数据传输的过程中,用户始终占用端到端的固定传输带宽。
- 电路建立后,除源结点和目的结点外,电路上的任何结点都采取“直通方式”接收数据和发送数据,即不会存在存储转发所耗费的时间。
- 优点
  - 通信时延小
    - 由于通信线路为通信双方用户专用,数据直达,因此传输数据的时延非常小。当传输的数据量较大时,这一优点非常明显。
    - 时延方面电路交换比另外两种交换方法都要小
  - 有序传输
    - 双方通信时按发送顺序传送数据,不存在失序问题 
  - 没有冲突
    - 不同的通信双方拥有不同的信道,不会出现争用物理信道的问题。
  - 适用范围广
    - 电路交换既适用于传输模拟信号,又适用于传输数字信号 。
  - 实时性强 
    - 通信双方之间的物理通路一旦建立,双方就可以随时通信 。
  - 控制简单
    - 电路交换的交换设备(交换机等)及控制均较简单 。
- 缺点
  - 建立连接时间长
    - 电路交换的平均连接建立时间对计算机通信来说太长。
  - 线路独占，使用效率低
    - 电路交换连接建立后,物理通路被通信双方独占,即使通信线路空闲,也不能供其他用户使用,因而信道利用率低。
  - 灵活性差
    - 只要在通信双方建立的通路中的任何一点出了故障,就必须重新拨号建立新的连接,这对十分紧急和重要的通信是很不利的。
  - 无数据存储能力，难以平滑通信量
  - 难以规格化
    - 电路交换时 ,数据直达,不同类型、不同规格、不同速率的终端很难相互进行通信。
  - 难以在通信过程中进行差错控制

**Pg**

- 连接建立【呼叫应答的电路建立过程】、数据传输和连接释放【同样需要发送释放请求和返回释放应答】。

**Mt-数据传输时间计算**

### 报文交换

**Cc**

- **报文**:报文(message)是网络中交换与传输的数据单元,即站点一次性要发送的数据块。报文包含了将要发送的完整的数据信息,其长短很不一致,长度不限且可变。

**Ab**

- 数据交换的单位是报文,报文携带有目标地址、源地址等信息，报文大小没有限制。
- 报文交换在交换结点采用的是<u>存储转发</u>的传输方式。
- 报文交换主要使用在早期的电报通信网中,现在较少使用,通常被较先进的分组交换
  方式所取代。
- 优点
  - 无须建立连接
    - 报文交换不需要为通信双方预先建立一条用的通信线路,不存在建立连接时延,用户可以随时发送报文。
  - 动态分配线路
    - 当发送方把报文交给交换设备时,交换设备先存储所有的报文,然后选择一条合适的空闲线路,将报文发送出去。
  - 提高线路可靠性
    - 如果某条传输路径发生故障,那么可重新选择另一条路径传输数据,因此提高了传输的可靠性。
  - 提高线路利用率
    - 通信双方不是固定占有一条通信线路,而是在不同的时间一段一段地部分占有这条物理通道,因而大大提高了通信线路的利用率。
  - 提供多目标服务
    - 一个报文可以同时发送给多个目的地址,这在电路交换中是很难实现的。
  - 在存储转发中容易实现代码转换和速率匹配,甚至收发双方可以不同时处于可用状态。这样就便于类型、 规格和速度不同的计算机之间进行通信。
  - 具有差错控制
- 缺点
  - 报文交换只适用于数字信号，不能传输模拟信号
  - 实时性较差，会引起转发时延
    - 由于数据进入交换结点后要经历存储、转发这一过程,因此会引起转发时延(包括接收报文、检验正确性、排队、发送时间等)。
    - 不适合传送实时或交互式业务的数据
  - 报文交换对报文的大小没有限制,这就要求网络结点需要有较大的缓存空间。
    - 为了降低成本,减少结点的缓冲存储器的容量,有时要把等待转发的报文存在磁盘上，进一步增加了传送时延大

**Pg**

- 信源发送报文
- 每个节点收下整个报文后,暂存报文并检查有无错误
- 当所需要的输出电路空闲时, 利用路由信息找到下一个结点地址,传送给下一个结点，直到到达信宿
- 在两个通信用户间的其他线路段,可传输其他用户的报文,不像电路交换那样必须占用端到端的全部信道

**Mt-数据传输时间计算**

- 设数据传输速率为$v \ \mathrm{b/s}$，发送数据大小为$m \ \mathrm{b}$
- 发送时延$t$为$t = m/v \ \mathrm{s}$，接收时延也是$t$，所以总的发送和接收时延为$t_1 = 2t$
- 总时延为
  $$
  t = \sum t_i
  $$
  

### 分组交换

**Cc**

- **分组**: 把大的数据块划分为合理的小数据块,再加上一些必要的控制信息(如源地址、目的地址和编号信息等),构成分组 (Packet) 

**Ab**

- 分组交换限制了每次传送的数据块大小的上限【一般为128B】。网络结点根据控制信息把分组送到下一个结点,下一个结点接收到分组后,暂时保存并排队等待传输,然后根据分组控制信息选择它的下一个结点,直到到达目的结点。
- 分组交换也采用存储转发方式,但解决了报文交换中大报文传输的问题。
- 优点
  - 无建立时延
    - 不需要为通信双方预先建立一条专用的通信线路,不存在连接建立时延,用户可随时发送分组。
  - 线路利用率高
    - 通信双方不是固定占有一条通信线路,而是在不同的时间一段一段地部分占有这条物理通路,因而大大提高了通信线路的利用率。
    - 相较于报文交换必须要等到所有报文在结点存储后再进行下一次转发，分组交换中的每一个分组都能独立进行存储转发
  - 简化了存储管理(相对千报文交换)
    - 因为分组的长度固定,相应的缓冲区的大小也固定,在交换结点中存储器的管理通常被简化为对缓冲区的管理,相对比较容易。
  - 加速传输
    - 分组是逐个传输的,可以使后一个分组的存储操作与前一个分组的转发操作并行,这种流水线方式减少了报文的传输时间。此外,传输一个分组所需的缓冲区比传输一次报文所需的缓冲区小得多,这样因缓冲区不足而等待发送的概率及时间也必然少得多。
  - 减少了出错概率和重发数据量。
    - 因为分组较短,其出错概率必然减小,所以每次重发的数据量也就大大减少,这样不仅提高了可靠性,也减少了传输时延。
  - 分组短小,适用于计算机之间突发式数据通信。
- 缺点
  - 存在存储转发时延
    - 尽管分组交换比报文交换的传输时延少,但相对于电路交换仍存在存储转发时延,而且其结点交换机必须具有更强的处理能力。
  - 需要传输额外的信息量
    - 每个小数据块都要加上源地址、目的地址和分组编号等控制信息,从而构成分组,因此使得传送的信息量增大了 5%~ 10% , 一定程度上降低了通信效率,增加了处理的时间,使控制复杂,时延增加。
  - 当分组交换采用数据报服务时,可能会出现<u>失序、丢失或重复分组</u>,分组到达目的结点时,<u>要对分组按编号进行排序等工作</u>,因此很麻烦。
    - 若采用虚电路服务,虽无失序问题,但有呼叫建立、数据传输和虚电路释放三个过程。

**Mt-数据传输时间计算**

- 设数据传输速率为$v \ \mathrm{b/s}$，发送数据大小为$m \ \mathrm{b}$，分组大小为$n \ \mathrm{b}$
- 发送一个分组的发送时延$t = n/v \ \mathrm{s}$，发送的分组数$num = m/n$，需要注意的是在分组交换中发送和接收是可以同时进行的，即一般可以认为发送第二份分组和接收第一份分组的时间是同一段时间，所以实际总的发送和接收时延为$t_1 = (num+1)t$

#### Cat-数据报方式

**Cc**

- 作为通信子网用户的端系统发送一个报文时,在端系统中实现的高层协议先把报文拆成若干带有序号的数据单元,并在网络层加上<u>源地址和目的地址</u>等控制信息后形成数据报分组(即网络层的 PDU) 。
- 中间结点存储分组很短一段时间,找到最佳的路由后,尽快转 发每个分组。
- 不同的分组可以走不同的路径,也可以按不同的顺序到达目的结点 。

**Ab**

- 无连接服务
  - 发送分组前不需要建立连接。发送方可随时发送分组,网络中的结点可随时接收分组。
  - 网络尽最大努力交付,传输不保证可靠性,所以可能丢失;网络为每个分组独立地选择
    路由,转发的路径可能不同,因而分组不一定按序到达目的结点，且不保证分组的有序到达

- 服务方式由网络层提供
- 当分组正在某一链路上传送时,分组并不占用网络的其他部分资源。因为采用存储转发技术,资源是共享的,所以主机 A 在发送分组时,主机 B 也可同时向其他主机发送分组 。
- 发送的分组中要包括发送端和接收端的完整地址,以便可以独立传输。
- 分组在交换结点存储转发时,需要排队等候处理,这会带来一定的时延。通过交换结点
  的通信量较大或网络发生拥塞时,这种时延会大大增加,交换结点还可根据情况丢弃部
  分分组。
- 网络具有冗余路径,当某个交换结点或一条链路出现故障时,可相应地更新转发表,寻
  找另一条路径转发分组,对故障的适应能力强。
- 存储转发的延时一般较小,提高了网络的吞吐量。
- 收发双方不独占某条链路,资源利用率较高。

#### Cat-虚电路方式

**Cc**

- **虚电路**: 一条源主机到目的主机类似于电路的路径(逻辑连接),路径上所有结点都要维持这条虚电路的建立, 都维持一张虚电路表,每一项记录了一个打开的虚电路的信息。

- 虚电路方式试图将数据报方式与电路交换方式结合起来,充分发挥两种方法的优点,以达到最佳的数据交换效果。

- 在分组发送之前,要求在发送方和接收方建立一条逻辑上相连的虚电路,并且连接一旦建立,就固定了虚电路所对应的物理路径。【但并非实际的物理连接】
  - 建立虚电路时需要进行路由选择，但完成建立之后就不需要了

- 在虚电路方式 中,端系统每次建立虚电路时,选择一个未用过的<u>虚电路号/虚电路标识符</u>分配给该虚电路, 以区别于本系统中的其他虚电路。在传送数据时,每个数据分组不仅要有分组号、校验和等控制信息,还要有它要通过的虚电路号,以区别于其他虚电路上的分组 。
- 在虚电路网络中的每个结点上都维持一张<u>虚电路表</u>,表中的每项记录了一个打开的虚电路的信息,包括在接收链路和发送链路上的虚电路号、前一结点和下一结点的标识。
- 数据的传输是双向进行的,上述信息是在虚电路的建立过程中确定的。
- 

**Ab**

- 面向连接服务
  - 网络中的传输是否有确认与网络层提供的两种服务没有任何关系，与网络层上层有关系
- 服务方式由网络层提供
- 虚电路通信链路的建立和拆除需要时间开销,对交互式应用和小量的短分组情况显得很
  浪费,但对长时间、频繁的数据交换效率较高。
- 虚电路的路由选择体现在连接建立阶段,连接建立后,就<u>确定了传输路径</u>。因此在经过虚电路的结点时只需要进行差错控制而不需要路径选择。
  - 一个特定会话的虚电路是事先建立好的,因此它的数据分组所走的路径也是固定的

- 虚电路提供了可靠的通信功能，能保证每个分组正确且有序地到达。
- 虚电路可以对两个端点的数据流量进行控制，可以进行暂缓发送之类的流量控制操作
- 分组首部不包含目的地址,包含的是<u>虚电路标识符</u>,相对于数据报方式,其开销小
- 虚电路的缺点在于当一个结点或链路失效时，所有经过该结点或链路的虚电路都将失效，因此对于出错率高的传输系统,易出现结点故障，不宜使用虚电路
- 虚电路之所以是“虚”的,是因为这条电路不是专用的,每个结点到其他结点之间的链路可能同时有若干虚电路通过,也可能同时与多个结点之间建立虚电路。
  - 每条虚电路支待特定的两个端系统之间的数据传输,两个端系统之间也可以有多条虚电路为不同的进程服务,这些虚电路的实际路由可能相同也可能不同。
  - 因此也不需要为每条虚电路预分配带宽
- 虚电路交换是多路复用技术
  - 虚电路不只是临时性的,它提供的服务包括永久性虚电路 (PVC) 和交换型虚电路 (SVC) ,其中前者是一种提前定义好的、基本上不需要任何建立时间的端点之间的连接,而后者是端点之间的一种临时性连接,这些连接只持续所需的时间,并且在会话结束时就取消这种连接



**Pg**

- 与电路交换类似,整个通信过程分为三个阶段:虚电路建立、数据传输与虚电路释放。
- 虚电路建立
  - 为进行数据传输,主机 A 与主机 B 之间先建立 一 条逻辑通路,主机 A 发出 一 个特殊的“呼叫请求“分组,该分组通过中间结点送往主机 B, 若主机 B 同意连接,则发送“呼叫应答“分组予以确认。
- 数据传输
  - 虚电路建立后,主机 A 就可向主机 B 发送数据分组。当然,主机 B 也可在该虚电路上向主机 A 发送数据。
- 虚电路释放
  - 传送结束后主机 A 通过发送“释放请求“分组来拆除虚电路,逐段断开整个连接。

#### Pcp-数据报服务和虚电路服务的比较

|                    | 数据报服务                                                  | 虚电路服务                                                   |
| ------------------ | ----------------------------------------------------------- | ------------------------------------------------------------ |
| 连接的建立         | 不需要                                                      | 必须有                                                       |
| 目的地址           | 每个分组都有完整的目的地址                                  | 仅在建立连接阶段使用,之后每个分组使用长度较短的虚电路号      |
| 路由选择           | 每个分组独立地进行路由选择和转发                            | 属于同一条虚电路的分组按照同一路由转发，建立虚电路时需要进行路由选择，但完成建立之后就不需要了 |
| 分组顺序           | 不保证分组的有序到达                                        | 保证分组的有序到达                                           |
| 可靠性             | 无连接的，不保证可靠通信,可靠性由用户主机来保证             | 面向连接的，可靠性由网络保证                                 |
| 对网络故障的适应性 | 出故障的结点丢失分组,其他分组路径选择发生变化时可以正常传输 | 所有经过故障结点的虚电路均不能正常工作                       |
| 差错处理和流量控制 | 由用户主机进行流量控制,不保证数据报的可靠性                 | 可由分组交换网负责,也可由用户主机负责                        |
|                    |                                                             |                                                              |



### Pcp-交换技术的比较

- 要传送的数据量很大且其传送时间远大于呼叫时间时,采用<u>电路交换</u>较为合适。
- 端到端的通路由多段链路组成时,采用<u>分组交换</u>传送数据较为合适。
- 从提高整个网络的信道利用率上看,报文交换和分组交换优于电路交换,其中分组交换比报文交换的时延小,尤其适合于计算机之间的突发式数据通信 。





- 





# 传输介质 Transmission Media

## Cc

**传输介质**

- **Def:** 传输介质也称传输媒体,它是数据传输系统中发送设备和接收设备之间的物理通路。
  - 

- 注意传输介质本身并不完全代表物理层，物理层更多地强调特性。
- **Cat**

  - 传输介质可分为导向传输介质和非导向传输介质。
    - 在导向传输介质中,电磁波被导向沿着固体媒介(铜线或光纤)传播
    - 而非导向传输介质可以是空气、真空或海水等。



## Persistent Storage

**Cc**

- One of the most common ways to transport data from one device to another is to write them onto persistent storage, such as magnetic or solid-state storage (e.g., recordable DVDs), physically transport the tape or disks to the destination machine, and read them back in again. 

**Ab**

- especially for applications where a high data rate or cost per bit transported is the key factor.
- Although the bandwidth characteristics of persistent storage are excellent, the delay characteristics are poor: Transmission time is measured in hours or days, not milliseconds. 

**F**

- For moving very large amounts of data, this is often the best solution.



## 导向型传输介质 Guided Transmission Media

### Cc

**Guided transmission media**

- **Def:** Transmission media that rely on a physical cable or wire are often called guided transmission media because the signal transmissions are guided along a path with a physical cable or wire.
- 

### 双绞线  Twisted Pairs

**Cc+Cpn**

- 双绞线是最常用的古老传输介质,它由两根采用一定规则并排绞合的、相互绝缘的铜导线组成。
  - One of the oldest and still most common transmission media is twisted pair.
  - A twisted pair consists of two insulated copper wires, typically about 1 mm thick.

**Ab**

- 双绞线的价格便宜,是最常用的传输介质之一。
- A signal is usually carried as <u>the difference in voltage</u> between the two wires in the pair. 
  - Transmitting the signal as <u>the difference between the two voltage levels</u>, as opposed to an absolute voltage, <u>provides better immunity to external noise</u> because the noise tends to affect the voltage traveling through both wires in the same way, leaving the differential relatively unchanged.
- 模拟传输和数字传输都可使用双绞线
  - Twisted pairs can be used for transmitting either analog or digital information
- 双绞线的带宽取决于铜线的粗细和传输的距离,其通信距离一般为几千米到数十千米。
  - The bandwidth depends on the thickness of the wire and the distance traveled, but hundreds of megabits/sec can be achieved for a few kilometers, in many cases, and more when various tricks are used.
  - Twisted pairs can run several kilometers without amplification, but for longer distances the signal becomes too attenuated and repeaters are needed.
  - 距离太远时,对于模拟传输,要用放大器放大衰减的信号;对于数字传输, 要用中继器将失真的信号整形。

- 绞合可以减少对相邻导线的电磁干扰。
  - The wires are twisted together in a helical form, similar to a DNA molecule. Two parallel wires constitute a fine antenna: when the wires are twisted, the waves from different twists cancel out, so the wire radiates less effectively. 
  - More twists per meter result in less crosstalk and a better-quality signal over longer distances, making the cables more suitable for high-speed computer communication, especially 100-Mbps and 1-Gbps Ethernet LANs.

- When many twisted pairs <u>run in parallel</u> for a substantial distance, they are bundled together and encased in a protective sheath.
  - The pairs in these bundles would interfere with one another if it were not for the twisting. 

- Different LAN standards may use the twisted pairs differently. 
- 为了进一步提高抗电磁干扰的能力,可在双绞线的外面再加上一层,即用金属丝编织成的屏蔽层, 这就是屏蔽双绞线 (STP) 。无屏蔽层的双绞线称为非屏蔽双绞线 (UTP (Unshielded Twisted Pair) 。
  - Shielding reduces the susceptibility to external interference and crosstalk with other nearby cables to meet demanding performance specifications.


**St**

- 从内到外依次排序
  - 双绞铜导线
  - 绝缘层
  - 屏蔽层（屏蔽双绞线 (STP) 有，非屏蔽双绞线 (UTP)没有）
  - 聚氯乙烯套层


**F**

- The most common application of the twisted pair is the telephone system.
  - Both telephone calls and ADSL Internet access run over these lines
- 在局域网和传统电话网中普遍使用

**Cat-Cat 3e**

- Cat 5 replaced earlier Category 3 cables with a similar cable that uses the same connector, but has more twists per meter

**Cat-Cat 5e**

![Figure 2-1. Category 5e UTP cable with four twisted pairs](./img/Figure 2-1. Category 5e UTP cable with four twisted pairs.png)

- **Def-Category 5e cabling or Cat 5e**
  - A Category 5e twisted pair consists of two insulated wires gently twisted
    together. Four such pairs are typically grouped in a plastic sheath to protect the wires and keep them together.

**Cat-Cat 6e and Cat 7e**

- **Def**
  - These categories have more stringent specifications to handle signals with greater bandwidths. 
  - Some cables in Category 6 and above can support the 10-Gbps links that are now commonly deployed in many networks, such as in new office buildings.
- **Ab**
  - Through Category 6, these wiring types are referred to as **UTP (Unshielded Twisted Pair)** as they consist simply of wires and insulators.
  - In contrast to these, Category 7 cables have shielding on the individual twisted pairs, as well as around the entire cable (but inside the plastic protective sheath).

**Cat-Cat 8e**

- **Def**
  - Category 8 wiring runs at higher speeds than the lower categories, but operates only at short distances of around 30 meters and is thus only suitable in data centers.
- **Cat**
  - The Category 8 standard has two options: Class I, which is compatible with
    Category 6A; and Class II, which is compatible with Category 7A.

### 同轴电缆 Coaxial Cable

**Cc**

- coaxial cable (known to its many friends as just "coax" and pronounced "co-ax")

**Ab**

- 由于外导体屏蔽层的作用,同轴电缆具有良好的抗干扰特性,被广泛用于传输较高速率的数据,其传输距离更远,但价格较双绞线贵。
  - 同轴电缆比双绞线的传输速率更快,得益于同轴电缆具有更高的屏蔽性,同时有更好的抗啋声性
  - The construction and shielding of the coaxial cable give it a good combination of high bandwidth and excellent noise immunity
  - It has better shielding and greater bandwidth than unshielded twisted pairs, so it can span longer distances at higher speeds.

-  **Bandwidth**: The bandwidth possible depends on the cable quality and length.
  - Coaxial cable has extremely wide bandwidth; modern cables have a bandwidth of up to 6 GHz, thus allowing many conversations to be simultaneously transmitted over a single coaxial cable.

- 利用一根同轴电缆互连主机构成以太网,则主机间的通信方式为半双工

**St**

![Figure 2-2. A coaxial cable](./img/Figure 2-2. A coaxial cable.png)

- 从内到外依次排序
  - 同轴电缆由内导体、绝缘层、网状编织屏蔽层和塑料外层/绝缘保护套层构成。

  - A coaxial cable consists of **a stiff copper** wire as the **core**, surrounded by an **insulating material**.
  - The insulator is encased by a **cylindrical conductor**, often as a closely woven braided mesh. 
  - The outer conductor is covered in a **protective plastic sheath**. 

**F**

- Coaxial cables were once widely used within the telephone system for long-distance lines but have now largely been replaced by fiber optics on long-haul routes. 
- Coax is still widely used for cable television and metropolitan area networks and is also used for delivering high-speed Internet connectivity to homes in many parts of the world.

**Cat**

- 按特性阻抗数值的不同, 通常将同轴电缆分为两类: 50$\Omega$ 同轴电缆和 75$\Omega$ 同轴电缆。
  - This distinction is based on historical, rather than technical, factor.

- 50$\Omega$ 同轴电缆主要用于传送基带数字信号,又称基带同轴电缆,它在局域网中应用广泛; 
  - **50-ohm cable** is commonly used when it is intended for <u>digital transmission</u> from the start.

- 75$\Omega$ 同轴电缆主要用于传送宽带信号,又称宽带同轴电缆,主要用于有线电视系统。
  - **75-ohm cable** is commonly used for <u>analog transmission</u> and cable
    television.
  - Starting in the mid-1990s, cable TV operators began to provide Internet access over cable, which has made 75-ohm cable more important for data communication.


### 电缆 Power Lines

**Cc**

- **Electrical power lines** deliver electrical power to houses, and electrical wiring within houses distributes the power to electrical outlets.

**Ab**

- Convenience
- The data signal is superimposed on the low-frequency power signal (on the active or "hot" wire) as both signals use the wiring at the same time.
- The difficulty with using household electrical wiring for a network is that it
  was designed to distribute power signals.
  - Electrical signals are sent at 50–60 Hz and the wiring attenuates the much higher frequency (MHz) signals needed for high-rate data communication.
  - The electrical properties of the wiring vary from one house to the next and change as appliances are turned on and off, which causes data signals to bounce around the wiring.
- it is practical to send at least 500 Mbps short distances over typical household electrical wiring by using communication schemes that resist impaired frequencies and bursts of errors. 

**F**

- Power lines have been used by electricity companies for low-rate communication such as remote metering for many years, as well in the home to control devices 
- In recent years there has been renewed interest in high-rate communication over these lines, both inside the home as a LAN and outside the home for
  broadband Internet access. 

### 光纤 fiber optics

**Cc**

- **Def:** 光纤通信就是利用光导纤维(简称光纤)传递光脉冲来进行通信。

**Ab-基本**

- 有光脉冲表示 1, 无光脉冲表示 0。
- 可见光的频率约为 $10^8$​ MHz, 因此光纤通信系统的带宽范围极大。
  - In contrast, the achievable bandwidth with fiber technology is in excess of 50,000 Gbps (50 Tbps) and we are nowhere near reaching these limits.
  - The current practical limit of around 100 Gbps is simply due to our inability to convert between electrical and optical signals any faster. 

- To build higher-capacity links, many channels are simply <u>carried in parallel over a single fiber</u>.
- 当光线从高折射率的介质射向低折射率的介质时,其折射角将大于入射角。因此,只要入射角大于某个临界角度,就会出现全反射,即光线碰到包层时就会折射回纤芯,这个过程不断重复,光也就沿着光纤传输下去。

**Ab-特点**

- 通信容量非常大
- 传输损耗小,中继距离长,对远距离传输特别经济。
- 抗雷电和电磁干扰性能好。这在有大电流脉冲干扰的环境下尤为重要。
- 无串音干扰,保密性好,也不易被窃听或截取数据。
- 体积小,重量轻。这在现有电缆管道已拥塞不堪的清况下特别有利。

**St**

- 光纤主要由纤芯和包层构成
- 纤芯是实心的，很细,其直径只有 8 至 10 μm, 光波通过纤芯进行传导
- 包层较纤芯有较低的折射率。

**Cat**

- **多模光纤**
  - **Def:** 利用光的全反射特性,可以将从不同角度入射的多条光线在一根光纤中传输,这种光纤称为多模光纤
  - **Ab**
    - 多模光纤的光源为发光二极管。
    - 光脉冲在多模光纤中传输时会逐渐展宽,输出脉冲不明显，造成失真,因此多模光纤只适合近距离传输。
- **单模光纤**
  - **Def:** 光纤的直径减小到只有一个光的波长时,光纤就像一根波导那样,可使光线一直向前传播,而不会产生多次反射,从而保证其输出脉冲明显失真小，这样的以横向模式传输的光纤就是单模光纤
  - **Ab**
    - 单模光纤的纤芯很细,直径只有几微米,制造成本较高。
    - 同时,单模光纤的光源为定向性很好的半导体激光器
    - 激光器和单模光纤的横向传输性质使其衰减较小,可传输数公里甚至数十千米而不必采用中继器,适合远距离传输。



## 无线传输介质 Wireless Transmission

### Cc

**Ab**

- 都是非导向型传输介质

### 无线电波

#### Cc

#### Ab

- 无线电波具有较强的穿透能力,可以传输很长的距离,所以它被广泛应用于通信领域,如无
  线手机通信、计算机网络中的无线局域网 CWLAN) 等。
- 因为无线电波使信号向所有方向散播, 因此有效距离范围内的接收设备无须对准某个方向,就可与无线电波发射者进行通信连接,大大简化了通信连接。这也是无线电传输的最重要优点之一。

### ---------- 视线介质 ----------

### Cc-视线介质

- 微波、红外线和激光有时统称为视线介质

### Ab-共有

- 用于高带宽的无线通信
- 它们都需要发送方和接收方之间存在一条视线 (Line-of-sight) 通路,有很强的方向性,都沿直线传播

### 微波

#### Ab

- 微波通信的频率较高,频段范围也很宽,载波频率通常为 2~40 GHz, 因而通信信道的容量
  大 。
- 与通常的无线电波不同,微波通信的信号是沿直线传播的,因此在地面的传播距离有限,超过一定距离后就要用中继站来接力。

#### Cat-地面微波接力通信

- 通过地面中继站重新将微波信号转发给下一个中继站

#### Cat-卫星通信

- 卫星通信利用地球同步卫星作为中继来转发微波信号,可以克服地面微波通信距离的限制。
- 三颗相隔 120° 的同步卫星几乎能覆盖整个地球表面,因而基本能实现全球通信。卫星通信的优点是通信容量大、距离远、覆盖广,缺点是保密性差、端到端传播时延长。
- **Ab**
  - 优点
    - 通信容量大
    - 距离远
    - 覆盖广
    - 广播通信和多址通信

  - 缺点
    - 传播时延长
    - 受气候影响大
    - 误码率较高
    - 成本较高


### 红外线

#### Ab

- 红外通信和激光通信把要传输的信号分别转换为各自的信号格式,即红外光信号和激光信号,再直接在空间中传播。

### 激光

#### Ab

- 红外通信和激光通信把要传输的信号分别转换为各自的信号格式,即红外光信号和激光信号,再直接在空间中传播。

### --------------------





# 物理层设备

## 中继器

### Cc

### Pcp

- 信号再生【并非简单的信号放大】
  - 放大器和中继器都起放大作用
  - 只不过放大器放大的是<u>模拟信号</u>,原理是将衰减的信号放大
  - 而中继器放大的是<u>数字信号</u>,原理是将衰减的信号整形再生。

### Ab

- 中继器是用来扩大网络规模的最简单廉价的互连设备
- 中继器两端的网络部分是网段,而不是子网,使用中继器连接的几个网段仍然是一个局域网，是相同的两类网段。中继器若出现故障,对相邻两个网段的工作都将产生影响。
- 如果某个网络设备具有存储转发的功能,那么可以认为它能连接两个不同的协议;如果该网络设备没有存储转发功能,那么认为它不能连接两个不同的协议。中继器没有存储转发功能,因此它不能连接两个速率不同的网段,中继器两端的网段一定要使用同一个协议。
- 从理论上讲,中继器的使用数目是无限的,网络因而也可以无限延长 。 但事实上这不可能,因为网络标准中对信号的延迟范围做了具体的规定,中继器只能在此规定范围内进行有效的工作,否则会引起网络故障。
  - “5-4-3 规则”。在采用粗同轴电缆的10BASE5 以 太网规范中,互相串联的中继器的个数不能超过 4 个,而且用 4 个中继器串联的 5 段通信介质中只有 3 段可以挂接计算机,其余两段只能用作扩展通信 范围的链路段,不能挂接计算机。

### F

- 中继器的主要功能是将信号整形并放大再转发出去,以消除信号经过一长段电缆后而产生的失真和衰减,使信号的波形和强度达到所需要的要求,进而扩大网络传输的距离



### Cat-集线器

#### Cc

- **Def:** 集线器 (Hub) 实质上是一个多端口的中继器。

#### Ab

- 集线器主要使用双绞线组建共享网络,是从服务器连接到桌面的最经济方案。
- 在交换式网络中, Hub 直接与交换机相连,将交换机端口的数据送到桌面上。使用 Hub 组网灵活,它把所有结点的通信集中在以其为中心的结点上,对结点相连的工作站进行集中管理,不让出问题的工作站影响整个网络的正常运行,并且用户的加入和退出也很自由。
- 由 Hub 组成的网络是共享式网络,但逻辑上仍是一个总线网。
- Hub 的每个端口连接的网络部分是同一个网络的不同网段
- Hub也只能在半双工状态下工作,网络的吞吐率因而受到限制。
  - 多台计算机必然会发生同时通信的情形,因此<u>集线器不能分割冲突域</u>,所有集线器的
    端口都属于同一个冲突域。集线器在一个时钟周期中只能传输一组信息,如果一台集线器连接的机器数目较多,且多台机器经常需要同时通信,那么将导致信息碰撞,使得集线器的工作效率很差，<u>带宽会平均分配到每一个主机上</u>。
  - 集线器在物理层上扩大了物理网络的覆盖范围,但无法解决冲突域(第 二层 交换机可解决)与广播域(第三层交换机可解决)的问题,而且增大了冲突的概率
  - 用集线器连接的工作站集合同属一个冲突域 , 也同属一个广播域

#### Mt

- 当 Hub 工作时,一个端口接收到数据信号后,由于信号在从端口到 Hub 的传输过程中已有衰减,所以 Hub 便将该信号进行整形放大,使之再生(恢复)到发送时的状态,紧接着转发到其他所有(除输入端口外)处于工作状态的端口。
- 如果同时有两个或多个端口输入,那么输出时会发生冲突,致使这些数据都无效。

#### F

- 从 Hub的工作方式可以看出,它在网络中只起信号放大和转发作用,目的是扩大网络的传输范围,而不具备信号的定向传送能力,即信息传输的方向是固定的、广播的,是一个标准的共享式设备。







# ---------- 数据链路层 ----------

# 数据链路层

## Cc

- 



## Ab

- 通信子网
- 数据链路层的传输单位是帧 Frame
- 数据链路层的服务访问点是 “MAC 地址(网卡地址)“



## 协议

- 典型的数据链路层协议有 SDLC 、 HDLC 、 SLIP、PPP 、 STP、Ethernet 和帧中继等



# 网络链路基础

## Cc

**结点**

- **Def:** 一般指各种数据终端
- **e.g.**
  - 主机、路由器

**链路**

- **Def:** 网络中两个结点的物理通道
- **e.g.**
  - 物理层的传输介质

**数据链路**

- **Def:** 网络中两个结点的逻辑通道
- **Ab**
  - 把实现控制数据传输协议的硬件和软件加到链路上即为数据链路





# 数据链路层功能

## 为网络层提供服务

数据链路层的主要作用是加强物理层传输原始比特流的功能,将物理层提供的可能出错的物理连接改造为逻辑上无差错的数据链路,使之对网络层表现为一条无差错的链路。

对网络层而言,数据链路层的基本任务是将源机器中来自网络层的数据传输到目标机器的网
络层。数据链路层通常可为网络层提供如下服务:

- **无确认的无连接服务**。源机器发送数据帧时不需先建立链路连接,目的机器收到数据帧
  时不需发回确认。对丢失的帧,数据链路层不负责重发而交给上层处理。
  - 适用于实时通信或误码率较低的通信信道,如以太网。
  
- **有确认的无连接服务**。源机器发送数据帧时不需先建立链路连接,但目的机器收到数据
  帧时必须发回确认。源机器在所规定的时间内未收到确定信号时,就重传丢失的帧,以提高传输的可靠性。
  - 该服务适用于误码率较高的通信信道,如无线通信。

- **有确认的面向连接服务**。帧传输过程分为三个阶段:建立数据链路、传输帧、释放数据
  链路。目的机器对收到的每一帧都要给出确认,源机器收到确认后才能发送下一帧,因而该服务的可靠性最高。
  - 该服务适用于通信要求(可靠性、实时性)较高的场合。

- 注意:有连接就一定要有确认,即不存在无确认的面向连接的服务。



## 组帧

### Cc-帧

**帧**

- **Def**
  - 两台主机之间传输信息时,必须将网络层的分组封装成帧,以帧的格式进行传送 。 将一段数据的前后分别添加首部和尾部,就构成了帧。
- **Ab**
  - 帧长 = 数据部分的长度 + 首部和尾部的长度。
  - 组帧时既要加首部,又要加尾部，而分组(即 IP 数据报)仅是包含在帧中的数据部分，所以不需要加尾部来定界。
- **F**
  - 数据链路层之所以要把比特组合成帧为单位传输,是为了在出错时只重发出错的帧,而不必重发全部数据,从而提高效率。




### Mt-组帧

#### Cc

**成帧or组帧**

- **Def:** 定义帧的开始和结束，为了使接收方能正确地接收并检查所传输的帧 , 发送方必须依据一定的规则把网络层递交的分组封装成帧(称为组帧)
- **Mt:** 由于字符计数法中计数字段的脆弱性和字符填充法实现上的复杂性与不兼容性,目前较常用的组帧方法是零比特填充法和违规编码法。

**帧定界**

- **Def:** 帧的首部和尾部中含有很多控制信息,它们的一个重要作用是确定帧的界限,即帧定界

**帧同步**

- **Def:** 帧同步指的是接收方应能从接收到的二进制比特流中区分出帧的起始与终止。
- **Ab**
  - 为了提高帧的传输效率 , 应当使帧的数据部分的长度尽可能地大于首部和尾部的长度,但每种数据链路层协议都规定了帧的数据部分的长度上限-最大传送单元 (MTU) 
- **e.g.**
  - 如在 HDLC 协议中,用标识位 F (01111110) 来标识帧的开始和结束。通信过程中,检测到帧标识位 F 即认为是帧的开始,然后一旦再次检测到帧标识位 F 即表示帧的结束。
  - HDLC 标准帧格式: 标识位 F + 地址 + 控制 + 信息 + 帧校验序列 + 标识位 F

**最大传送单元 (MTU)**

- **Def:** 为了提高帧的传输效率 , 应当使帧的数据部分的长度尽可能地大于首部和尾部的长度,但每种数据链路层协议都规定了帧的数据部分的长度上限，即最大传送单元 (MTU) 

**透明传输**

- **Def:** 
  - 如果在数据中恰好出现与帧定界符相同的比特组合(会误认为“传输结束”而丢弃后面的数据),那么就要采取有效的措施解决这个问题,即透明传输。更确切地说,透明传输就是不管所传数据是什么样的比特组合 ,都应当能在链路上传送 。

### Mt-字符计数法

**Cc**

- 字符计数法是指在帧头部使用一个计数字段来标明帧内字符数。目的结点的数据链路层收到字节计数值时,就知道后面跟随的字节数,从而可以确定帧结束的位置(计数字段提供的字 节数包含自身所占用的一个字节)。

**Ab**

- 这种方法最大的问题在于如果计数字段出错,即失去了帧边界划分的依据,那么接收方就无法判断所传输帧的结束位和下一帧的开始位 ,收发双方将失去同步,从而造成灾难性后果。

### Mt-字符填充的首尾定界符法

**Tips:** 字符填充法

**Cc**

- 字符填充法使用特定字符来定界一帧的开始与结束。

**Ab**

- 一般使用SOH(Start of Header)和EOT(End of Transmission)分别表示开始和结束的特定字符。【注意，并非就是这三个字母，下同】
- 为了使信息位中出现的特殊字符不被误判为帧的首尾定界符,可在特殊字符前面填充一个转义字符 (ESC) 来加以区分【注意,转义字符是 ASCII 码中的控制字符,是一个字符,而非 “E" "S" "C" 三个字符的组合】,以实现数据的透明传输。接收方收到转义字符后,就知道其后面紧跟的是数据信息,而不是控制信息。
- 如果转义字符 ESC 也出现在数据中,那么解决方法仍是在转义字符前插入一个转义字符。
- 当传送的帧是由ASCII码的纯文本文件时，即便不使用字符填充法也可以保证透明传输；而如果时非ASCII码的文件如图片、程序等可以使用字符填充法进行透明传输。

### Mt-零比特填充的首尾标志法

**Cc**

- 零比特填充法允许数据帧包含任意个数的比特,也允许每个字符的编码包含任意个数的比特。它使用一个特定的比特模式,即 01111110 来标志一帧的开始和结束。

**Ab**

- 为了不使信息位中出现的比特流 01111110 被误判为帧的首尾标志,发送方的数据链路层在信息位中遇到 5个连续的 “1 "时,将自动在其后插入一个 “0"; 而接收方做该过程的逆操作,即每收到 5 个连续的 “1" 时,自动删除后面紧跟的 “0", 以恢复原信息。
- 零比特填充法很容易由硬件来实现,性能优于字符填充法。

### Mt-违规编码法

**Cc**

- 在物理层进行比特编码时,通常采用违规编码法。

**Ab**

- 违规编码法不需要采用任何填充技术,便能实现数据传输的透明性,但它只适用千采用冗余编码的特殊编码环境。

**e.g.**

- 例如,曼彻斯特编码方法将数据比特 ”1 "编码成“高-低“电平对,将数据比特“0" 编码成“低-高”电平对。而“高-高“电平对和“低-低“电平对在数据比特中是违规的(即没有采用)。可以借用这些违规编码序列来定界帧的起始和终止。局域网 IEEE 802 标准就采用了这种方法。
- 



## 差错控制

### Cc-差错控制

**差错控制**

- **Def:** 由于外界噪声的干扰,原始的物理连接在传输比特流时可能发生错误。两个结点之间如果规定了数据链路层协议,那么可以检测出这些差错,然后把收到的错误信息丢弃或者纠错，这就是差错控制功能。

- 由于信道噪声等各种原因,帧在传输过程中可能会出现错误。用以使发送方确定接收方是否正确收到由其发送的数据的方法称为差错控制。

### Cc+Cat-差错类型

**位错** or **比特差错**

- **Def:** 位错指帧中某些位出现了差错，比特在传输过程中可能会产生差错, 1 可能会变成0, 0 也可能会变成 1, 这就是比特差错

**帧错**

- 帧错指帧的丢失、重复或失序等错误。
  - 在数据链路层引入定时器和编号机制,能保证每一帧最终都能有且仅有一次正确地交付给目的结点。

### Mt-检错编码

#### Cc-冗余编码

- 检错编码都采用冗余编码技术,其核心思想是在有效数据(信息位)被发送前,先按某种关
  系附加一定的冗余位,构成一个符合某一规则的码字后再发送。
- 当要发送的有效数据变化时,相应的冗余位也随之变化,使得码字遵从不变的规则。
- 接收端根据收到的码字是否仍符合原规则来判断是否出错 。 

**Cat**

- 常见的检错编码有奇偶校验码和循环冗余码。


#### Mt-奇偶校验码

**Cc**

- 奇偶校验码是奇校验码和偶校验码的统称,是一种最基本的检错码。

**Cpn**

- 它由 n - 1 位信息元和 1 位校验元组成【校验元即为冗余码】
  - 如果是奇校验码,那么在附加一个校验元后,码长为 n 的码字中 1 的个数为奇数。
  - 如果是偶校验码,那么在附加一个校验元以后,码长为 n 的码字中 1 的个数为偶数。

**F**

- 只能检测奇数位的出错情况,但并不知道哪些位错了,也不能发现偶数位的出错情况，也不能查出长度任意一个比特的错误

#### Mt-循环冗余码

**Cc**

- **循环冗余码 (Cyclic Redundancy Code, CRC)** 又称**多项式码**,任何一个由二进制数位串组成的代码都可与一个只含有 0 和 1 两个系数的多项式建立一一对应关系。
  - 一个$k$位帧可以视为从 $x^{k-1}$ 到$x^0$的$k$次多项式的系数序列【即用$1$和$0$作为多项式各幂次的$a x^k$的系数$a$，参考上一条可以互相印证】
    - **e.g.** 例如 1110011 就相当于$x^6 + x^5 + x^4 +x^1 + x^0$

  - 这个多项式的阶数为最高幂次的项的幂次，注意使用数据串表示时是从0阶开始从右到左依次递增阶数的，也就是说表示k次多项式需要使用$k+1$位的01数据串。

- **帧检验序列 (FCS)**: 给定一个 m bit 的帧或报文,发送器生成一个 r bit的序列,称为帧检验序列 (FCS)。
  - 这样所形成的帧将由 m + r 比特组成。
  - 假设一个帧有 m 位,其对应的 r 阶多项式为 G(x) ,则计算冗余码的步骤如下:
    - 加 0 。在帧的低位端加上多项式阶数个0，即 r 个 0 。
    - 模 2 除。利用模 2 除法,用 G(x)对应的数据串去除加零后计算出的数据串,得到的余数即为FCS冗余码(共 r 位,前面的 0 不可省略)。


**Mt**

- 简而言之就是对要进行传输的数据使用 生成多项式 得到FCS帧检验序列/冗余码，将数据与冗余码发送到接收端，接收端再使用同样的生成多项式检验数据是否有差错
- 发送方和接收方事先商定一个多项式 G(x) (最高位和最低位必须为 1) ,使这个带检验码的帧刚好能被预先确定的多项式 G(x)整除
- 首先准备好待传数据 m bit，将数据分为不同的组
- 通过多项式计算出FCS，每个组都加上r位的FCS构成帧然后发送
- 接收方用相同的多项式去除收到的帧,如果无余数,那么认为无差错；反之若存在余数则这个帧存在差错。

**e.g.** 

- 便于理解的例子：
  - 设生成多项式为 $f(x) = x \% 2$【取余数】，则对于数据$5$，其对应的冗余码为$1$，
  - 将二者合一的数据$5+1 = 6$发送到接收端，再进行一次生成多项式$6\%2 = 0$，
  - 若余数为0则无差错，若不为零则有差错
  
- 更加贴近实际的例子：冗余码的计算举例
  - 设 G(x) = 1101 (即 r=3) ,待传送数据 M= 101001 (即 m=6) 
  
  - 加零：即待除的数据为101001000
  
  - 模 2 除：经以G(x)为除数的模2除法运算后的结果是:商 Q= 110101,余数 FCS = 001 
  
  - 所以发送出去的数据为101001 001 (即 M +FCS) ,共有 m+r 位 。
  
    ![FCS](.\img\FCS.png)
  
  - 接收端再使用$1101$模 2 除实际收到的数据，若余数为0则无差错，若不为零则有差错

**F**

- 通过循环冗余码 (CRC) 的检错技术,数据链路层做到了对帧的无差错接收。也就是说,凡
  是接收端数据链路层接受的帧,我们都认为这些帧在传输过程中没有产生差错;而接收端丢弃的帧虽然也收到了,但最终因为有差错而被丢弃,即未被接受。
  - 但是需要注意的是CRC检测能够实现无比特差错的传输，但还不是发送即接收的可靠传输
  - CRC 校验码可以检测出所有的单比特错误
  
- 循环冗余码 (CRC) 是具有纠错功能的,只是数据链路层仅使用了它的检错功能,检测到帧出错则直接丢弃,是为了方便协议的实现,因此本节将 CRC 放在检错编码中介绍。

-  带 r 个校验位的多项式编码可以检测到所有长度小于或等于 r 的突发性错误

- CRC 校验可以使用硬件未完成

- 有一些特殊的多项式 , 因为其有很好的特性,而成了国际标准

### Mt-纠错编码

#### Cc

- 在数据通信的过程中,解决差错问题的一种方法是在每个要发送的数据块上附加足够的冗余信息,使接收方能够推导出发送方实际送出的应该是什么样的比特串

#### Mt-海明码

**Cc**

- 海明码的实现原理是在有效信息位中加入几个校验位形成海明码,并把海明码的每个二进制位分配到几个奇偶校验组中.
  - 当某一位出错后,就会引起有关的几个校验位的值发生变化,这不但可以发现错位,而且能指出错位的位置,为自动纠错提供依据。
- **海明距离/码距**：海明距离是两个码字/合法编码之间对应位不同的数量，而编码系统/编码集的海明距离是该系统中所有码字对之间的最小海明距离。
  - $n + 1$位的海明距离可以检测出$n$位的比特错误
  - $2n + 1$位的海明距离可以纠正$n$位的比特错误


**Mt**

- 确定海明码的位数

  - 设 $m$ 为有效信息的位数, $r$ 为校验位的位数,则信息位 $m$ 和校验位 $r$ 应满足
    $$
    m + r + 1 \leq 2^r
    $$
    若要检测两位错,则需再增加 1 位校验位,即 $r + 1$ 位

    以上标准是为了让校验码的位数能覆盖整个码字的二进制长度，海明码位数符合上述标准则成立，则 $m, r$ 有效。

  - 

- 确定校验位的分布

  - 规定校验位 $P_i$ 在海明位号为 $2^{i-1}$的位置上,其余各位为信息位，每一个校验位对应二进制中的一个进位。

- 分组以形成校验关系

  - 每个数据位用多个校验位进行校验,但要满足条件:被校验数据位的海明位号等于校验该数据位的各校验位海明位号之和。另外,校验位不需要再被校验。分组形成的校验关系如下

- 校验位取值

  - 校验位 $P_i$ 的值为第 $i$ 组(由该校验位校验的数据位)所有位求异或。

- 海明码的校验原理

  - 每个校验组分别利用校验位和参与形成该校验位的信息位进行奇偶校验检查,构成 $r$ 个校验方程:


**e.g.**

- 确定海明码的位数
  - 设信息位为$D_4 D_3 D_2 D_1$ (1010) ,共 4 位
  - 设校验位为$P_3 P_2 P_1$
  - 对应的海明码为$H_7 H_6 H_5 H_4 H_3 H_2 H_1$

- 

### Mt-前向纠错 FEC

**Ab**

- 在 FEC 方式中,接收端不但能发现差错,而且能确定比特串的错误位置,从而加以纠正。

### F

- 主要检查帧错和位错。
- 可以进行数据重发



## 流量控制

### Cc-流量控制

**Cc**

**流量控制**

- **场景**：
  - 在两个相邻结点之间传送数据时,由于两个结点性能的不同,可能结点 A 发送数据的速率会比结点 B 接收数据的速率快,如果不加控制,那么结点 B 就会丢弃很多来不及接收的正确数据,造成传输线路效率的下降。
  - 由于收发双方各自的工作速率和缓存空间的差异,可能出现发送方的发送能力大于接收方的接收能力的现象,如若此时不适当限制发送方的发送速率(即链路上的信息流量),前面来不及接收的帧将会被后面不断发送来的帧“淹没“,造成帧的丢失而出错。
- **Def:** 流量控制可以协调两个结点的速率,使结点 A 发送数据的速率是结点 B 可以接收的速率。因此,流量控制实际上就是限制发送方的数据流量,使其发送速率不超过接收方的接收能力。
  - 流量控制涉及对链路上的帧的发送速率的控制,以使接收方有足够的缓冲空间来接收每个帧。


**Ab**

- 数据链路层是相邻结点之间的流量控制
  - 流量控制并不是数据链路层特有的功能,许多高层协议中也提供此功能,只不过控制的对象不同而已。
  - 在 OSI 体系结构中,数据链路层具有流量控制的功能。而在 TCP/IP 体系结构中,流
    量控制功能被移到了传输层。因此,有部分教材将流量控制放在传输层进行讲解。

- 这个过程需要通过某种反馈机制使发送方能够知道接收方是否能跟上自己,即需要有一些规则使得发送方知道在什么情况下可以接着发送下一帧,而在什么情况下必须暂停发送,以等待收到某种反馈信息后继续发送。

### Pcp-停止-等待流量控制基本原理

#### Cc

- 发送方每发送一帧,都要等待接收方的应答信号,之后才能发送下一帧;接收方每接收一帧,都要反馈一个应答信号,表示可接收下一帧,如果接收方不反馈应答信号,那么发送方必须一直等待。每次只允许发送一帧,然后就陷入等待接收方确认信息的过程中,因而传输效率很低。

#### Ab

- 停止-等待协议也是一种滑动窗口协议, 在停止 -等待协议中,源站发送单个帧后必须等待确认,在目的站的回答到达源站之前,源站不能发送其他的数据帧。从滑动窗口机制的角度看,停止4等待 协议相当于发送窗口和接收窗口大小均为 1 的滑动窗口协议。

### Pcp-滑动窗口流量控制基本原理

#### Cc

- **发送窗口**: 在任意时刻,发送方都维持一组连续的允许发送的帧的序号,称为发送窗口。
  - **F:** 发送窗口用来对发送方进行流量控制,而发送窗口的大小咒代表在还未收到对方确认信息的情况下发送方最多还可以发送多少个数据帧。
- **接收窗口**: 同时接收方也维持一组连续的允许接收帧的序号,称为接收窗口。
  - **F:** 在接收端设置接收窗口是为了控制可以接收哪些数据帧和不可以接收哪些帧。
  - 接收方只有收到的数据帧的序号落入接收窗口内时,才允许将该数据帧收下。若接收到的数据帧落在接收窗口之外,则一律将其丢弃。

#### Ab

- 停止-等待协议也是一种滑动窗口协议, 在停止 -等待协议中,源站发送单个帧后必须等待确认,在目的站的回答到达源站之前,源站不能发送其他的数据帧。从滑动窗口机制的角度看,停止4等待 协议相当于发送窗口和接收窗口大小均为 1 的滑动窗口协议。
- 只有接收窗口向前滑动(同时接收方发送了确认帧)时,发送窗口才有可能(只有发送
  方收到确认帧后才一定)向前滑动。
- 从滑动窗口的概念看,停止-等待协议、后退 N 帧协议和选择重传协议只在发送窗口大小
  与接收窗口大小上有所差别:
  - 停止-等待协议:发送窗口大小= 1, 接收窗口大小= 1 。
  - 后退 N 帧协议:发送窗口大小> 1, 接收窗口大小= 1 。
  - 选择重传协议:发送窗口大小> 1, 接收窗口大小> 1 。
- 接收窗口的大小为 1 时,可保证帧的有序接收。
- 数据链路层的滑动窗口协议中,窗口的大小在传输过程中是固定的
  - 注意与第 5 章传输层的滑动窗口协议的区别。
- 滑动窗口同时在进行流量控制和可靠性传输

#### Mt

发送端每收到一个确认帧,发送窗口就向前滑动一个帧的位置,当发送窗口内没有可以发送
的帧(即窗口内的帧全部是已发送但未收到确认的帧)时,发送方就会停止发送,直到收到接收
方发送的确认帧使窗口移动,窗口内有可以发送的帧后,才开始继续发送。
接收端收到数据帧后,将窗口向前移一个位置,并发回确认帧,若收到的数据帧落在接收窗
口之外,则一律丢弃。



## 可靠性传输

### Cc

- 数据链路层的可靠传输通常使用确认和超时重传两种机制来完成。
- **确认**: 确认是一种无数据的控制帧,这种控制帧使得接收方可以让发送方知道哪些内容被正确接收。
  - 有些情况下为了提高传输效率,将确认捎带在一个回复帧 中,称为**捎带确认**。 
- **超时重传**: 超时重传是指发送方在发送某个数据帧后就开启一个计时器,在一定时间内如果没有得到发送的数据帧的确认帧,那么就重新发送该数据帧,直到发送成功为止。

### Ab

- 注意,在数据链路层中流量控制机制和可靠传输机制是交织在一起的。
- 注意:现有的实际有线网络的数据链路层很少采用可靠传输(不同于 OSI 参考模型的思路),因此大多数教材把这部分内容放在第 5 章传输层中讨论

### Mt-自动重传请求 ARQ

**Cc**

- 自动重传请求 (Automatic Repeat reQuest, ARQ) 通过接收方请求发送方重传出错的数据帧来恢复出错的帧,是通信中用于处理信道所带来差错的方法之一。
  - 为进一步提高信道的利用率,可设法只重传出现差错的数据帧或计时器超时的数据帧,但此时必须加大接收窗口,以便先收下发送序号不连续但仍处在接收窗口中的那些数据帧 。 等到所缺序号的数据帧收到后再一并送交主机。这就是选择重传 ARQ 协议 。
  - 传统自动重传请求分为三种,即停止-等待 (Stop-and-Wait)ARQ 、后退 N 帧 (Go-Back-N) ARQ 和选择性重传 (Selective Repeat )ARQ 。
    - 后两种协议是滑动窗口技术与请求重发技术的结合,由于窗口尺寸开到足够大时,帧在线路上可以连续地流动,因此又称其为连续 ARQ 协议。
  

**Mt**

- 通常采用循环冗余校验 (CRC) 方式发现位错,通过自动重传请求 (Automatic Repeat reQuest, ARQ) 方式来重传出错的帧。
  - ARQ法：让发送方将要发送的数据帧附加一定的 CRC 冗余检错码一并发送,接收方则根据检错码对数据帧进行错误检测,若发现错误则丢弃,发送方超时重传该数据帧。
  - ARQ 法只需返回很少的控制信息就可有效地确认所发数据帧是否被正确接收。

**Ab**

- 在 ARQ方式中,接收端检测到差错时,就设法通知发送端重发,直到接收到正确的码字为止。
- 已发送但不确认帧数 $\leq$ 窗口大小 - 1



## Cat-流量控制和可靠传输协议

### Cat-单帧滑动窗口与停止-等待协议

**Cc**

- 在停止-等待协议中,源站发送单个帧后必须等待确认,在目的站的回答到达源站之前,源
  站不能发送其他的数据帧。从滑动窗口机制的角度看,停止-等待 协议相当于发送窗口和接收窗口大小均为 1 的滑动窗口协议。

- 发送方每发送一帧,都要等待接收方的应答信号,之后才能发送下一帧;接收方每接收一帧,都要反馈一个应答信号,表示可接收下一帧,如果接收方不反馈应答信号,那么发送方必须一直等待。每次只允许发送一帧,然后就陷入等待接收方确认信息的过程中,因而传输效率很低。

  - 设确认帧的发送时延为$t_d$，传输时延为$RTT$，确认帧的发送时延为$t_a$

  - 信道利用率$U$为
    $$
    U = \frac{t_d}{t_d + RTT + t_a}
    $$

- 发送的帧交替地用 0 和 1 来标识,确认帧分别用 ACK0 和 ACK1 来表示,收到的确认帧有误时,重传已发送的帧。

  - 数据帧和确认帧都需要编号
  - 对于停止-等待协议,由于每发送一个数据帧就停止并等待,因此用 1 bit 来编号就已足够。

**Ab**

- 停止-等待协议:发送窗口大小= 1, 接收窗口大小= 1 。

**Ab-差错**

数据帧丢失

- 同差错控制部分

到达目的站的数据帧可能已遭破坏

- 到达目的站的帧可能已遭破坏,接收站利用前面讨论的差错检测技术检出后,简单地将该帧丢弃。
- 为了对付这种可能发生的情况,源站装备了计时器。在一个帧发送后,源站等待确认,若在计时器计满时仍未收到确认,就再次发送相同的帧。如此重复,直到该数据帧无错误地到达为止。
  - 超时计时器设置的重传时间应当比帧传输的平均RTT长一些。


数据帧正确而确认帧被破坏

- 另一种可能的差错是数据帧正确而确认帧被破坏,此时接收方已收到正确的数据帧,但发送方收不到确认帧,因此发送方会重传已被接收的数据帧,接收方收到同样的数据帧时会丢弃该帧,并重传一个该帧对应的确认帧。
- 在停止-等待协议中,若连续出现相同发送序号的数据帧,表明发送端进行了超时重传。
- 确认帧迟到：连续出现相同序号的确认帧时,表明接收端收到了重复帧。
- 此外,为了超时重发和判定重复帧的需要,发送方和接收方都须设置一个帧缓冲区。发送端在发送完数据帧时,必须在其发送缓存中保留此数据帧的副本,这样才能在出差错时进行重传。只有在收到对方发来的确认帧 ACK 时,方可清除此副本。

**F**

- 每次只允许发送一帧,然后就陷入等待接收方确认信息的过程中,因而传输效率很低。

### Cat-多帧滑动窗口与后退 N 帧协议 (GBN)

**Cc**

- 在后退 N 帧式 ARQ 中,发送方无须在收到上一个帧的 ACK 后才能开始发送下一帧,而是
  可以连续发送帧。
- 后退 N 帧协议: $1< 发送窗口大小 \leq 2^n - 1$, 接收窗口大小= 1 。
  - 发送窗口：发送方维持一组允许连续发送的帧以及对应的序号
  - 接收窗口：接收方维持一个允许连续发送的帧以及对应的序号


**Ab**

- 接收方只允许按顺序接收帧。接收端只按序接收数据帧，若其中有差错帧，即便该差错帧后续是正确的，也要丢弃了这些不按序的无差错帧,但应重复发送已发送的最后 一 个确认帧 ACK 1 (这是为了防止已发送的确认帧 ACK 1 丢失)。

- 后退 N 帧协议的接收窗口为 1, 可以保证按序接收数据帧。若采用 $n$ 位比特对帧编号,则其发送窗口的尺寸$W_T$应满足 $1 < W_T \leq 2^n - 1$ 。

  - 若发送窗口的尺寸大于 $2^n - 1$, 则会造成接收方无法分辨新帧和旧帧，因为当发送窗口尺寸大于这个界限时，若一个发送窗口$A$的帧全部丢失并重传，由于窗口之间的编号复用，会导致无法根据编号确认重新传输的帧是属于$A$本身还是下一个传输窗口。

- 由于窗口滑动，因此计算信道利用率时，发送周期为发送第一个帧并接受到确认帧所需要的时间，一般为
  $$
  发送周期 = 一个数据帧传输时延 + 一个数据帧传播时延 + 确认帧传输时延 + 确认帧传播时延
  $$
  其中若无说明或确认帧大小信息，确认帧传输时延一般忽略不计。


**Ab-差错**

- 当接收方检测出失序的信息帧后,要求发送方重发最后一个正确接收的信息帧之后的所有未被确认的帧
- 或者当发送方发送了 N 个帧后,若发现该 N 个帧的前一个帧在计时器超时后仍未返回其确认信息,则该帧被判为出错或丢失,此时发送方就不得不重传该出错帧及随后的 N 个帧。

**Mt-响应事件**

- 上层的调用
  - 上层要发送数据时，发送方先检查发送窗口是否已满，如果未满，则产生一个帧并将其发送；如果窗口已满，发送方只需将数据返回给上层，暗示上层窗口已满。上层等一会再发送。（实际实现中，发送方可以缓存这些数据，窗口不满时再发送帧）。
- 回应确认帧
  - GBN协议中，对n号帧的确认采用累积确认的方式，标明接收方已经收到n号帧和它之前的全部帧。
- 超时响应
  - 协议的名字为后退N帧/回退N帧，来源于出现丢失和时延过长帧时发送方的行为。就像在停等协议中一样，定时器将再次用于恢复数据帧或确认帧的丢失。如果出现超时，发送方重传所有已发送但未被确认的帧。

**Mt**

- 源站向目的站发送数据帧。当源站发完 0 号帧后,可以继续发送后续的 1 号帧、 2 号帧等。
- 源站每发送完一帧就要为该帧设置超时计时器。
- 由于连续发送了许多帧,所以确认帧必须要指明是对哪一帧进行确认。
- **累积确认**：为了减少开销, GBN 协议还规定接收端不一定每收到一个正确的数据帧就必须立即发回一个确认帧,而可以在连续收到好几个正确的数据帧后,才对最后一个数据帧发确认信息,或者可在自己有数据要发送时才将对以前正确收到的帧加以捎带确认。
  - 这就是说,对某一数据帧的确认就表明该数据帧和此前所有的数据帧均已正确无误地收到, 称为**累积确认**。

**F**

- 后退 N 帧协议一方面因连续发送数据帧而提高了信道的利用率
- 另一方面在重传时又必须把原来已传送正确的数据帧进行重传(仅因这些数据帧的前面有一个数据帧出了错),这种做法又使传送效率降低。
  - 由此可见,若信道的传输质量很差导致误码率较大时,后退 N 帧协议不一定优于停止-等待协议。

- 

### Cat-多帧滑动窗口与选择重传协议 (SR)

**Cc**

- 在选择重传协议中,每个发送缓冲区对应一个计时器,当计时器超时时,缓冲区的帧就会重
  传。
- 另外,该协议使用了比上述其他协议更有效的差错处理策略,即 一 旦接收方怀疑帧出错,就会发一个否定帧 NAK 给发送方,要求发送方对 NAK 中指定的帧进行重传。

**Ab**

- 选择重传协议的接收窗口尺寸 $W_R$ 和发送窗口尺寸 $W_T$ 都大于 1 ,一次可以发送或接收多个帧。
  - 在选择重传协议中,接收窗口和发送窗口的大小通常是相同的
    - 选择重传协议是对单帧进行确认,所以发送窗口大于接收窗口会导致溢出,发送窗口小于接收窗口没有意义)，但是很多时候并非相等，需要参考公式具体考量。
    - 最大值都为序号范围的一半,若采用 n 个比特对帧编号,则需要满足 $W_{Tmax} = W_{Rmax} =2^{n-1}$ 且$W_T + W_R \leq 2^n$
    - 如果不满足该条件, 即窗口大小大于序号范围一半,当一个或多个确认帧丢失时,发送方就会超时重传之前的数据帧, 但接收方无法分辨是新的数据帧还是重传的数据帧。
  - 如果窗口太小的话缓存空间太小，SR的优势不够明显，更极端一点如果真的变为窗口大小为1的情况，则SR其实与停止-等待协议差别不大。

**Mt-响应事件**

- 上层的调用
  - 从上层收到数据后，SR发送方检查下一个可用于该帧的序号，如果序号位于发送窗口内，则发送数据帧；否则就像GBN一样，要么将数据缓存，要么返回给上层之后再传输。
- 回应确认帧
  - 如果收到ACK，加入该帧序号在窗口内，则SR发送方将那个被确认的帧标记为已接收。如果该帧序号是窗口的下界（最左边第一个窗口对应的序号），则窗口向前移动到具有最小序号的未确认帧处。如果窗口移动了并且有序号在窗口内的未发送帧，则发送这些帧。
- 超时响应
  - 每帧个帧都有自己的定时器，一个超时事件发生后只重传一个。

**Mt**

- SR接收方将确认一个正确接收的帧而不管其是否按序。失序的帧将被缓存，并返回给发送方一个该帧的确认帧【收谁确认谁】，直到所有帧（即序号更小的帧）皆被收到为止，这时才可以将一批帧按序交付给上层，然后向前移动滑动窗口
- 如果收到了窗口序号外（小于窗口下界）的帧，就返回一个ACK。其他情况，就忽略该帧。

**F**

- 选择重传协议可以避免重复传送那些本已正确到达接收端的数据帧
- 但在接收端要设置具有相当容量的缓冲区来暂存那些未按序正确收到的帧。接收端不能接收窗口下界以下或窗口上界以上的序号的帧,因此所需缓冲区的数目等于窗口的大小,而不是序号数目。





## 链路管理

**Def:** 数据链路层连接的建立、维持和释放过程称为链路管理或传输管理

- 它主要用于面向连接的服务。链路两端的结点要进行通信,必须首先确认对方已处于就绪状态,并交换一些必要的信息以对帧序号初始化,然后才能建立连接,在传输过程中则要能维持连接,而在传输完毕后要释放该连接。
- 在多个站点共享同一物理信道的情况下(如在局域网中)如何在要求通信的站点间分配和管理信道也属于数据链路层管理的范畴 。

**访问控制**：控制对信道的访问，进行链路连接的建立、拆除、分离

**物理寻址**







## Others

- 任务是将网络层传来的 IP 数据报组装成帧。数据链路层的功能可以概括为<u>成帧、差错控制、流量控制和传输管理</u>等。
- 广播式网络在数据链路层还要处理新的问题,即如何控制对共享信道的访问。数据链路层的一个特殊的子层——介质访问子层,就是专门处理这个问题的。







# 数据链路层设备





# 局域网





# ----- 介质访问控制子层  -----

# 介质访问控制子层

**Cc**

**Ab**

- Technically, the MAC sublayer is the bottom part of the data link layer
  - 也就是说其实MAC更靠近物理层。

**F**

- 介质访问控制的功能是采取一定的措施使得在同一信道上的结点之间的通信不会发生互相干扰，为使用介质的每个结点隔离来自同一信道上其他结点所传送的信号,以协调活动结点的传输。
- 用来决定广播信道中信道分配的协议属于数据链路层的一个子层,称为介质访问控制 (Medium Access Control, MAC) 子层。

# 静态划分信道

## 信道划分介质访问控制



# 动态分配信道

## 随机访问介质访问控制

## 轮询访问介质访问控制





# ---------- 网络层 ----------

# 网络层

**Cc**

Ab

- 传输单位：Packet 分组/包/数据包/数据报.
  - a packet is a message at the network layer.
  - a packet is a quantity of information that is sent as a single unit from one computer to another on a network or on the Internet

# 路由算法

# IP协议

## IPv4

## IPv6

## IP 组播

## 移动 IP



# 路由协议



# 网络层设备

# 广域网









# ---------- 传输层 ----------

# 传输层

**Ab**

- 传输单位：Datagram 数据报

# UDP 协议



# TCP 协议



# ---------- 应用层 ----------

# 应用层

## Ab

- 传输单位：Message 消息

# 网络应用模型

### 客户/服务器 (Client/Server, C/S) 模型

### P2P (Peer-to-peer)模型



# 域名系统 (DNS)





# 文件传输协议 (FTP)





# 电子邮件







# 万维网 (WWW)





#  ---------- 网络安全 ----------





# ---------- Extra ----------

# FAQ

- 计算机网络与分布式计算机系统的主要区别是什么?
  - 用户对计算机网络和分布式计算机系统的认知情况不同。
  - 分布式计算机系统对整个系统中的各个计算机对用户都是透明的，用户通过输入命令就可以运行程序,但用户并不知道哪台计算机在为它运行程序。操作系统为用户选择一台最合适的计算机来运行其程序,并将运行的结果传送到合适的地方。
  - 计算机网络则与之不同,用户必须先登录欲运行程序的计算机,然后按照计算机的地址,将程序通过计算机网络传送到该计算机中运行,最后根据用户的命令将结果传送到指定的计算机中。二者的区别主要是软件的不同。







#  ---------- Web Service Introductory Notebook ----------

# Cc

- Database
  - F: Separate data collation+curation(developer) from usage(user/other developer) 
- Sequence Retrieval System

  - Def:The system to get biological sequencing data
  - e.g. NCBI 
- UFSRAT system

  - Soc: EDULISS database
- SQL(Structured Query Language): 

  - Ab: Design pattern
  - F: Access and manipulate databases of information in a standard structure
- Relational Database Management Systems(RDBMS)
  - Def: database management system allowing interrogation
- FTP
- Mozilla
- Apache web server
- Nginx web server
- Common Gateway Interface(CGI)
  - Def: The regulations for the inputs of web program
  - F: More regular parsing and inputs citeria
  - Problem: Slow and it is hard to duel with large amount of requests
  - Lk: PERL (not the Perl programming language)
  - Lk: DBI database for early website development
- Hadloop distributed computer system
- Object orientated database management system
  - Ab: scalability
  - F: extend the access for every object orientated programming languages. This programming languages expand the services of web application.





# Mt+St-General Web Service

**Inputs from User**

- request via URL

**Outputs for web browser**

- web server response to requests with the resources/web pages etc.





# Web Service

## Daemon/Server Side

**Cc**

- Server for a web site is usually be considered as a Daemon process listening for HTTP requests on TCP port 80/22.
- 



## Client Side

**Cc**

- 





# HTTP and HTTPS

## Cc

- Secure HTTP: https
  - Ab:
    - secure sockets layer
      - F
        - encrypt transaction
        - security certificate
        - Password to access the data from the website
- mime type
  - Def: Multipurpose internet mail extensions





# Cookies



**F**

- Manage state information for users
- Restore the situation for host with cookies
- Track and Store the user information/browser history for better service in a server



















