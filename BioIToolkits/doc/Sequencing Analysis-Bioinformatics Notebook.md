Pipeline

<table><tr><td  style="background: #3ADDBA !important;"><center><font style="font-size:45px; color:black !important;"><b>
    Sequencing Analysis-Bioinformatics Notebook
    </b></font></center></td></tr></table>
# References and Resouces

1. MSc Bioinformatics Next Generation Genetics
2. FastQC documentation https://www.bioinformatics.babraham.ac.uk/projects/fastqc/

# Pcp

1. This Note book record the analysis and assembly process of different sequencing data.
2. The wet experiment part should be record in the Wiznote/EX-实验/ and you can find more experiment skills there.

# Location

1. GitHub: TODO: Maybe all the material will be uploaded to the same repo in the GitHub and I will manipulate in these months.
2. Course Practice Part: /media/ninomoriaty/My Passport/2 作死日常/2 MSc/Semester 2/Next Generation Genomics/NGG-Course
3. Coding part: /home/ninomoriaty/iToolkit/BioIToolkits/Sequencing Analysis
4. Text part: Wiznote Here



# Cc/Mt-Index

Tips: You can search the key words with underline to find the techniques in this Markdown notebook

## Cc/Mt-General Assembly Workflow

1. Sequence Quality Control and Data Trimming
   1. Assess the quality of sequence data
   2. Filter and Trimming the low quality data
   3. Reassess the quality of pruned data until the quality is acceptable for downstream analysis.
2. Genome Assembly
   1. Short read assembly
      1. DBG assembler
   2. Long read assembly
      1. 
   3. Meta-genomics Assembly
      1. sdf
3. Assembly Quality Control
   1. 



## Cc/Mt-Genome Analysis

Tips: Mainly focus on downstream analysis

### Read Mapping

### Genome Annotation

#### Taxonomic Annotation

### Conservation Analysis





# Sequence Quality Control and Data Trimming

## Cc-Basic Information

**Obj2**

1. Assess the reliability for the sequencing data
2. Filter and Trimming the data if there are unreliable data to improve the quality of data
3. Balance the quality of data and make sure the downstream analysis is biological meaningful.



## Mt-Pipeline





## Mt-Quality Assessment Tools

### FastQC

#### Ref

- Website https://www.bioinformatics.babraham.ac.uk/projects/fastqc/

#### Obj1

- File format
  - fastq
  - sam
- Data Source/Platform
  - Sanger
  - illumina
  - ONT

#### F

- Provide a simple way to do quick quality control on raw sequence data to check whether data has any problems that may influence downstream analysis.

#### Mt-Installation

**Mt-Install from zip file**

- Download the zip file from the website

- Extract the folder from the zip file

- ```bash
  # Access the fastqc program
  chmod 755 ./fastqc
  # Settle the PATH for fastqc in .bashrc
  export PATH=~/[Your dir]/FastQC:$PATH
  # Try
  fastqc
  ```

**Mt-Install from the pkg store**

- ```bash
  # From apt
  sudo apt install fastqc
  # Frome conda
  conda install
  ```


#### Mt-Quality Assessment

**Obj1-Input**

- fastq
- sam/bam data.

**Obj2-Output**

- The fastqc assessment report

**Pc**

```bash
fastqc SRR9198436.fastq

# options
 -t <number> # Threads
 -outdir=[targeted_dir] # Set the targeted dir for outputs
 
 # e.g.
 fastqc -t 12 SRR9198436.fastq --outdir=../fastqcONT/SRR9198436
```



#### R/Conc-Report Interpretation

##### Per base sequence quality

- **Cc-Overview**

![Screenshot from 2021-02-01 12-18-31.png](https://i.loli.net/2021/02/01/dwbKjElhWntaO5A.png)

- **Mt-Pred Score**
  - The box plot shows the Pred Score of base quality for each reads
  - x-axis: position on reads(bp) 
  - y-axis Pred score(for each base)
  - The higher the Pred Score, the higher the accuracy (> 28 or > 30 will be considered as good quality). The distribution of reads can also be used to find possible low quality area and indicate the error profile.

##### Per sequence  quality score

- **Cc-Overview**
  - ![Screenshot from 2021-05-02 17-20-31.png](https://i.loli.net/2021/05/03/6LmctjYV83QEa7u.png)
- **Mt-Pred Score**
  - The curve shows the Pread Score distribution of each reads (The middle red lines of box plot in Per base sequence quality section.)
  - x-axis: Mean sequence quality (Pread Score)
  - y-axis: The quantity of reads achieve the quality score
  - 





### NanoPlot





## Mt-Quality Control Tools

### Cutadapt

#### Ref

- 
- Documentation: https://cutadapt.readthedocs.io/en/stable/

#### F

- 



# Sequence Alignment/Mapping





# Genome Assembly















































