PyMOL Pipeline

<table><tr><td  style="background: #3ADDBA !important;"><center><font style="font-size:45px; color:black !important;"><b>
    PyMOL Introductory Manual
    </b></font></center></td></tr></table>
# References and Resouces

1. PyMOL Website: https://pymol.org/2/





# Pcp

1. This notebook is about the bioinformatics software or packages used in Biophysics research and crystal structure research.





# Cc

- **PyMOL**

  - Def: PyMOL is the 
  - Ab
    - TODO 
  - F
    - TODO

- **Focus**: The centre point of the screen (usually)

- **Clip Plane**(截面): The plane that 'cut' the object and show on the screen

- **Object**: The items that show on the right column

  - Cat
    - object x/y : Main object with selections 
    - (object):  Selected object within main objects
    - 

- **Amino acid representation** in PyMOL:

  - zero(0): represent water molecule
  - numbers: represent specific ligand or domain.
    - Lk: You can check this demonstration on the PDB website.
  - Alphabets for amino acid: represent corresponding amino acid.

  



# Pcp

- 





#  Mt-Navigation with mouse

- **Move** the focus (Not the object)
  - `keep pressing middle button and move` 
- **Rotate** around the focus with virtual trackball
  - `keep pressing left button and  move` 
- **Zoom** according to the focus
  - `Keep pressing right button and move`
- **Move/Modify the Clip Plane**
  - `Shift+Keep pressing right button and move`
  - F: 
    - ![dd2.png](https://i.loli.net/2021/02/09/XC1d9VnbDp4aP32.png)
    - ![dd2.2.png](https://i.loli.net/2021/02/09/Uq7HguQ3BslTb8Z.png)
- 





# Mt-Create Objects

- **Get the Object with PDB number**
  - Mt: Main Tool bar > File > Get PDB ... 
  - Ab:
  - 
- **Get Objects from file**
  - (TODO)
- **Select part of object and create new object**
  - Mt(Copy): Right Click the chain > chain > fragement > copy to object > new
  - Mt(Extract): Right Click the chain > chain > fragement > extract to object > new
- 





# Mt-Selection

- **Show the amino acid sequence **

  - Mt: Main tool bar > Display > Click on the Sequence

- **Select the ligand**

  - Mt: Select the ligand according to the specific number

- **Select items in/from `object M` within `x_distance` Å around the `object X`**

  - ```python
    # General command
    select <name this selection>, br. <object M> near <x_distance> of <object X> 
    
    # e.g.
    select ligd1,  br. 1hwk near 4 of liptor
    select intTNF, br. TNF  near 3.5 of (chH or chL) # you can select multiple object to match
    ```





# Mt-Modify the object

## Action(A)

**Mt-pre-action for this section**: Click `A` button on the object row

**F-general**: Manipulate the object and basic operation to interact.

### F-Rename

**Mt**

-  GUI: Click `rename` and type the name for the object

- Command:

  ```python
  set_name <original name>, <new name>
  ```

- 



## Show(S)

**Mt-pre-action for this section**: Click `S` button on the object row 

**F-general**: Change the shape or appearance of object

### Cartoon

### Sticks

### Surface

Mt-show as: as > surface

Mt-add: surface



## Hide(H)

**Mt-pre-action for this section**: Click `H` button on the object row

**F-general**: Hide some part of object

Mt-Hide part of the object

Mt-Re-show the object or cancel the hidden parts



## Label(L)

**Mt-pre-action for this section**: Click `L` button on the object row 

**F-general**:



## Colour(C)

**Mt-pre-action for this section**: Click `C` button on the object row 

**F-general**: Colour the object with different patterns in different levels

### Colour by elements

Mt: by element > select the default colour pattern for each elements/atoms









# Mt-Show the Distances/Bonds

- Show the hydrogen bonds and short van der Waals contacts between `ligand A`and `protein B` within `x_distance` Å.

  - ```python
    # General
    dist <name this selection>, <ligand A>(<Selected objects condition>), <x_distance>
    
    # Condition
    # TODO: add the instruction of writing conditions
    
    # Selection within one object but exclude 
    dist <name this selection>, <ligand A>(<proteinB> and not <ligand A>), <x_distance>
    
    # e.g.
    dist ligd2, liptor, (1hwk and not liptor), 3.4
    dist dint, tnf, ( chH or chL), 3.4
    ```

  - ligand A is the centre of the selection

  - protein B and ligand A are both the objects that will be applied selection by default.

  - 

- Measure the distance between two atoms

  - Mt: Main tool bar > Wizard >  Measurement > Click the first atom > Click the second atom
  - 

- 





# Mt-Protein-Protein Interactions

















