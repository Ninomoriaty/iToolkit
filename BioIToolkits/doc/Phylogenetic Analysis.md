Pipeline

<table><tr><td  style="background: #3ADDBA !important;"><center><font style="font-size:45px; color:black !important;"><b>
    Phylogenetic Analysis
    </b></font></center></td></tr></table>

# 1 Part I: Theory

# Ref

- MSc QProject Quantum Phylogenetic Research中有MSc研究过程中出现的研究资料，多数算法的原始文章以及对应的软件都有在里面进行收录。
- Most toolkits can be found in the BioIToolkits > Guideline > Phylogeny/Evolution Relationship
- This Wiznote will describe the method to build and implement a phylogenetic tree in different algorithm with data from GISAID
- GitHub: BioIToolkits Phylogenetic Tree

# Pcp

- The principles and application of software will be detailed introduced in this Wiznote and the information can be found in the references of each part.
- This part should also be used as the development knowledge base for the MSc final dissertation and you should continue the informmation with further learning.



# Introduction-Phylodynamics/Phylogeny

## Cc-Intro

- **Phylodynamics**
  - the integration of phylogenetic and population dynamic models to study tree-generating processes
  - quantify population dynamic processes
  - Tree-thinking rationalizes the organization of the developmental process
- **Phylogenetics**
  - Individual level
    - A discipline concerned with the reconstruction of ancestral relationship trees from gene or genome sequence and the molecular state of individual cells can also be used to investigate the tissue formation and tumorigenesis process.
  - Molecular and Cell level
    - Tracing cell division and change through the time from a stem cell or fertilized eggs.
    - Observe the spatiotemporal history of cell devision and change.
    - how cellular phylodynamics can help us to interpret empirical data on the growth and diversification of populations of cells and to formulate and test hypotheses about developmental processes.
  - Organism level
    - Trace the complete development of an entire organism
  - Individual Development Level
    - the partial development of embryos



## F-Phylodynamics

- species tree estimation





# Graphical Models

## Cc-Graphical Models

- phytogeography
- Molecular Clock: The distance of different clusters in time scales
- Tree+Spatial Model: The locations will be ploted with the branched
- Supernetwork 
- LB 
- rtPCR
- Reassortments



H5N1 Exmample

- Hemagglutinin(HA):
- Neuraminidase(NA): 
- 





## Mt-Phylogenetic trees

Tips: Phylogenetic trees / Evolutionary trees / genealogies

### Cc-Phylogenetic Trees

- **Tree**: A graph that contains no cycles
- **Network**: A graph that may contain cycles
- **Graphical models**
  - Graphical representation:
- **Tree topology**: 



### Cpn-Phylogenetic trees

- **Nodes**: The nodes that represent the events
  - **Tips**: Nodes with only one branch attached. Features combined with nodes and branches
  - **Internal nodes**: Nodes between different nodes with linkages
- **Edges/Branches**: The edges linked between nodes representsthe cells
  - cell states can be lablled on the branches
  - 

- **Mt-Figure out the proper graphical tree model**
  - first to clarify the interpretation and communication of experimental results
  - identify correctly the appropriate null models for subsequent statistical hypothesis testing



### F-Phylogenetic trees

- Track the number and state of the progeny through the cell development time and study the mode, tempo and the concomitant changes in cell states(cell division, cell death and other processes) through the state transition diagram
  - assign cells to <u>differentiation states</u> using <u>characteristics</u>, such as cell surface markers, gene expression profiles, rates of change in gene expression, or three-dimensional genome structure
- 



### Cat-Phylogenetic trees

- Cell level / Cell lineage trees
  - Lk: Review: Phylodynamics for cell biologists
  - **Cell population tree**
    - **Def**: a population tree represents <u>the whole process</u> by which an ancestral cell yields a population of descendent cells through division and death
    - A population tree representing 13 cells. 
    - **Branch**es represent cells
      - branch lengths are proportional to time. 
    - **Node**s represent cell division, cell death(crosses), or cell survival events to the present
      - Numbers at nodes indicate which cell (1 to 13) is represented by the branch ancestral to that node.
    - This **labeling** reveals whether one daughter branch is also the parent cell (for example, righthand branch of the first
      cell division). Colors at nodes represent the state (type) of the
      corresponding cell or branch.
  - **Cell phylogeny**
    - **Def:** cell phylogeny contains information about <u>a subset of the population tree</u> but loses information about parent-offspring
      relationships at cell-division events
    - A cell phylogeny, obtained by sampling four present-day cells from the population tree.
    - **Nodes** represent cell division events or cell samples.
  - **Cell pedigree**
    - **Def**: cell pedigree displaying the parent-offspring relationships of all cells in the population tree but typically all information
      about event timings is omitted
    - **Node**: present individual cells
      - One **node** in a pedigree is equivalent to all the nodes in the population tree that share the same number. If a cell’s state is unchanged throughout its lifetime, then the node is labeled with that state
    - **Branches**: represent the parent-offspring relationships
  - **Cell state transition diagram**
    - **Def**: A state transition diagram showing <u>which changes in cell state were permitted or observed</u> in the population tree.
    - **Ab**: a network or a tree
    - **Node** in this diagram represents a possible cellular state
    - directed **branches** between nodes represent transitions between states
  - ![image-20210601151042231.png](https://i.loli.net/2021/06/01/LKG1VQeXMFa3IZ2.png)



## F-Graphical Models

- These graphicalmodels are compatible with classic developmental biology theory. 
  - Example: C. H. Waddington devised an early model describing the manner in which a stem cell relates to the fates of its daughter cells
- 





# Ab-Parameters in Phylogenetic Trees

- branch lengths / distances
- divergence times
- transition/transversion rate ratio
- 





# Mt-Bayesian Phylogenetic Inference

## Cc-Bayesian Statistics

- **Bayesian Method**: a statistical inference methodology.

  - Pcp

    - 

  - Ab

    - use probability distributions to describe the uncertainty of all unknowns

  - Posterior distribution of $\theta$: 
    $$
    f(\theta | D) = \frac{1}{z}f(\theta)f(D | \theta)
    $$
    D: observed data

    $\theta$: unknown parameter needed to be inferred

    $f(\theta)$: prior distribution

    $f(\theta | D)$: posterior distribution

    $f(D | \theta )$: likelihood, summarizes the information about θ in the data

    $z$: normalizing constant, ensures that $f(\theta | D)$ integrates to 1, $ z = \int f(\theta)f(D | \theta)$

- **Trans-model inference**: Assume that the models are known and competing and we need to infer which model with parameters is more possible in this data asssumption

  - Mt-Equation

    - $$
      f(m, \theta_m) = f(m) f(\theta_m | m)
      $$

      $m$: known models

      $\theta_m$: unknown parameters in model $m$ needed to be inferred

    - Posterior distribution of $\theta_m$
      $$
      f(m, \theta_m | D) \propto f(m, \theta_m)f(D | m, \theta_m)
      $$

- substitution models

- site models

  

  

- **Bayesian phylogenetic inference**: 

- **Fully Bayesian multispecies coalescent (MSC) methods**: 

- **Markov chain Monte Carlo(MCMC) method**: 



## Ab-Bayesian Statistics

- It makes <u>direct probabilistic statements</u> about the model or unknown parameter
- <u>Alignment accuracy</u> is important in phylogenetic analysis.
- 



## F-Bayesian Statistics

- Analyse large phylogenetic trees and complex evolutionary models and the detection of the footprint of natural selection in DNA sequences
- Phylogenetic relationship inference
- Multispecies coalescent (MSC) model
  - Xu, B. & Yang, Z. Challenges in species tree estimation under the multispecies coalescent model. Genetics 204, 1353–1368 (2016).
- Divergence time estimation 
  - dos Reis, M. et al. Uncertainty in the timing of origin of animals and the
    limits of precision in molecular timescales. Curr. Biol. 25, 2939–2950 (2015).
  - O’Reilly, J. E., dos Reis, M. & Donoghue, P. C. Dating tips for divergence time estimation. Trends Genet. 31, 637–650 (2015).
- Species tree estimation
  - Liu, L., Xi, Z., Wu, S., Davis, C. C. & Edwards, S. V. Estimating phylogenetic trees from genome-scale data. Ann. NY Acad. Sci. 1360, 36–53 (2015).
  - Szöllosi, G. J., Tannier, E., Daubin, V. & Boussau, B. The inference of gene
    trees with species trees. Syst. Biol. 64, e42–e62 (2015).
  - Yang, Z. Molecular Evolution: A Statistical Approach (Oxford Univ. Press,
    Oxford, 2014).
- 



## Mt-Markov chain Monte Carlo method

### Ref

- Nascimento, F. F., Reis, M. D. & Yang, Z. A biologist’s guide to Bayesian phylogenetic analysis. *Nat Ecol Evol* **1**, 1446–1454 (2017).
- 



### Cc-MCMC

- **Markov chain Monte Carlo(MCMC): ** a simulation technique for sampling from a probability distribution that is known up to a normalizing constant. 
  - The algorithm generates a sample from the posterior, which can be used to estimate the mean, the standard deviation of the posterior or even the whole posterior distribution
- **Substitution model**: the models describe nucleotide or amino acid substitutions





### Pcp-MCMC

- **Metropolis–Hastings algorithm**:  a <u>Markov chain Monte Carlo (**MCMC**) method</u> for obtaining a sequence of random samples from a probability distribution from which direct sampling is difficult.



### Ab-MCMC

- The sliding window (even with reflection) is a symmetrical proposal, in the sense that the probability density of proposing d* from d is equal to that of proposing d from d*. If the proposal is asymmetrical, a correction term called the Hastings ratio needs to be applied.
- Markovian property: No memory
- Markov chain: the sequence of visited parameter values form a Markov chain
- it requires the calculation of the ratio of posterior densities, but not the posterior density itself.
- the algorithm visits parameter values with a high posterior more often than those with a low posterior
- Time: it may take weeks or months to run MCMC
- Mixing efficiency: 
- Acceptance proportion: 



### Obj2-MCMC Proposal

- 



### Pc-How to set up a MCMC analysis

#### 1. Input data

**Obj1**

- a molecular sequence alignment
  - DNA and amino acid sequence alignments
- an alignment of morphological characters 
- a combination of both data types

**Ab**

- orthologs

#### 2. Choice of a substitution model

**Ref**

- Felsenstein, J. *Inferring Phylogenies*. (OUP USA, 2003).

**Pcp**

-  When in doubt, note that it is more problematic to under-specify than to
   over-specify the model in Bayesian phylogenetics
-  the substitution model is a trade-off between bias on the one hand and variance and computation expense on the other

**Common Models**

- JC69
  - assume that the nucleotide changes occur at the same rate
- GTR
  - assume that the nucleotide changes occur at the different rates
- UNREST
  - assume that the nucleotide changes occur at the different rates
- Gramma Γ model
  - describe variable rates across sites in the analysis of coding DNA or protein sequences
- HKY+ Γ
- GTR+ Γ
  - Parameters-Rich
    - describe the exchangeabilities between nucleotides
  - F
    - analyse highly similar sequences from closely related species
  - Problems
    - If there are only a few variable sites in the alignment, there will be little information about those parameters.

**Apps to select model** 

- jModelTest
- Modelgenerator
- PartitionFinder

**Problems**

- The Apps only consider the <u>fitness of the model</u> to data but <u>robustness of the analysis to model assumptions</u> are not considered.
- **Over-parameterization of models**
  - **Def**
    - the model is non-identifiable if f(D|θ1) = f(D|θ2) for certain θ1 ≠ θ2 and for all possible data D
    - A model is non-identifiable if different values of parameters make the
      same predictions about the data
  - **Problems from over-parameterization**
    - inference difficulties 
      - (such as loss of power, strong correlations between parameters, large variance in the posterior, and extreme sensitivity to the prior and model assumptions)
    - computational problems 
      - (such as poor mixing of the MCMC).
- **Under-parameterization of models**
  - **Problems from under-parameterization**
    - systematically incorrect phylogenetic trees and seriously biased estimates of branch lengths and substitution parameters, 
    - as well as an over-confident assessment of uncertainties such as spuriously high posterior probabilities for trees or clades

#### 3. Concatenate or partition the data

**Cc-Evolutionary Characteristics**

- substitution rates
- base composition
- branch lengths
- tree topology

**Pcp-Decide**

- Partition: sites in the same partition have similar evolutionary characteristics while those in different partitions have different characteristics
- different regions in chromosomes

#### 4.1 Choose and Specify the prior

- Supposed to specify a joint prior distribution for all parameters, the common practice is to ignore the correlation, and assign independent priors for the parameters
- (Should be avoided) Such independent and identically distributed (i.i.d.) prior makes a strong statement about the mean or sum of those parameters and can cause problems when there are many parameters of the same kind.
- (Robustness analysis)By evaluating the posteriors generated under different priors, the biologist can evaluate whether the posterior is robust to the prior.
- (Molecular Clock Related) In Bayesian estimation of phylogenetic trees without the assumption of a molecular clock, it is common to assign a uniform prior on the unrooted tree topologies. When phylogenetic analysis is conducted on rooted trees under the clock or relaxed clock models, rooted trees are commonly assigned a prior using models of cladogenesis such as the Yule process and the birth–death-sampling process.
- MSC model has default prior distribution as part of model
- Calibration density
- It is also necessary to specify a prior on the evolutionary rates for the different loci or partitions. gamma–Dirichlet prior





#### 5.1. Specify Iteration parameters

- Window sizes
  - influence the step length to investigate the posterior surface
  - too large: most of the proposals will fall in the tails of the posterior and be rejected. the value will not move
  - too small: takes tiny baby steps. ineffective in exploring the posterior surface
  - Obj1: The step lengths should be adjusted to achieve a near optimal acceptance proportion, at about 30–40%.
- Number of iterations and samples
  - However, currently reliable automatic stopping rules do not exist.
  - Thus, MCMC iterations from different programs are not compa
    rable.
- After initalisation, the parameters should be fine-tuning to obtain a high efficient phylogenetic MCMC algorithm
  - Most current MCMC phylogenetic programs have automatic
    fine-tuning algorithms, and this is therefore not usually a concern
    for the user.

#### 5.2. Conduct MCMC

- use two sliding windows (uniform distributions centred around the current parameter value) to update parameters d and κ.
  1. Initialization
     1. Initialize window sizes $w_d$ and $w_K$. Choose random starting
        values $(d, \kappa)$.
  2. Main Loop
     1. **Proposal to change $d$.** 
     2. **Proposal to change $\kappa$.** 
     3. **Save the states of the Markov chain**: get the current values of $d$ and $\kappa$ and then iterate the main loop section to obtain samples.
- over many iterations, and then uses the visited values of d and κ to construct a histogram to estimate the posterior distribution or to calculate the mean and standard deviation of the posterior.
  - The model with the highest posterior probability will be considered as the maximum a posteriori (MAP) model.
  - 

#### 6. Diagnosis of a MCMC run

- convergence
  - Def: The convergence rate is the rate at which a chain starting from any initial position (which may be in the tails of the posterior) moves to the high-posterior region of the parameter
    space
  - Ab
    - Convergence rate is affected by the proposals used and by the shape of the posterior in the tails69
- burn-in
  - Def: Parameter values sampled before reaching this stationary phase are usually discarded as the burn-in
- mixing efficiency
  - Def: Mixing efficiency refers to how efficiently the chain traverses the
    posterior after it has reached the stationary distribution
  - Ab: 
    - If the chain is more efficient, the estimate based on the MCMC sample will have a smaller variance, and the results will show less variation among independent runs
    - The proposal (such as the uniform sliding window versus the normal-distribution sliding window) as well as the step length for the same proposal (such as the width of the sliding window) can have a great effect on mixing efficiency
- Mt-Use trace plots to diagnose the problems from convergence and mixing efficiency
  - Def: a trace plot, in which we plot the log likelihood or sampled
    parameter values against the MCMC iteration,
  - Mt: R or Tracer in BEAST
- Problems
  - In theory, the consistency among multiple runs could
    be because all of the runs got stuck in a region of the parameter
    space, giving the false impression that convergence was reached. Thus, it is important to initiate the runs from starting points that
    are widely dispersed.
  - All of those comments apply to Bayesian phylogenetic MCMC
    algorithms, which include both within-tree moves that change
    the branch lengths and substitution parameters without changing
    the tree topology and cross-tree moves that change the tree
    topology. The cross-tree moves are typically constructed using
    tree-perturbation (branch-swapping) algorithms such as nearestneighbour
    interchange, subtree pruning and re-grafting and tree
    bisection and reconnection26,80. About a dozen MCMC phylogenetic
    programs are now available (Table 1).
- 

#### 7. Summarise the posterior MCMC sample

- Running the chain without data is a good way of checking the correctness of the software, because the mean, variance, and so on of the prior are often analytically available and can be checked against the MCMC sample.
  - Running the program without using the sequences allows one to generate the prior used by the program.
- The sample from the prior can also be compared with the sample from the posterior (which is generated by using the data) to assess how informative the data are, and whether there are serious conflicts between the prior and the data. 
  - Considerable overlap between the prior and posterior but with the posterior being much more concentrated than the prior means that the data are informative and the prior is reasonable.
    - Reversed situation possibly caused by misspecified priors.
  - modify the prior to assess the impact of the prior on the posterior
- Problems
  - however, that it is incorrect to specify the prior by trying to match
    the posterior, since the prior is supposed to reflect our knowledge
    before the analysis of the data.

### Cat-Software



## Example- Bayesian phylogenetic researches

- Prum, R. O. et al. A comprehensive phylogeny of birds (Aves) using targeted
  next-generation DNA sequencing. Nature 526, 569–573 (2015).
- Nascimento, F. F. et al. Evolution of endogenous retroviruses in the Suidae:
  evidence for different viral subpopulations in African and Eurasian host
  species. BMC Evol. Biol. 11, 139 (2011).
- Jarvis, E. D. et al. Whole-genome analyses resolve early branches in the tree
  of life of modern birds. Science 346, 1320–1331 (2014).
- Misof, B. et al. Phylogenomics resolves the timing and pattern of insect
  evolution. Science 346, 763–767 (2014).
- Raymann, K., Brochier-Armanet, C. & Gribaldo, S. The two-domain tree of
  life is linked to a new root for the Archaea. Proc. Natl Acad. Sci. USA 112,
  6670–6675 (2015).
- Foley, N. M., Springer, M. S. & Teeling, E. C. Mammal madness:
  is the mammal tree of life not yet resolved? Phil. Trans. R. Soc. B 371,
  20150140 (2016).





<table><tr><td  style="background: #3ADDBA !important;"><center><font style="font-size:45px; color:black !important;"><b>
    Develop phylogenetic pipelines and algorithms
    </b></font></center></td></tr></table>


# 2 Part II: Practice

# Workflow-Build a phylogenetic tree from existing toolkits



## BEAST2 - Bayesian Evolutionary Analysis by Sampling  Trees

### Ref+Lk

- Articles: 
  - BEAST 2.5: An advanced software platform for Bayesian evolutionary analysis
- Tutorial: https://www.beast2.org/tutorials/index.html
  - Package manuals: http://taming-the-beast.org/ 
- Official Website: https://www.beast2.org/
- Community
  - Website: https://beast.community/
  - Google Group Forum: https://groups.google.com/forum/#!forum/beast-users
- GitHub: https://github.com/CompEvol/BEAST2
- Lk-Related software family
  - BEAST 1 focus on epidemiology of infectiou disease
  - X-BEAST: (https://github.com/rbouckaert/xbeast)
    - F: make the analysis in BEAST 1 and BEAST 2 interoperable.
    - Problem: But it seems that the project is failed on GitHub...

### Cc

- **BEAST:** a package for conducting Bayesian phylogenetic inference using MCMC.
- **Rooted time trees/tree networks**: 

### Obj1-Data

-  nucleotides
-  amino acids
-  codon models
-  discrete and continuous morphological features
-  language
-  microsatellites
-  SNPs
-  User-defined discrete and biogeographical data

### F/ Mt index

Lk: Bayesian Phylogenetic Inference in this notebook

- Bayesian methods
- MCMC
- Robust development of compatible models
- Jointing modelling of evolutionary data
- determining the age and location of the origin of species and cultures, rates of mutation and migration, and rate of spread of epidemics
- (i) hierarchical multi-
  species coalescent models for species tree estimation, (ii) fossilized birth-death models for
  macroevolution and total-evidence analyses and (iii) multi-state birth-death and structured
  coalescent epidemiological models for understanding rapidly evolving infectious diseases, (iv)
  new model averaging and model comparison methods including nested sampling. 
- 
- BEAUti
  -  GUI of BEAST2
- LogAnalyser
- LogCombiner
- TreeAnnotator
- DensiTree
- MASTER
  - simulate stochastic population dynamics models
- MultiTypeTree
  - infer structured coalescent models
- RBS
  - Reversible jump across substitution models
- SNAPP
  - Multi species coalescent over SNP data
- subst-bma
  - Bayesian model averaging over site models
- BDSKY
  - birth-death skyline tree model
- 

### Mt-Install and env var setting

- Common method

  - Download the packages with JRE or install JRE with specific version

  - Uncompress the downloaded packages

    ```bash
    tar fxz BEAST_with_JRE.v2.6.4.Linux.tgz
    ```

  - export the bin folder within the beast folder after uncompresssion in the .bashrc file.

    ```bash
    export PATH=~/beast/bin:$PATH
    ```

  - 

- Conda install

  - Press this command in the terminal and install the dependencies

    ```bash
    conda install -c bioconda beast2
    ```



## BEAST-Bayesian evolutionary analysis by Sampling Trees

- Soc
  - Alexei J Drummond, Bioinformatics Institute, University of Auckland
  - Andrew Rambaut, Institute of Evolutionary Biology, University of Edinburgh
- Programming Language: Java
- 





## MEGA - Molecular Evolutionary Genetics Analysis (MEGA)

### Ref

- Articles: 
  - MEGA11: Molecular Evolutionary Genetics Analysis Version 11
  - MEGA X: Molecular Evolutionary Genetics Analysis across Computing Platforms
- Website: https://www.megasoftware.net/index.php
- 

### Mt-Install

- GUI MEGA

  - Download the [mega].deb package after the verification

  - Install with the command

    ```bash
    sudo dpkg -i megax_10.2.5-1_amd64.deb
    ```

  - However, the computing methods can not be called directly from the command line.

- MEGA-CC (better)

  - Download the [mega-cc].deb package after the verification

  - Install with the command

    ```bash
    sudo dpkg -i megax-cc_10.2.5-1_amd64.deb
    ```

  - After installation, the computing core and the MEGA-Proto will be installed automatically. (GUI interface will be installed as well)

### F/Index of Mt

- Selecting the best-fit substitution model(s)

- Estimating evolutionary distances and divergence times

- reconstructing phylogenies

- predicting ancestral sequences

- testing for selection

- diagnosing disease mutations

- Batch processing with multithreaded and incorporated multicore parallelization

  - Mt-MEGA-CC

- Discover the organismal and genome evolutionary patterns and processes

  - Comparative Sequence Analysis

- Distance-based methods

- Maximum parsimony methods

- Maximum likelihood analysis

- Bayesian Methods

- Machine Learning method

  - **Autocorrelation** of evolutionary rates

- Build **timetree** of species, genes with Rapid relaxed-clock methods:\

  - Mt-Tree Explorer

- estimate **divergence time and confident intervals** by **tip-dating analyses**:

  - node-dating
  - sequence sampling dates 
  - Mt-Node Calibrations Editor

- Estimate **neutral evolutionary probabilities** of alleles in a species

  - Mt-multispecies sequence alignment

- GUI development

  - 

  





## RAxML

### Ref

- Notes
  - RAxML - a very brief overview By Daniel Baláž

- Articles
  - RAxML version 8: a tool for phylogenetic analysis and post-analysis of large phylogenies
- Website: 
  - Index: https://cme.h-its.org/exelixis/web/software/raxml/
  - Introduction: https://cme.h-its.org/exelixis/web/software/raxml/hands_on.html
- GitHub: 
- 

### Mt-Install









# Workflow-Develop a phylogenetic algorithm













