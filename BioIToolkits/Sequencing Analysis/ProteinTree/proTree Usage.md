```bash
./proTree.py -h
./proTree.py --help
```

```bash
./proTree.py
SAMPLE
```

Remember to add " " when there are spaces in the name of protein 

```./proTree.py
./proTree.py -v
glucose-6-phosphatase
Aves
```

When d1.txt must be the absolute path

File empty line should be clean.

dir should end with /

fig svg . x11 will provided directly to the screen



```bash
./proTree.py -s ./sample_inputs/profamilies.txt ./sample_inputs/taxgroups.txt ./ ./sample_inputs/selectmode.txt ./sample_inputs/selectid.txt
```

