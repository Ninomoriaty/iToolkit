proTree history













Normal

```bash
################################## proTree ##################################

Interactive Mode(Default):

**** Validating the inputs... ****
Done

Input information: 
Protein Family: glucose-6-phosphatase
Taxonomy Group: Aves
Selection Mode: None
Selection Target: None
Project Location: ./
Output Directory: ./glucose-6-phosphatase_Aves

################################## Data Collection and Selection ##################################

**** Collecting the data by esearch and efetch... ****
Done


Basic Information of collected sequences:
Sequences Quantity: 500
Species Quantity: 90

The number of species is more than 10. Thus the names of these species will not be shown on stdout.


**** Summarise the protein properties by pepstats method... ****

Do you want to calculate statistics of protein sequecnes' properties by pepstats method? 
Please enter YES or NO to decide whether you want to use pepstats method: 
YES
Calculate statistics of protein properties
Done. The output will be stored in the proSeqProperties.txt in the sum_data directory. 


**** Select sequences by protein sequence id or species names... ****

Do you want to select target protein in specific species to conduct further analysis? 
Please enter YES or NO to decide whether you want to select species: 
yes

If you want to use protein sequence IDs to select protein sequences, please enter ID
If you want to use species name to select protein sequences, please enter SP. 
If you want to exclude these protein IDs or species names, you can add EXCLUDE before ID or SP symbol. 
If you want to skip this selection process, you can enter SKIP to skip it. 
sp

Please enter the species names that you want to select for further analysis and separate them with \n . 
If you have prepare the txt file containing protein id or species names separated by lines, you can also provide the pathway to the file: 
/home/ninomoriaty/Assignment/BPSM/Assignment2/sample_inputs/id1.txt
Done


Basic Information of chosen sequences:
Sequences Quantity: 14
Species Quantity: 3

Species:
Melopsittacus undulatus
Falco rusticolus
Turdus rufiventris

################################## MSA and Conservation ##################################

**** Processing multiple sequence alignment... ****
Done


**** Using infoalign to summarise the protein sequences... ****

Do you want to get the summary of the protein sequences by infoalign? 
Please enter YES or NO to decide whether you want to use infoalign method: 
YES
Display basic information about a multiple sequence alignment
Done


**** Using figtree to plot the guide tree... ****

Do you want to plot the guide tree by figtree? 
Please enter YES or NO to decide whether you want to use figtree method: 
YES

                 FigTree v1.4.4, 2006-2018
                  Tree Figure Drawing Tool
                       Andrew Rambaut

             Institute of Evolutionary Biology
                  University of Edinburgh
                     a.rambaut@ed.ac.uk

                 http://tree.bio.ed.ac.uk/
    Uses the Java Evolutionary Biology 2 Library (JEBL2)
                http://jebl2.googlecode.com/
                 Uses the iText PDF Library
                    http://itextpdf.com/
               Uses the Apache Batik Library
            http://xmlgraphics.apache.org/batik/
                 Uses the JDOM XML Library
                    http://www.jdom.org/
 Thanks to Alexei Drummond, Joseph Heled, Philippe Lemey, 
Tulio de Oliveira, Oliver Pybus, Beth Shapiro & Marc Suchard

Creating PDF graphic: ./figures/guide_tree.pdf
Done


**** Check if quantity of the sequences is too large for plotcon... ****

**** Using plotcon to visualise the conversation level of protein sequences... ****
Plot conservation of a sequence alignment
Window size [4]: 
Graph type [png]: 
Created ./figures/similarity.1.png
Do you want to see the visualisation result from plotcon directly? 
Please enter YES or NO to decide whether you need to inspect the plotcon outputs: 
NO
Done


################################## Motifs and Special Sites ##################################

**** Searching the motifs from the PROSITE database... ****

Do you want to show the motifs information in stdout? 
Please enter YES or NO to choose whether to show the motifs.
YES

Motifs: 


** XP_037254777.1 **
Scan a protein sequence with motifs from the PROSITE database


** XP_037254776.1 **
Scan a protein sequence with motifs from the PROSITE database
Motif = AMIDATION

** XP_037254775.1 **
Scan a protein sequence with motifs from the PROSITE database


** XP_037266979.1 **
Scan a protein sequence with motifs from the PROSITE database
Motif = AMIDATION

** XP_037266894.1 **
Scan a protein sequence with motifs from the PROSITE database


** XP_037266893.1 **
Scan a protein sequence with motifs from the PROSITE database


** XP_037266891.1 **
Scan a protein sequence with motifs from the PROSITE database


** KAF4804467.1 **
Scan a protein sequence with motifs from the PROSITE database


** KAF4787459.1 **
Scan a protein sequence with motifs from the PROSITE database
Motif = AMIDATION

** XP_030908082.1 **
Scan a protein sequence with motifs from the PROSITE database


** XP_030908081.1 **
Scan a protein sequence with motifs from the PROSITE database


** XP_030908080.1 **
Scan a protein sequence with motifs from the PROSITE database


** XP_005139910.2 **
Scan a protein sequence with motifs from the PROSITE database
Motif = AMIDATION

** XP_005152833.1 **
Scan a protein sequence with motifs from the PROSITE database

Done. The output results will be stored in ./motifResults/motifsReport


**** Find potentially antigenic regions of a protein sequence... ****

Do you want to find potentially antigenic regions of a protein sequence by antigenic? 
Please enter YES or NO to decide whether you need to use antigenic program: 
YES
Find antigenic sites in proteins
Done. The output will been stored in ./motifResults2/antigenic_sites.antigenic


**** Find helix-turn-helix nucleic acid binding motifs... ****

Do you want to find find helix-turn-helix nucleic acid binding motifs by helixturnhelix? 
Please enter YES or NO to decide whether you need to use helixturnhelix program: 
YES
Identify nucleic acid-binding motifs in protein sequences
Warning: No hits in output report file './motifResults2/n2p.hth'
Done.  The output will been stored in ./motifResults2/n2p.hth


Finish: glucose-6-phosphatase and Aves

```

Advanced

```bash
################################## proTree ##################################

Silent Mode:

**** Validating the inputs... ****
Done

################################## Total Process: 1/2 ##################################


Input information: 
Protein Family: glucose-6-phosphatase
Taxonomy Group: Aves
Selection Mode: exclude sp
Selection Target: /home/ninomoriaty/Assignment/BPSM/Assignment2/sample_inputs/id1.txt
Project Location: ./
Output Directory: ./glucose-6-phosphatase_Aves

################################## Data Collection and Selection ##################################

**** Collecting the data by esearch and efetch... ****
Done


Basic Information of collected sequences:
Sequences Quantity: 500
Species Quantity: 90

The number of species is more than 10. Thus the names of these species will not be shown on stdout.


**** Summarise the protein properties by pepstats method... ****
Calculate statistics of protein properties
Done. The output will be stored in the proSeqProperties.txt in the sum_data directory. 


**** Select sequences by protein sequence id or species names... ****

Select sequences according to /home/ninomoriaty/Assignment/BPSM/Assignment2/sample_inputs/id1.txt...
Done


Basic Information of chosen sequences:
Sequences Quantity: 486
Species Quantity: 87

The number of species is more than 10. Thus the names of these species will not be shown on stdout.


################################## MSA and Conservation ##################################

**** Processing multiple sequence alignment... ****
Done


**** Using infoalign to summarise the protein sequences... ****
Display basic information about a multiple sequence alignment
Done


**** Using figtree to plot the guide tree... ****

                 FigTree v1.4.4, 2006-2018
                  Tree Figure Drawing Tool
                       Andrew Rambaut

             Institute of Evolutionary Biology
                  University of Edinburgh
                     a.rambaut@ed.ac.uk

                 http://tree.bio.ed.ac.uk/
    Uses the Java Evolutionary Biology 2 Library (JEBL2)
                http://jebl2.googlecode.com/
                 Uses the iText PDF Library
                    http://itextpdf.com/
               Uses the Apache Batik Library
            http://xmlgraphics.apache.org/batik/
                 Uses the JDOM XML Library
                    http://www.jdom.org/
 Thanks to Alexei Drummond, Joseph Heled, Philippe Lemey, 
Tulio de Oliveira, Oliver Pybus, Beth Shapiro & Marc Suchard

Creating PDF graphic: ./figures/guide_tree.pdf
Done


**** Check if quantity of the sequences is too large for plotcon... ****

**** Select the 250 most similar sequences as the input of plotcon... ****
Done


**** Using plotcon to visualise the conversation level of protein sequences... ****
Plot conservation of a sequence alignment
Created ./figures/similarity.svg
Done


################################## Motifs and Special Sites ##################################

**** Searching the motifs from the PROSITE database... ****
Done. The output results will be stored in ./motifResults/motifsReport


**** Find potentially antigenic regions of a protein sequence... ****
Find antigenic sites in proteins
Done. The output will been stored in ./motifResults2/antigenic_sites.antigenic


**** Find helix-turn-helix nucleic acid binding motifs... ****
Identify nucleic acid-binding motifs in protein sequences
Done.  The output will been stored in ./motifResults2/n2p.hth


Finish: glucose-6-phosphatase and Aves

################################## Total Process: 2/2 ##################################


Input information: 
Protein Family: ABC transporter
Taxonomy Group: Mus musculus
Selection Mode: id
Selection Target: /home/ninomoriaty/Assignment/BPSM/Assignment2/sample_inputs/id2.txt
Project Location: ./
Output Directory: ./ABC-transporter_Mus-musculus

################################## Data Collection and Selection ##################################

**** Collecting the data by esearch and efetch... ****
Done


Basic Information of collected sequences:
Sequences Quantity: 637
Species Quantity: 3

Species: 

Mus musculus
Mus musculus domesticus
Mus musculus castaneus

**** Summarise the protein properties by pepstats method... ****
Calculate statistics of protein properties
Done. The output will be stored in the proSeqProperties.txt in the sum_data directory. 


**** Select sequences by protein sequence id or species names... ****

Select sequences according to /home/ninomoriaty/Assignment/BPSM/Assignment2/sample_inputs/id2.txt...
Done


Basic Information of chosen sequences:
Sequences Quantity: 3
Species Quantity: 3

Species:
Mus musculus
Mus musculus domesticus
Mus musculus castaneus

################################## MSA and Conservation ##################################

**** Processing multiple sequence alignment... ****
Done


**** Using infoalign to summarise the protein sequences... ****
Display basic information about a multiple sequence alignment
Done


**** Using figtree to plot the guide tree... ****

                 FigTree v1.4.4, 2006-2018
                  Tree Figure Drawing Tool
                       Andrew Rambaut

             Institute of Evolutionary Biology
                  University of Edinburgh
                     a.rambaut@ed.ac.uk

                 http://tree.bio.ed.ac.uk/
    Uses the Java Evolutionary Biology 2 Library (JEBL2)
                http://jebl2.googlecode.com/
                 Uses the iText PDF Library
                    http://itextpdf.com/
               Uses the Apache Batik Library
            http://xmlgraphics.apache.org/batik/
                 Uses the JDOM XML Library
                    http://www.jdom.org/
 Thanks to Alexei Drummond, Joseph Heled, Philippe Lemey, 
Tulio de Oliveira, Oliver Pybus, Beth Shapiro & Marc Suchard

Creating PDF graphic: ./figures/guide_tree.pdf
Done


**** Check if quantity of the sequences is too large for plotcon... ****

**** Select the 250 most similar sequences as the input of plotcon... ****
Done


**** Using plotcon to visualise the conversation level of protein sequences... ****
Plot conservation of a sequence alignment
Created ./figures/similarity.svg
Done


################################## Motifs and Special Sites ##################################

**** Searching the motifs from the PROSITE database... ****
Done. The output results will be stored in ./motifResults/motifsReport


**** Find potentially antigenic regions of a protein sequence... ****
Find antigenic sites in proteins
Done. The output will been stored in ./motifResults2/antigenic_sites.antigenic


**** Find helix-turn-helix nucleic acid binding motifs... ****
Identify nucleic acid-binding motifs in protein sequences
Warning: No hits in output report file './motifResults2/n2p.hth'
Done.  The output will been stored in ./motifResults2/n2p.hth


Finish: ABC transporter and Mus musculus

```



















