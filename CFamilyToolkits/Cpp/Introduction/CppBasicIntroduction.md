Guideline level

<table><tr><td  style="background: smokewhite !important;"><center><font style="font-size:45px; color:lightblue!important;">C++ Programming Language</b></font></center></td></tr></table>

# Ref

**Courses**

- [ ] 中国大学 MOOC C++语言程序设计系列
  - C++语言程序设计基础: https://www.xuetangx.com/course/THU08091000247/10322314?channel=i.area.learn_title
  - C++语言程序设计进阶: https://www.xuetangx.com/course/THU08091000248/10318294?cha

**Textbooks**

- [ ] C++ Primer Plus 6th
  - 

- [ ] C++ Primer 5th
  - 
- [ ] 


**Documents**

- Microsoft C and Cpp documents: https://docs.microsoft.com/en-us/cpp/?view=msvc-170





# C++

## Cc

**Low-level language**

- **Def**
  - Low-level language—that is, it works directly with the hardware (for instance, accessing CPU registers and memory locations directly).
- **Ab**
  - Thus, assembly language is specific to a particular computer processor.

**High-level language**

- **Def**
  - A high-level language is oriented toward problem solving instead of toward specific hardware. 
- **Ab**
  - Special programs called compilers translate a high-level language to the internal language of a particular computer.

**Data**

- The data constitutes the information a program uses and processes.

**Algorithms**

- The algorithms are the methods the program uses

**Library**

- Libraries are collections of programming modules that you can call up from a program.



## Ab

- case sensitive
- C++ is a superset of C, meaning that any valid C program is a valid C++ program, too.



## Pcp-Paradigm

C++ joins three separate programming categories/paradigms: 

- the procedural language, represented by C; 
- the object-oriented language, represented by the <u>class</u> enhancements C++ adds to C;
- generic programming, supported by C++ <u>templates</u>.

### Procedural language

**Lk:** Note CProgrammingLanguage.md

**Def** 

- Procedural language consists of figuring out the actions a computer should take and then using the programming language to implement those actions.

- That means it emphasizes the algorithm side of programming.


**Problem**

- tangled routing or spaghetti programming

**Cat**

- Fortran、BASIC、C are procedural languages

### Object-oriented programming

**Cc+F**

- Object-oriented programming is a philosophy that attempts to mold the language to fit a problem instead of molding the problem to fit the language.
- OOP attempts to fit the language to the problem. The idea is to design data forms that correspond to the essential features of a problem.
- The OOP aspect of C++ gives the language the ability to relate to concepts involved in the problem, and the C part of C++ gives the language the ability to get close to the hardware
- 

**Obj1**

- **Class**
  - a class is a specification describing such a new data form.
  - In general, a class defines what <u>data</u> is used to represent an object and the <u>operations</u> that can be performed on that data.

- **Object**
  - an object is a particular data structure constructed according to the specification described by class.


**Mt**

- The OOP approach to program design is to first <u>design classes</u> that accurately represent those things with which the program deals

**Ab**

- Reusable Class

  - class libraries

- Abstract

- Polymorphism

  - Polymorphism lets you create multiple definitions for operators and functions, with the programming context determining which definition is used.

- Information hiding

  - Information hiding safeguards data from improper access.

- Inheritance

  - Inheritance lets you derive new classes from old ones.

- C++ use **bottom-up approach/bottom-up programming**

  - The process of going from a lower level of organization, such as classes, to a higher level, such as program design, is called bottom-up programming.

  - However， C is top-down design.

  - **Top-down design**

    - the idea is to break a large program into smaller, more manageable tasks. If one of these tasks is still too broad, you divide it into yet smaller tasks. You continue with this process until the program is compartmentalized into small, easily programmed modules. 

    **Bottom-up programming**

    - The process of going from a lower level of organization, such as classes, to a higher level, such as program design, is called bottom-up programming.

**F**

- OOP is a tool for managing large projects. 

### Generic programming

**Cc+F**

- In programming, the term generic programming indicates code that is not specific to a particular type but which, once a type is specified, can be translated into code for that type. 
- generic programming emphasizes independence from a particular data type. The term generic refers to code that is type independent.
- Generic programming involves extending the language so that you can write a function for a generic (that is, an unspecified) type once and use it for a variety of actual types.

**Obj1**

- C++ Template

**Ab**

- Reusable
- Abstract

**F**

- Generic programming provides tools for performing common tasks



## Portability and C++ Standards

### Portability

**Cc**

- If you can recompile a program between different platforms without making changes and it runs without a hitch, we say the program is portable.

**Problem-Obstacles to portability**

- hardware
  - You can minimize portability problems by localizing the hardware-dependent parts in function modules; then you just have to rewrite those specific modules.
- language divergence
  - **Lk:** C++ Standards
  - Different computer languages can develop dialects.
  - Although most implementers would like to make their versions of C++ compatible with others, it's difficult to do so without a published standard describing exactly how the language works.
  - Thus, C++ standards are created.
- 为了解决这些问题，所以才需要统一的C++标准




### C++ Standards

- the American National Standards Institute (ANSI) The International Organization for Standardization (ISO) **ANSI/ISO**

  - ANSI X3J16

  - ISO-WG-21

- Adopted by ISO, the International Electrotechnical Commission (IEC), and ANSI.

  - C++98 (ISO/IEC 14882:1998)
    - First edition
    - exceptions, run-time type identification (RTTI), templates, and the Standard Template Library (STL).
  - C++ 03 (ISO/IEC 14882:2003)
    - technical revision, meaning that it tidies up the first edition C++98, but doesn't change the language features.
    - C++98/C++03
  - C++ 11 (ISO/IEC 14882:2011)
    - C++0x
      - the original expectation that x would be 7 or 8, but standards work is a slow, exhaustive, and exhausting process



## St-C++ Program

### Mt-The Mechanics of Creating a Program

#### Creating the Source Code File

**Lk:** This notebook > 下面的Cpn中的Source code files

- Use a text editor of some sort to write the program and save it in a file.
  - This file constitutes **the source code** for your program
- Text editor
  - **integrated development environments (IDEs)** that let you manage all steps of program development, including editing, from one master program
  - **Command line editor**: just handle the compilation and linking stages and expect you to type commands on the system command line.

#### Compilation and Linking

**Lk:** This notebook > C++ Compiling 和 下面的Cpn中的object code files 和 Executable files

**Cc**

- Compile the source code.This means running a program that translates the source code to the internal language, called **machine language**, used by the host computer. 
  - The file containing the translated program is **the object code** for your program.
  - 
- Link the object code with additional code. 
  - For example, C++ programs normally use **libraries**. 
  - A C++ library contains object code for a collection of computer routines, called **functions**, to perform tasks such as displaying information onscreen or calculating the square root of a number. 
  - Linking combines your object code with object code for the functions you use and with some standard startup code to produce a runtime version of your program. 
  - The file containing this final product is called **sthe executable code**.

### St

![Figure 1.3 Programming steps](./img/Figure 1.3 Programming steps.png)

### Cpn

**Tips:** Similar to C Program

**Source code files** 

- **Def**

  - The text file containing the program written in the c language

- **Ab** 

  - **Filename**

    - C++ source code files end with .cpp as their extension
    - **Basename:** The part of the name before the period(".")
    - **Extension:** The part of the name after the period("."), suffix
      - [base name for file].[file name extension]
      - use the proper **suffix** to identify the file as a C++ file.
      - The suffix consists of a period followed by a character or group of characters called the **extension** .
  
  - **Extension:** .cpp
  
    | C++ Implementation    | Source Code Extensions |
    | --------------------- | ---------------------- |
    | Unix                  | C, cc, cxx, c          |
    | GNU C++               | C, cc, cxx, cpp, c++   |
    | Digital Mars          | cpp, cxx               |
    | Borland C++           | cpp                    |
    | Watcom                | cpp                    |
    | Microsoft Visual C++  | cpp, cxx, cc           |
    | Freestyle CodeWarrior | cpp, cp, cc, cxx, c++  |
  
    > - Note that Unix is case sensitive, meaning you should use an uppercase C character. Actually, a lowercase c extension also works, but standard C uses that extension.

**Object code file, or object file** 

- **Def**
  - The intermediate files transformed from the source code files by a c language compiler 
- **Soc+Obj1**
  - Compiler
- **Ab** 
  - The object file contains machine language code, it is not ready to run. The object file contains the translation of your source code, but it is not yet a complete program.
  - **Extension:** .o

**Executable files**

- **Def**
  - The machine executable object code files with startup code and library routines.
- **Soc+Obj1**
  - Linker or system linker
- **Ab**
  - **Extension:** 
    - .out
    - .exe (Windows)

- **Cpn**
  - **Startup code**
    - **Def:** The code that acts as an interface between your program and the operating system
    - **Ab:** System dependence
  - **Library code**
    - **Def:**  Make use of library routines (called functions) that are part of the standard C library, which contains object code for many functions
    - **Ab:** The linker extracts only the code needed for the functions you
      use from the library
  - **Object code**
    - **Lk:** Object code file

- - 
  





# C++ Preprocessor

## Cc-C++ Preprocessor

**Preprocessing and Preprocessor:** 

- **Def-Preprocessing:** In general, C++ compilers perform some preparatory work on source code before compiling
- **t:** The preprocessor looks at your program before it is compiled.
- **Pcp**
  - The preprocessor looks for independent tokens (separate words) and skips embedded words
    - e.g. That is, the preprocessor doesn't replace PINT_MAXIM with P32767IM even if there is a symbolic constant called INT_MAX.

- **F**
  - Following your preprocessor directives, the preprocessor replaces the symbolic abbreviations in your program with the directions they represent. 
  - The preprocessor can include other files at your request, and it can select which code the compiler sees.
  - the C preprocessor should perform, establishes which functions form the standard C library, and details how these functions work
  - The preprocessor provides several directives that help the programmer produce code that can be moved from one system to another by changing the values of some #define macros. 


**Tokens**

- **Def:** tokens are groups separated from each other by spaces, tabs, or carriage return, which collectively are termed white space
  - C preprocessor tokens are the separate “words” in the body of a macro definition
  - The indivisible elements in a line of code are called tokens
- **Pcp**
  - Some single characters, such as parentheses and commas, are tokens that need not be set off by white space.



## Cc-C++ preprocessor directive

## #include directive  `#include` 

```cpp
#include <iostream>
```

- **Def:** #include statement: a cut-and-paste operation sharing information that is common to many programs.
- **Mt**
- **Cpn**
- **F**
  - This directive causes the preprocessor to add the contents of the header file to your program
- 



## using directive `using`

```cpp
// import namespace
using namespace std;  // This using directive makes all the names in the std namespace available.

// import name
using std::cout;  // make cout available
```

**Cc**

**Ab**

- The Scope within the braces
  - if you place the using directives within the function, it will only be activated in the function scope rather than the file scope, even in main() function.

**F**

-  using namespace std means you can use names defined in the std namespace without using the std:: prefix.
- Unless you should **Lk:** Specify a function within a namespace



## #define directive  `#define` for Manifest Constants / Macro

```c
// object-like macros
#define SC 100
#define TWO 2  /* you can use comments if you like */
#define OW "Consistency is the last refuge of the unimagina\
tive. - Oscar Wilde" /* a backslash continues a definition */
                    /* to the next line */
#define FOUR TWO*TWO
#define PX printf("X is %d.\n",x);
#define FMT "X is %d.\n"

//function-like macros
#define MEAN(X,Y) (((X)+(Y))/2)
MEAN(x+2, y-2);  // (((X+2)+(Y-2))/2)
// Pecular problems
#define SQUARE(X) X * X  
SQUARE(x+2);  // x + 2 * x + 2
SQUARE(++x); // ++x * ++x(increment again)

```

**Cc**

- **Lk:** This notebook > `#define` Manifest Constants
- **Def:** The #define preprocessor directive, like all preprocessor directives, begins with the # symbol at the beginning of a line.
- **St**
  - Each #define line (logical line, that is) has three parts. 
  - The first part is the #define **directive** itself. 
  - The second part is your chosen abbreviation, known as a **macro**/macro name/macro identifier. 
    - The **macro name** must have no spaces in it and it must conform
      to the same naming rules that C variables follow: Only letters, digits, and the underscore (_) character can be used, and the first character cannot be a digit.
      - Remember that there are no spaces in the macro name, but that spaces can appear in the replacement string. ANSI C permits spaces in the argument list.
      - Use capital letters for macro function names. This convention is not as widespread as that of using capitals for macro constants. However, one good reason for using capitals is to remind yourself to be alert to possible macro side effects.
    - Some macros, like these examples, represent values; they are called **object-like macros**
    - **function-like macros**: A macro <u>with macro arguments</u> looks very similar to a function because the arguments are enclosed within parentheses
      - Function-like macro definitions have one or more arguments in parentheses, and these arguments then appear in the replacement portion.
      - A function call passes the value of the argument to the function while the program is running. A macro call passes the argument token to the program before compilation without calculation; it's a different process at a different time. 
      - The lesson here is to use as many parentheses as necessary to ensure that operations and associations are done in the right order.
      - Note that the X in the quoted string is treated as ordinary text, not as a token that can be replaced.

  - The third part (the remainder of the line) is termed **the replacement list or body** (see Figure 16.1). 
    - Technically, the body of a macro is considered to be a string of tokens rather than a string of characters
    - 

**Pcp + Ab**

- without semicolon`;`
- **Name Conventions**
  - It is a sensible C tradition to **capitalizing constants in uppercase.**
  - Naming conventions include prefixing a name with a c_ or k_ to indicate a constant, producing names such as c_level or k_line.
  - You can use uppercase and lowercase letters, digits, and the underscore character. The first character cannot be a digit. 
- **Compile-time substitution/macro expansion.** The constant name will be directly substituted by the value during the Preprocessing process before compiling. By the time you run the program, all the substitutions have already been made. 
  - When the preprocessor finds an example of one of your macros within your program, it almost always replaces it with the body literally. 
  - The one exception to replacement is a macro found within double quotation marks. It remains the original string rather than replaces the macro name within the double quotation.
- Here you see that a macro can express any string, even a whole C expression. Note, though, that this is a constant string; PX will print only a variable named x.
- a macro definition can include other macros
- Calculations and expressions in replacement lists are permitted. 
  - However, the actual calculation takes place not while preprocessor works, but during compilation, because the C compiler evaluates all constant expressions (expressions with just constants) at compile time.
  - The preprocessor does no calculation; it just makes the suggested substitutions very literally.
- **Lk:** This notebook > Cc-Preprocessor > Definition

**F**

- When should you use symbolic constants? You should use them for most numeric constants.
  - The name of symbolic constants can contain more information about the value than a number does.
  - Accessible and changeable
- Mnemonic value, easy alter ability, portability—these features all make symbolic constants worthwhile.
- Compared with const keyword, #define symbolic constants make sure that the result must be a constant expression while const constants are not required to be valid.
  - This is one respect in which C++ differs from C; in C++ you can use const values as part of constant expressions.

**Pcp-Choice between functions and macros**

- Macros are somewhat trickier to use than regular functions because they can have odd side effects if you are unwary.
- The macro-versus-function choice represents a trade-off between time and space. 
  - A macro produces inline code; that is, you get a statement in your program. If you use the macro 20 times, you get 20 lines of code inserted into your program. 
  - If you use a function 20 times, you have just one copy of the function statements in your program, so less space is used.
  - On the other hand, program control must shift to where the function is and then return to the calling program, and this takes longer than inline code.
- Macros have an advantage in that they don’t worry about variable types. (This is because they deal with character strings, not with actual values.) Therefore, the SQUARE(x) macro can be used equally well with int or float.
- If you intend to use a macro instead of a function primarily to speed up a program, first try to determine whether it is likely to make a significant difference.
  - A macro that is used once in a program probably won't make any noticeable improvement in running time.
  - A macro inside a nested loop is a much better candidate for speed improvements.
  - Many systems offer program profilers to help you pin down where a program spends the most time.
- 



# C++ Compiling

## C++-to-C compiler program

- This program, called `cfront` (for C front end), translated C++ source code to C source code, which could then be compiled by a standard C compiler.



## C++ Compiler

### Cc-Compiler

- Special programs called compilers translate a high-level language to the internal language of a particular computer.
- C++ compilers that generate object code directly from C++ source code.
- This direct approach speeds up the compilation process and emphasizes that C++ is a separate, if similar, language.

### Pcp

- The mechanics of compiling depend on the implementation, and the following sections outline a few common forms.
- These sections outline the basic steps, but they are no substitute for consulting the documentation for your system.
- Be aware of the fact that a particular compiler accepts a program doesn’t necessarily mean that the program is valid C++.And the fact that a particular compiler rejects a program doesn't necessarily mean that the program is invalid C++.

### GNU Compiler Collection

**Cc**

- GCC:GNU Compiler Collection(GUN 编译器集合)，编译C、C++、JAV、Fortran、Pascal、Object-C等语言。
- gcc是调用GCC中的GUN C Compiler（C 编译器）
- g++是调用GCC中的GUN C++ Compiler（C++编译器）

**Ab**

- The GNU compiler is available for many platforms, including the command-line mode for Windows-based PCs as well as for Unix systems on a variety of platforms.
- The mechanics of compiling depend on the implementation.

**Ab-Differences between gcc and g++**

- 对于 `*.c`和`*.cpp`文件，gcc分别当做c和cpp文件编译（c和cpp的语法强度是不一样的）
- 对于 `*.c`和`*.cpp`文件，g++则统一当做cpp文件编译
- 使用g++编译文件时，**g++会自动链接标准库STL，而gcc不会自动链接STL**
- gcc在编译C文件时，可使用的预定义宏是比较少的
- gcc在编译cpp文件时/g++在编译c文件和cpp文件时（这时候gcc和g++调用的都是cpp文件的编译器），会加入一些额外的宏。
- 在用gcc编译c++文件时，为了能够使用STL，需要加参数 –lstdc++ ，但这并不代表 gcc –lstdc++ 和 g++等价，它们的区别不仅仅是这个。

**Mt-Compiling and Linking**

**Lk:** 大体与 Unix CC 的流程类似，详细说明直接参考即可

```shell
# Compile
g++ spiffy.cpp

# Multiple files
g++ my.cpp precious.cpp

# Recompile multiple files
# subsequently modify just one of the source code files
g++ my.cpp precious.o

# identify/link some libraries explicitly
# Some g++ versions might require that you link in the C++ library
g++ spiffy.cpp -lg++ # 
g++ usingmath.C -lm # math library

# C++ 11 Standards
g++ -std=c++0x use_auto.cpp
```

### Unix CC

**Cc**

- The CC compiler is a command-line compiler, meaning you type compilation commands on the Unix command line.
- The name is in uppercase letters to distinguish it from the standard Unix C compiler cc.

**Mt-Compiling and Linking**

```shell
# Compile
CC spiffy.C

# Exectue the program
a.out
```

**Pc**

- Compile the C++ <u>source code file</u> `spiffy.C`
- If your program has no errors, the compiler generates <u>an object code file with an o extension</u>. In this case, the compiler produces a file named `spiffy.o`.
- Next, the compiler automatically passes the object code file to the system linker, a program that combines your code with library code to produce the executable file. By default, <u>the executable file</u> is called `a.out`.
  - If you used just one source file, the linker also deletes the` spiffy.o` file because it's no longer needed.
  - If there are multiple source code files, the compiler does not delete the object code files.
- To run the program, you just type the name of the executable file:

**Mt-Multiple Source Code Files**

In C++, as in C, you can spread a program over more than one file and compile these files with command in # Multiple files.

```shell
# Multiple files
CC my.C precious.C
```

That way, if you just change the my.C file, you can recompile the program with the command below # Recompile multiple files. This recompiles the my.C file and links it with the previously compiled precious.o file.

```shell
# Recompile multiple files
CC my.C precious.o
```

**Mt-Library**

You might have to identify some libraries explicitly.
For example, To access functions defined in the math library, you may have to add the -lm flag to the command line:

```shell
# Link libraries
# e.g. math library
CC usingmath.C -lm
```



### Windows

#### Command-Line Compilers for Windows Command Prompt Mode

- GNU C++ compiler are **Cygwin and MinGW**; they use g++ as the compiler name.
- 具体操作参考`g++`，本质上都是GNU的。
- If the program compiles successfully, the resultant executable file is named a.exe.

#### Windows Compiler

**Cc**

- Microsoft Visual C++, C++Builder XE
  - 注意这与IDE VS code 和 VS 是完全不同的东西。
- Use them with the IDE would be a comon choice.

**Mt**

- Tyically, you must create a project for a program and add to the project the file or files constituting the program.

- One very important matter you have to establish is the kind of program you're creating.

  - In general, you should look to see if there is an option labeled Console, character-mode, or DOS executable and try that.

- After you have the project set up, you have to compile and link your program.

  - **IDE Methods**

  - `Compile` typically means compile the code in the file that is currently open.

  - `Build` or `Make` typically means compile the code for all the source code files in the project.

    - This is often an incremental process.

      That is, if the project has three files, and you change just one, and then just that one is recompiled.

  - `Build All` typically means compile all the source code files from scratch.

  - As described earlier, `Link` means combine the compiled source code with the necessary library code.

  - `Run` or `Execute` means run the program.Typically, if you have not yet done the earlier steps, Run does them before trying to run a program.

  - `Debug` means run the program with the option of going through step-by-step.

    - A compiler may offer the option of `Debug` and `Release` versions.The former contains extra code that increases the program size, slows program execution, but enables detailed debugging features.

- A compiler generates an error message when you violate a language rule and identifies the line that has the problem.

  - When fixing errors, fix the first error first. If you can't find it on the line identified as the line with the error, check the preceding line.
  - Occasionally, compilers get confused after incompletely building a program and respond by giving meaningless error messages that cannot be fixed. In such cases, you can clear things up by selecting Build All to restart the process from scratch. Unfortunately, it is difficult to distinguish this situation from the more common one in which the error messages merely seem to be meaningless.



### Mac

- Xcode

**F**

- Not only does it provide an IDE that supports several programming languages, it also installs a couple of compilers—g++ and clang—that can be used as command-line programs in the Unix mode accessible through the Terminal
  utility.





# C++ Library

## Cc

- **Library**
  - **Def:** Libraries are collections of programming modules that you can call up from a program.

- **Header files / include files**

  - **Def:**

  - **Pcp-Naming Conventions**

    | Kind of Header | Convention             | Example    | Comments                                                     |
    | -------------- | ---------------------- | ---------- | ------------------------------------------------------------ |
    | C++ old style  | Ends in .h             | iostream.h | Usable by C++ programs                                       |
    | C old style    | Ends in .h             | math.h     | Usable by C and C++ programs                                 |
    | C++ new style  | No extension           | iostream   | Usable by C++ programs, uses namespace std                   |
    | Converted C    | c prefix, no extension | cmath      | Usable by C++ programs, might use non-C features, such as namespace std |

    - For purely C++ header files such as iostream, dropping the h is more than a cosmetic change, for the h-free header files also incorporate namespaces

  - 




## Standard C++ library

### I/O

```cpp
#include <iostream>
using namespace std;

```

- If you use iostream instead of iostream.h, you should use the following namespace directive to make the definitions in iostream available to your program



## Others

**climits cfloat**

**Cc**

- **climits**
  - **Def+F:** The climits header file defines symbolic constants to represent type limits.

**Mt-Check the type limits and byte size**

- **Lk:** GitLab > cpp-primer-plus-programming-exercise-records > list_3_1_limits.cpp
- **Lk:** C++ Primer Plus 6th > Table 3.1 Symbolic Constants from climits

**F**

- supply detailed information about the size limits of integer types and floating types
- climits: These constants represent the largest and smallest possible values for the int type
  -  In particular, it defines symbolic names to represent different limits
- cfloat: the float.h file defines constants such as FLT_DIG and DBL_DIG, which represent the number of significant figures supported by the float type and the double type. 

**cfloat or float.h**

**F**

- provide the limits of floating-point types
  - minimum number of significant digits
  - the number of bits used to represent the mantissa
  - the maximum and minimum exponent values



## Problem

### 无法include标准Libaray

如果出现形如 cpp can't not open source file iostream 之类的include基本库的问题，可以查询IDE或编辑器中Settings > compiler path【尤其是Cpp Compiler Path】的设置，一般在Linux中设置为`/usr/bin`或者系统中`g++`等编译器的具体位置即可。







# Comment

```cpp
// one-line comment

/* 
multiline
comments
*/

```

**Cc**

- **Def+F:** A comment is a remark from the programmer to the reader that usually identifies a section of a program or explains some aspect of the code.





# Expressions and Statements

## Cc-Expressions and Statements

- **Instruction**
  - **Pcp**
    - Instruction 包含 Expression 和 Statement
- **Expression**
  - **Def:** An expression consists of a combination of operators and operands.
  - **Ab**
    - every C expression has a value
      - **Sp**
        - what about the ones with = signs? Those expressions simply have the same value that the variable to the left of the = sign receives. 
        - What about the expression q > 3? Such relational expressions have the value 1 if true and 0 if false. 
  - **Cpn**
    - **Subexpression:** Some expressions are combinations of smaller expressions, called subexpressions
    - **Full expression**: A full expression is one that's not a subexpression of a larger expression.
      - **e.g.** 
        - the expression in an expression statement
        - the expression serving as a test condition for a while loop.
    - **Relational expression**: test expressions that make comparisons
- **Statement**
  - **Def:** A statement is a complete instruction to the computer.
    - **Att:** Although a statement (or, at least, a sensible statement) is a complete instruction, <u>not all complete instructions are statements.</u> 
    - The semicolon(`;`) at the end of the line identifies the line as a **C statement or instruction.**
  - **Pcp**
    - Exactly, the semicolons tell the compiler where one statement ends and the next begins.
    - **free-form format:** a separated-line statement/one-line statement are acceptable for C compiler as long as you add `;` at the end of a statement.
  - **Ab**
    - In C, statements are indicated by a semicolon at the end.
    - semicolon is the terminator of C++ Statements rather than as a separator.
      - The difference is that a semicolon acting as a terminator is part
        of the statement rather than a marker between statements.
  - **F**
    - Statements are the primary building blocks of a program. A program is a series of statements with some necessary punctuation.
  - **Cat**
    - **Null statement**: `;`
    - **Expression statements**: C considers any expression to be a statement if you append a semicolon
    - **Declaration statement**
      - **Ab**
        - Declaration statement is generally not an expression statement since it could not return a value.
    - **Assignment statement**
      - **Def+F:** it assigns a value to a variable. 
      - **Cpn**
        - It consists of a variable name followed by the assignment operator (=) followed by an expression followed by a semicolon
    - **Structured statement/Control Statement**
      - **Def+F:** Possess a structure more complex than that of a simple assignment statement.
      - **Cat**
        - Loops
    - **Function statement/Function call statement**
      - **Def+F:** function statement causes the function to do whatever it does.
      - **Att:** function statement is not a function
    - **Return statement**
      - **Def+F:** The return statement terminates the execution of a function
    - **Compound Statements (Blocks)**
      - **Def+F:** A compound statement is two or more statements grouped together by enclosing them in braces; it is also called a block.
      - **Ab** 
        - Compound Statement counts syntactically as  a single statement
      - **e.g.** while loop block with braces.
- **Side effect**
  - **Def:** side effect is the modification of a data object or file
  - **Cpn**
    - **Sequence point:** a point in program execution <u>at which all side effects are evaluated before going on to the next step.</u>
      - In C, the <u>semicolon</u> in a statement marks a sequence point
      - The end of any <u>full expression</u> is also a kind of sequence point.
      - Some <u>operators</u>, like `,` `&&` and `||`
  - **Cat**
    - The increment and decrement operators, like the assignment operator, have side effects and are used primarily because of their side effects



## Declaration Statement

### Cc

- In general, then, a declaration indicates the type of data to be stored and the name the program will use for the data that's stored there. 

### Defining Declaration Statement / Definition

```cpp
int num;
int var1, var2;  // Multiple declarations
```

> - using a variable called num and that num will be an int (integer) type

**Cc**

- **Declaration**
  - **Def+F:** a defining declaration statement, or definition, for short means that its presence causes the compiler to allocate memory space for the variable
- **Identifier:** The name you select for a variable, a function, or some other entity.

**Pcp**

- All variables must be declared before they are used.
- C has required that variables be declared at the beginning of a block with no
  other kind of statement allowed to come before any of the declarations.
- Putting all the variables in one place makes it easier for a reader to grasp what the program is about.

### Reference Declarations

**Lk** 

- This notebook or C guideline notebook > Memory Management System > `extern`

**Cc**

- reference declarations tell the computer to use a variable that has already been defined elsewhere.



## Assignment Statement

```cpp
num = 1;
num1 = num2 = 2;
```

> - The statement num = 1; assigns the value 1 to the variable called num.

**Cc**

- **Assignment:** The assignment statement sets aside space in computer memory for the variable and assigns a value to that storage location. 

**Cpn**

- **Assignment operator:** =



## Control Statements

### Loops

#### for loop

#### while loop

#### do while loop



### Branching Statements 

#### ----- Branching Statements / Selection Statements -----

#### if Statement

#### Switch Statement

#### ----- Jumping Statements -----

#### Break Statement





## Return Statement

**Cc**

- **Def+F:** return statement, terminates the function



# Data Type

## Pcp-C++ Naming Rules

- The only characters you can use in names are alphabetic characters, numeric digits, and the underscore (_) character.
- The first character in a name cannot be a numeric digit.
- Uppercase characters are considered distinct from lowercase characters.
- You can't use a C++ keyword for a name.
- Names <u>beginning with two underscore characters or with an underscore character followed by an uppercase letter</u> are reserved for use by the implementation—that is, the compiler and the resources it uses. Names <u>beginning with a single underscore character</u> are reserved for use as global identifiers by the implementation.
- C++ places no limits on the length of a name, and all characters in a name are significant. However, some platforms might have their own length limits.
  - In ANSI C(C99), two names that have the same first 63 characters are considered identical, even if the 64th characters differ.)



## Pcp-Type Convertion

**Lk:** C++ Primer Plus > C3 >  Type Conversions

**Potential Numeric Conversion Problems**

- When you initialize a variable of one numeric type to a value of a different type, C converts the value to match the variable. This means you may lose some data.
  - when converting floating-point values to integers, C simply throws away the decimal part (truncation) instead of rounding.
  - Type conversion may cause lost in precision, because a float is guaranteed to represent only the first six digits accurately

- When the destination is some form of unsigned integer and the signed value is an integer or Bigger integer type to smaller integer type, the extra bits that make the value too big are ignored.
  - Original value might be out of range for target type; typically just the low-order bytes are copied.


**Cc**

- **Promotion**: conversions to larger types
  - When appearing in an expression, char and short, both signed and unsigned, are automatically converted to int or, if necessary, to unsigned int.
  - If short is the same size as int, unsigned short is larger than int; in that case, unsigned short is converted to unsigned int
  - Under K&R C, but not under current C, float is automatically converted to double
- **Demotion**: a value is converted to a lower-ranking type.
  - Promotion is usually a smooth, uneventful process, but demotion can lead to real trouble. The lower-ranking type may not be big enough to hold the complete number which could lead to precision problems.

**Pcp-Conversion Checklist/Ranking Types**

1.  If either operand is type long double, the other operand is converted to long
   double.
2. Otherwise, if either operand is double, the other operand is converted to double.
3. Otherwise, if either operand is float, the other operand is converted to float.
4. Otherwise, the operands are integer types and the integral promotions are made.
5. In that case, if both operands are signed or if both are unsigned, and one is of lower rank than the other, it is converted to the higher rank.
6. Otherwise, one operand is signed and one is unsigned. If the unsigned operand is of higher rank than the signed operand, the latter is converted to the type of the unsigned operand.
7. Otherwise, if the signed type can represent all values of the unsigned type, the
    unsigned operand is converted to the type of the signed type.
8. Otherwise, both operands are converted to the unsigned version of the signed type.

**Rules for automatically type conversion**

- Scenes when type conversions activate automatically
  - C++ converts values when you assign a value of one arithmetic type to a variable of another arithmetic type.
    - In an assignment statement, the final result of the calculations is converted to the type of the variable being assigned a value. (i.e. `int var = val;` The `int` type) 
      - This process can result in promotion, as described in rule 1, or demotion, in which a value is converted to a lower-ranking type.

  - C++ converts values when you combine mixed types in expressions.
    - In any operation involving two types, both values are converted to the higher ranking of the two types.
      - The rank of type: The size of the type./Tolerance
    - Integral promotions: When it evaluates expressions, C++ converts bool, char, unsigned char, signed char, and short values to int.
    - 

  - C++ converts values when you pass arguments to functions.
    - When passed as function arguments, char and short are converted to int, and float is converted to double. 
      - This automatic promotion is overridden/controlled by function prototyping
      - However, it is possible, although usually unwise, to waive prototype control for argument passing.

- **Att:** You should usually **steer clear of automatic type conversions**





## Mt-Symbolic Constant 符号常量

### `#define` Manifest Constants

**Lk:** This notebook > C++ Preprocessor > #define directive  `#define` for Manifest Constants / Macro

### `const` Modifier

**Lk:** This notebook > Memory Management System > Memory Management > Move/Modify the memory > `const` Modifier



## ----------  Fundamental Types ----------

## Cc-Arithmetic types

- Integer and floating-point types are collectively termed arithmetic types.



## Integer

### Cc-Integer

**Integer** 

- **Def**
  - An integer is the type which contains a number with no fractional part. 
  - An integer literal, or constant, is one you write out explicitly, such as 212 or 1776.
- **Ab**
  - Store: Binary number
  - Size: The usual term for describing the amount of memory used for an integer is **width**.
    - 4 bytes 字节
    - An int integer is at least as big as short.
  - Natural size refers to the integer form that the computer handles most efficiently.
  - A decimal integer without a suffix is represented by the smallest of the following types that can hold it: int, long, or long long
  - A hexadecimal or octal integer without a suffix is represented by the smallest of the following types that can hold it: int, unsigned int, long, unsigned long, long long, or unsigned long long. 
- **Pcp**
  - The int type is a signed integer. 
    - Typically, systems represent signed integers by using the value of a particular bit to indicate the sign.

**Adjective keywords to modify the basic integer type:**

- **Cc**
- `short` or `short int`
  - **Pcp+F**
    - Saving space when only small numbers are needed.
    - C automatically expands a type short value to a type int value when it's passed as an argument to a function.

  - **Ab**
    - `short` is a signed type
    - use less storage than `int`
    - **Size**: A short integer is at least 2 bytes wide.
- `long` or `long int`
  - **Pcp+F**
    - Enabling you to express larger integer values
  - **Ab**
    - `long` is a signed type
    - **Size:** A long integer is at least 4 bytes wide and at least as big as int.
    - Used when the integer is >= 5 digits
    - with l or L suffix
  - **Cat**
    - `long long` or `long long int`
      - **Pcp+F**
        - Enabling you to express larger integer values
      - **Ab**
        - `long long` is a signed type
        - **Size:** A long long integer is at least 8 bytes wide and at least as big as long.
        - C++11 provides the ll and LL suffixes for type long long, and ull, Ull, uLL, and ULL for unsigned long long.
- `unsigned` or `unsigned int`
  - **Pcp+F**
    - Used for variables that have only nonnegative values. 
    - `unsigned` shifts the range of numbers that can be stored

  - **Ab**
    - The bit used to indicate the sign of signed numbers now becomes another binary digit, allowing the larger number.
    - with u or U suffix
- `signed`
  - **Pcp+F**
    - The keyword `signed` can be used with any of the signed types to make your intent explicit.
- **Pcp-Choice**
  - The most common practice today on personal computers is to set up long long as 64 bits, long as 32 bits, short as 16 bits, and int as either 16 bits or 32 bits, depending on the machine's natural word size. 
  - First, consider `unsigned` types if you don't need negative numbers, and the unsigned types enable you to reach higher positive numbers than the signed types.
  - Use the `long` type if you need to use numbers that long can handle and that `int` cannot. Similarly, use `long long`  if you need 64-bit integer values.
    - If you are writing code on a machine for which int and long are the same size, and you do need 32-bit integers, you should use long instead of int so that the program will function correctly if transferred to a 16-bit machine.
  - Use `short` to save storage space if, say, you need a 16-bit value on a system where int is 32-bit only if your program uses arrays of integers that are large in relation to a system's available memory.
    - it may correspond in size to hardware registers used by particular components in a computer.
  - To cause a small constant to be treated as type `long`, you can append an `l` (lowercase L) or `L` as <u>a suffix.</u> Similarly, on those systems supporting the `long long` type, you can use an `ll` or `LL` suffix to indicate a long long value. Add a `u` or `U` to the suffix for `unsigned long long`.
    - e.g. `12l or 7L`
    - e.g. `12ll or 7LL`
    - e.g. `12ull or 7ULL or 12uLL or 7Ull`

### Mt-Integer Initialization

```c
int var = 2;
int wrens(432);  // C++11 alternative C++ syntax, set wrens to 432
int rheas = {12}; // C++98 set rheas to a single value
int rheas = {};  // the braces can be left empty, in which case the variable is initialized to 0

```

- C++ added the parentheses form of initialization to make initializing ordinary variables more like initializing class variables. C++11 makes it possible to use the braces syntax (with or without the =) with all types—a universal initialization syntax. 
- C++11 calls an initialization that uses braces a **list-initialization**.
  -  In particular, list-initialization doesn't permit narrowing, which is when the type of the variable may not be able to represent the assigned value
    - e.g.-Not allowed
    - Type conversions
    - Variable initialization


### Char

```c++
char fodo; // may be signed, may be unsigned
unsigned char bar; // definitely unsigned
signed char snark; // definitely signed
```

**Cc-`char`**

- **Def**
  - The `char` type is used for storing characters such as letters and punctuation marks.
  - characters, digits, punctuation marks and escape sequences
- **Ab**
  - Technically `char` type is the smallest integer type. The `char` type actually stores integers, not characters.
  - Size: 1 byte 字节
    - The C language defines a byte to be the number of bits used by type char
  -  char is not signed or unsigned by default. Could be `signed` or `unsigned`
    - The choice is left to the C++ implementation in order to allow the compiler developer to best fit the type to the hardware properties.
- **Cat**
  - the ASCII code
  - Unicode
  - EBCDIC
  - ISO/IEC 10646

**Mt-Declaration**

```c
char response;
char itable, latan;
```

**Mt-Initialization and Assignment**

```c
// Assignment
char first_char = 'A';
char first_char_ASCII = 65; 

```

**Mt-Escape sequence**

- **Def:** Represent difficult- or impossible-to-type characters. In each case, the escape sequence begins with the backslash character(`\`).

```c
// Use ASCII code
char beep = 7;

// Use escape sequences
char backslash = '\\';

// Using a hexadecimal form for character constants
```

| Character Name  | ASCII Symbol | C++ Code | ASCII Decimal Code |
| --------------- | ------------ | -------- | ------------------ |
| Newline         | NL (LF)      | \n       | 10                 |
| Horizontal tab  | HT           | \t       | 9                  |
| Vertical tab    | VT           | \v       | 11                 |
| Backspace       | BS           | \b       | 8                  |
| Carriage return | CR           | \r       | 13                 |
| Alert           | BEL          | \a       | 7                  |
| Backslash       | \            | `\\`     | 92                 |
| Question mark   | ?            | \?       | 63                 |
| Single quote    | '            | `\'`     | 39                 |
| Double quote    | "            | `\"`     | 34                 |

**Ab**

- When you have a choice between using a numeric escape sequence or a symbolic escape sequence, as in \0x8 versus \b, use the symbolic code. The numeric representation is tied to a particular code, such as ASCII, but the symbolic representation works with all codes and is more readable.

**Cat**

- Null character: `\0`
  - **Ab**
    - it is the non-printing character whose ASCII code value (or equivalent) is 0.
    - However, the null character is not the digit zero;
    - The presence of the null character means that the array must have at least <u>one more cell than the number of characters to be stored</u>.
  - **F:** C uses it to mark the end of a string

**Universal character names**

```c++
\u00F6
```

- **Background**
  - C++ implementations support a basic source character set—that is, the set of characters you can use to write source code.
  - Then there is a basic execution character set, which includes characters that can be processed during the execution of a program
  - The C++ Standard also allows an implementation to offer extended source character sets and extended execution character sets. 
- **Cc**
  - C++ has a mechanism for representing such international characters that is independent of any particular keyboard: the use of universal character names.
- **Ab**
  - Your source code can use the same universal code name on all systems, and the compiler will then represent it by the appropriate internal code used on the particular system.
- **Mt**
  - Using universal character names is similar to using escape sequences. A universal character name begins either with \u or \U.
    - The \u form is followed by 8 hexadecimal digits.
    - The \U form by 16 hexadecimal digits.
    - These digits represent the ISO 10646 code.
      - ISO 10646 is an international standard under development that
        provides numeric codes for a wide range of characters.

**Unicode**

```c++
U-222B
```

- **Cc**
  - Unicode provides a solution to the representation of various character sets by providing a standard numbering system for a great number of characters and symbols, grouping them by type.
- **Ab**
  - Unicode assigns a number, called a code point, for each of its characters. 
  - The U identifies this as a Unicode character, and the 222B is the hexadecimal number for the character—an integral sign, in this case.

#### wchar_t type

```cpp
wchar_t bob = L'P';  // a wide-character constant
wcout << L"tall" << endl;  // outputting a wide-character strin
```

**Cc-`wchar_t`**

- The wchar_t type is an integer type with sufficient space to represent the largest extended character set used on the system.

**Ab**

- This type has the same size and sign properties as one of the other integer types, which is called the **underlying type**.
- The iostream header file provides parallel facilities in the form of wcin and wcout for handling wchar_t streams
- you can indicate a wide-character constant or string by preceding it with an L.
- **Size:** 2-bytes

**F**

- wchar_t (for wide character type), can represent the extended character set

#### char16_t and char32_t(C++11)

```cpp
char16_t ch1 = u'q'; // basic character in 16-bit form
char32_t ch2 = U'\U0000222B'; // universal character name in 32-bit form
```

**Cc**

- C++11 introduces the types char16_t, which is unsigned and 16 bits, and char32_t, which is unsigned and 32 bits. 

**Ab**

- Like wchar_t, char16_t and char32_t each have an underlying type, which is one of the built-in integer types.
- C++11 uses the u prefix for char16_t character and string constants, as in u'C' and u"be good"
- Similarly, it uses the U prefix for char32_t constants, as in U'R' and U"dirty rat".
- The char16_t type is a natural match for universal character names of the form \u00F6, and the char32_t type is a natural match for universal character names of the form \U0000222B.



### Boolean

```cpp
bool is_ready = true;

int ans = true; // ans assigned 1
int promise = false; // promise assigned 0

bool start1 = 10; // start1 assigned true
bool start2 = -100; // start2 assigned true
bool stop = 0; // stop assigned false
```

**Cc**

- a Boolean variable is one whose value can be either true or false

**Ab**

- C++ interprets nonzero values as true and zero values as false. 



## Floating-Point Numbers

### Float

```cpp
// Literals
12.34
0.00023 // floating-point
8.0 // still floating-point
1.234f // a float constant
2.45E20F // a float constant
    
//Exponential notation
2.52e+8
8.33E-4
```

**Cc**

-  Floating-point type is the type which numbers with fractional parts
- The scaling factor serves to move the decimal point, hence the term
  floating-point. 

**Ab**

- at least 6 significant figures(total number) and allow a range of at least $10^{–37}$ to  $10^{37}$ 
  - the range in exponents for all three types is at least –37 to +37

- Size: 4 bytes 字节
  - In effect, the C and C++ requirements for significant digits amount to float being at least 32 bits
  - Typically, however, float is 32 bits, double is 64 bits, and long double is 80, 96, or 128 bits.

- General minimum number of significant digits: 6

**Cpn**

- One part represents a value, and the other part scales that value up or down

**Space**

- Floating-point representation involves breaking up a number
  into a fractional part and an exponent part and storing the parts separately
- Often, systems use 32 bits to store a floating-point number. 8 bits are used to give the **exponent** its value and sign, and 24 bits are used to represent the **non-exponent part,** called the **mantissa or significand**, and its sign.

**Pcp-Exponential notation/e-notation**

- **Def:** Sign(+-) + Fraction + Exponent of 10
- 3.16E7 = $3.16 \times 10^{7}$

**Pcp-Precision**

- Floating-point numbers are subject to greater loss of precision when calculating large numbers.
- Computer floating-point numbers can't represent all the values in the range. Instead, floating-point values are often approximations of a true value. 

**e.g.**

- **NaN(Not a Number):** Not existing or not valid float-point value

- - 

### Double

```cpp
2.345324E28 // a double constant
```

**Ab**

- double being at least 48 bits and certainly no smaller than float.
  - Typically, however, float is 32 bits, double is 64 bits, and long double is 80, 96, or 128 bits.
- the range in exponents for all three types is at least –37 to +37
- Remember, floating-point constants are type double by default.
- General minimum number of significant digits: 15

#### long double

```cpp
2.2L // a long double constant
```

**Ab**

- long double being at least as big as double.
  - Typically, however, float is 32 bits, double is 64 bits, and long double is 80, 96, or 128 bits.
- the range in exponents for all three types is at least –37 to +37
- General minimum number of significant digits: 18



## ---------- Compound Types ----------

## Cc

- **Compound types**: These are types built from the basic integer
  and floating-point types.



## Arrays

### Cc-Array

```c
// Array Declaration
int int_list[20];  // Type and elements
int int_list[constant_integer_expression];
int int_list[var];  // C99, variable-length array(VLA), you can’t initialize a VLA in its declaration

```

- **Array**
  - **Def:** An array is an <u>ordered</u> sequence of data elements of <u>one type</u>
    - An array is a series of values of the same type
    - The brackets([]) indicates that the variables as arrays.
  - **Ab**
    - Array 是构造类型, derived types because they are built on other types.
    - The array elements are stored next to each other in memory
    - So sizeof arr_var is the size, in bytes, of the whole array, and sizeof arr_var[0] is the size, in bytes, of one element.
    - An array name is also the address of the first element of
      the array.
      - Att: The array name means the address rather than the value as other variable name means.
      - The elements of an array are variables (unless the array was declared as const), but the name is not a variable.
  - **Cpn**
    - **Elements:** memory cell each of which can store one particular type value
    - **Subscripts, indices/index, or offsets**: The numbers used to identify the array elements are called subscripts, indices, or offsets.
      - **Ab**
        - The subscripts must be integers, and the subscripting begins with 0.
  - **Problem**
    - C <u>doesn't</u> check to see whether you use a correct subscript/element number.
- **Scalar variables**: Single-valued variable
- 

### Ab-Storage

- Arrays, like other variables, can be created using different storage classes
- Default: belong to the automatic storage class
  - That means they are declared inside of a function and without using the keyword static.


### Pcp

- if you want a read-only array, In such cases, you can, and should, use the `const` keyword when you declare and initialize the array.

### Mt-Declaration

```c
int list[10];  // oridinary

int var = 10;
int list[var];  // VLA, dynamic array

list = (int *) malloc(10 * sizeof(int));
list = (int *) malloc(var * sizeof(int));
```

- **Cc**
  - dynamic array, one that's allocated while the program runs and that
    you can choose a size for while the program runs

### Mt-Initialization

```c
int list[8] = {1, 2, 3, 4, 5, 6, 7, 8};
int list[4] = {0};  // Other number will be initialized as zero.

// Const Array
const int list[8] = {1, 2, 3, 4, 5, 6, 7, 8};  // This makes the program treat each element in the array as a constant. Once it’s declared const, you can’t assign values later. 

// Match the array size
int list[] = {1, 2, 3};  // Let the compiler match the array size to the list by omitting the size from the braces
length = sizeof list / sizeof list[0];  // Dividing the size of the entire array by the size of one element tells us how many elements are in the array.

// Designated initializer
int list[5] = {[4] = 2};
int days[MONTHS] = {31,28, [4] = 31,30,31, [1] = 29};
int days[] = {1, [4] = 5};  // The compiler will make the array big enough to accommodate the initialization values.

// C++11
double earnings[4] {1.2e4, 1.6e4, 1.1e4, 1.7e4};	// drop the = sign when initializing an array. It is okay with C++11
unsigned int counts[10] = {};	// all elements set to 0// use empty braces to set all the elements to 0
```

**Pcp**

- Use constant to define the size of array, which could somehow helps ensure that you use the same array size consistently throughout the program.
- if you don't initialize an array at all, its elements, like uninitialized ordinary variables, get garbage values, <u>but if you partially initialize an array, the remaining elements are set to 0</u>.
- When the subscript is out of the scale of the array, this overgenerosity is considered an error.
- When you use empty brackets to initialize an array, the compiler counts the number of items in the list and makes the array that large.
- list-initialization protects against narrowing/type demotion.

#### Mt-Designated initializers

- **Designated initializers:** This feature allows you to pick and choose which elements are initialized.
  - **Pcp+Ab**
  - As with regular initialization, after you initialize at least one element, the uninitialized elements are set to 0. 
  - First, if the code follows a designated initializer with further values, as in the sequence [4] = 31,30,31, these further values are used to initialize the subsequent elements. 
  - Second, if the code initializes a particular element to a value more than once, the last initialization is the one that takes effect.

### Mt-Assignment

```c
// Assignment
int list[10];
list[1] = 2;
list[2] = list[1];
int_list[0] = 0;  // First element
int_list[20 - 1] = 19;  // Final element

// false assignmnet
list = list;  // Not allowed, Array could not be assigned to other variable.
list[200000] = list[19999999];  // Out of rage
list[2] = {1, 2, 3, 4, 5, 6};  // use the list-in-braces form except when initializing is not allowed.


```

### Mt-Pointer Related Array Manipulation

```c
// Pointer related
list[2] == *(list + 2)
```

**Lk:** This note > Pointer Addition

### Mt-Multidimensional array

```c
// Declaration
int mat[5][4];  // 5 row * 4 col matrix

// Initialization
int mat[2][3] = 
{
    {1, 2, 3},
    {0, 1, 2}
};  // Att the semicolon

int mat[2][3] = {1, 2, 3, 0, 1, 2};  // The same as the former example. As long as the number of entries is correct.

// Pointer
/// Address
mat == mat[0];
mat == &mat[0][0];

mat + 1 != mat[0] + 1;
*mat + 1 == mat[0] + 1;

/// Value
mat[0][0] == *mat[0];
mat[0][0] == **mat;
mat[1][2] == *(*(mat + 1) + 2);

/// Pointer to array
int (* pz)[3];  // Pointer to an array of 3 integer elements
pz = mat;

/// Pointer to pointer
int **p2p;
```

**Pcp**

- The rules we discussed about mismatches between data and array sizes in one-dimensional array apply to each row. 
- Without inner braces, the values of arrays are filled row by row until the data runs out even if you are short of entries. Then the remaining elements are initialized to 0. 
- `mat` is a pointer to array or an array of arrays, but `arr[0]` is a pointer of type.

**Pcp-Multidimensional Arrays and Pointers**

```c
int zippo[4][2];
// Address
zippo = &zippo[0];
zippo[0] = &zippo[0][0];

// Dereference / Double Indirection
*zippo == &zippo[0][0];
**zippo == *&zippo[0][0];
```

- In short, <u>zippo[0] is the address of an int-sized object, and zippo is the address of a two-int-sized object</u>. Because both the integer and the array of two integers begin at the same location, both zippo and zippo[0] have the same numeric value
- double indirection: In short, zippo is the address of an address and must be dereferenced twice to get an ordinary value. An address of an address or a pointer of a pointer is an example of double indirection.

### Mt-Function Argument

```c
int func(int * arr);  // Prototype

// In the context of a function prototype or function definition header, and only in that context, you can substitute int ar[] for int * ar
int func(int arr[]);


// Use const to protect array in function from being changed
int sum(const int ar[], int n);  // It’s important to understand that using const this way does not require that the original array be constant


// Multidimensional Array Arguments
void sum_rows(int ar[][COLS], int rows);
void sum_cols(int [][COLS], int );  // ok to omit names
int sum2d(int (*ar)[COLS], int rows); // another syntax
```

**Pcp**

- Since the array is the address of the its first element, the type of array argument for function should be the pointer-to-type. 
- Arrays don't give you that choice to pass the value of the array; you must pass a pointer to function. 
  - The reason is efficiency. If a function passed an array by value, it would have to allocate enough space to hold a copy of the original array and then copy all the data from the original array to the new array. It is much quicker to pass the address of the array and have the function work with the original data.

- The reason C ordinarily passes data by value is to preserve the integrity of the data. If a function works with a copy of the original data, it won't accidentally modify the original data. <u>But, because array-processing functions do work with the original data, they can modify the array.</u>
- In general, to declare a pointer corresponding to an <u>N-dimensional array, you must supply values for all but the leftmost set of brackets for the pointer to recognize the length of each unit of array.</u>

### Mt-Pointer From start to end

```c
int sump(int * start, int * end)
{
	int total = 0;
	while (start < end)
	{
		total += *start; // add value to total
		start++;  // advance pointer to next element
	}
    return total;
}

void main(void)
{
    const int SIZE = 5;
	int arr[SIZE] = {0, 1, 2, 3 ,4};
	sump(arr, arr[SIZE]);  // The size of the whole array as it was declared.
}

```

### Mt-Variable-Length Arrays (VLAs)

```c
int row = 4;
int col = 5;
double mat[row][col];  // VLA

// Function
double func(int row, int col, mat[row][col]);
double func(int, int, mat[*][*]);  // VLAs omit names for prototype
```

**Def:** variable-length arrays, which allow you to use variables when dimensioning an array

**Pcp**

- They need to have the automatic storage class, which means they are declared either in a function without using the static or extern storage class modifiers (Chapter 12) or as function parameters. 
- You can't initialize VLAs in a declaration. 
- Finally, under C11, VLAs are an optional feature rather than a mandatory feature, as they were under C99.
- What the term variable does mean is that you can use a variable when specifying the array dimensions when first creating the array. The term variable in variable-length array does not mean that you can modify the length of the array after you create it. <u>Once created, a VLA keeps the same size.</u>
- Because the ar declaration uses rows and cols, they have to be declared
  before ar in the parameter list. 
- VLAs are still pointers and  a VLA parameter actually works with the data in the original array, and therefore has the ability to modify the array passed as an argument.
- VLAs allow for dynamic memory allocation. This means you can specify the size of the array while the program is running.
  - Regular C arrays have static memory allocation, meaning the size of the array is determined at compile time. 
- 

### Mt-Compound Literals

**Cc**

- **Literal**: Constants that are not symbolic.

  - e.g. 1.1 is a type double literal.

- **Compound Literal**: Constants compounded of different types.

  - e.g. 

    ```c
    // Array Constant
    (int [2]){10, 20};
    (int []){1, 2, 3};
    (int [2][3]){{1, 2, 3}, {0, 1, 2}};
    (int [][3]){{1, 2, 3}, {0, 1, 2}};
    (int [][]){{1, 2, 3}, {0, 1, 2}};
    
    // Use pointers to keep track of compound literals
    int * pt;
    pt = (int []){1, 2, 3};
    ```

  - **Pcp**

    - Keep in mind that a compound literal is a means for providing values that are needed only temporarily.  
    - Block Scope
      - That means its existence is not guaranteed once program execution leaves the block in which the compound literal is defined, that is, the innermost pair of braces containing the definition.
    - Storage Duration: 
      - Compound literals occurring <u>outside of any function</u> have <u>static storage duration</u>, and those occurring <u>inside a block</u> have <u>automatic storage duration</u>. 

### Character String

#### Cc-Character string

-  **Def:** The string is a `char` array if the array contains the null character, `\0`, which marks the end of the string
   -  A string is a series of characters stored in consecutive bytes of memory.

-  **Ab**
   -  The double quotation marks are not part of the string.
   -  Strings are stored in an array of type `char`. Characters in a string are stored in adjacent memory cells, one character per cell(each cell is one byte), and an array consists of adjacent memory locations
   -  The name of a character array, like any array name, yields the address of the first element of the array.
   -  **Size:** Num of char/elements + 1 (for the null character)
      -  Often, it is convenient to let the compiler determine the array size; recall that if you omit the size in an initializing declaration, the compiler determines the size for you
      -  the array size must evaluate to an integer constant, which includes
         the possibility of an expression formed from constant integer values.
      -  The null character, or '\0', is the character used to mark the end
         of a C string. It's the character whose code is zero. Because that isn't the code of any character, it won't show up accidentally in some other part of the string.
-  **Cat**
   -  **String Literal**: A string literal, also termed a string constant, is anything enclosed in double quotation marks. 
      -  **Ab**
         -  Character string constants are placed in the static storage class, which means that if you use a string constant in a function, the string is stored just once and lasts for the duration of the program, even if the function is called several times.

#### Pcp-Difference between array and pointer form

-  Typically, what happens is that the quoted string is stored in a data segment that is part of the executable file; when the program is loaded into memory, so is that string. The quoted string is said to be in <u>static memory</u>. But the memory for the array is allocated only after the program begins running.  There are two copies of the string. One is the string literal in static memory, and one is the string stored in the `ar1` array
   -  **Array Form:** the compiler will recognize the name ar1 as a synonym for the address of the first array element, &ar1[0]. One important point here is that in the array form, ar1 is an <u>address constant</u>. An address constant could not be changed so the `ar1++` would not be allowed.
   -  **Pointer Form:** The pointer form (*pt1) also causes 29 elements in static storage to be set aside for the string. In addition, once the program begins execution, it sets aside one more storage location for the <u>pointer variable</u> pt1 and stores the address of the string in the pointer variable. Since pt1 is a pointer variable, you can use the increment operator, such as `pt1++`.
   -  In short, initializing the array copies a string from static storage to the array, whereas initializing the pointer merely copies the address of the string.
-  In short, don't use a pointer to a string literal if you plan to alter the string.

#### Mt-Initialization

```c
// string constant
#define STR_RABBIT "rabbit"  
const char str[10] = "ten char";

// Array form
char str[10] = "ten char";  // char array
char str[10] = "ten" "ch""ar";  // C concatenates string literals if they are separated by nothing or by whitespace.
char str[10] = {'t', 'e', 'n', '\0'};
char str[10];

// The compiler determines the size of the string only if you initialize the array
char str[] = "ten char";
char *str = "\"char_array_with_null_character\"";  // char pointer form

// Extract
str[0] = 't';
str[1] = 'e';
str[2] = 'n';
```

**Pcp**

- When you specify the array size, be sure that the number of elements is at least one more (that null character again) than the string length. 
- Any unused elements are automatically initialized to 0 (which in char form is the null character, not the zero digit character).

#### Mt-Multidimensional Array/ Array of strings

```c
// An array of potiners of strings
const char* mytalents[LIM] = {
"Adding numbers swiftly",
"Multiplying accurately", "Stashing data",
"Following instructions to the letter",
"Understanding the C language"
};

// An array of char arrays
char yourtalents[LIM][SLEN] = {
"Walking in a straight line",
"Sleeping", "Watching television",
"Mailing letters", "Reading email"
};

// Extract the ith string
mytalents[i]
yourtablents[i]

```

-  The mytalents array is an array of five pointers, taking up 40 bytes on our system. But yourtalents is an array of five arrays, each of 40 char values, occupying 200 bytes on our system
-  The pointers in mytalents point to the locations of the string literals used for initialization, which are stored in static memory. The arrays in yourtalents, however, contain copies of the string literals, so  each string is stored twice.
-  Furthermore, the allocation of memory in the arrays is inefficient, for each
   element of yourtalents has to be the same size, and that size has to be at least large enough to hold the longest string.

**Pcp-Comparison**

- an array of pointers is more efficient than an array of character arrays.
- Because the pointers in mytalents point to string literals, these strings shouldn't be altered. The contents of yourtalents, however, can be changed. 

#### Mt-Multiple line

The `\` could be used to separate strings into two lines.

```c
printf("Here's another way to print a \
long string.\n");
```



#### Mt-Declaration

```c
char str_name[num]
```

> - The brackets after name identify it as an array. 
> - The number within the brackets indicates the <u>number of elements(The number also means the space/cells to store elements, including the null character)</u> in the array.

#### Mt-String Concatenation

- **Def:** You follow one quoted string constant with another, separated only by whitespace, C treats the combination as a single string.
- **Soc:** ANSI C 

```c
printf("Hello, young lovers, wherever you are.");
printf("Hello, young " "lovers" ", wherever you are.");
printf("Hello, young lovers"
", wherever you are.");
```

#### Mt-Input Strings

**`scanf()` function for strings**

```c
char str[10] = "1234567";
scanf("%s", str); // automatically add `\0` when reaching the end of the input string, not at the end of the string.
scanf("%10s", input_string);  // Search the first 10 character of the stdin string
```

```shell
$ hello
> hello # rather than hello67
```

> - 即便存在后续的空位，scanf得到的字符串也会在后面加入`\0`，导致其输出时到输入字符串的位置就终止了，即便str本身后面还有字符也会停止输出。
> - 输入格式中有空格的话不会影响"%s"，在输入中的目标字符串前的空格也会被忽略，这与`"%c"`不同。

**`gets()` function**

```c
char str[10];
gets(str);

```

- **Def+F:** `gets()` fetches the stdin value until reach the `\n` and load them to string variable().
- **F:** Read an entire line of input at a time instead of a single word
- **Ab**
  - It reads an entire line up through the newline character, discards the newline character, stores the remaining characters, adding a null character to create a C string. 
- **F-Return**
  - The string input
- **Problem-Banish**
  - The `gets` function doesn't check to see if the input actually fit into the array. If the input string is too long, you get **buffer overflow**, meaning the excess characters overflow the designated target.
  - “**Segmentation fault**” doesn't sound healthy, and it isn't. On a Unix system, this message indicates the program attempted to access memory not allocated to it.

**`fgets()` function**

```c
// General
fgets(str_ptr_target, length, source);

// Get string inputs from stdin
char str[10]
fgets(str, 10, stdin);
    
// Get string lines from file pointer
FILE *fp;
char *str;
fp = fopen("file_name", "a+");
fgets(str, length, fp);
```

- **Def+F:** The fgets() function meets the possible overflow problem by taking a second argument that limits the number of characters to be read
- **Cpn-Arguments**
  - **str_or_ptr**: the address where the string will be stored.
  - **length**: indicating the maximum number of characters to read.(The length should include the null character.) 
  - **source**: where to find the string inputs. indicating which file to read. 
    - To read from the keyboard, use `stdin` (for standard input) as the argument; this identifier is defined in `stdio.h`.
- **Ab**
  -  If fgets() reads the newline, it stores it in the string, unlike gets(), which discards it.
  -  Automatically add a null character to create a C string. 
- **F-Return**
  -  A pointer to char, which is the address of the str if all goes well.
  -  If the function encounters end-of-file, however, it returns a special pointer called the **null pointer.**
     -  This is a pointer guaranteed not to point to valid data so it can be used to indicate a special case.
     -  In code it can be represented by the digit `0` or, more commonly in C, by the macro `NULL`. 
     -  It's often used by functions that otherwise return valid addresses to indicate some special occurrence, such as encountering end-of-file or failing to perform as expected
  -  The function also returns NULL if there is some sort of read error.

**`gets_s()` function**

```c
// General
gets_s(str, strlen);



```

- **Def+F:** Read a line of input into the `str` array providing the newline shows up in the first `strlen-1` characters of input
- **Ab**
  - gets_s() just reads from the standard input
  - If gets_s() does read a newline; it discards it rather than storing it.
  - If gets_s() reads the maximum number of characters and fails to read a newline, it takes several steps. 
    - It sets the first character of the destination array to the null character. 
    - It reads and discards subsequent input until a newline or end-of-file is encountered. 
    - It returns the null pointer. 
    - It invokes an implementation-dependent “handler” function (or else one you've selected), which may cause the program to exit or abort.



**Pcp-General**

- you need to allocate enough storage to hold whatever strings you expect to
  read.
  - Don't expect the computer to count the string length as it is read and then allot space for it. 
  - Allocating inputs/string inputs to uninitialized pointer will probably get by the compiler, most likely with a warning, but when the pointer is read, the pointer might be written over data or code in your program, and it might cause a program abort.

#### Mt-Output Strings

**`printf()` function for strings**

```c
char str[10] = "str"; 
printf("str");
printf("%10s", str);
```

- C++ can, in fact, use printf(), scanf(), and all the other standard C input and output functions, provided that you include the usual C stdio.h file.

**`puts()` function**

```c
// General
puts(str);

// Slice
char str[10] = "ten char";
puts(&str[2]);
puts(str+2);
```

- **Def+F:** `puts()` fetches the value in the str variable and put them to stdout.
- **Soc:**  `stdio.h`
- **Cpn**
  - **str**: the address of strings.

- **Ab**
  - Automatically remove `\0`
  - Automatically add `\n` at the end of the string
  - It stops when it encounters the null character

**`fputs()` function**

```c
// General
fputs(str, source);

//
fputs(str, stdout);
fputs(str, fp);
```

- **Def+F:** 
- **Cpn**
  - **str**: the output string
  - **source**: indicate which file to write to. The fputs() function takes a second argument indicating the file to which to write. You can use stdout (for standard output), which is defined in stdio.h, as an argument to output to your display.
    - For the computer monitor we can use stdout (for standard output) as an argument. this identifier is defined in `stdio.h`.
- **Ab**
  - fputs() does not automatically append a newline to the output.
  - Because fgets() keeps the newline and fputs() doesn’t add one, they work well in tandem.
- **F-Return**
  - 


#### Mt-Function Argument

```c
void func(char str[]);
```

#### Mt-String Functions

**Pcp**

- Note that these prototypes use the keyword const to indicate which strings are not altered by a function. 

**Cpn**

`strlen()` **function**

```c
int str[10];
strncpy(str, "hello", 5);
strlen(str) == 5;
sizeof(str) == 40;
```

- **Def+F:** `strlen()` gives the length of a string in characters.
- **Soc:** string.h
- **Ab**
  -  not including the terminating null character, found in the string s.


**`strcpy()` Function**

```c
int str_target[10];
int str_source[10] = "hello";
strcpy(str_target, str_source);
strcpy(str_target + 2, str_source);  // copied from str_source to the 3rd elemnet of str_target
```

- **Def+F: ** `strcpy` copies the string `str_target` to `str_source`.
- **Soc:** string.h
- **Ab**
  - The second pointer, which points to the original string, can be a declared pointer, an array name, or a string constant. The first pointer, which points to the copy, should point to a data object, such as an array, roomy enough to hold the string.
    - declaring an array allocates storage space for data;
      declaring a pointer only allocates storage space for one address.
  - The copy is called the target, and the original string is called the source. 
    - You can remember the order of the arguments by noting that it is the same as the order in an assignment statement (the target string is on the left)
  - It is your responsibility to make sure the destination array has enough room to copy the source.
  - The first argument need not point to the beginning of an array; this lets you copy just part of an array.
  - The `\0` will be copied as well.
- **F-Return**
  - `strcpy` is type char *. It returns the value of its first argument—the address of a character. 
    - The first argument need not point to the beginning of an array; this lets you copy just part of an array.


**`strncpy()` Function**

```c
// Prototype
char *strncpy(char * restrict s1, const char * restrict s2, size_t n);

// General
strncpy(str_target, str_source, int_length);
```

- **Def+F:** `strncpy` copies the first `int_length` characters in the string `str_target` to `str_source`.
- **Soc:** string.h
- **Cpn**
  - `int_length` is the maximum number of characters to copy.

- **Ab**
  - The same as `strcpy`
- **F-Return**
  - The same as `strcpy`

**`strcmp()` Function**

```c
int str_target[10] = "smaller";
int str_source[10] = "BBBBBBBigger";
strcmp(str_target, str_source);
```

- **Def+F: ** `strcmp` compares the ASCII number of each character within the strings from left to right.
  - The strcmp() function compares strings until it finds corresponding characters that differ, which could take the search to the end of one of the strings. 

- **Soc:** string.h
- **Ab**
  - strcmp() is that it compares strings`(/0` marks the end), not arrays. Therefore, strcmp() can be used to compare strings stored in arrays of different sizes.
  - strcmp() compares all characters, not just letters, so instead of saying the comparison is alphabetic, we should say that strcmp() goes by the machine collating sequence.

- **F-Return**
  - The sum of `target - source` in ASCII code for each elements in strings
    - The exact numerical values, however, are left open to the implementation
    - 0 when the two strings are equal.
    - positive/1 when the target is larger. 
    - negative/-1 when the source is larger

**`strncmp()` function**

```c
strncmp(str_target, str_source, int_length)
```

- **Def+F**
  - The strncmp() function compares the strings until they differ or until it has compared a number of characters specified by a third argument.

- **Ab**
  - The same as strcmp function
- **F-Return**
  - The same as strcmp function

**`strcat()` Function**

```c
int str_target[20] = "hello";  // Larger than the size of the combination
int str_source[10] = "world";
strcat(str_target, str_source);
```

- **Def+F:** The `strcat()` (for string concatenation) function takes two strings for arguments. A copy of the second string is tacked onto the end of the first, and this combined version becomes the new first string. The second string is not altered
- **Soc:** string.h
- **Ab**
  - **Function type:** char*
  - `str_target` must be a string variable.
  - The `strcat()` function does not check to see whether the second string will fit in the first array. 
  - The first character of the s2 string is copied over the null character of the s1 string.
- **F-Return**
  - It returns the value of its first argument—the address of the first character of the string to which the second string is appended.


**`strncat()` Function**

```c
// General
strncat(str1, str2, length);
```

- **Def+F:**  strncat() functions like `strcat`, except that it takes a third
  argument indicating the maximum number of characters to add.
- **Cpn**
  - **length**:  indicating the maximum number of characters to add
- **Ab**
  - The same as `strcat`

- **F-Return**
  - The same as `strcat`


**`sprintf()` Function**

```c
sprintf(str_target, "%s, %-19s: $%6.2f\n", str_source1, str_source2, str_source3);
```

- **Def+F:**  `sprintf` works like `printf()`, but it writes to a string instead of writing to a display. Therefore, it provides a way to combine several elements into a single string.
- **Soc:** stdio.h

- **Cpn**
  - The first argument to sprintf() is the address of the target string. 
  - The remaining arguments are the same as for printf()—a conversion specification string followed by a list of items to be written.

**`strchr()` Function**

```c
strchr(str, c);
```

- **Def+F:** `strchr` search the address of the character c in the string str. (search character)
- Space
  - string.h

- **F-Return**
  - This function returns a pointer to the first location in the string str that holds the character c. 
  - null pointer if not found

**`strpbrk()` Function**

```c
strpbrk(str_s1, str_s2);
```

- **Def+F:** (string pointer break) This function returns a pointer to the first location in the string s1 that holds any character found in the s2 string. 
- **F-Return**
  - This function returns a pointer to the first location in the string str that holds any character found in the s2 string. 
  - The function returns the null pointer if no character is found.

**`strrchr()` Function**

```c
strrchr(str_s, char_c);
```

- **Def+F:** This function returns a pointer to the last occurrence of the character c in the string s. (The terminating null character is part of the string, so it can be searched for.) 
- **F-Return**
  - Returns a pointer to the last occurrence of the character c in the string s.
  - The function returns the null pointer if the character is not found.

**`strstr()` Function**

```c
strstr(str1, str2);
```

- **Def+F:**  This function returns a pointer to the first occurrence of string s2 in string s1. The function returns the null pointer if the string is not found.

**`atoi()` function**

```c
atoi(str);
```

- **Def+F:** It takes a string as an argument and returns the corresponding integer value. (alphanumeric to integer)
- **Soc:** stdlib.h
- **F-Return**
  - The atoi() function still works if the string only begins with an integer until it encounters something that is not part of an integer.
  - the atoi() function returns a value of 0 if its argument is not recognizable as a number
  - **Lk:** strtol() function, discussed shortly, provides error checking that is more reliable.

**`atof()` function**

- **Def+F:** It takes a string as an argument and returns the corresponding `double` value. (alphanumeric to float)

**`atol()` function**

- **Def+F:** It takes a string as an argument and returns the corresponding integer `long` value. (alphanumeric to long)

**`strtol()` function**

```c
// Prototype
long strtol(const char * restrict nptr, char ** restrict endptr, int base);

// General
strtol(str_nptr, ptr_endptr, int_base);
```

- **Def+F:** strtol() converts a string to a long,
- **Cpn**
  - nptr is a pointer to the string you want to convert
  - endptr is the address of a pointer that gets set to the address of the character terminating the input number
  - base is the number base the number is written in. 
- **Ab**
  - The functions identify and report the first character in the string that is not part of a number. Also, strtol() and strtoul() allow you to specify a number base.
  - **Base:** The strtol() function goes up to base 36, using the letters through 'z' as digits. 

**`strtoul()` function**

- **Def+F:** strtoul() converts a string to an unsigned long
- **Ab**
  - The functions identify and report the first character in the string that is not part of a number. Also, strtol() and strtoul() allow you to specify a number base.
  - **Base:** The strtoul() function does the same as strtol, but converts unsigned values. 

**`stdtod` function**

- **Def+F:** strtod() converts a string to double.
- **Ab**
  - **Base:** The strtod() function does only base 10, so it uses just two arguments.

**casting away const Tips**

```c
#include <stdio.h>  /* for NULL definition */
char * strblk(const char * string)
{
    while (*string != ' ' && *string != '\0')
        string++;  /* stops at first blank or null */
    if (*string == '\0')
        return NULL; /* NULL is the null pointer */
    else
        return (char *) string;
}
```

- Here is a second solution that prevents the function from modifying the string but that allows the return v  alue to be used to change the string. The expression (char *) string is called **“casting away const.”**



## Structures



## Pointers



## Class

**Lk:** This notebook > Classes and Objects









# Classes and Objects

**Tips:** Although it should be contained in the Data Type section like `struct` in C guideline notebook, the overloaded volume really requests another H1 section.

## Cc-Classes and Objects

**Cc**

- **Class**
  - A class is a user-defined type

- **Object**
  - an object(such as a variable) is an instance of a class.
  - **Ab**
    - object type

**Cpn**

- the public portion
- the private portion

**F-Class**

- In general, a class defines what data is used to represent an object and the operations that can be performed on that data. 

## Mt-class declaration

- info and operations

## Mt-constructors and destructors

class constructor

destructor

copy constructor

assignment operator

## Mt-pointers to objects

## Mt+Ab-Class Inheritance

**Cc**

- **Def+F:** Class inheritance means that a derived class inherits the features of a base class, enabling you to reuse the base class code. 

**Ab**

- **Polymorphic**: meaning you can write code using a mixture of related classes for which the same method name may invoke behavior that depends on the object type.

#### is-a relationships

- is-a relationships, meaning that a derived object is a special case of a base object.

#### has-a relationships

#### Public inheritance

#### Multiple public inheritance

- **Def+F:** a class can derive from more than one class.

#### Containment

## Mt-Class Template

**Cc**

- **Def+F:** define a class in terms of some unspecified generic type, and then use the template to create specific classes in terms of specific types

## Mt-Friend Classes



## RTTI

- **Def:** a mechanism for identifying object types



# Memory Management System

## Cc-Storage and range terms

scope, linkage, and namespaces

**Block**: The section of program between and including the braces is called a block. 

## Namespaces

**Cc**

- Namespace support is a C++ feature designed to simplify the writing of large programs and of programs that combine pre-existing code from several vendors and to help organize programs.

**F**

- The namespace facility lets a vendor package its wares in a unit called a *namespace* so that you can use the name of a namespace to explicitly indicate which vendor's product you want. 

### Mt-Specify a function within a namespace

```cpp
// Prototype 
Namespace::func();

// General
Microflop::wanda(); 
```

- 

## ANSI C++ Type Qualifiers

### Pcp

- **Idempotent**: You can use the same qualifier more than once in a declaration, and the superfluous ones are ignored.

  ```c
  const const const int n = 6; // same as const int n = 6;
  ```

### Type Qualifiers

`const`

- **Def:** (Constancy) The `const` keyword in a declaration establishes a variable whose value cannot be modified by assignment or by incrementing or decrementing. 

- **Ab**

  - Could be used as function formal parameter
  - A common practice is to capitalize the first character in a name to help remind yourself that Months is a constant
  - Another convention is to make all the characters uppercase; this is the usual convention for constants created using #define.
  - If you don't provide a value when you declare the constant, it ends up with an unspecified value that you cannot modify.

- **Mt-Variable**

  ```c
  const int CON;
  const int CON = 2;
  ```

  - 

- **Mt-Pointer**

  ```c
  const float * pf;  /* pf points to a constant float value */
  float const * pfc;  // same as const float * pf;
  
  float * const pt;  /* pt is a const pointer */
  
  const float * const ptr;
  
  const int arr[10];
  int arr[const];
  ```

  - The value pointer points to is const: The pointer pf points to a value that must remain constant. The value of pf itself can be changed. 
  - The pointer itself is const: The pointer pt itself cannot have its value changed. It must always point to the same address, but the pointed-to value can change.
  - The pointer ptr means both that ptr must always point to the same location and that the value stored at the location must not change.

- **Mt-Global Data**

  - Sharing `const` data across files

    - Follow the usual rules for external variables—use defining declarations in one file and reference declarations (using the keyword extern) in the other files

      ```c
      /* file1.c -- defines some global constants */
      const double PI = 3.14159;
      const char * MONTHS[12] =
      {"January", "February", "March", "April", "May", "June", "July",
      "August", "September", "October", "November", "December"};
      
      /* file2.c -- use global constants defined elsewhere */
      extern const double PI;
      extern const * MONTHS[];
      ```

    - Place the constants in an include file. Here, you must take the additional step of using the static external storage class

      ```c
      /* constant.h -- defines some global constants */
      static const double PI = 3.14159;
      static const char * MONTHS[12] =
      {"January", "February", "March", "April", "May", "June", "July",
      "August", "September", "October", "November", "December"};
      
      /* file1.c -- use global constants defined elsewhere */
      #include "constant.h"
      /* file2.c -- use global constants defined elsewhere */
      #include "constant.h"
      ```

      > - The `static` in header file is to make these files share the same defining declaration of the same identifier.
      > - By making each identifier static external, you actually give each file a separate copy of the data.

    - **Compare**

      - The advantage of the header file approach is that you don't have to remember to use defining declarations in one file and reference declarations in the next; all files simply include the same header file. 
      - The disadvantage is that the data is duplicated for common external variables, which could be a obstacle when dealing  with enormous arrays.



## Memory Management

### Move/Modify the memory

`const` Modifier

**Cc**

- `const` is a method that creates an object, but one whose value cannot be changed.

**Mt**

```c
const int C_VAR = 1;
```

**F**

-  a second way to create symbolic constants—using the `const` keyword to convert a declaration for a variable into a declaration for a constant
-  it lets you declare a type, and it allows better control over which parts of a program can use the constant





# Input/Output

## Cc-I/O

## Mt-Input/Output Facilities

### cin and cout

**`cin` Facility**

```cpp
int carrots;
cin >> carrots;
```

**Soc**

- iostream std namespace

**Cc**

- cin is an object created with the properties of the istream class

**Ab**

-  **Class:** istream
-  It converts input, which is just a series of characters typed from the keyboard, into a form acceptable to the variable receiving the information.
-  cin and cout are guided by the type of variable.



**`cout` Facility**

```cpp
int carrots = 2;

// Concatenate
cout << "Now you have " << carrots << " carrots." << endl;

// Base
int chest = 42;
cout << hex; // manipulator for changing number base
cout << "chest = " << chest << " (hexadecimal for 42)" << endl;

// Put char
char ch = 'X';
cout.put(ch);  // Member Function

// forces output to stay in fixed-point notation
cout.setf();
```

**Soc**

- iostream std namespace

**Cc**

- `cout` is an object created to have the properties of the ostream class.

**Ab**

- **Class:** ostream
- note that the beginning of one string comes immediately after the end of the
  preceding string.
- cout works with both strings and integers even interchangeably.
- cin and cout are guided by the type of variable.

**F**

- Similarly, it provides the <u>dec, hex, and oct</u> manipulators to give cout the messages to display integers in decimal, hexadecimal, and octal formats, respectively.



**`endl` Manipulator**

```cpp
cout << "Pluto is a dwarf planet.\n"; // show text, go to next line
cout << "Pluto is a dwarf planet." << endl; // show text, go to next line

// start a new line
cout << endl;
cout << "\n";
```

**Soc**

- iostream std namespace

**Cc**

- `endl` is a special C++ notation that represents the important concept of beginning a new line. 

**Pcp-Difference**

- One difference is that endl guarantees the output will be flushed (in, this case, immediately displayed onscreen) before the program moves on. You don't get that guarantee with "\n", which means that it is possible on some systems in some circumstances a prompt might not be displayed until after you enter the
  information being prompted for.
- 

**F**

- Inserting endl into the output stream causes the screen cursor to move to the beginning of the next line. 



### Show the auxiliary window

Usually, the IDE lets you run the program in an auxiliary window. Some IDEs close
the window as soon as the program finishes execution, and some leave it open.

To see the output, you must place some additional code at the end of the program:

```c++
cin.get(); // add this statement
cin.get(); // and maybe this, too
return 0;
```

The cin.get() statement reads the next keystroke, so this statement causes the program to wait until you press the Enter key. (No keystrokes get sent to a program until you press Enter, so there's no point in pressing another key.)

The second statement is needed if the program otherwise leaves an unprocessed keystroke after its regular input. 



## File Input/Output



# Functions

## Cc-Function

```cpp
// Function Definition
func_type func_name(arg_type para, arg_type para){
    function body
}
```

**Cc**

- **Def:** A function is a self-contained unit of program code designed to accomplish a particular task.
- You construct C++ programs from building blocks called functions.
- Typically, you organize a program into major tasks and then design separate functions to handle those tasks.

**Cpn-Function Definition**

- **Function header:** The function header is a capsule summary of the function's interface with the rest of the program. The line contains <u>the function name</u> along with information about t<u>he type of information/arguments passed to the function and returned by the function</u>
  - **Function name**
    - a function name can be used: in defining a function, in declaring a function, in calling a function, and as a pointer.
  - **Function Signature:** The function return type together with the function parameter list constitute the **function signature.**
    - Return type/function type (Output)
    - Argument/parameter declaration list (Input)
      - **Arguments**
        - **Def:** The information/items passed to a function
        - **Space:** Enclosed in the parentheses
        - **Ab**
          - Arguments are local variables private to the function.
        - **Pcp**
          - C uses commas to separate arguments to a function. 
          - unlike the case with regular declarations, you can't use a list of variables of the same type in the ();
        - **Cat**
          - In short, the formal parameter is a variable in the called function, and the actual argument is the particular value assigned to the function variable by the calling function.
          - **Formal Argument/formal parameter:** a variable in the function used to hold the value; Declaring an argument creates a variable
            - **Def:** The formal parameter is a variable declared in the header of a function definition.
          - **Actual argument or the Actual parameter**: the function call passes a value, and this value is called the actual argument or the actual parameter
            - **Def:** The actual argument is an expression that appears in the parentheses of a function call. 
            - a constant, a variable, or an even more elaborate expression1
        - **Pcp-Arguments and Parameters**
          - With this C99 convention, we can say that <u>parameters are variables</u> and that <u>arguments are values provided by a function call and assigned to the corresponding parameters</u> 
            - e.g. `int func(para)` `func(arg)`
          - a name defined in one function doesn't conflict with the same name defined elsewhere. 
          - In C language, actual arguments swaps <u>values</u> to formal parameters rather than the actual arguments themselves. 即C语言是值传递而非变量传递。
- **Function body:** The function body represents instructions to
  the computer about what the function should do. The block construct the usage of the function with proper statements within the braces {}.

**Ab**

- **Function Type:** Like variables, functions have types. Any program that uses a function should declare the type for that function before it is used. 
  - If the returned value is of a type different from the declared return type, the value is type cast to the declared type.
- Compilers like to add an underscore prefix to function names—another subtle reminder that they have the last say about your program

**F-Return**

- The actual return value is what you would get if you assigned the indicated return value to a variable of the declared return type.



## Mt-Funciton Prototype

**Tips:** forward declaration/function declaration

```c
#include <stdio.h>

int proto(int numx);  // Prototype informs the compiler that the function called proto() will be used
int proto(int); // only type of the argument could also be accepted.

int proto();  // Forgo function prototype and the check of args

int proto(void);  // function without args

int proto(int arg, ...);  // partial prototyping,  there may be further arguments of an unspecified nature.

int * proto_ptr(int * ptr);  // Pointer


....


int proto(int num_proto){  // Function definition
    printf("%d", num_proto);
}
```

**Soc-History**

- Before ANSI C, C used function declarations that weren't prototypes; they just indicated the name and return type but not the argument types. 

**Cc**

- **Prototype/Function declaration**
  - **Def:** A prototype is a function declaration to the compiler that you are using a particular function and specifies properties, such as return type, its arguments with corresponding types,  of the function.
  - **Space**
    - The function prototype could be used inside other functions or outside the function.
    - In either case, your chief concern should be that <u>the function declaration appears before the function is used.</u>
  - **Pcp**
    - When a function takes arguments, the prototype indicates their number and type by using a comma-separated list of the types. If you like, you can omit variable names in the prototype.(For the reason of function prototype scope)
      - One case in which the names matter a little is with variable-length array parameters. If you use names in the array brackets, they have to be names declared earlier in the prototype.
      - Using variable names in a prototype doesn't actually create variables. It merely clarifies the fact that char means a char variable, and so on.
      - Remember that the variable names in function prototypes are dummy names and don't have to match the names used in the function definition.
    - When using function in multiple files, function prototype is mandatory for the file which use that function but does not contain the function definition.
    - If there is a type mismatch and if both types are numbers, the compiler converts the values of the actual arguments to the same type as the formal arguments(if convertable)
    - if there are nothing within the parentheses, an ANSI C compiler will assume that you have decided to forgo function prototyping, and it will not check arguments. `type func();`
  - **F**
    - In this case, the prototype informs the compiler that proto() expects an int argument. In response, when the compiler reaches the proto(var) expression, it <u>automatically applies a type-cast to the  argument, converting it to a correct type for this argument.</u>
    - a prototype specifies both the type of value <u>a function returns and the types of arguments it expects.</u> Collectively, this information is called <u>the signature of the function.</u> 

- 



## Mt-Function Definition

```c
int func(int para1, float para2){
    //function statements
    return para1;
}
```

> - The parentheses identify `func()` as a function name.
> - The `int` indicates that the `func() `function's return type should be an integer
> - The `void` indicates that `func()` doesn't take any arguments passed along to the function.

- When you define a function, state the type of value it returns.
- Use the keyword return to indicate the value to be returned.



## Mt-Function Call/Invoke/Summon

```c
func_name(argument);
printf("I am a simple ");
```

**Cc**

- **Function Call**: The expression func(args) is termed a function call
- **Calling function**: The original function before invoking another function
- **Called function**: It is the function which be invoked by the calling function.

**Pc**

- Whenever the computer reaches a function calling statement, it looks for the targeted function and follows the instructions there. 
  - The calling function places its arguments in a temporary storage area called the stack, and the called function reads those arguments off the stack.
- When finished with the code within function, the computer returns to the next line of the original calling function—main()



## Return Statement

**Cc-Return**

- **Def+F:** The keyword return causes the value of the following expression to be the return value of the function.

**Return Void**

```c
return;
```


It causes the function to terminate and return control to the calling function. Because no expression follows return, no value is returned, and this form should be used only in a type void function.z





## Mt-inline functions

```c++

```

**Cc**

- **Def:** inline functions can speed program execution at the cost of additional program size



## Mt-Reference variable

**Cc**

- **Def:** Reference variables provide an alternative way to pass information to functions

- 



## Mt-Function overloading

**Cc**

- **Def:** Function overloading lets you create functions having the same name but taking different argument lists



## Mt-Function templates

**Cc**

- **Def:**  function templates, which allow you to specify the design of a family of related functions.



## Mt-friend functions

- Def+F: access class data that’s inaccessible to the world at large

friend member function



## Mt-Virtual function





## Member Function

**Cc-Member Function**

- **Def:** A member function belongs to a class and describes a method for manipulating class data

**Ab**

- You can use a member function only with a particular object of that class
- The period is called the **membership operator**. To use a class member function with an object such as cout, you use a period to combine the object name (cout) with the function name (put())
  - i.e. `cout.put()`



## Cc-General Functions

`main()` function

```cpp
int main()
{
	cout << "main function";
	return 0;
}
```

**Cc**

**Ab**

- In this case, the empty parentheses mean that the main() function takes no information, or in the usual terminology, main() takes no arguments.
- Using the keyword void in the parentheses is an explicit way of saying that the function takes no arguments. 
  - Under C++ (but not C), leaving the parentheses empty is the same as using void in the parentheses. 
  - In C, leaving the parentheses empty means you are remaining silent about whether there are arguments.
- When you run a C++ program, execution always begins at the beginning of the main() function. Therefore, if you don't have main(), you don't have a complete program, and the compiler points out that you haven't defined a main() function.
  - **Exception:** 
    - dynamic link library (DLL) module
    - Programs for specialized environments, such as for a controller chip in a robot, might not need a main().
    - skeleton program calling some nonstandard function

**F-Return/Exit Value**

- If the compiler reaches the end of main() without encountering a return statement, the effect will be the same as if you ended main() with this statement: return 0;
  - **Att:** This implicit return is provided only for main() and not for any other function.
- The normal convention is that an exit value of zero means the program ran successfully, whereas a nonzero value means there was a problem.

**F**

- Ordinarily, a C++ program requires a function called main(). 
- Typically, however, main() is called by startup code that the compiler adds to your program to mediate between the program and the operating system (Unix,Windows 7, Linux, or whatever).
- In effect, the function header describes the interface between main() and the operating system.
- 









# Operators

## Cc-Operators and Operands

- **Operator**
  - **Def:** An operator is a built-in language element that operates on one or more items to produce a value. 
- **Operand 操作数**
  - **Def:** Operands are what operators operate on.
  - **Cat**
    - **Unary operator:** meaning that it takes just one operand
    - **Binary, or dyadic, operators**: operators that require two operands.
    - **Sign Operators:**
      - **Def:** The minus sign can also be used to <u>indicate or to change the algebraic sign of a value.</u>
      - **e.g.** – and +
        - **Att:** plus sign doesn't alter the value or sign of its operand;
          it just enables you to use statements without getting a compiler complaint.
- **Lvalue/left value**
  - **Def**: C uses the term lvalue to mean any such name or expression that <u>identifies a particular data object.</u> 
  - **Ab+Pcp**
    - Object refers to the actual data storage, but an lvalue is a label used to identify, or locate, that storage
    - It specified an object, hence referred to an address in memory.
    - It could be used on the left side of an assignment operator, hence the "l" in lvalue.
    - At this point the standard continued to use lvalue for any expression identifying an object, even though some lvalues could not be used on the left side of an assignment operator.
  - **Cat**
    - **modifiable lvalue/object locator value **: identify an object whose value can be changed
      - **Ab**
        - can be used either on the left side or the right side of an assignment operator
    - **non-modifiable lvalue:** dentify an object whose value can not be changed
      - **e.g.** symbolic constants
- **Rvalue/right value/value of an expression**
  - **Def:** quantities that <u>can be assigned to modifiable lvalues</u> but which are not themselves lvalues
  - **Cat**
    - constants
    - expressions



## Cc-Operator Precedence and Associativity

**Cc**

- **Expression Tree**:  represent the precedence order of evaluation with a type of diagram

**Pcp**

- C does this by setting up an <u>operator pecking order</u>. Each operator is assigned <u>a precedence level</u>
- If two operators that have the same precedence share an operand, they are executed according to t<u>he order in which they occur in the statement.</u>
- For most operators, the order is from left to right. (The = operator was an exception to this.)

**Cpn-Detailed Precedence**

- **Lk:** C Primer Plus > Table 5.1 Operators in Order of <u>Decreasing Precedence</u>
- **Pcp:** 
  - Note that the precedence and associativity rules come into play only when two operators share the same operand. 
  - First, consider the precedences between the operators
  - When two operators have the same precedence, C++ looks at whether the operators have a left-to-right associativity or a right-to-left associativity.
    - The associativity column tells you how an operator associates with its operands
      - Left-to-right associativity means that if two operators acting on the same operand have the same precedence, you apply the left-hand operator first. 
      - For right-to-left associativity, you apply the right-hand operator first.
    - The association rule applies for operators that share an operand. The left-to-right rule applies in this case that the choice does matter.
      - e.g. `1*2/3` the center number `2` is the shared operand and the left-to-right rule should be applied.
  - What precedence does not establish is which of these two multiplications occurs first. C/C++ leaves that choice to the implementation since the choice doesn't affect the final value for this particular example.
    - e.g. `1*2 + 2*3` the first two multiplications do not have precedence.

| Operators(High to Low Precedence)                            | Associativity |
| ------------------------------------------------------------ | ------------- |
| ++ (postfix) --(postfix) ( ) (function call) [ ] {} (compound literal) . -> | Right to Left |
| ++ (prefix) --(prefix) - + ~ ! *(dereference)&(address)      | R-L           |
| sizeof_Alignof  (type) (all unary)                           | Left to right |
| (type name)                                                  | L–R           |
| * / %                                                        | L–R           |
| +(binary) -(binary)                                          | L–R           |
| << >>                                                        | L–R           |
| < > <= >=                                                    | L–R           |
| == !=                                                        | L–R           |
| &                                                            | L–R           |
| ^                                                            | L–R           |
| \|                                                           | L–R           |
| &&                                                           | L–R           |
| \|\|                                                         | L–R           |
| ? :(conditional expression)                                  | R-L           |
| = *= /= %= += -= <<= >>= &= \|= ^=                           | R-L           |
| , (comma operator)                                           | L–R           |

**Add**

- The unary operators * and ++ have the same precedence but associate from right to left. Thus `*arr++` will get the next unit of array rather than get the value of the address add 1.



## Assignment Operator 赋值运算符

`=`

```c
var = num2
```

**Pcp**

- the left side/operand of an assignment operator should be a modifiable lvalue
- The assignments are made from right to left

**F**: assignment statement. Its purpose is to store a value at a memory location.



## Arithmetic Operators 算术运算符

### Basic

```c
int num1, num2;
num1 + num2;  // Plus
num1 - num2;  // Minus
num1 * num2;  // Multiplication
num1 / num2;  // Division
num1 % num2;  // Modulus, works only with integers

#include <math.h>
pow(num1, num2); // exponentiating operator
```

**`%` Modulus Operator**

```c
5 % 2 == 1
```

- **Def+F**:  `%` gives the <u>remainder</u> that results when the integer to its left is divided by the integer to its right
- **Ab**
  - integer arithmetic
- **F**
  - One common use is to help you control the flow of a program. 
  - With the C99 rule in place, you get a negative modulus value <u>if the first operand is negative(Not the second one)</u>, and you get a positive modulus otherwise.
    - Different from the `%`, `/` obey the basic arithmetic rule about division.
    - `x%y = x - (x/y)*y`
- **F-Return**
  - The modulus operator returns the remainder of an integer division.


### Pcp

- **Truncation**: Division works differently for integer types than it does for floating types. Floating-type division gives a floating-point answer, but integer division yields an integer answer.  <u>In C, any fraction resulting from integer division is discarded. This process is called truncation.</u>
  - Notice how integer division <u>does not round to</u> the nearest integer, but always truncates (that is, <u>discards the entire fractional part</u>). 
- **Truncating toward zero** : deciding how integer division with negative numbers worked
  - **Def:** the integer division rounding process is that it just dumps the fractional part when the result of the integer division comes with a fraction part and negative sign.
- **Mixed Types:** When you mixed integers with floating point, the answer
  came out the same as floating point. e.g. 6.3/2
  - Actually, the computer is not really capable of dividing a floating-point type by an integer type, so the compiler converts both operands to a single type.
  - normally you should avoid mixing types.



## Relational Operator 关系运算符

## Logical Operators 逻辑运算符





## Miscellaneous Operators

### The Cast Operator 强制类型转换

`(typeName) variable`

```c
// Prototype
(typeName) value // converts value to typeName type
typeName (value) // converts value to typeName type

// General
int thorn;
long t;
t = (long) thorn;	// 1st; returns a type long conversion of thorn
t = long (thorn);	// 2nd; returns a type long conversion of thorn

// Cast
float float_result = (float)int_l/int_r;  // Cast operator
```

**Cc**

- **Cast/Type Cast**
  - **Def+F**
    - The precise type conversion that you want or else document that you know you're making a type conversion
- **Cast Operator**
  - **Def+F**
    - preceding the quantity with the name of the desired type in parentheses. The parentheses and type name together constitute a cast operator.
  - **St**
    - `(type)var` 

**Ab**

- 强制类型转换并不会直接转换原本的变量类型，应当作是一种运算符作用于变量
  - The type cast doesn't alter the thorn variable itself; instead, it creates a new value of the indicated type, which you can then use in an expression

- The first form is straight C. The second form is pure C++
  - The idea behind the 2nd form is to make a type cast look like a function call. This makes type casts for the built-in types look like the type conversions you can design for user-defined classes.


**e.g.**

- 因为左右操作数同时为`int`，即便结果在数学上是浮点数，但实际上会进行整数运算（例如整除），导致结果只有整数部分，需要通过强制类型转换将其左右操作数中的一个转变为`float`

`static_cast<typeName> (variable)`

```cpp
// Prototype
static_cast<typeName> (value)	// converts value to typeName type

// General
int thorn;
long t;
t = static_cast<long> (thorn);	// 1st; returns a type long conversion of thorn
```

**Cc**

- **Def+F:** the static_cast<> operator, can be used for converting values from one <u>numeric type</u> to another.

**F**

- The static_cast<> operator is more restrictive than the traditional type cast.



### Sizeof Operator 求字节运算符

```c
sizeof(type);
sizeof value;
sizeof var;

// e.g.
int int_var;
sizeof(int);
sizeof (int_var);
sizeof 2;
sizeof int_var;

```

- **Def+F:** `sizeof` gives the memory size of a type/particular value.
- **Obj2-Operand**
  - The operand can be a specific data object, such as the name of a variable, or it can be a type. 
    - If it is a type, such as `float`, the operand must be enclosed in parentheses.
- **Pcp**
  - 其实全用带上括号的就好。Whether you use parentheses depends on whether you want the size of a type or the size of a particular quantity. <u>Parentheses are required for types but are optional for particular quantities or variables.</u>
- **F-Return**
  - `sizeof` returns a value of type `size_t`
  - `size_t` is an unsigned integer type
  - the C header files system can use `typedef` to make `size_t` a synonym for `unsigned int` on one system or for `unsigned long` on another. **Thus, when you use the `size_t` type, the compiler will substitute the standard type that works for your system**



## Mt-Operator Overloading

**Cc**

- **Def+F:** The operator overloading means that a single operator such as `+` might have different meanings with the compiler determining the proper meaning from the context/type of operands.

**Ab**

- The important point here is not the exact function of these operators but that the same symbol can have more than one meaning, with the compiler determining the proper meaning from the context.

**Mt**

- **Lk:**
- C++ extends the operator overloading concept by letting you redefine operator meanings for the user-defined types called classes.

**Cat**

- `&` address operator and bitwise AND operator
- `*` pointer dereference and mulitpilication



## Insertion and Extraction Operator

`<<`

```cpp
cout << "test";
```

**Cc**

- The insertion
  operator (<<), which is defined for the ostream class, lets you insert data into the output
  stream, and 

**Ab**

- 
- **Operator overloading**: In essence, the C++ insertion operator (<<) adjusts its behavior to fit the type of data that follows it.
- **Extensible**: That is, you can redefine the << operator so that cout can recognize and display new data types you develop.

`>>`

```cpp
int var;
cin >> var;
```

**Cc**

- the extraction operator (>>), which is defined for the istream class, lets you
  extract information from the input stream.



# Bit Fiddling

## Obj2 + F

- With C, you can manipulate the individual bits in a variable.

- High-level languages generally don't deal with this level of detail;
  C's ability to provide high-level language facilities while also being able to work at a level typically reserved for assembly language makes it a preferred language for writing device drivers and embedded code.



## Cc-Bits, and Bytes

### Bit: bit, byte, and word

**bit** 比特/位

- **Def+F:** The smallest unit of memory which hold one of two values: 0 or 1

**byte** 字节

- **Def+F:** The usual unit of computer memory
  - C++ defines byte differently. The C++ byte consists of at least enough adjacent bits to accommodate the basic character set for the implementation. That is, the number of possible values must equal or exceed the number of distinct characters
    - In the United States, the basic character sets are usually the
      ASCII and EBCDIC sets, each of which can be accommodated by 8 bits. So the C++ byte is typically 8 bits on systems using those character sets.
    - However, international programming can require much larger character sets, such as Unicode, so some implementations may
      use a 16-bit byte or even a 32-bit byte.
  - the **8-bit byte / octet** is the byte used to describe memory chips and the byte used to describe data transfer rates. 
    - You can think of these 8 bits as being numbered from 7 to 0, left to right. <u>Bit 7 is called the high-order bit, and bit 0 is the low- order bit in the byte.</u>
    - The smallest binary number would be 00000000, or a simple 0. 
    - The largest number this byte can hold is 1, with all bits set to 1: 11111111. The value of this binary number is as follows: 128 + 64 + 32 + 16 + 8 + 4 + 2 + 1 = 255
    - A byte can store numbers from 0 to 255, for a total of 256 possible values. ($2^{8} = 256$ )(unsigned int). Or, by interpreting the bit pattern differently, a program can use a byte to store numbers from –128 to +127. (signed char)
- **Pcp-General:** 1 byte = 8 bits
- **F:** Other memory measurement is based on the byte unit.
  - 1K = 1024 bytes
  - 1M = 1024 K
  - 1G = 1024 M
  - 1T = 1024G
  - etc.

**word**

- **Def:** The natural unit of memory for a given computer design. 

**F-All**

- bit, byte, and word describe units of computer data or to describe units of computer memory

## Cc-Base: decimal, binary, octal and hexadecimal

### Decimal / Base 10

- **Def:** Because our system of writing numbers is based on powers of 10, we say that 2157 is written in base 10.
- **Space:** $x \in 0-9$, $\sum x \times 10^{n}(n \geq 0)$

### Binary / Base 2

- **Def:** Binary refers to a base 2 system. 
- **Space:** $x \in 0-1$, $\sum x \times 2^{n}(n \geq 0)$
- **e.g.**
  - `0111 1011`

### Octal

- **Def:**  Octal refers to a base 8 system.

- **Space:** $x \in 0-7$, $\sum x \times 8^{n}(n \geq 0)$

- **Ab**

  - A handy thing to know about octal is that each octal digit corresponds to three binary digits.

- **Mt-Binary Equivalents for Octal Digits**

  - Tips: Transfer three by three

  | Octal Digit | Binary Equivalent |
  | ----------- | ----------------- |
  | 0           | 000               |
  | 1           | 001               |
  | 2           | 010               |
  | 3           | 011               |
  | 4           | 100               |
  | 5           | 101               |
  | 6           | 110               |
  | 7           | 111               |

  - The only awkward part is that a 3-digit octal number might take up to 9 bits in binary form, so an octal value larger than 0377 requires more than a byte. 
  - Note that internal 0s are not dropped: 0173 is 01 111 011, not 01 111 11

- **e.g.**

  - `173` 

### Hexadecimal

- **Def:** Hexadecimal (or hex) refers to a base 16 system. 

- **Space:** $x \in 0-9+A-F$ , $\sum x \times 16^{n}(n \geq 0)$

  - A represents 10 and  F represents 15
  - In C, you can use either lowercase or uppercase letters for the additional hex digits.

- **Ab**

  - Each hexadecimal digit corresponds to a 4-digit binary number, so two hexadecimal digits correspond exactly to an 8-bit byte.
  - For the 8-bit bytes, the first hex digit represents the upper 4 bits, and the second digit the last 4 bits. This makes hexadecimal a natural choice for representing byte values.

- **Mt-Decimal, Hexadecimal, and Binary Equivalents**

  | Decimal Digit | Hexadecimal Digit | Binary Equivalent |
  | ------------- | ----------------- | ----------------- |
  | 0             | 0                 | 0000              |
  | 1             | 1                 | 0001              |
  | 2             | 2                 | 0010              |
  | 3             | 3                 | 0011              |
  | 4             | 4                 | 0100              |
  | 5             | 5                 | 0101              |
  | 6             | 6                 | 0110              |
  | 7             | 7                 | 0111              |
  | 8             | 8                 | 1000              |
  | 9             | 9                 | 1001              |
  | 10            | A                 | 1010              |
  | 11            | B                 | 1011              |
  | 12            | C                 | 1100              |
  | 13            | D                 | 1101              |
  | 14            | E                 | 1110              |
  | 15            | F                 | 1111              |

  

  - 

- **e.g.**

  - `A3F`, `0xA3F` in C

### Mt-Transfer between different bases based on base 10

##### n -> 10

- $\sum number * n^{power}$ 

- **e.g.**

  -  1101 base 2 to base 10

  $$
  1 \times 2^{3} + 1 \times 2^{2} + 0 \times 2^{1} + 1 \times 2^{0} = 13
  $$

##### 10 -> n

- num % n and collect the remainder until $num \leq n$
- collect the remainder from bottom(right) to top(left) since the higher power should be on the right/bottom side.
- add the prefix.
- **e.g.**
  - 13 base 10 to base 2
    - 13 / 2 = 6%**1**(remainder)  =>  6 / 2 = 3%**0** => 3 / 2 = **1**%**1**
    - 1101



# Debugging

## Cc

- **Driver**: A program designed to test functions to check to see whether functions works is sometimes called a driver.



# Others

Encapsulation





## When IDEs close the output windows...

Some IDEs close the window as soon as the program finishes executionTo see the output, you must place some additional code at the end of the program:

```cpp
cin.get();  // add this statement
cin.get(); // and maybe this, too
return 0;
}


```

> - The cin.get() statement reads the next keystroke, so this statement causes the program to wait until you press the Enter key. (No keystrokes get sent to a program until you press Enter, so there's no point in pressing another key.) 
> - The second statement is needed if the program otherwise leaves an unprocessed keystroke after its regular input. 