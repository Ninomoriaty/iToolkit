Guideline level

<table><tr><td  style="background: smokewhite !important;"><center><font style="font-size:45px; color:lightblue!important;">C Programming Language</b></font></center></td></tr></table>

# Ref

**Textbooks**

- [x] C Primer Plus 6th
- [ ] The C Programming Language 2nd

**Courses**

- [ ] 浙江大学 翁凯 中国大学MOOC
     - Advanced: https://www.icourse163.org/course/ZJU-200001
     - Introductory: https://www.icourse163.org/course/ZJU-199001
- [ ] Harvard CS50: https://cs50.harvard.edu/college/2021/spring
     - Tips: This course is the introduction of computer science rather than C language.
- [ ] 王道论坛 C语言督学训练营 中国大学MOOC
     - Personal Courses.

**Documents**

- [ ] Microsoft C and Cpp documents: https://docs.microsoft.com/en-us/cpp/?view=msvc-170





# C

## Soc

- Dennis Ritchie of Bell Labs created C in 1972 as he and Ken Thompson worked on designing the Unix operating system from Thompson's B language,



## C Standards

**Cpn**

- C Language
  - C keywords
  - Expressions and Statements
- C Preprocessor

**Cat**

- **K&R C or Classic C**
  - a de facto standard accepted by community based on the book *The C Programming Language* by Brian Kernighan and Dennis Ritchie (1978)
  - defined the C language, it did not define the C library
- **The First ANSI/ISO C Standard (C89 or C90)**
  - defined both the language and a standard C library
  - Keep the spirit of C
- **The C99 Standard**
  - Internationalization, support international programming by, for example, providing ways to deal with international character sets. 
  - Correction of deficiencies, A second goal was to "codify existing practice to address evident deficiencies."
  - Improvement of computational usefulness, A third goal was to improve the suitability of C for doing critical numeric calculations for scientific and engineering projects
- **The C11 Standard**
  - face of contemporary concerns of programming security and safety
  - the standard was being revised in order to track new technologies



## Cc-C language

### Cc-General

**Project:** A project describes the resources a particular program uses. The resources include your source code files.

- project oriented: IDE

**C program:** The C scripts or C libraries for main frameworks

- **Def:** A program is a series of statements with some necessary punctuation.
- **St**
  - expression > statement > program

**C compiler**: Programs that convert your C code into the instructions a computer uses internally
- **Compiler:** The **compiler** is a program that translates the high-level language program into the detailed set of machine language instructions the computer requires.

**Low-level language**

- **Def**
  - Low-level language—that is, it works directly with the hardware (for instance, accessing CPU registers and memory locations directly).
- **Ab**
  - Thus, assembly language is specific to a particular computer processor.

**High-level language**

- **Def**
  - A high-level language is oriented toward problem solving instead of toward specific hardware. 
- **Ab**
  - Special programs called compilers translate a high-level language to the internal language of a particular computer.

**Data**

- The data constitutes the information a program uses and processes.

**Algorithms**

- The algorithms are the methods the program uses

**Library**

- Libraries are collections of programming modules that you can call up from a program.

### Cc-Paradigm

**Object-oriented programming:** Object-oriented programming is a philosophy that attempts to mold the language to fit a problem instead of molding the problem to fit the language.

- C is <u>not</u> an object-oriented programming language but a procedural programming language. 

- However, C++ is an object-oriented programming language

**Generic programming:** In programming, the term generic programming indicates code that is not specific to a particular type but which, once a type is specified, can be translated into code for that type. 
- C++ has Template feature to fit this programming model.

**Procedural language**

- **Def:** Procedural language consists of figuring out the actions a computer should take and then using the programming language to implement those actions.
- That means it emphasizes the algorithm side of programming.
- **Cat**
  - Fortran、BASIC、C are procedural languages
- **Problem**
  - tangled routing or spaghetti programming

**Structured programming**

- **Def:** Structured Programming is a programming paradigm aimed at improving computer programs' clarity, quality, and development time by extensively using subroutines, block structures, for loops, and while loops.
- **Ab**
  - the structured programming techniques reflect a procedural mind-set,
    thinking of a program in terms of the actions it performs.
- C is a structured programming language
- C includes features to facilitate this approach. For example, structured programming limits branching (choosing which instruction to do next) to a small set of well-behaved constructions.
- C incorporates these constructions (the for loop, the while loop, the do while loop, and the if else statement) into its vocabulary.

**Concurrent programming:** Concurrent programming divides program execution into threads that may be executed in parallel.

**Top-down design**

- the idea is to break a large program into smaller, more manageable tasks. If one of these tasks is still too broad, you divide it into yet smaller tasks. You continue with this process until the program is compartmentalized into small, easily programmed modules. 

**Bottom-up programming**

- The process of going from a lower level of organization, such as classes, to a higher level, such as program design, is called bottom-up programming.


### Keywords

**Lk:** 

**Cc**

- **Keywords:** The words used to express a language, and you can't use them for other purposes





## Ab-C language

### Strengths

- **High-level programming languages**
- case sensitive
- **Efficiency**
  - compact and to run quickly
  -  fine control usually associated with an assembly language
  -  fine-tune your programs for maximum speed or most efficient use of memory
- **Portability**
  - C is a portable language
    - simply changing a few entries in a header file accompanying the main program
  - The portions of a program written specifically to access particular hardware devices or systems typically are not portable.
- **Power and Flexibility**
- **Programmer Oriented**
- **Procedural Language** 
  - That means it emphasizes the algorithm side of programming
  - procedural programming consists of figuring out the actions a computer should take and then using the programming language to implement those actions


### Shortcomings

- This flexibility is both an advantage and a danger. make mistakes that are impossible in some languages
- C' s conciseness, combined with its wealth of operators, make it possible to prepare code that is extremely difficult to follow
- programming errors that are difficult to trace



## St-C Program

### St

**Lk:** 

- Figure 1.4 Compiler and linker. *C Primer Plus 6th*
- Figure 1.5 Preparing a C program using Unix. *C Primer Plus 6th*



### Cpn

**Source code files** 

- **Def**
  - The text file containing the program written in the c language
- **Ab** 
  - **Filename**
    - C source code files end with .c as their extension
    - **Basename:** The part of the name before the period(".")
    - **Extension:** The part of the name after the period(".")
  - **Extension:** .c

**Object code file, or object file** 

- **Def**
  - The intermediate files transformed from the source code files by a c language compiler 
- **Soc+Obj1**
  - Compiler
- **Ab** 
  - The object file contains machine language code, it is not ready to run. The object file contains the translation of your source code, but it is not yet a complete program.
  - **Extension:** .o

**Executable files**

- **Def**
  - The machine executable object code files with startup code and library routines.
- **Soc+Obj1**
  - Linker
- **Ab**
  - **Extension:** .out

- **Cpn**
  - **Startup code**
    - **Def:** The code that acts as an interface between your program and the operating system
    - **Ab:** System dependence
  - **Library code**
    - **Def:**  Make use of library routines (called functions) that are part of the standard C library, which contains object code for many functions
    - **Ab:** The linker extracts only the code needed for the functions you
      use from the library
  - **Object code**
    - **Lk:** Object code file





## Mt-Programming

### 1. Define the Program Objectives

- start with a clear idea of what you want the program to do
- target user
- general function of the program

### 2. Design the Program

- Jot down the objectives of your program and to outline the design
- decide how the program will go about it
  - user interface 
  - program organization
  - calculation complex
  - information/data representation

### 3. Write the Code

- Implement the program
- Document your work
  - commentary
  - work project
  - guide/plan documents
  - The documentation for a particular C implementation should include a description of the functions in the C library. These function descriptions identify which header files are needed. 

### 4. Compile

**Cc**

- compile the source code file
  - **Compiler, compiling:** convert the source code to intermediate code
    - compilers are often combined with linkers
  - **Linker, linking:** the linker combines this with other code modules to produce the executable file as well as incorporate code from precompiled C libraries into the final program


**Compile on a Linux system**

```bash
# gcc
gcc -std=c99 source_code.c  # compile the source code with proper C standards
./a.out  # execute the executable code

# clang
## TODO: add Pc

# cc
cc source_code.c  # compile the source code
./a.out  # execute the executable code

# Compile more c source code files.
cc source1.c source2.c  # Other compilers should also provide a similar method.
./a.out  # execute the compound executable code
# There will also be source1.o and source2.o.
# if you change one of the file, you can combined them like this.
cc source1.c source2.o


# vsccode
./source_code  # After press the "Run the C/C++ file" button, executable output 
```

- Unix has a `make` command that automates management of multi-file programs

### 5. Run the Program

you can make the program pause until you press the Enter key.

```c
getchar();  // before the return
```

### 6. Test and Debug the Program

-  check to see that your program does what it is supposed to do
- Debugging is the process of finding and fixing program errors

### 7. Maintain and Modify the Program

- maintain the original functions and find out more improvements with the responses
- renew or extend the functions and add new features
- Portable, adapt the program to different computer systems honorable 



## Locale

**Lk:** C Primer Plus > Appendix B, Section V > The Standard ANSI C Library with C99 Additions

- **Locale:** gives a C program the option of choosing a particular locale/localization for particular conventions.
- The C standard requires but two locales: "C" and "". By default, programs use the "C" locale which, basically, is U.S. usage. The "" locale stands for a local locale in use on your system.
- In practice, operating systems such as Unix, Linux, and Windows offer long lists of locale choices.



## Cc-Function-prototyping mechanism

- C now has a function-prototyping mechanism that checks whether a function call has the correct number and correct kind of arguments



## F-C language

- C is still an important language in its own right, as well a migration path to these others



## Cc-Other Info

### Cc-Other

- **小端存储：**n进制内存表示低位在前，高位在后 （以一个字节为一位的情况）
  - **Cat:** 英特尔CPU









# C Script

## Pcp-C Programming

### Pcp-Name choice

- **Case-sensitive**
- The characters at your disposal are lowercase letters, uppercase letters, digits, and the underscore (_)
  - The first character must be a letter or an underscore
  - Operating systems and the C library often use identifiers with one or two initial underscore characters, such as in `_kcab`, so it is better to avoid that usage yourself.
    - The standard labels beginning with one or two underscore characters, such as library identifiers, are reserved. Although it is not a syntax error to use them, it could lead to name conflicts.
- C99 and C11 consider the first 63 characters as significant.
  - Actually, you can use more than the maximum number of characters, but the compiler isn't required to pay attention to the extra characters
- If the name doesn't suffice, use comments to explain what the variables represent. Documenting a program in this manner is one of the basic techniques of good programming.
- **Systematic Conventions** 
  - Assigning variable names in which the name indicates the type of variable
    - e.g. `int_varname`

### Pcp-Documentation

- Begin your C program with a comment (using the new comment style) identifying the filename and the purpose of the program.

### Pcp- Other tips to make your program readable

- using blank lines to separate one conceptual section of a function from another.
- use one line per statement

### Pcp-Modularity

- a program should be broken into separate units, with each unit having one task to perform.
- Functions could be a method to modularize the computational cores and input validations.

### Pcp-Information

- Third, a good practice is to have a program repeat or “echo” the values it has just read in. This helps ensure that the program is processing the data you think it is.

### Pcp-Function Order

- Because main() usually provides the overall framework for a program, it's best to show main() first with proper forward declarations.

### Pcp-Nesting

-  the C99 standard requires that a compiler support a minimum of 127 levels of nesting.

### Pcp-Programming

- One of the golden rules of protective programming is the “need to know” principle. 
  - Keep the inner workings of each function as private to that function as possible, sharing only those variables that need to be shared. 
  - The other storage classes are useful, and they are available. Before using one, though, ask yourself whether it is necessary.

### Pcp-Program Design

#### Data Representation

- What data form would you use to store information?
  -  The actions you want to perform might affect how you decide to store the information. In short, you have a lot of design decisions to make before
    plunging into coding.
- 







## St

**Lk:** Figure 2.1 Anatomy of a C program. *C Primer Plus 6th*



## Mt-Tips Collection

- How to read only the first character? or remove the final \n
  - **Lk:** C Primer Plus > Listing 7.11 The animals.c Program





# C Preprocessor Instruction

**Lk:** Chapter 16 *C Primer Plus 6th*

## Cc-Preprocessor

**Translation phases** 

- **t:** before jumping into preprocessing.

- **F + Pc**

  - The compiler starts its work by <u>mapping characters appearing in the source code to the source character set.</u> 

    - This takes care of multibyte characters and trigraphs—character extensions that make the outer face of C more international

  - Second, the compiler locates each instance of <u>a backslash`\` followed by a newline character</u> and deletes them. That is, two physical lines are converted to a single logical line 

    ```c
    // Physical lines
    printf("That's wond\
    erful!\n");
    
    // Logical line
    printf("That's wonderful\n!");
    ```

    - Note that in this context, "newline character" means the character produced by pressing the Enter key to start a new line in your source code file; it doesn't mean the symbolic representation `\n`.
    - This feature is useful as a preparation for preprocessing because preprocessing expressions are required to be one logical line long, but that one logical line can be more than one physical line.

  - Next, the compiler breaks the text into a sequence of preprocessing tokens and sequences of whitespace and comments.

    - One point of interest now is that each comment is replaced by one space character
    - Also, an implementation may choose to replace each sequence of whitespace characters (other than a newline) with a single space. 

  - Finally, the program is ready for the preprocessing phase, and the preprocessor looks for potential preprocessing directives, indicated by a # symbol at the beginning of a line.

**Preprocessing and Preprocessor:** 

- **Def-Preprocessing:** In general, C compilers perform some preparatory work on source code before compiling
- **t:** The preprocessor looks at your program before it is compiled.
- **Pcp**
  - The preprocessor looks for independent tokens (separate words) and skips embedded words
    - e.g. That is, the preprocessor doesn't replace PINT_MAXIM with P32767IM even if there is a symbolic constant called INT_MAX.
- **F**
  - Following your preprocessor directives, the preprocessor replaces the symbolic abbreviations in your program with the directions they represent. 
  - The preprocessor can include other files at your request, and it can select which code the compiler sees.
  - the C preprocessor should perform, establishes which functions form the standard C library, and details how these functions work
  - The preprocessor provides several directives that help the programmer produce code that can be moved from one system to another by changing the values of some #define macros. 

**Header** 
- **Def:** A collection of information that goes at the top of a file termed header.
- **F**
  - header files contain information used by the compiler to build the final executable program. 
  - Header files help guide the compiler in putting your program together correctly
  - C divides the C function library into families of related functions and provides a header file for each family

**Tokens**

- **Def:** tokens are groups separated from each other by spaces, tabs, or line breaks
  - C preprocessor tokens are the separate “words” in the body of a macro definition

- **Ab**
  - **Space:** in the body of a macro definition
  - The extra spaces would be part of the replacement
    - For instance, `#define FOUR 2 *          2`  `FOUR`will be interpreted as  `2 * 2`
    - In other words, the character string interpretation views the spaces as part of the body, but the token interpretation views the spaces as separators between the tokens of the body.
    -  In practice, some C compilers have viewed macro bodies as strings rather than as tokens. The difference is of practical importance only for usages more intricate than what we're attempting here.

  - Incidentally, the C compiler takes a more complex view of tokens than the preprocessor does. The compiler understands the rules of C and doesn't necessarily require spaces to separate tokens. 
    - For example, the C compiler would view 2*2 as three tokens because it recognizes that each 2 is a constant and that * is an operator.

**Definition**

- When the preprocessor encounters an identifier in a preprocessor directive, it considers it to be either defined or undefined. 
- **Def-Defined** 
  - Here, defined means defined by the preprocessor. 
- **Ab**
  - If the identifier is a macro name created by a prior #define directive in
    the same file and it hasn't been turned off by an #undef directive, it's defined. 
  -  If the identifier is not a macro but is, say, a file-scope C variable, it's not defined as far as the preprocessor is concerned.
  - Note that the scope of a #define macro extends from the point it is declared in a file until it is the subject of an #undef directive or until the end of the file, whichever comes first. 
  - A few predefined macros, such as `__DATE__` and `__FILE__`  are always considered defined and cannot be undefined




## Cc-C preprocessor directive

**Def**

**Ab**
- The ANSI and subsequent standards permit the # symbol to be preceded by spaces or tabs, and it allows for space between the # and the  remainder of the directive. 
  - However, older versions of C typically require that the directive begin in the leftmost column and that there be no spaces between the # and the remainder of the directive
- Preprocessor directives run until the first newline following the #. That is, a directive is limited to one line in length.
  - However, as mentioned earlier, the combination backslash/newline is
    deleted before preprocessing begins, so you can spread the directive over several physical lines. These lines, however, constitute a single logical line.
- **Space:** A directive can appear anywhere in the source file, and the definition holds from its place of appearance to the end of the file.



## #define directive  `#define` for Manifest Constants / Macro

```c
// object-like macros
#define SC 100
#define TWO 2  /* you can use comments if you like */
#define OW "Consistency is the last refuge of the unimagina\
tive. - Oscar Wilde" /* a backslash continues a definition */
                    /* to the next line */
#define FOUR TWO*TWO
#define PX printf("X is %d.\n",x);
#define FMT "X is %d.\n"

//function-like macros
#define MEAN(X,Y) (((X)+(Y))/2)
MEAN(x+2, y-2);  // (((X+2)+(Y-2))/2)
// Pecular problems
#define SQUARE(X) X * X  
SQUARE(x+2);  // x + 2 * x + 2
SQUARE(++x); // ++x * ++x(increment again)

```

### Cc

- **Lk:** This notebook > `#define` Manifest Constants
- **Def:** The #define preprocessor directive, like all preprocessor directives, begins with the # symbol at the beginning of a line.
- **St**
  - Each #define line (logical line, that is) has three parts. 
  - The first part is the #define **directive** itself. 
  -  The second part is your chosen abbreviation, known as a **macro**/macro name/macro identifier. 
    - The **macro name** must have no spaces in it and it must conform
      to the same naming rules that C variables follow: Only letters, digits, and the underscore (_) character can be used, and the first character cannot be a digit.
      - Remember that there are no spaces in the macro name, but that spaces can appear in the replacement string. ANSI C permits spaces in the argument list.
      - Use capital letters for macro function names. This convention is not as widespread as that of using capitals for macro constants. However, one good reason for using capitals is to remind yourself to be alert to possible macro side effects.
    - Some macros, like these examples, represent values; they are called **object-like macros**
    - **function-like macros**: A macro <u>with macro arguments</u> looks very similar to a function because the arguments are enclosed within parentheses
      - Function-like macro definitions have one or more arguments in parentheses, and these arguments then appear in the replacement portion.
      - A function call passes the value of the argument to the function while the program is running. A macro call passes the argument token to the program before compilation without calculation; it's a different process at a different time. 
      - The lesson here is to use as many parentheses as necessary to ensure that operations and associations are done in the right order.
      - Note that the X in the quoted string is treated as ordinary text, not as a token that can be replaced.
    
  - The third part (the remainder of the line) is termed **the replacement list or body** (see Figure 16.1). 
    - Technically, the body of a macro is considered to be a string of tokens rather than a string of characters
    - 
- **Pcp + Ab**

  - without semicolon`;`
  - **Name Conventions**
    - It is a sensible C tradition to **capitalizing constants in uppercase.**
    - Naming conventions include prefixing a name with a c_ or k_ to indicate a constant, producing names such as c_level or k_line.
    - You can use uppercase and lowercase letters, digits, and the underscore character. The first character cannot be a digit. 
  - **Compile-time substitution/macro expansion.** The constant name will be directly substituted by the value during the Preprocessing process before compiling. By the time you run the program, all the substitutions have already been made. 
    - When the preprocessor finds an example of one of your macros within your program, it almost always replaces it with the body literally. 
    - The one exception to replacement is a macro found within double quotation marks. It remains the original string rather than replaces the macro name within the double quotation.
  - Here you see that a macro can express any string, even a whole C expression. Note, though, that this is a constant string; PX will print only a variable named x.
  - a macro definition can include other macros
  - Calculations and expressions in replacement lists are permitted. 
    - However, the actual calculation takes place not while preprocessor works, but during compilation, because the C compiler evaluates all constant expressions (expressions with just constants) at compile time.
    - The preprocessor does no calculation; it just makes the suggested substitutions very literally.
  - **Lk:** This notebook > Cc-Preprocessor > Definition
- **F**
  - When should you use symbolic constants? You should use them for most numeric constants.
    - The name of symbolic constants can contain more information about the value than a number does.
    - Accessible and changeable
  - Mnemonic value, easy alter ability, portability—these features all make symbolic constants worthwhile.
  - Compared with const keyword, #define symbolic constants make sure that the result must be a constant expression while const constants are not required to be valid.
    - This is one respect in which C++ differs from C; in C++ you can use const values as part of constant expressions.
- **Pcp-Choice between functions and macros**
  - Macros are somewhat trickier to use than regular functions because they can have odd side effects if you are unwary.
  - The macro-versus-function choice represents a trade-off between time and space. 
    - A macro produces inline code; that is, you get a statement in your program. If you use the macro 20 times, you get 20 lines of code inserted into your program. 
    - If you use a function 20 times, you have just one copy of the function statements in your program, so less space is used.
    -  On the other hand, program control must shift to where the function is and then return to the calling program, and this takes longer than inline code.
  - Macros have an advantage in that they don’t worry about variable types. (This is because they deal with character strings, not with actual values.) Therefore, the SQUARE(x) macro can be used equally well with int or float.
  - If you intend to use a macro instead of a function primarily to speed up a program, first try to determine whether it is likely to make a significant difference.
    - A macro that is used once in a program probably won't make any noticeable improvement in running time.
    - A macro inside a nested loop is a much better candidate for speed improvements.
    - Many systems offer program profilers to help you pin down where a program spends the most time.
  - 


### Mt-Redefining Constants

**Cc**

- **Redefining a constant:** Suppose you define LIMIT to be 20, and then later in the same file you define it again as 25. This process is called **redefining a constant**. 

**Ab**

- Implementations differ on redefinition policy.
  - Some consider it an error unless the new definition is the same as the old. 
    - The ANSI standard takes the first view, allowing redefinition only if
      the new definition duplicates the old.
  - Others allow redefinition, perhaps issuing a warning.
- Having the same definition means the bodies must have the same tokens in the same order.
- If you do have constants that you need to redefine, it might be easier to use the const keyword and scope rules to accomplish that end.

### Mt- Creating Strings from Macro Arguments: The # Operator

```c
#define PSQR(x) printf("The square of " #x " is %d.\n",((x)*(x)))
```

- **Lk:** C Primer Plus > Listing 16.3 The subst.c Program
- **Def+F:** Within the replacement part of a function-like macro, the # symbol becomes a preprocessing operator that converts <u>tokens into strings.</u>
- **F:** For example, say that x is a macro parameter, and then #x is that parameter name converted to the string "x". This process is called **stringizing**.

### Mt-Preprocessor Glue: The ## Operator

```c
#define XNAME(n) x ## n
```

- **Ab**
  - Like the # operator, the ## operator can be used in the replacement section of a function-like macro. 
  - Additionally, it can be used in the replacement section of an object-like macro. 
- **F:** The ## operator combines two tokens into a single token, which could combine tokens into a new identifier. 

### Mt-Variadic Macros: `...` and `__VA_ARGS__`

```c
#define PR(...) printf(_ _VA_ARGS_ _)
PR("Howdy");
PR("weight = %d, shipping = $%.2f\n", wt, sp);

#define PR(X, ...) printf("Message " #X ": " _ _VA_ARGS_ _)
```

- **Cc**
  - `__VA_ARGS__`: the predefined macro `__VA_ARGS__` can be used in the substitution part to indicate what will be substituted for the ellipses
  - `...`: The variadic macro argument

- **Ab**
  - Don’t forget, the ellipses have to be the last macro argument

- **F:** The idea is that the final argument in an argument list for a macro definition can be ellipses (that is, three periods). 

### Cat-Predefined Macros

**Standard**

| Macro              | Meaning                                                      |
| ------------------ | ------------------------------------------------------------ |
| `__DATE__`         | A character string literal in the form “Mmm dd yyyy” representing the date of preprocessing, as in Nov 23 2013 |
| `__FILE__`         | A character string literal representing the name of the current source code file |
| `__LINE__`         | An integer constant representing the line number in the current source code file |
| `__STDC__`         | Set to 1 to indicate the implementation conforms to the C Standard |
| `__STDC_HOSTED__`  | Set to 1 for a hosted environment; 0 otherwise               |
| `__STDC_VERSION__` | For C99, set to 199901L; for C11, set to 201112L             |
| `__TIME__`         | The time of translation in the form “hh:mm:ss”               |

**Others**

- `__func__`. It expands to a string representing the name of the function containing the identifier. 
  - For this reason, the identifier has to have function scope, whereas macros essentially have file scope. Therefore, `__func__` is a C language predefined identifier rather than a predefined macro.





## #include directive  `#include` 

```c
#include <stdio.h>
#include "fun.h"
```

- **Lk:** This notebook > C Library
- **Def:** #include statement: a cut-and-paste operation sharing information that is common to many programs.
- **Mt**
- **Cpn-Common forms of header file contents**
  - Manifest constants—A typical stdio.h file, for instance, defines EOF, NULL, and BUFSIZ (the size of the standard I/O buffer).
  - Macro functions—For example, getchar() is usually defined as getc(stdin), getc() is usually defined as a rather complex macro, and the ctype.h header typically contains macro definitions for the ctype functions.
  - Function declarations—The string.h header (strings.h on some older systems), for example, contains function declarations for the family of string functions. Under ANSI C and later, the declarations are in function prototype form.
  - Structure template definitions—The standard I/O functions make use of a FILE structure containing information about a file and its associated buffer. The stdio.h file holds the declaration for this structure.
  - Type definitions—You might recall that the standard I/O functions use a pointer-to-FILE argument. Typically, stdio.h uses a #define or a typedef to make FILE represent a pointer to a structure. Similarly, the size_t and time_t types are defined in header files
  - External variable--you can use header files to declare external variables to be shared by several files. 
  - Internal variable--a variable or array with file scope, internal linkage, and const qualification.
    - The const part protects against accidental changes
    - The static part(internal) means that each file including the header gets its own copy of the constants so that there isn't the problem of needing one file with a defining declaration and the rest with
      reference declarations.

- **F**
  - When the preprocessor spots an #include directive, it looks for the following filename and includes the contents of that file within the current file. 
  - The #include directive in your source code file is replaced with the text from the included file.
  - Including a large header file doesn't necessarily add much to the size of your program. The content of header files, for the most part, is information used by the compiler to generate the final code, not material to be added to the final code.

### Mt-Use header file to manage multiple files

```c
// a header file called fun.h

```

- <u>Store the function prototypes in a header file.</u> to prototype these functions when including the header file in different source code files.
- <u>Place the #define directives in a header file</u> and then use the #include directive in each source code file.
- Recall that in the Unix and DOS environments, the double quotes in the
  directive #include "hotels.h" indicate that the include file is in the current working directory (typically the directory containing the source code).
- This header file includes many of the kinds of things commonly found in header files: 
  - #define directives
  - structure declarations
  - typedef statements
  - function prototypes.
  - Note that none of these things are executable code; rather, they are information that the compiler uses when it creates executable code.


### Mt-Include Directives and Header Files

```c
#include <stdio.h>  // Standard C library, Searches directive>> system directories

#include "funcs.h"  // Searches your current working directory
#include "/usr/biff/p.h" //Searches the /usr/biff directory
#include "list_16_7_name_st.c"
```

> - This line tells the compiler to include the information found in the file stdio.h, which is a standard part of all C compiler packages; 
> - Enclosing the filename in double quotation marks instead of in angle brackets instructs the compiler to look locally for the file instead of in the standard locations the compiler uses for the standard header files. 

- On a Unix system, the angle brackets tell the preprocessor to look for the file in one or more standard system directories. 
- The double quotation marks tell it to first look in your current directory (or some other directory that you have specified in the filename) and then look in the standard places
- In general, the method used to name files is system dependent, but the use of the angle brackets and double quotation marks is not.



## #undef Directive `#undef`

```c
#define LIMIT 400
#undef LIMIT
```

- **Def+F:** The #undef directive cancels an earlier #define definition. 
  - The #undef directive "undefines"/removes a given #define definition
- **Ab**
  - Even if LIMIT is not defined in the first place, it is still valid to undefine it.
- 





## #if Directives `#if`

Tips: This section includes #if, #ifdef, #ifndef, #else, #elif, and #endif directives

```c
#ifdef MAVIS
	#include "horse.h" // gets done if MAVIS is #defined
	#define STABLES 5
	printf("Choice1");
#else
	#include "cow.h" // gets done if MAVIS isn't #defined
	#define STABLES 15
	printf("Choice2");
#endif


#ifndef SIZE
	#define SIZE 100
#endif

#if SYS == 1
	#include "ibmpc.h"
#elif SYS == 2
	#include "vax.h"
#elif SYS == 3
	#include "mac.h"
#else
	#include "general.h"
#endif
```

**Cc**

-  **Conditional compilations**: That is, you can use them to tell the compiler to accept or ignore blocks of information or code according to conditions at the time of compilation.
- The #if, #ifdef, #ifndef, #else, #elif, and #endif directives allow you to specify different alternatives for which code is compiled.

**Ab**

-  Here we've used the indentation allowed by newer implementations and by the ANSI standard. 
   -  If you have an older implementation, you might have to move all the directives, or at least the # symbols to flush left
-  The main difference between C if else and #ifdef #else is that the preprocessor doesn't recognize the braces ({}) method of marking a block, so it uses the #else (if any) and the #endif (which must be present) to mark blocks of directives. 
   -  **#ifdef**: if the following identifier (MAVIS) has been <u>defined</u> by the preprocessor, follow all the directives and compile all the C code up to the next #else or #endif, whichever comes first. 
   -  **#ifndef**: The #ifndef asks whether the following identifier is not defined; #ifndef is the negative of #ifdef. 
   -  **#if**: The #if directive is more like the regular C if. It is followed by a constant integer expression that is considered true if nonzero, and you can use C's relational and logical operators with it
      -  `#if defined(VAX)` is equal to `#ifdef VASX`
      -  `defined` is a preprocessor operator that returns 1 if its argument is #defined and 0 otherwise. 
   -  **#elif**: You can use the #elif directive (not available in some older implementations) to extend an if-else sequence. 
   -  **#else**: If there is an #else, everything from the #else to the #endif is done if the identifier isn't defined.
-  These conditional structures can be nested. 

**F**

-  You can use this approach, for example, to help in program debugging. 
   -  Use the condition macro definition as the switch of debugger, which decides whether includes the debugging program section.
-  Use #ifdef to select among alternative chunks of codes suited for different C implementations.
-  Typically, this idiom is used to prevent multiple definitions of the same macro when you include several header files, each of which may contain a definition. 
   -  In this case, the definition in the first header file included becomes the active definition and subsequent definitions in other header files are ignored.
-  prevent multiple inclusions of a file. 
   -  The standard C header files use the #ifndef technique to avoid multiple inclusions
   -  Why would you include a file more than once? 
      -  The most common reason is that many include files include other files, so you may include a file explicitly that another include file has already included.

   -  Why is this a problem? 
      -  Some items that appear in include files, such as declarations of structure types, can appear only once in a file. 

-  One use for these conditional compilation features is to make a program more portable.
   -   you can set up different values and include different files for different systems.



## #line Directive`#line`

```c
#line 1000 // reset current line number to 1000
#line 10 "cool.c" // reset line number to 10, file name to cool.c
```

- **Def + F**: The #line directive lets you reset line and file information
  - The #line directive lets you reset the line numbering and the filename as reported by the `__LINE__` and `__FILE__` macros
  - 



## #error directive `#error`

```c
#error Not C11
```

- **Def + F**: the #error directive lets you issue error messages
  - The #error directive causes the preprocessor to issue an error message that includes any text in the directive.



## #pragma directive `#pragma`

```c
#pragma c9x on // turn on C9X support, including C99

#pragma nonstandardtreatmenttypeB on
_Pragma("nonstandardtreatmenttypeB on")
#define PRAGMA(X) _Pragma(#X)
```

- **Def + F:** the #pragma directive lets you give instructions to the compiler.
  - The #pragma lets you place compiler instructions in the source code.
- **Ab**
  - Generally, each compiler has its own set of pragmas.
  - C99 also provides the `_Pragma` preprocessor operator. It converts a string into a regular pragma.
    - Because the operator doesn't use the # symbol, you can use it as part of a macro expansion
    - Tips
      - The compiler doesn't concatenate strings until after preprocessing is complete. Thus the _Pragma should not rely on string concatenation.
      - The _Pragma operator does a complete job of “destringizing”; that is, escape sequences in a string are converted to the character represented. 



## Generic Selection (C11)

### Cc-Generic Selection

- **Def:** C11 adds a new sort of expression, called a generic selection expression, that can be used to select a value on the basis of the type of an expression, that is, on whether the expression type is int, double, or some other type. 

### Ab

- The generic selection expression is not a preprocessor statement, but its usual use is a part of a #define macro definition that has some aspects of generic programming.

### Mt- _Generic

```c
_Generic(x, int: 0, float: 1, double: 2, default: 3)
    
#define MYTYPE(X) _Generic((X),\
int: "int",\
float : "float",\
double: "double",\
default: "other"\
)
    
// generic square root function
#define SQRT(X) _Generic((X),\
    long double: sqrtl, \
    default: sqrt, \
    float: sqrtf)(X)
    
// generic sine function, angle in degrees
#define SIN(X) _Generic((X),\
    long double: sinl((X)/RAD_TO_DEG),\
    default: sin((X)/RAD_TO_DEG),\
    float: sinf((X)/RAD_TO_DEG)\
)
```

**_Generic is a new C11 keyword**
- **Cpn**
  - The first term is an expression for further calling of the generic function
  - Each remaining item is a type followed by a colon followed by a value
  - The type of the first term is matched to one of the labels, and the value of the whole expression is the value following the matched label.
  - If the type doesn't match a label, the value associated with the default: label is used for the whole expression.
- **Ab**
  - When evaluating a generic selection expression, the program does not evaluate the first term; it only determines the type. And the only expression it does evaluate is the one with the matching label.
- **Mt-how to get a macro using _Generic to act like a function**
  - Each labeled value is a function call, so the value of the `_Generic` expression is a particular function call, such as `sinf((X)/RAD_TO_
    DEG)`, with the argument to `SIN()` replacing the `X`.
  - In this case the value of the `_Generic` expression is the name of a function, such as sinf. The name of a function is replaced by the address of the function, so <u>the value of the `_Generic` expression is a pointer to a function</u>.
    - However, the entire _Generic expression is followed by (X), and the combination of **function-pointer(argument)** calls the pointed-to function with the indicated argument.

  - In short, for SIN(), the function call is inside the generic selection expression, while for SQRT() the generic selection expression evaluates to a pointer, which is then used to invoke a function.

- **F**
  - You can use _Generic to define macros that act like type-independent (“generic”) functions.









# C Library

## Mt-Access the C Library / Library Functions

- header file or library file to find the functions
- Automatic Access
  - On many systems, you just compile the program and the more common library functions are made available automatically.
  - Keep in mind that you should declare the function type for functions you use. Usually you can do that by including the appropriate header file. 
  - User manuals describing library functions tell you which files to include
- File Inclusion
  - If a function is defined as a macro, you can include the file containing its definition by using the #include directive. 
  - Often, similar macros are collected in an appropriately named header file. 
- Library Inclusion
  - At some stage in compiling or linking a program, you might have to specify a library option.
  - These libraries have to be requested explicitly by using a compile-time option. 
  - Note that this process is distinct from including a header file. 
    - A header file provides a function declaration or prototype.
    - The library option tells the system where to find the function code. 
- 



## Mt-Using the Library Descriptions

- Find function documentation.
- The key skill you need in reading the documentation is interpreting function headings.



## Standard C library / Header files

### Math Library

```c
#include <math.h>
sqrt(4);
```

#### Soc

- `math.h`

#### F

- The math library contains many useful mathematical functions.

#### Cat-Some ANSI C Standard Math Functions

| Prototype                       | Description                                                |
| ------------------------------- | ---------------------------------------------------------- |
| double acos(double x)           | Returns the angle (0 to π radians) whose cosine is x       |
| double asin(double x)           | Returns the angle (—π/2 to π/2 radians) whose sine is x    |
| double atan(double x)           | Returns the angle (−π/2 to π/2 radians) whose tangent is x |
| double atan2(double y,double x) | Returns the angle (−π to π radians) whose tangent is y / x |
| double cos(double x)            | Returns the cosine of x (x in radians)                     |
| double sin(double x)            | Returns the sine of x (x in radians)                       |
| double tan(double x)            | Returns the tangent of x (x in radians)                    |
| double exp(double x)            | Returns the exponential function of x (ex)                 |
| double log(double x)            | Returns the natural logarithm of x                         |
| double log10(double x)          | Returns the base 10 logarithm of x                         |
| double pow(double x, double y)  | Returns x to the y power                                   |
| double sqrt(double x)           | Returns the square root of x                               |
| double cbrt(double x)           | Returns the cube root of x                                 |
| double ceil(double x)           | Returns the smallest integral value not less than x        |
| double floor(double x)          | Returns the largest integral value not greater than x      |
| double fabs(double x)           | Returns the absolute value of x                            |

#### Tips

```c
gcc list_16_5_variadic.c -o list_16_5_variadic -lm
```

- 在Linux下，若要调用C中的math库里的函数，必须在编译时命令最后加上“-lm”，表示链接到math库里，IDE里好一些例如Pycharm
- Unix systems may require that you instruct the linker to search the math library by using the -lm flag. Note that the –lm flag comes at the end of the command. <u>That's because the linker comes into play after the compiler compiles the C file.</u>



### The tgmath.h Library (C99)

```c
#include <tgmath.h>
float x = 44.0;
double y;
y = sqrt(x);  // invoke macro, hence sqrtf(x)
y = (sqrt)(x);  // invoke function sqrt()
```

#### Soc

- tgmath.h

#### Ab

- If the compiler supports complex arithmetic, it supports the complex.h header file, which declares complex analogs to math functions.

#### F

- The C99 standard provides a tgmath.h header file that defines <u>type-generic macros similar in effect of the generic function with arguments</u>.
- If a math.h function is defined for each of the three types float, double, and long double, the tgmath.h file creates a type-generic macro with the same name as the double version. 

### The General Utilities Library

```c
#include <stdlib.h>
```

#### Soc

**stdio.h**

- **Def:** Standard input/output header
- **Mt**
  - EOF is a special value defined in the stdio.h file. Typically, a #define directive gives EOF the value –1.

- **F:** It contains a collection of information about input and output functions for the compiler to use. 

#### F

- The general utilities library contains a grab bag of functions, including a random-number generator, searching and sorting functions, conversion functions, and memory-management functions. 

#### Cat

- rand(), srand()
- qsort()
- malloc(), and free() 

- exit(), atexit()



## The Assert Library

```c
#include <assert.h>
```

### Soc

- `assert.h`

### Cc

- **Def:** The assert library, supported by the assert.h header file, is a small one designed to help with debugging programs

### Ab

- 

### F

- 

### Cat

**`assert()` Macro**

```c
assert(z >= 0);

if (z < 0)
{
    puts("z less than 0");
    abort();
}
```

- **Def + F:** 
  - assert() takes as its argument an integer expression. If the expression evaluates as false (nonzero), the assert() macro writes an error message to the standard error stream (stderr) and calls the abort() function, which terminates the program.
  - The abort() function is prototyped in the stdlib.h header file.
- **Cpn**
  - the argument is a relational or logical expression
- **Ab**
  - It identifies the file automatically. It identifies the line number where the problem occurs automatically
  - place the macro definition
    `#define NDEBUG`
    before the location where `assert.h` is included and then recompile the program, and the compiler will deactivate all assert() statements in the file.
  - The assert() expression is a run-time check
- **F-Return**
  - If assert() does abort the program, it first displays the test that failed, the name of the file containing the test, and a line number.
- **F**
  - Identify critical locations in a program where certain conditions should be true and to use the assert() statement to terminate the program if one of the specified conditions is not true.

**`_Static_assert` Macro (C11)**

```c
_Static_assert(CHAR_BIT == 16, "16-bit char falsely assumed");
```

- **Cpn**
  - The first is a <u>constant integer expression</u>, which act as a conditional expression.
    - an integer constant expression guarantees that it can be evaluated during compilation

  - the second is a string for error message.

- **Ab**
  - The assert() expression is a <u>run-time check.</u> C11 adds a feature, the _Static_assert declaration, that does a <u>compile-time check.</u> 
    - assert() can cause a running program to abort, while _Static_assert() can cause a program not to compile
  - In terms of syntax, _Static_assert is treated as a declaration statement. Thus, unlike most kinds of C statements, it can appear either in a function or, as in this case, external to a function.
  - That's to make C more compatible with C++, which uses static_assert as its keyword for this feature.



## The String Library

### Soc

- `string.h`

### Cc

- The string.h file contains function prototypes for several string-related functions, including `strlen()`, `gets`

### Cat

- memcpy and memmove



## Variable Arguments: stdarg.h

```c
#include <stdarg.h>

// Prototype
double sum(int, ...);

// Variadic Function
double sum(int lim,...)
{
	double arg;
    va_list ap; // declare object to hold arguments
    va_list apcopy;
    va_start(ap, lim); // initialize ap to argument list
    va_copy(apcopy, ap);
    arg = va_arg(ap, double);  // access each item in argument list
    va_end(ap); // clean up
}
```

### Soc

- `stdarg.h`

### Cc

- The stdarg.h header file provides a similar capability of variadic macros for functions.
  - Accept a variable number of arguments at last argument position

### Mt

**Lk:** Listing 16.21 The varargs.c Program

- Provide a function prototype using an ellipsis at last argument position.
  - at least another parameter before the ellipsis
  - The rightmost parameter (the one just before the ellipses) plays a special role; the standard uses the term `parmN` as a name to use in discussion.
    - The actual argument passed to this parameter will be the number of arguments represented by the ellipses section
    - 
- Create a va_list type variable in the function definition.
  - va_list type, which is declared in the stdargs.h header file, represents a data object used to hold the parameters corresponding to the ellipsis part of the parameter list. 

- Use a macro to initialize the variable to an argument list.
  - the va_start() macro, also defined in stdargs.h, to copy the argument list to the va_list variable. 
    - two arguments: the va_list variable and the parmN parameter. 

- Use a macro to access the argument list.
  - va_arg() gain access to the contents of the argument list sequentially
    - two arguments: a type va_list variable and a type name. 
    - The type argument specifies the type of value returned.
    - The argument type really has to match the specification. The automatic conversion of double to int that works for assignment doesn’t take place here.

- Use a macro to clean up.
  -  va_end() macro  free memory dynamically allocated to hold the arguments.
  - After you do this, the variable ap may not be usable unless you use va_start to reinitialize it.

- Add: Back up the arguments
  - va_copy(): it copies the second va_list to the first one
    - Its two arguments are both type va_list variables.
    - 


### Ab

- Because va_arg() doesn't provide a way to back up to previous arguments, it may be useful to preserve a copy of the va_list type variable. 



## Others

 **stdint.h and inttypes.h**

- **Def**
  - Create more names for the existing types in a header file called `stdint.h`
  - **Lk:** Portable types
- **Ab**
  -  when you write a program using `int32_t` as a type and include the `stdint.h` header file, the compiler will substitute `int` or `long` for the type in a manner appropriate for your particular system
- **F**
  - Exactly define/indicate the types

**limits.h and float.h**

**F**

- supply detailed information about the size limits of integer types and floating types
- limits.h: These constants represent the largest and smallest possible values for the int type
- float.h: the float.h file defines constants such as FLT_DIG and DBL_DIG, which represent the number of significant figures supported by the float type and the double type. 

**stdbool.h** 

- This header file makes `bool` an alias for `_Bool` and defines `true` and `false` as symbolic constants for the values 1 and 0. 
- Including this header file allows you to write code that is compatible with C++, which defines bool, true, and false as keywords

**ctype.h**

- This headfile contain character-related functions/prototypes
- **Lk:** C Primer Plus > Table 7.1 The ctype.h <u>Character-Testing Functions</u> + Table 7.2 The ctype.h <u>Character-Mapping Functions</u>

**iso646.h**

- **Lk:** C Primer Plus > Reference Section V, “The Standard ANSI C Library with C99 and C11 Additions"
- **Def:** ISO standard
- **Cpn**
  - alternative spellings for the logical operators
    - you can use `and` instead of `&&`, `or` instead of `||`, and `not`
      instead of `!`. 

**conio.h**

- Many compilers for IBM PC compatibles, for example, supply a special family of functions, supported by the conio.h header file, for unbuffered input. 

**stdarg.h** 

- stdarg.h header file, provides a standard way for defining a function with a variable number of parameters

**stdlib.h**

- `malloc`

**stdvar.h**

- The stdvar.h header file, discussed later in this chapter, provides tools for creating user-defined functions with a variable number of arguments

- 









# Comment

```C
// comment

/* 
multiline
comments
*/

```







# Expressions and Statements 表达式

## Cc-Expressions and Statements

- **Instruction**
  - **Pcp**
    - Instruction 包含 Expression 和 Statement
- **Expression**
  - **Def:** An expression consists of a combination of operators and operands.
  - **Ab**
    - every C expression has a value
      - **Sp**
        - what about the ones with = signs? Those expressions simply have the same value that the variable to the left of the = sign receives. 
        - What about the expression q > 3? Such relational expressions have the value 1 if true and 0 if false. 
  - **Cpn**
    - **Subexpression:** Some expressions are combinations of smaller expressions, called subexpressions
    - **Full expression**: A full expression is one that's not a subexpression of a larger expression.
      - **e.g.** 
        - the expression in an expression statement
        - the expression serving as a test condition for a while loop.
    - **Relational expression**: test expressions that make comparisons
- **Statement**
  - **Def:** A statement is a complete instruction to the computer.
    - **Att:** Although a statement (or, at least, a sensible statement) is a complete instruction, <u>not all complete instructions are statements.</u> 
    - The semicolon(`;`) at the end of the line identifies the line as a **C statement or instruction.**
  - **Pcp**
    - Exactly, the semicolons tell the compiler where one statement ends and the next begins.
    - **free-form format:** a separated-line statement/one-line statement are acceptable for C compiler as long as you add `;` at the end of a statement.
  - **Ab**
    - In C, statements are indicated by a semicolon at the end.
  - **F**
    - Statements are the primary building blocks of a program. A program is a series of statements with some necessary punctuation.
  - **Cat**
    - **Null statement**: `;`
    - **Expression statements**: C considers any expression to be a statement if you append a semicolon
    - **Declaration statement**
      - **Ab**
        - Declaration statement is generally not an expression statement since it could not return a value.
    - **Assignment statement**
      - **Def+F:** it assigns a value to a variable. 
      - **Cpn**
        - It consists of a variable name followed by the assignment operator (=) followed by an expression followed by a semicolon
    - **Structured statement/Control Statement**
      - **Def+F:** Possess a structure more complex than that of a simple assignment statement.
      - **Cat**
        - Loops
    - **Function statement/Function call statement**
      - **Def+F:** function statement causes the function to do whatever it does.
      - **Att:** function statement is not a function
    - **Return statement**
      - **Def+F:** The return statement terminates the execution of a function
    - **Compound Statements (Blocks)**
      - **Def+F:** A compound statement is two or more statements grouped together by enclosing them in braces; it is also called a block.
      - **Ab** 
        - Compound Statement counts syntactically as  a single statement
      - **e.g.** while loop block with braces.
- **Side effect**
  - **Def:** side effect is the modification of a data object or file
  - **Cpn**
    - **Sequence point:** a point in program execution <u>at which all side effects are evaluated before going on to the next step.</u>
      - In C, the <u>semicolon</u> in a statement marks a sequence point
      - The end of any <u>full expression</u> is also a kind of sequence point.
      - Some <u>operators</u>, like `,` `&&` and `||`
  - **Cat**
    - The increment and decrement operators, like the assignment operator, have side effects and are used primarily because of their side effects



## Pcp-Relationship

- Statements form the basic program steps of C, and most statements are constructed from expressions



## Declaration Statement

```c
int num;
int var1, var2;  // Multiple declarations
```

> - using a variable called num and that num will be an int (integer) type

**Cc**

- **Declaration**
  - **Def:** Declare the variable with particular data type
- **Identifier:** The name you select for a variable, a function, or some other entity.

**Pcp**

- All variables must be declared before they are used.
- C has required that variables be declared at the beginning of a block with no
  other kind of statement allowed to come before any of the declarations.
- Putting all the variables in one place makes it easier for a reader to grasp what the program is about.



## Assignment Statement

```c
num = 1;
```

> - The statement num = 1; assigns the value 1 to the variable called num.

**Cc**

- **Assignment:** The declaration line set aside space in computer memory for the variable and the assignment line stores a value in that location. 

**Cpn**

- **Assignment operator:** =



## Control Statement

### Cc

- **Iteration:** Each cycle of the loop



### Pcp-Three forms of program flow

**Tips:** Pcp-a good language should provide these three forms of program flow

- Executing a sequence of statements
- Repeating a sequence of statements until some condition is met (looping)
- Using a test to decide between alternative sequences (branching)



### Looping

#### Pcp-Which Loop

- Decide whether you need an entry-condition loop or an exit-condition loop

#### while loop

```c
// Single Statement
while (true)
    printf("seele");

// Coumpound Statement
while (true)
{
	printf("seele");
}

// C-Style Reading Loop
while (scanf("%ld", &num) == 1) // Reading and Testing
{
	/* loop actions */
}
```

**Pcp**

- **Only one statement is included in the loop.** It can be a simple statement, as in this example, in which case no braces are needed to mark it off, **or the statement can be a compound statement,** like some of the earlier examples, in which case braces are required.
- When you construct a while loop, <u>it must include something that changes the value of the test expression</u> so that the expression eventually becomes false.
- 

**Ab**

- **Entry-condition loop**/**Conditional Loop**: get the input and check the value of status before it goes to the body of the loop.
  - the execution of the statement portion depends on the condition described by the test expression
  - The expression is an **entry condition** because the condition must be
    <u>met before the body of the loop is entered.</u>
- This cycle of test and execution is repeated until expression becomes false (zero).

**Mt-Indefinite loop**

- **Def:** Indefinite loop means we don't know in advance how many times the loop will be executed before the expression becomes false.

**Mt-Counting loops**

- **Def:** Counting loops execute a <u>predetermined number of repetitions</u>. 
- **Cpn**
  - counter
  -  limiting value
  - changes

#### for loop

```c
// Single Statement
for (count = 1; count <= 10; count++)
    printf("For Loop");

// Coumpound Statement
for (count = 1; count <= 10; count++)
{
	printf("seele");
}
```

**St**

- Initialization > Condition > Loop statements > Update
- The for loop gathers all three actions (initializing, testing, and updating) into one place. 
- You could use most statements to fit the Initialization and Update Part as long as the condition statement could be evaluated and terminate the loop. 

**Cpn**

- Initialization: Initialize expression only once before loop begins
- Condition: Test, the final value of counter
- Change or Update: this expression is done at end of each loop

**Ab**

- Entry-condition loop
- Each of the three control expressions is a full expression, so any side effects in a control expression, such as incrementing a variable, take place before the program evaluates another expression.

**Mt-Flexibility Usage of C for loop**

- This flexibility stems from how the three expressions in a for specification can be used.

#### do while

```c
// Single statement
do
	scanf("%d", &number);
while (number != 20);

// Compound statement
do
{
    // statements
} while (condition == 0);
```

**Ab**

- An Exit-Condition Loop:  the condition is checked after each iteration of the loop
- Note that the do while loop itself counts as single statement and, therefore, requires a terminating semicolon.

#### Mt-Nested Loop

- **Def:** A nested loop is one loop inside another loop. 
- **Cpn**
  - Outer loop
  - Inner loop
- **Pcp-Nested Variation**
  - Variables in different inner loops are independent

#### Problem- Infinite loop

- When the test is always true, the loop will unceasingly continue until you break/terminate the process.
- 



### Branching Statements and Jumps

#### ----- Branching Statements / Selection Statements -----

#### if Statement

```c
// Single
if (expression)
    statements;
else
    statements;


// Compound
if (expression)
{
    
} 
else if (expression) 
{
    
} 
else
{
    
}
    
```

**Cc**

- **flag**: The variable contains the value to indicate the changes of particular statements

**Ab**

- The entire if structure counts as a single statement, even when it uses a compound statement.
- More generally, any expression can be used in the if expression part, and an expression with a 0 value is taken to be false. Usually use relational expression.
- The rule is that an `else` goes with the most recent `if` unless braces indicate otherwise 注意else是就近从属，尤其是在使用single statement形式的时候
  -  remember that the compiler ignores indentation.



#### Switch Statement

```c
switch (integer test expression)
{
    case branch_condition_constant:
    case branch_condition_constant2:
        statements;
    case branch_condition2_constant:
        statements;
    default :
        final_else statements;
}
```

**Cc**

- **Label**: `case branch_condition` part

**Cpn's Pcp**

- The switch statement is a single statement.
  
- The switch test expression in the parentheses should be <u>one with an integer value (including type char).</u> 
  - `i++` could be used and the side effect will be evaluated after jumping to the label.

- The case labels must be <u>integer-type (including char) constants or integer constant expressions (expressions containing only integer constants).</u> You can't use a variable for a case label. 
- default case is optional

**Pcp-Usage**

- You can't use a switch if your choice is based on evaluating a floating-point variable or expression. 
- Nor can you conveniently use a switch if a variable must fall into a certain range.

**Mt-Flow**

- The expression in the parentheses following the word switch is evaluated. 
- Then the program scans the list of labels (here, case 'a' :, case 'b' :, and so on) until it finds one matching that value of the expression
- The program then jumps to that line and execute the statements within the case.
- If there is no match, there is a line labeled `default :`, the program
  jumps there. Otherwise, the program proceeds to the statement following the switch.

**Mt-Break**

- Most cases should be terminated with break statements, or it will continue to execute the next case rather than jump out of the switch statement.
  - Without the break statement, every statement from the matched label to the end of the switch would be processed.
- Incidentally, a break statement works with loops and with switch, but continue works just with loops.



#### ----- Jumping Statements -----

#### Continue Statement

```c
continue;
```

**Def+F:** The continue statement causes the rest of an iteration to be skipped and the next iteration to be started.

**Ab**

- If the continue statement is inside nested structures, it affects only the innermost structure containing it.

**F-Where exactly the loop resume?**

- For the while and do while loops, the next action taken after the continue statement is to evaluate the loop test expression.
- For a for loop, the next actions are to evaluate the update expression and then the loop test expression.

#### Break Statement

```c
break;
```

**Def+F:** A break statement in a loop causes the program to break free of the loop that encloses it and to proceed to the next stage of the program. 

**Ab**

- If the break statement is inside nested loops, it affects only the innermost loop containing it.
- The break statement is an essential adjunct to the switch statement

**F-Where to resume?**

- A break statement takes execution directly to the first statement following the loop

#### Goto Statement

```c
goto label;

label: statements;
```

**Def**

- A goto statement causes program control to jump to a statement bearing the indicated label

**Pcp**

- Don't rely on goto statement. In principle, you never need to use the goto statement in a C program.
- The labeled statement can come either before or after the goto.

**Mt**

- A more useful solution is use goto statement to get out of a nested set of loops if trouble. (All loops rather than the innermost loop)





## Return Statement

**Lk:** This notebook > Function



# Data Type

## Cc

- **Data:** The numbers and characters that bear the information you use.
- **Constant:** Some types of data are preset before a program is used and keep their values unchanged throughout the life of the program
- **Variable:** Some types of data may change or be assigned values as the program runs. 
  - **Pcp-Variable and Pointer**
    - In short, a regular variable makes the value the primary quantity and the address a derived quantity, via the & operator.
    - A pointer variable makes the address the primary quantity and the
      value a derived quantity via the * operator.
  - **Ab**
    - **Type**
    - **Type qualifier**
    - **Storage class**
    - **Name**
      - An address is the computer's version of a name. 变量名实际上以一个名字代表对应所在的存储单元地址。编译、链接程序时由编译系统为每一个变量名分配对应的内存地址/空间
      - 变量名应当以英文字母或下划线开头
      - case-sensitive
    - **Address**
    - **Value**
  




## Mt-Type size

```c
printf("Type int has a size of %zd bytes.\n", sizeof(int));
```

> C has a built-in operator called `sizeof` that gives sizes in bytes. C99 and C11 provide a `%zd` specifier for this type used by `sizeof`. Non-compliant compilers may require `%u` or `%lu` instead.





## Mt-Symbolic Constant 符号常量

### `#define` Manifest Constants

**Lk:** This notebook > C++ Preprocessor > #define directive  `#define` for Manifest Constants / Macro

### `const` Modifier

**Lk:** This notebook > Memory Management System > Memory Management > Move/Modify the memory > `const` Modifier

### `enum` Facility

**Lk:** This notebook > Data Type > Enumeration



## Pcp-Type Convertion

**Lk:** C Primer Plus > C5 >  Type Conversions

**Problem**

- When you initialize a variable of one numeric type to a value of a different type, C converts the value to match the variable. This means you may lose some data.
- when converting floating-point values to integers, C simply throws away the decimal part (truncation) instead of rounding.
- Type conversion may cause lost in precision, because a float is guaranteed to represent only the first six digits accurately
- When the destination is some form of unsigned integer and the assigned value is an integer, the extra bits that make the value too big are ignored.
- 

**Rules for automatically type conversion**

- You should usually **steer clear of automatic type conversions**
- **Promotion**: conversions to larger types
  - When appearing in an expression, char and short, both signed and unsigned, are automatically converted to int or, if necessary, to unsigned int.
  - If short is the same size as int, unsigned short is larger than int; in that case, unsigned short is converted to unsigned int
  - Under K&R C, but not under current C, float is automatically converted to double
- **Demotion**: a value is converted to a lower-ranking type.
  - Promotion is usually a smooth, uneventful process, but demotion can lead to real trouble. The lower-ranking type may not be big enough to hold the complete number which could lead to problems.
- In any operation involving two types, both values are converted to the higher ranking of the two types.
  - The rank of type: The size of the type./Tolerance
- In an assignment statement, the final result of the calculations is converted to the type of the variable being assigned a value. (i.e. `int var = val;` The `int` type) 
  - This process can result in promotion, as described in rule 1, or demotion, in which a value is converted to a lower-ranking type.
- When passed as function arguments, char and short are converted to int, and float is converted to double. 
  - This automatic promotion is overridden by function prototyping, as discussed in Chapter 9, “Functions.”



## Number 

**Cc-`num`**

### Integer 

#### Int

##### **Cc-`int`**

**Integer**

- **Def**
  - An integer is a number with no fractional part. 
- **Ab**
  - Store: Binary number
  - Size: 4 bytes 字节
- **Pcp**
  - The int type is a signed integer. 
    - Typically, systems represent signed integers by using the value of a particular bit to indicate the sign.

**Adjective keywords to modify the basic integer type:**

- **Cc**
- `short` or `short int`
  - **Pcp+F**
    - Saving space when only small numbers are needed.
    - C automatically expands a type short value to a type int value when it's passed as an argument to a function.
    
  - **Ab**
    - `short` is a signed type
    - use less storage than `int`
- `long` or `long int`
  - **Pcp+F**
    - Enabling you to express larger integer values
  - **Ab**
    - `long` is a signed type
    - use more storage than `int`
    - Used when the integer is >= 5 digits
  - **Cat**
    - `long long` or `long long int`
      - **Pcp+F**
        - Enabling you to express larger integer values
      - **Ab**
        - `long long` is a signed type
        - use more storage than `long`, at least  64 bits.
- `unsigned` or `unsigned int`
  - **Pcp+F**
    - Used for variables that have only nonnegative values. 
    - `unsigned` shifts the range of numbers that can be stored

  - **Ab**
    - The bit used to indicate the sign of signed numbers now becomes another binary digit, allowing the larger number.
- `signed`
  - **Pcp+F**
    - The keyword `signed` can be used with any of the signed types to make your intent explicit.
- **Pcp-Choice**
  - The most common practice today on personal computers is to set up long long as 64 bits, long as 32 bits, short as 16 bits, and int as either 16 bits or 32 bits, depending on the machine's natural word size. 
  - First, consider `unsigned` types if you don't need negative numbers, and the unsigned types enable you to reach higher positive numbers than the signed types.
  - Use the `long` type if you need to use numbers that long can handle and that `int` cannot. Similarly, use `long long`  if you need 64-bit integer values.
    - If you are writing code on a machine for which int and long are the same size, and you do need 32-bit integers, you should use long instead of int so that the program will function correctly if transferred to a 16-bit machine.
  - Use `short` to save storage space if, say, you need a 16-bit value on a system where int is 32-bit only if your program uses arrays of integers that are large in relation to a system's available memory.
    - it may correspond in size to hardware registers used by particular components in a computer.
  - To cause a small constant to be treated as type `long`, you can append an `l` (lowercase L) or `L` as <u>a suffix.</u> Similarly, on those systems supporting the `long long` type, you can use an `ll` or `LL` suffix to indicate a long long value. Add a `u` or `U` to the suffix for `unsigned long long`.
    - e.g. `12l or 7L`
    - e.g. `12ll or 7LL`
    - e.g. `12ull or 7ULL or 12uLL or 7Ull`

##### Mt-Declaration

```c
// int
int var1_name, var2_name,var3_name;

// long
long int estine;
long johns;
long long ago;

// short
short int erns;
short ribs;

// unsigned
unsigned int s_count;
unsigned players;
unsigned long headcount;
unsigned short yesvotes;

```

**Mt-Assignment**

```c
var1_name = 1;
var1_name = func_1;
```

**Mt-Initialization**

- **Def:** Initialization = declaration + assignment. Initialize a variable means to assign it a starting, or initial, value
- **Pcp**
  - Avoid putting initialized and non-initialized variables in the same declaration statement
- **Pc**

```c
int var1_name = 1, var2_name = 2, var3_name = 3;
```

**Mt-Base prefix**

- Decimal: Default
- Octal: `0`
  - e.g. `020` is a octal value which means 16 in decimal
- Hexadecimal: `0x` or `0X`
  - e.g. `0x10` is a hexadecimal value which means 16 in decimal

**Problem-Integer Overflow**

- When `int`  type reaches its maximum value, it starts over at the beginning. 

---

#### Char

**Cc-`char`**

- **Def**
  - The `char` type is used for storing characters such as letters and punctuation marks.
  - characters, digits, punctuation marks and escape sequences
- **Ab**
  - Technically `char` type is the smallest integer type. The `char` type actually stores integers, not characters.
  - Size: 1 byte 字节
    - The C language defines a byte to be the number of bits used by type char
  - Could be `signed` or `unsigned`
- **Cat**
  - the ASCII code
  - Unicode
  - EBCDIC
  - ISO/IEC 10646

**Mt-Declaration**

```c
char response;
char itable, latan;
```

**Mt-Initialization and Assignment**

```c
// Assignment
char first_char = 'A';
char first_char_ASCII = 65; 

```

**Mt-Escape sequence**

- **Def:** Represent difficult- or impossible-to-type characters. In each case, the escape sequence begins with the backslash character(`\`).

```c
// Use ASCII code
char beep = 7;

// Use escape sequences
char backslash = '\\';

// Using a hexadecimal form for character constants
```

| Sequence | Meaning                                                |
| -------- | ------------------------------------------------------ |
| `\a`     | Alert (ANSI C). An audible or visible alert            |
| `\b`     | Backspace.                                             |
| `\f`     | Form feed.                                             |
| `\n`     | Newline.                                               |
| `\r`     | Carriage return.                                       |
| `\t`     | Horizontal tab.                                        |
| `\v`     | Vertical tab.                                          |
| `\\`     | Backslash (`\`).                                       |
| `\'`     | Single quote (').                                      |
| `\"`     | Double quote (").                                      |
| `\?`     | Question mark (?).                                     |
| `\0oo`   | Octal value. (o represents an octal digit.)            |
| `\xhh`   | Hexadecimal value. (h represents a hexadecimal digit.) |
|          |                                                        |

**Cat**

- Null character: `\0`
  - **Ab**
    - it is the non-printing character whose ASCII code value (or equivalent) is 0.
    - However, the null character is not the digit zero;
    - The presence of the null character means that the array must have at least <u>one more cell than the number of characters to be stored</u>.
  - **F:** C uses it to mark the end of a string







---

#### Boolean

**Cc-`_Bool`**

- `_Bool` is the C type name used to represent Boolean values—that is, the logical values true and false
-  **Mt**
  - If you try to assign a nonzero numeric value to a _Bool variable, the variable is set to 1, reflecting that C considers any nonzero value to be true.


**Pcp-Truth and False**

- C uses the value 1 for true and 0 for false.  Just an integer type that only requires 1 bit of memory
- For C, a true expression has the value 1, and a false expression has the value 0.
- More generally, all nonzero values are regarded as true, and only 0 is recognized as false. 

**Pcp-Name**

- Give Boolean variables names that suggest expected true or false values for the test condition.





---

#### Portable Types

**Lk:** 

- stdint.h or inttypes.h
- *C Primer Plus* > Reference Section VI, “Extended Integer Types,” in Appendix B

**Cc+Cat**

- **Exact-width integer types:** The alternative type names used to specify particular integer type for particular systems in `stdint.h` or `inttypes.h`
  - **e.g.** `int32_t` uses a 32-bit int
-  **Minimum width types:** This set of names promises the type is <u>at least</u> big enough to meet the specification and that no other type that can do the job is smaller
  - **e.g.** `int_least8_t` the smallest available type that can hold an 8-bit signed integer value
- **fastest minimum width types:** define a set of types that will allow the fastest computations
  - **e.g.:** `int_fast8_t` will be defined as an alternative name for the integer type on your system that allows the fastest calculations for 8-bit signed values
-  **The biggest possible integer type:** These types could be bigger than long long and unsigned long because C implementations are permitted to define types beyond the required ones.
  - **e.g.:** `intmax_t` stands for that type, a type that can hold any valid signed integer value. Similarly, `uintmax_t` stands for the largest available unsigned type. 



### Float, Double

#### Cc-`float`

- **Floating-point Number**
  - **Def**
    - Real number in math
  - **Ab**
    - at least 6 significant figures(total number) and allow a range of at least $10^{–37}$ to  $10^{37}$ 
    - Size: 4 bytes 字节
    
  - **Space**
    - Floating-point representation involves breaking up a number
      into a fractional part and an exponent part and storing the parts separately
    - Often, systems use 32 bits to store a floating-point number. 8 bits are used to give the **exponent** its value and sign, and 24 bits are used to represent the **non-exponent part,** called the **mantissa or significand**, and its sign.
  
  - **Pcp-Exponential notation/e-notation**
    - **Def:** Sign(+-) + Fraction + Exponent of 10
    - 3.16E7 = $3.16 \times 10^{7}$
  - **Pcp-Precision**
    - Floating-point numbers are subject to greater loss of precision when calculating large numbers.
    - Computer floating-point numbers can't represent all the values in the range. Instead, floating-point values are often approximations of a true value. 
  - **e.g.**
    - **NaN(Not a Number):** Not existing or not valid float-point value

#### Cc-`double`

- **Double (for double precision) floating-point type**

  - **Ab**

    - `double type` 1extends the minimum number of significant figures that can be represented to 10
    - By default, the compiler assumes floating-point constants are double precision. 
    - C automatically expands type float values to type double when they are passed as arguments to any function
    - Size: 8 bytes 字节
    
    **Space**
    
    - Often, systems use 64 bits to store a floating-point number. 
    
    **Cat**
    
    - `long double`
      -  intent is to provide for even more precision than double. However, C guarantees only that long double is at least as precise as double.

#### Mt-Declaration

```c
float noah, jonah;
double trouble;
long double gnp;
```

#### Mt-Assignment and Initialization

```c
//
float planck = .123;
float planck = 123.;
float planck = 6.63e-34;
float planck = -6.63E-34;

// The number constant are double by default.
double test = 3E10;

// The float number constant
float test = 2.3f;
float test = 2.4E10F;

// The long double type constant
long double test = 2.4E10l;
long double test = 3.14L;

// hexadecimal p notation
double test = 0xa.1fp10;  // a = 16
double test = 0Xa.1fP10;

```

#### Problem-Floating-Point Overflow

- The overflow behavior for this case used to be undefined, but now C specifies that variable that contains too large number gets assigned a special value that stands for infinity and that printf() displays either `inf` or `infinity` (or some variation on that theme) for the value.

#### Problem-Floating-Point Underflow

**Cc**

- **Underflow:** vacating the first position and losing the last binary digit
- **Subnormal:** C refers to floating-point values that have lost the full precision of the type

**Mt**

- The C library now provides functions that let you check whether your
  computations are producing subnormal values.

**e.g.** 

- dividing the smallest positive normal floating-point value
  by 2 results in a subnormal value

#### Problem-Floating-Point Round-off Errors

- The reason for these odd results is that the computer doesn't keep track of enough decimal places to do the operation correctly. 

#### Pcp-Floating-Point Representation

-  **Lk:** *C Primer Plus* > Appendix B, Section V for more details





### Complex 

#### Cc-`_Complex ` & ` _Imaginary`

- `float _Complex` 
  - **Cpn:** contain two float values, one representing the real part of a complex number and one representing the imaginary part.
- `double _Complex`
- `long double _Complex`
- `float _Imaginary`
- `double _Imaginary`
- `long double _Imaginary`
- `I`: square root of -1

#### Soc

```c
#include<complex.h>
```

#### Mt-Declaration

```c

```



#### Mt-Assignment and Initialization

```c

```



## Array[]

### Cc-Array

```c
// Array Declaration
int int_list[20];  // Type and elements
int int_list[constant_integer_expression];
int int_list[var];  // C99, variable-length array(VLA), you can’t initialize a VLA in its declaration

```

- **Array**
  - **Def:** An array is an <u>ordered</u> sequence of data elements of <u>one type</u>
    - An array is a series of values of the same type
    - The brackets([]) indicates that the variables as arrays.
  - **Ab**
    - Array 是构造类型, derived types because they are built on other types.
    - The array elements are stored next to each other in memory
    - So sizeof arr_var is the size, in bytes, of the whole array, and sizeof arr_var[0] is the size, in bytes, of one element.
    - An array name is also the address of the first element of
      the array.
      - Att: The array name means the address rather than the value as other variable name means.
      - The elements of an array are variables (unless the array was declared as const), but the name is not a variable.
  - **Cpn**
    - **Elements:** memory cell each of which can store one particular type value
    - **Subscripts, indices/index, or offsets**: The numbers used to identify the array elements are called subscripts, indices, or offsets.
      - **Ab**
        - The subscripts must be integers, and the subscripting begins with 0.
  - **Problem**
    - C <u>doesn't</u> check to see whether you use a correct subscript/element number.
-  **Scalar variables**: Single-valued variable
- 

### Ab-Storage

- Arrays, like other variables, can be created using different storage classes
- Default: belong to the automatic storage class
  - That means they are declared inside of a function and without using the keyword static.


### Pcp

- if you want a read-only array, In such cases, you can, and should, use the `const` keyword when you declare and initialize the array.

### Mt-Declaration

```c
int list[10];  // oridinary

int var = 10;
int list[var];  // VLA, dynamic array

list = (int *) malloc(10 * sizeof(int));
list = (int *) malloc(var * sizeof(int));
```

- **Cc**
  - dynamic array, one that's allocated while the program runs and that
    you can choose a size for while the program runs

### Mt-Initialization

```c
int list[8] = {1, 2, 3, 4, 5, 6, 7, 8};
int list[4] = {0};  // Other number will be initialized as zero.

// Const Array
const int list[8] = {1, 2, 3, 4, 5, 6, 7, 8};  // This makes the program treat each element in the array as a constant. Once it’s declared const, you can’t assign values later. 

// Match the array size
int list[] = {1, 2, 3};  // Let the compiler match the array size to the list by omitting the size from the braces
length = sizeof list / sizeof list[0];  // Dividing the size of the entire array by the size of one element tells us how many elements are in the array.

// Designated initializer
int list[5] = {[4] = 2};
int days[MONTHS] = {31,28, [4] = 31,30,31, [1] = 29};
int days[] = {1, [4] = 5};  // The compiler will make the array big enough to accommodate the initialization values.
```

**Pcp**

- Use constant to define the size of array, which could somehow helps ensure that you use the same array size consistently throughout the program.
- if you don't initialize an array at all, its elements, like uninitialized ordinary variables, get garbage values, <u>but if you partially initialize an array, the remaining elements are set to 0</u>.
- When the subscript is out of the scale of the array, this overgenerosity is considered an error.
- When you use empty brackets to initialize an array, the compiler counts the number of items in the list and makes the array that large.

#### Mt-Designated initializers

- **Designated initializers:** This feature allows you to pick and choose which elements are initialized.
  - **Pcp+Ab**
  - As with regular initialization, after you initialize at least one element, the uninitialized elements are set to 0. 
  - First, if the code follows a designated initializer with further values, as in the sequence [4] = 31,30,31, these further values are used to initialize the subsequent elements. 
  - Second, if the code initializes a particular element to a value more than once, the last initialization is the one that takes effect.

### Mt-Assignment

```c

// Assignment
int list[10];
list[1] = 2;
list[2] = list[1];
int_list[0] = 0;  // First element
int_list[20 - 1] = 19;  // Final element

// false assignmnet
list = list;  // Not allowed, Array could not be assigned to other variable.
list[200000] = list[19999999];  // Out of rage
list[2] = {1, 2, 3, 4, 5, 6};  // use the list-in-braces form except when initializing is not allowed.


```

### Mt-Pointer Related Array Manipulation

```c
// Pointer related
list[2] == *(list + 2)
```

**Lk:** This note > Pointer Addition

### Mt-Multidimensional array

```c
// Declaration
int mat[5][4];  // 5 row * 4 col matrix

// Initialization
int mat[2][3] = 
{
    {1, 2, 3},
    {0, 1, 2}
};  // Att the semicolon

int mat[2][3] = {1, 2, 3, 0, 1, 2};  // The same as the former example. As long as the number of entries is correct.

// Pointer
/// Address
mat == mat[0];
mat == &mat[0][0];

mat + 1 != mat[0] + 1;
*mat + 1 == mat[0] + 1;

/// Value
mat[0][0] == *mat[0];
mat[0][0] == **mat;
mat[1][2] == *(*(mat + 1) + 2);

/// Pointer to array
int (* pz)[3];  // Pointer to an array of 3 integer elements
pz = mat;

/// Pointer to pointer
int **p2p;
```

**Pcp**

- The rules we discussed about mismatches between data and array sizes in one-dimensional array apply to each row. 
- Without inner braces, the values of arrays are filled row by row until the data runs out even if you are short of entries. Then the remaining elements are initialized to 0. 
- `mat` is a pointer to array or an array of arrays, but `arr[0]` is a pointer of type.

**Pcp-Multidimensional Arrays and Pointers**

```c
int zippo[4][2];
// Address
zippo = &zippo[0];
zippo[0] = &zippo[0][0];

// Dereference / Double Indirection
*zippo == &zippo[0][0];
**zippo == *&zippo[0][0];
```

- In short, <u>zippo[0] is the address of an int-sized object, and zippo is the address of a two-int-sized object</u>. Because both the integer and the array of two integers begin at the same location, both zippo and zippo[0] have the same numeric value
- double indirection: In short, zippo is the address of an address and must be dereferenced twice to get an ordinary value. An address of an address or a pointer of a pointer is an example of double indirection.

### Mt-Function Argument

```c
int func(int * arr);  // Prototype

// In the context of a function prototype or function definition header, and only in that context, you can substitute int ar[] for int * ar
int func(int arr[]);


// Use const to protect array in function from being changed
int sum(const int ar[], int n);  // It’s important to understand that using const this way does not require that the original array be constant


// Multidimensional Array Arguments
void sum_rows(int ar[][COLS], int rows);
void sum_cols(int [][COLS], int );  // ok to omit names
int sum2d(int (*ar)[COLS], int rows); // another syntax
```

**Pcp**

- Since the array is the address of the its first element, the type of array argument for function should be the pointer-to-type. 
-  Arrays don't give you that choice to pass the value of the array; you must pass a pointer to function. 
  - The reason is efficiency. If a function passed an array by value, it would have to allocate enough space to hold a copy of the original array and then copy all the data from the original array to the new array. It is much quicker to pass the address of the array and have the function work with the original data.

- The reason C ordinarily passes data by value is to preserve the integrity of the data. If a function works with a copy of the original data, it won't accidentally modify the original data. <u>But, because array-processing functions do work with the original data, they can modify the array.</u>
- In general, to declare a pointer corresponding to an <u>N-dimensional array, you must supply values for all but the leftmost set of brackets for the pointer to recognize the length of each unit of array.</u>

### Mt-Pointer From start to end

```c
int sump(int * start, int * end)
{
	int total = 0;
	while (start < end)
	{
		total += *start; // add value to total
		start++;  // advance pointer to next element
	}
    return total;
}

void main(void)
{
    const int SIZE = 5;
	int arr[SIZE] = {0, 1, 2, 3 ,4};
	sump(arr, arr[SIZE]);  // The size of the whole array as it was declared.
}

```

### Mt-Variable-Length Arrays (VLAs)

```c
int row = 4;
int col = 5;
double mat[row][col];  // VLA

// Function
double func(int row, int col, mat[row][col]);
double func(int, int, mat[*][*]);  // VLAs omit names for prototype
```

**Def:** variable-length arrays, which allow you to use variables when dimensioning an array

**Pcp**

- They need to have the automatic storage class, which means they are declared either in a function without using the static or extern storage class modifiers (Chapter 12) or as function parameters. 
- You can't initialize VLAs in a declaration. 
- Finally, under C11, VLAs are an optional feature rather than a mandatory feature, as they were under C99.
- What the term variable does mean is that you can use a variable when specifying the array dimensions when first creating the array. The term variable in variable-length array does not mean that you can modify the length of the array after you create it. <u>Once created, a VLA keeps the same size.</u>
- Because the ar declaration uses rows and cols, they have to be declared
  before ar in the parameter list. 
- VLAs are still pointers and  a VLA parameter actually works with the data in the original array, and therefore has the ability to modify the array passed as an argument.
- VLAs allow for dynamic memory allocation. This means you can specify the size of the array while the program is running.
  - Regular C arrays have static memory allocation, meaning the size of the array is determined at compile time. 
- 

### Mt-Compound Literals

**Cc**

- **Literal**: Constants that are not symbolic.

  - e.g. 1.1 is a type double literal.

- **Compound Literal**: Constants compounded of different types.

  - e.g. 

    ```c
    // Array Constant
    (int [2]){10, 20};
    (int []){1, 2, 3};
    (int [2][3]){{1, 2, 3}, {0, 1, 2}};
    (int [][3]){{1, 2, 3}, {0, 1, 2}};
    (int [][]){{1, 2, 3}, {0, 1, 2}};
    
    // Use pointers to keep track of compound literals
    int * pt;
    pt = (int []){1, 2, 3};
    ```

  - **Pcp**
  
    - Keep in mind that a compound literal is a means for providing values that are needed only temporarily.  
    - Block Scope
      - That means its existence is not guaranteed once program execution leaves the block in which the compound literal is defined, that is, the innermost pair of braces containing the definition.
    - Storage Duration: 
      - Compound literals occurring <u>outside of any function</u> have <u>static storage duration</u>, and those occurring <u>inside a block</u> have <u>automatic storage duration</u>. 

### Character String

#### Cc-Character string

-  **Def:** The string is a `char` array if the array contains the null character, `\0`, which marks the end of the string
-  **Ab**
   -  The double quotation marks are not part of the string.
   -  Strings are stored in an array of type `char`. Characters in a string are stored in adjacent memory cells, one character per cell(each cell is one byte), and an array consists of adjacent memory locations
   -  The name of a character array, like any array name, yields the address of the first element of the array.
   -  **Size:** Num of char/elements + 1 (for the null character)
      -  Often, it is convenient to let the compiler determine the array size; recall that if you omit the size in an initializing declaration, the compiler determines the size for you
      -  the array size must evaluate to an integer constant, which includes
         the possibility of an expression formed from constant integer values.
      -  The null character, or '\0', is the character used to mark the end
         of a C string. It's the character whose code is zero. Because that isn't the code of any character, it won't show up accidentally in some other part of the string.
-  **Cat**
   -  **String Literal**: A string literal, also termed a string constant, is anything enclosed in double quotation marks. 
      -  **Ab**
         -  Character string constants are placed in the static storage class, which means that if you use a string constant in a function, the string is stored just once and lasts for the duration of the program, even if the function is called several times.

#### Pcp-Difference between array and pointer form

-  Typically, what happens is that the quoted string is stored in a data segment that is part of the executable file; when the program is loaded into memory, so is that string. The quoted string is said to be in <u>static memory</u>. But the memory for the array is allocated only after the program begins running.  There are two copies of the string. One is the string literal in static memory, and one is the string stored in the `ar1` array
   -  **Array Form:** the compiler will recognize the name ar1 as a synonym for the address of the first array element, &ar1[0]. One important point here is that in the array form, ar1 is an <u>address constant</u>. An address constant could not be changed so the `ar1++` would not be allowed.
   -  **Pointer Form:** The pointer form (*pt1) also causes 29 elements in static storage to be set aside for the string. In addition, once the program begins execution, it sets aside one more storage location for the <u>pointer variable</u> pt1 and stores the address of the string in the pointer variable. Since pt1 is a pointer variable, you can use the increment operator, such as `pt1++`.
   -  In short, initializing the array copies a string from static storage to the array, whereas initializing the pointer merely copies the address of the string.
-  In short, don't use a pointer to a string literal if you plan to alter the string.

#### Mt-Initialization

```c
// string constant
#define STR_RABBIT "rabbit"  
const char str[10] = "ten char";

// Array form
char str[10] = "ten char";  // char array
char str[10] = "ten" "ch""ar";  // C concatenates string literals if they are separated by nothing or by whitespace.
char str[10] = {'t', 'e', 'n', '\0'};
char str[10];

// The compiler determines the size of the string only if you initialize the array
char str[] = "ten char";
char *str = "\"char_array_with_null_character\"";  // char pointer form

// Extract
str[0] = 't';
str[1] = 'e';
str[2] = 'n';
```

**Pcp**

- When you specify the array size, be sure that the number of elements is at least one more (that null character again) than the string length. 
- Any unused elements are automatically initialized to 0 (which in char form is the null character, not the zero digit character).

#### Mt-Multidimensional Array/ Array of strings

```c
// An array of potiners of strings
const char* mytalents[LIM] = {
"Adding numbers swiftly",
"Multiplying accurately", "Stashing data",
"Following instructions to the letter",
"Understanding the C language"
};

// An array of char arrays
char yourtalents[LIM][SLEN] = {
"Walking in a straight line",
"Sleeping", "Watching television",
"Mailing letters", "Reading email"
};

// Extract the ith string
mytalents[i]
yourtablents[i]

```

-  The mytalents array is an array of five pointers, taking up 40 bytes on our system. But yourtalents is an array of five arrays, each of 40 char values, occupying 200 bytes on our system
- The pointers in mytalents point to the locations of the string literals used for initialization, which are stored in static memory. The arrays in yourtalents, however, contain copies of the string literals, so  each string is stored twice.
- Furthermore, the allocation of memory in the arrays is inefficient, for each
  element of yourtalents has to be the same size, and that size has to be at least large enough to hold the longest string.

**Pcp-Comparison**

- an array of pointers is more efficient than an array of character arrays.
- Because the pointers in mytalents point to string literals, these strings shouldn't be altered. The contents of yourtalents, however, can be changed. 

#### Mt-Multiple line

The `\` could be used to separate strings into two lines.

```c
printf("Here's another way to print a \
long string.\n");
```



#### Mt-Declaration

```c
char str_name[num]
```

> - The brackets after name identify it as an array. 
> - The number within the brackets indicates the <u>number of elements(The number also means the space/cells to store elements, including the null character)</u> in the array.

#### Mt-String Concatenation

- **Def:** You follow one quoted string constant with another, separated only by whitespace, C treats the combination as a single string.
- **Soc:** ANSI C 

```c
printf("Hello, young lovers, wherever you are.");
printf("Hello, young " "lovers" ", wherever you are.");
printf("Hello, young lovers"
", wherever you are.");
```

#### Mt-Input Strings

**`scanf()` function for strings**

```c
char str[10] = "1234567";
scanf("%s", str); // automatically add `\0` when reaching the end of the input string, not at the end of the string.
scanf("%10s", input_string);  // Search the first 10 character of the stdin string
```

```shell
$ hello
> hello # rather than hello67
```

> - 即便存在后续的空位，scanf得到的字符串也会在后面加入`\0`，导致其输出时到输入字符串的位置就终止了，即便str本身后面还有字符也会停止输出。
> - 输入格式中有空格的话不会影响"%s"，在输入中的目标字符串前的空格也会被忽略，这与`"%c"`不同。

**`gets()` function**

```c
char str[10];
gets(str);

```

- **Def+F:** `gets()` fetches the stdin value until reach the `\n` and load them to string variable().
- **F:** Read an entire line of input at a time instead of a single word
- **Ab**
  - It reads an entire line up through the newline character, discards the newline character, stores the remaining characters, adding a null character to create a C string. 
- **F-Return**
  - The string input
- **Problem-Banish**
  - The `gets` function doesn't check to see if the input actually fit into the array. If the input string is too long, you get **buffer overflow**, meaning the excess characters overflow the designated target.
  - “**Segmentation fault**” doesn't sound healthy, and it isn't. On a Unix system, this message indicates the program attempted to access memory not allocated to it.

**`fgets()` function**

```c
// General
fgets(str_ptr_target, length, source);

// Get string inputs from stdin
char str[10]
fgets(str, 10, stdin);
    
// Get string lines from file pointer
FILE *fp;
char *str;
fp = fopen("file_name", "a+");
fgets(str, length, fp);
```

- **Def+F:** The fgets() function meets the possible overflow problem by taking a second argument that limits the number of characters to be read
- **Cpn-Arguments**
  - **str_or_ptr**: the address where the string will be stored.
  - **length**: indicating the maximum number of characters to read.(The length should include the null character.) 
  - **source**: where to find the string inputs. indicating which file to read. 
    - To read from the keyboard, use `stdin` (for standard input) as the argument; this identifier is defined in `stdio.h`.
- **Ab**
  -  If fgets() reads the newline, it stores it in the string, unlike gets(), which discards it.
  - Automatically add a null character to create a C string. 
- **F-Return**
  -  A pointer to char, which is the address of the str if all goes well.
  -  If the function encounters end-of-file, however, it returns a special pointer called the **null pointer.**
     -  This is a pointer guaranteed not to point to valid data so it can be used to indicate a special case.
     -   In code it can be represented by the digit `0` or, more commonly in C, by the macro `NULL`. 
     -   It's often used by functions that otherwise return valid addresses to indicate some special occurrence, such as encountering end-of-file or failing to perform as expected
  -  The function also returns NULL if there is some sort of read error.

**`gets_s()` function**

```c
// General
gets_s(str, strlen);



```

- **Def+F:** Read a line of input into the `str` array providing the newline shows up in the first `strlen-1` characters of input
- **Ab**
  - gets_s() just reads from the standard input
  - If gets_s() does read a newline; it discards it rather than storing it.
  -  If gets_s() reads the maximum number of characters and fails to read a newline, it takes several steps. 
    - It sets the first character of the destination array to the null character. 
    - It reads and discards subsequent input until a newline or end-of-file is encountered. 
    - It returns the null pointer. 
    - It invokes an implementation-dependent “handler” function (or else one you've selected), which may cause the program to exit or abort.



**Pcp-General**

- you need to allocate enough storage to hold whatever strings you expect to
  read.
  - Don't expect the computer to count the string length as it is read and then allot space for it. 
  - Allocating inputs/string inputs to uninitialized pointer will probably get by the compiler, most likely with a warning, but when the pointer is read, the pointer might be written over data or code in your program, and it might cause a program abort.

#### Mt-Output Strings

**`printf()` function for strings**

```c
char str[10] = "str"; 
printf("str");
printf("%10s", str);
```

**`puts()` function**

```c
// General
puts(str);

// Slice
char str[10] = "ten char";
puts(&str[2]);
puts(str+2);
```

- **Def+F:** `puts()` fetches the value in the str variable and put them to stdout.
- **Soc:**  `stdio.h`
- **Cpn**
  - **str**: the address of strings.

- **Ab**
  - Automatically remove `\0`
  - Automatically add `\n` at the end of the string
  - It stops when it encounters the null character

**`fputs()` function**

```c
// General
fputs(str, source);

//
fputs(str, stdout);
fputs(str, fp);
```

- **Def+F:** 
- **Cpn**
  - **str**: the output string
  - **source**: indicate which file to write to. The fputs() function takes a second argument indicating the file to which to write. You can use stdout (for standard output), which is defined in stdio.h, as an argument to output to your display.
    - For the computer monitor we can use stdout (for standard output) as an argument. this identifier is defined in `stdio.h`.
- **Ab**
  - fputs() does not automatically append a newline to the output.
  - Because fgets() keeps the newline and fputs() doesn’t add one, they work well in tandem.
- **F-Return**
  - 


#### Mt-Function Argument

```c
void func(char str[]);
```

#### Mt-String Functions

**Pcp**

- Note that these prototypes use the keyword const to indicate which strings are not altered by a function. 

**Cpn**

`strlen()` **function**

```c
int str[10];
strncpy(str, "hello", 5);
strlen(str) == 5;
sizeof(str) == 40;
```

- **Def+F:** `strlen()` gives the length of a string in characters.
- **Soc:** string.h
- **Ab**
  -  not including the terminating null character, found in the string s.


**`strcpy()` Function**

```c
int str_target[10];
int str_source[10] = "hello";
strcpy(str_target, str_source);
strcpy(str_target + 2, str_source);  // copied from str_source to the 3rd elemnet of str_target
```

- **Def+F: ** `strcpy` copies the string `str_target` to `str_source`.
- **Soc:** string.h
- **Ab**
  - The second pointer, which points to the original string, can be a declared pointer, an array name, or a string constant. The first pointer, which points to the copy, should point to a data object, such as an array, roomy enough to hold the string.
    - declaring an array allocates storage space for data;
      declaring a pointer only allocates storage space for one address.
  - The copy is called the target, and the original string is called the source. 
    - You can remember the order of the arguments by noting that it is the same as the order in an assignment statement (the target string is on the left)
  - It is your responsibility to make sure the destination array has enough room to copy the source.
  - The first argument need not point to the beginning of an array; this lets you copy just part of an array.
  - The `\0` will be copied as well.
- **F-Return**
  - `strcpy` is type char *. It returns the value of its first argument—the address of a character. 
    - The first argument need not point to the beginning of an array; this lets you copy just part of an array.


**`strncpy()` Function**

```c
// Prototype
char *strncpy(char * restrict s1, const char * restrict s2, size_t n);

// General
strncpy(str_target, str_source, int_length);
```

- **Def+F:** `strncpy` copies the first `int_length` characters in the string `str_target` to `str_source`.
- **Soc:** string.h
- **Cpn**
  - `int_length` is the maximum number of characters to copy.

- **Ab**
  - The same as `strcpy`
- **F-Return**
  - The same as `strcpy`

**`strcmp()` Function**

```c
int str_target[10] = "smaller";
int str_source[10] = "BBBBBBBigger";
strcmp(str_target, str_source);
```

- **Def+F: ** `strcmp` compares the ASCII number of each character within the strings from left to right.
  - The strcmp() function compares strings until it finds corresponding characters that differ, which could take the search to the end of one of the strings. 

- **Soc:** string.h
- **Ab**
  - strcmp() is that it compares strings`(/0` marks the end), not arrays. Therefore, strcmp() can be used to compare strings stored in arrays of different sizes.
  - strcmp() compares all characters, not just letters, so instead of saying the comparison is alphabetic, we should say that strcmp() goes by the machine collating sequence.

- **F-Return**
  - The sum of `target - source` in ASCII code for each elements in strings
    - The exact numerical values, however, are left open to the implementation
    - 0 when the two strings are equal.
    - positive/1 when the target is larger. 
    - negative/-1 when the source is larger

**`strncmp()` function**

```c
strncmp(str_target, str_source, int_length)
```

- **Def+F**
  - The strncmp() function compares the strings until they differ or until it has compared a number of characters specified by a third argument.

- **Ab**
  - The same as strcmp function
- **F-Return**
  - The same as strcmp function

**`strcat()` Function**

```c
int str_target[20] = "hello";  // Larger than the size of the combination
int str_source[10] = "world";
strcat(str_target, str_source);
```

- **Def+F:** The `strcat()` (for string concatenation) function takes two strings for arguments. A copy of the second string is tacked onto the end of the first, and this combined version becomes the new first string. The second string is not altered
- **Soc:** string.h
- **Ab**
  - **Function type:** char*
  - `str_target` must be a string variable.
  - The `strcat()` function does not check to see whether the second string will fit in the first array. 
  -  The first character of the s2 string is copied over the null character of the s1 string.
- **F-Return**
  - It returns the value of its first argument—the address of the first character of the string to which the second string is appended.


**`strncat()` Function**

```c
// General
strncat(str1, str2, length);
```

- **Def+F:**  strncat() functions like `strcat`, except that it takes a third
  argument indicating the maximum number of characters to add.
- **Cpn**
  - **length**:  indicating the maximum number of characters to add
- **Ab**
  - The same as `strcat`

- **F-Return**
  - The same as `strcat`


**`sprintf()` Function**

```c
sprintf(str_target, "%s, %-19s: $%6.2f\n", str_source1, str_source2, str_source3);
```

- **Def+F:**  `sprintf` works like `printf()`, but it writes to a string instead of writing to a display. Therefore, it provides a way to combine several elements into a single string.
- **Soc:** stdio.h

- **Cpn**
  - The first argument to sprintf() is the address of the target string. 
  - The remaining arguments are the same as for printf()—a conversion specification string followed by a list of items to be written.

**`strchr()` Function**

```c
strchr(str, c);
```

- **Def+F:** `strchr` search the address of the character c in the string str. (search character)
- Space
  - string.h

- **F-Return**
  - This function returns a pointer to the first location in the string str that holds the character c. 
  - null pointer if not found

**`strpbrk()` Function**

```c
strpbrk(str_s1, str_s2);
```

- **Def+F:** (string pointer break) This function returns a pointer to the first location in the string s1 that holds any character found in the s2 string. 
- **F-Return**
  - This function returns a pointer to the first location in the string str that holds any character found in the s2 string. 
  - The function returns the null pointer if no character is found.

**`strrchr()` Function**

```c
strrchr(str_s, char_c);
```

- **Def+F:** This function returns a pointer to the last occurrence of the character c in the string s. (The terminating null character is part of the string, so it can be searched for.) 
- **F-Return**
  - Returns a pointer to the last occurrence of the character c in the string s.
  - The function returns the null pointer if the character is not found.

**`strstr()` Function**

```c
strstr(str1, str2);
```

- **Def+F:**  This function returns a pointer to the first occurrence of string s2 in string s1. The function returns the null pointer if the string is not found.

**`atoi()` function**

```c
atoi(str);
```

- **Def+F:** It takes a string as an argument and returns the corresponding integer value. (alphanumeric to integer)
- **Soc:** stdlib.h
- **F-Return**
  - The atoi() function still works if the string only begins with an integer until it encounters something that is not part of an integer.
  - the atoi() function returns a value of 0 if its argument is not recognizable as a number
  - **Lk:** strtol() function, discussed shortly, provides error checking that is more reliable.

**`atof()` function**

- **Def+F:** It takes a string as an argument and returns the corresponding `double` value. (alphanumeric to float)

**`atol()` function**

- **Def+F:** It takes a string as an argument and returns the corresponding integer `long` value. (alphanumeric to long)

**`strtol()` function**

```c
// Prototype
long strtol(const char * restrict nptr, char ** restrict endptr, int base);

// General
strtol(str_nptr, ptr_endptr, int_base);
```

- **Def+F:** strtol() converts a string to a long,
- **Cpn**
  - nptr is a pointer to the string you want to convert
  - endptr is the address of a pointer that gets set to the address of the character terminating the input number
  - base is the number base the number is written in. 
- **Ab**
  - The functions identify and report the first character in the string that is not part of a number. Also, strtol() and strtoul() allow you to specify a number base.
  - **Base:** The strtol() function goes up to base 36, using the letters through 'z' as digits. 

**`strtoul()` function**

- **Def+F:** strtoul() converts a string to an unsigned long
- **Ab**
  - The functions identify and report the first character in the string that is not part of a number. Also, strtol() and strtoul() allow you to specify a number base.
  - **Base:** The strtoul() function does the same as strtol, but converts unsigned values. 

**`stdtod` function**

- **Def+F:** strtod() converts a string to double.
- **Ab**
  - **Base:** The strtod() function does only base 10, so it uses just two arguments.

**casting away const Tips**

```c
#include <stdio.h>  /* for NULL definition */
char * strblk(const char * string)
{
    while (*string != ' ' && *string != '\0')
        string++;  /* stops at first blank or null */
    if (*string == '\0')
        return NULL; /* NULL is the null pointer */
    else
        return (char *) string;
}
```

- Here is a second solution that prevents the function from modifying the string but that allows the return v  alue to be used to change the string. The expression (char *) string is called **“casting away const.”**



## Struct Type 结构体

### Cc-Struct

- `struct` 结构体
  - **Def+F:** 将不同类型的数据组合成一个整体
  - **Ab**
    - Type, or define new types/categories.
  - **Cpn**
    -  members or fields
      - **Ab:** A member can be any C data type—and that includes other structures!
      - **Cat-String**
        - **Lk:** C Primer Plus > Character Arrays or Character Pointers in a Structure
        - If you want a structure to store the strings, it's simpler to use character array members. Storing pointers-to-char has its uses, but it also has the potential for serious misuse.
        - You might have wondered if you can use pointers-to-char instead of character arrays.
          - The answer is that you can, but you might get into trouble unless you understand the implications.
        - character array allocated corresponding space within the structure.
        - pointer to char allocated no space within the structure.
        - In short, the pointers in a pnames structure should be used only to manage strings that were created and allocated
          elsewhere in the program.
  - **F**
    - The C structure is flexible enough in its basic form to represent a diversity of data, and it enables you to invent new forms. 
  - **Cat**
    - linked structures. 
      - Typically, each structure contains one or two items of data plus one or two pointers to other structures of the same type. Those pointers link one structure to another and furnish a path to enable you to search through the overall assemblage of structures. 
- 结构体指针

### Pcp-struct

- 结构体对齐：
  - 结构体的每个成员的大小必须是其最大成员的整数倍。将其转化为最大成员整数倍的和则为该结构体的大小。对于数组也是总体大小而非其类型。
  - 如果多个成员的大小之和小于最大成员(非数组)的大小，那么该成员们共用一个最大成员的大小。

### Mt-Structure Declaration

```c
struct struct_name
{
    member_lists;
};

struct student
{
    int age;
    char name[20];
    float score;
};

struct
{
    int age;
    char name[20];
    float score;
} student; 


```

- **Def:** Setting up a format or layout for a structure
  - A structure declaration is the master plan / template that describes how a structure is put together.
- **Ab**
  - It does not create an actual data object, but it describes what constitutes such an object.
  - A semicolon after the closing brace ends the definition of the structure design. 

**`struct`**

- **Def+F:** `struct` identifies what comes next as a structure. 
  - Next comes an optional `tag` which is a shorthand label you can
    use to refer to this structure.
    - The tag name is optional, but you must use one when you set up structures as we did, with the structure design defined one place and the actual variables defined elsewhere.

### Mt-Define structure variables

-  Declaring a variable to fit that layout

```c
// General
struct tag variable_name;

// Another form
struct book {
char title[MAXTITL];
char author[AXAUTL];
float value;
} library;  /* follow declaration with variable name */

struct {  /* no tag */
char title[MAXTITL];
char author[MAXAUTL];
float value;
} library;

// After declaring the struct type
struct student variable_name;
```

**Pcp**

- Variable names should not be the same as the `struct` type name.

### Mt-Initialization 

```c
// Initialization
struct tag st_var_name =
{
    .member1 = 12,
    .member2 = "Hello",
};

// Initialization
struct student variable_name = {12, "Nino", 100.1};  


```

**Pcp**

- In short, you use a comma-separated list of initializers enclosed in braces. 
- Each initializer should match the type of the structure member being initialized. 
- a regular initializer following a designated initializer provides a value for the
  member following the designated member. Also, the last value supplied for a particular member is the value it gets.
- The new value of 100.1 supersedes the value of 12 provided earlier. 

### Mt-Access the members of structure variables

- Gaining access to the individual components of a structure variable

```c
// After initalization
variable_name.age;
variable_name.name;
struct_arr_name[0];
```

**Structure member operator `.`**

- 

### Mt-IO

```c
scanf("%d%s%f", &variable_name.age, variable_name.name, &variable_name.score);
```

### Mt-Declare an array of structures

```c
// Initialization
struct student struct_arr_name[3];

// Initialization
struct tag st_arr_name[2] =
{
    {.member11 = 12,
    .member12 = "Hello",},
    {.member21 = 12,
    .member22 = "Hello",}
};

// Access Structure
struct_arr_name[1];

// Identify members of an array of structures
struct_arr_name[0].age;



// Input
scanf("%d%s%f", &struct_arr_name[0].age, struct_arr_name[0].name, &struct_arr_name[0].score);
```

- The struct array contain n struct type elements.

### Mt-Nested Structures

```c
// Declaration
struct names {  // first structure
    char first[LEN];
    char last[LEN];
};

struct guy {  // second structure
    struct names handle;  // nested structure
    char favfood[LEN];
    char job[LEN];
    float income;
};

// Define
struct guy fellow = {  // initialize a variable
    { "Ewen", "Villard" },
    "grilled salmon",
    "personality coach",
    68112.00
    };

// Access members in the nested structure.
printf("Hello, %s!\n", fellow.handle.first);
```



### Mt-Pointers to Structures

```c
// Definition
struct
{
    int age;
    char name[20];
    float score;
} student, *student_ptr;  // pointer to structure student 


struct student
{
    int age;
    char name[20];
    float score;
};
struct student *ptr;

// Assignment
struct student str_var_student1;
ptr = &str_var_student1;

// Member Access by Pointer
ptr -> age;
ptr -> age = 80;
(*ptr).age;  // The parentheses are required because the . operator has higher precedence than *.

barney.income == (*him).income == him->income  // assuming the pointer him and the structrue barney: him == &barney

// Nested
ptr -> student_nested.age;
(*ptr).student_nested.age;


// struct array
ptr = arr_students;
ptr = &arr_students[0];
ptr -> age;

//Additon and subtraction of struct array
ptr = ptr + 1;
ptr = ptr - 1;
```

**Lk:** Listing 14.4 The friends.c Program

**Cc- `->`**

- **Def+F:** a structure pointer followed by the -> operator works the same way as a structure name followed by the . (dot) operator. 
- **Ab**
  - It is important to note that him is a pointer, but him->income is a member of the pointed-to structure. So in this case, him->income is a float variable

**Pcp**

- Unlike the case for arrays, the name of a structure is not the address of the structure; you need to use the & operator.

- 

### Mt-Functions using Array of structure

```c
struct funds {
    char bank[FUNDLEN];
    double bankfund;
    char save[FUNDLEN];
    double savefund;
};

struct funds jones[N] = {
        {
            "Garlic-Melon Bank",
            4032.27,
            "Lucky's Savings and Loan",
            8543.94
        },
        {
            "Honest Jack's Bank",
            3620.88,
            "Party Time Savings",
            3802.91
        }
    };

double sum(const struct funds money[], int n)
{
    double total;
    int i;
    for (i = 0, total = 0; i < n; i++)
        total += money[i].bankfund + money[i].savefund;
    return(total);
}
```

- **Lk:** C Primer Plus > listing 14.13 funds4.c
- 

### Mt-Function and Structures

```c
// pase memebers to the function
func(str_var_name.member);
func(&str_var_name.member);  // Of course, if you want a called function to affect the value of a member in the calling function,you can transmit the address of the member:

// pass structure pointer
void func(struct str_name * str_var_name);
func(&str_var_name);

// pass structure as arguments
struct struct_name func(struct struct_name moolah);
func(moolah);
```

**Lk:** C Primer Plus > Listing 14.5 14.6 14.7 funds.c

**Pcp**

- **Pass structure as arguments:** When sum() is called, an automatic variable called moolah is created according to the funds template. The members of this structure are then initialized to be copies of the values held in the corresponding members of the structure stan. Therefore, the computations are done by using a copy of the original structure.
- **Return structures:** Under modern C, including ANSI C, not only can structures be passed as function arguments, they can be returned as function return values. 

**Pcp-Difference between pointer-to-structure and structures when using structures in functions**

-  pointer argument method
  - advantages
    - it works on older as well as newer C implementations
    - it is quick; you just pass a single address. 
  - disadvantage
    -  you have less protection for your data since operations in functions may affect the original structure members.
    -  However, the ANSI C addition of the const qualifier solves that problem even for pointer argument for structures.
- passing structures as arguments
  - advantages
    - One advantage of passing structures as arguments is that the function works with copies of the original data, which is safer than working with the original data.\
    - The programming style tends to be clearer. 
  - disadvantage
    - passing structures are that older implementations might not handle the code 
    - it wastes time and space
- **Mt-Choice**
  - Typically, programmers use structure pointers as function arguments for reasons of efficiency, using const when needed to protect data from unintended changes.
  - Passing structures by value is most often done for structures that are small to begin with.



### Mt-Assignment

```c
struct struct_name struct_var_name1;
struct struct_name struct_var_name2;

// assigning one structure to another
struct_var_name1 = struct_var_name2;  // The same structrue.
```

**Pcp**

- This causes each member of struct_var_name2 to be assigned the value of the corresponding member of struct_var_name1 . This works even if a member happens to be an array.
- 

### Mt-Memory Allocation of structure

```c
void getinfo (struct namect * pst)
{
    char temp[SLEN];
    printf("Please enter your first name.\n");
    s_gets(temp, SLEN);  
    pst->fname = (char *) malloc(strlen(temp) + 1);  // allocate memory to hold name
    strcpy(pst->fname, temp);  // copy name to allocated memory
    printf("Please enter your last name.\n");
    s_gets(temp, SLEN);
    pst->lname = (char *) malloc(strlen(temp) + 1);
    strcpy(pst->lname, temp);
}
```

**Lk:** C Primer Plus > Listing 14.10 names3.c

- One instance in which it does make sense to use a pointer in a structure to handle a string is if you use malloc() to allocate memory and use a pointer to store the address.

### Mt-Compound Literal with structures

```c
(struct book) {"The Idiot", "Fyodor Dostoyevsky", 6.99}  // compound literal of the struct book type
```

- **Ab**
  - It's handy if you just need <u>a temporary structure value.</u>
  - 

### Mt-Flexible Array Member (C99)

```c
struct flex
{
    int count;
    double average;
    double scores[];  // flexible array member
};

struct flex * pf; // declare a pointer
// ask for space for a structure and an array
pf = malloc(sizeof(struct flex) + 5 * sizeof(double));

pf->count = 5;  // set count member
pf->scores[2] = 18.5;  // access an element of the array member
```

- **Def+F:** It lets you declare a structure for which the last member is an array with special properties.
- **Lk-Related**
  - struct hack
  -  However, the struct hack is something that worked for a particular compiler (GCC); it wasn't standard C. The flexible member approach provides a standard-sanctioned version of the technique.

- **Ab**
  - One special property is that the array doesn't exist—at least, not immediately. 
  - The second special property is that, with the right code, you can use the flexible array member as if it did exist and has whatever number of elements you need. 
  - The flexible array member must be the last member of the structure.
  - There must be at least one other member.
  - The flexible array is declared like an ordinary array, except that the brackets are empty.
  - First, don't use structure assignment for copying
    - *pf2 = *pf1;  // don't do this
      This would just copy the nonflexible members of the structure. Instead, use the memcpy() function described in Chapter 16, “The C Preprocessor and the C Library.”
  - Second, don't use this sort of structure with functions that pass structures by value.
    - Instead, use functions that pass the address of the structure.
  - Third, don't use a structure with a flexible array member as an element of an array or a member of another structure.
- **Mt**
  - Instead, you are supposed to declare a pointer to the struct flex type and then use malloc() to allocate enough space for the ordinary contents of struct flex plus any extra space you want for the flexible array member.


### Mt-Anonymous Structures (C11) 

```c
struct person
{
int id;
struct {char first[20]; char last[20];};  // anonymous structure
};

ted.first;
```

- **Def+F:** An anonymous structure is a structure member that is an unnamed structure.
- **F**
  - eliminate nested structures. 
  - The anonymous feature becomes more useful with nested unions.


### Mt-Saving the Structure Contents in a File

```c

fwrite(&primer, sizeof (struct book), 1, pbooks);
```

- **Lk:** 
  - C Primer Plus > Saving the Structure Contents in a File
    - Listing 14.14 The booksave.c Program
- **Cc**
  - A database file could contain an arbitrary number of such data objects. The entire set of information held in a structure is termed <u>a record</u>, and the individual items are <u>fields</u>.
  - A better solution is to use fread() and fwrite() to read and write structure-sized units. 
    - In short, these functions read and write one whole record at a
      time instead of a field at a time.
  - One drawback to saving data in binary representation is that different systems might use different binary representations, so the data file might not be portable. 
    - Even on the same system, different compiler settings could result in different binary layouts.

- 

### Mt-typedef

```C
// General
typedef type_name alias;
typedef unsigned char CHAR;
typedef char * STRING;
typedef void (*V_FP_CHARP)(char *);

// Similar
#define STRING char *

// struct
typedef struct student
{
    int age;
    char name[20];
    float score;
} stu, *stu_ptr;  // struct alias and pointer alias, equal to `struct student`

typedef struct
{
    int age;
    char name[20];
    float score;
} stu;  // name the structure type

// Initialize variables
stu student1 = {0, "F", 0};

// Pointer
stu *ptr = &student1;
stu_ptr *ptr = &student1;
```

```c
// Provide an alias to normal types
typedef int INTEGER;

INTEGER var = 1;
```

- **Def:** The typedef facility is an advanced data feature that enables you to create your own name for a type.
- **Ab**
  - Unlike #define, typedef is limited to giving symbolic names to types only and not to values.
  - The typedef interpretation is performed by the compiler, not the preprocessor.
  - Within its limits, typedef is more flexible than #define.
  - **Space-Scope**
    - The scope of this definition depends on the location of the typedef statement. 
      - If the definition is inside a function, the scope is local, confined to that function. 
      - If the definition is outside a function, the scope is global.
  - Often, uppercase letters are used for these definitions to remind the user that the type name is really a symbolic abbreviation, but you can use lowercase, too.
- Difference between #define and typedef
  - **Lk:** C Primer Plus > typedef: A Quick Look

- **F:** 
  - Define or alias the struct type.
    - use typedef is to create convenient, recognizable names for types that
      turn up often. 
    - using typedef is that typedef names are often used for complicated types.
    - You can omit a tag when using typedef to name a structure type

  - Help document
  - Increase portability

### Mt-A systematic approach to defining types

**Pcp**

- What constitutes a type? 
  - A type specifies two kinds of information: a set of properties and a set
    of operations
  - Thus, firstly you have to provide a way to store the data, perhaps by designing a structure. Second, you have to provide ways of manipulating the data.

**Pc**

1. Provide an abstract description of the type's properties and of the operations you can perform on the type. 
   - This description shouldn't be tied to any particular implementation. It shouldn't even be tied to a particular programming language. Such a formal abstract description is called an abstract data type (ADT).

2. Develop a programming interface that implements the ADT. 
  - That is, indicate how to store the data and describe a set of functions that perform the desired operations. 
  - In C, for example, you might supply a structure definition along with prototypes for functions to manipulate the structures. These functions play the same role for the user-defined type that C's built-in operators play for the fundamental C types. Someone who wants to use the new type will use this interface for her or his programming.
3. Write code to implement the interface. 
  - This step is essential, of course, but the programmer using the new type need not be aware of the details of the implementation.






## Unions

```c
// Union Template
union hold {
	int digit;
	double bigfl;
	char letter;
};

// Union Variable
union hold fit;  // union variable of hold type
union hold save[10];  // array of 10 union variables
union hold * pu;  // pointer to a variable of hold type

// Access fields
fit.digit = 23;  // 23 is stored in fit; 2 bytes used
fit.bigfl = 2.0;  // 23 cleared, 2.0 stored; 8 bytes used
fit.letter = 'h';  // 2.0 cleared, h stored; 1 byte used
pu = &fit; // pointer to union
x = pu->digit;  // same as x = fit.digit
```

- **Def+F:** A union is a type that enables you to store different data types in the same memory space (but not simultaneously). 
- **Ab**
  - The members in unions need not to be filled totally. 
    - A structure with a similar declaration would be able to hold <u>an int value and a double value and a char value</u>. This union, however, can hold <u>an int value or a double value or a char value.</u>
  - The compiler allots enough space so that it can hold the largest of the described possibilities. 
    - In this case, the biggest possibility listed is double, which requires 64 bits, or 8 bytes, on our system.
  - <u>Only one value is stored/held in an union at a time</u>. The union stores a single data item type from a list of choices. You can't store a char and an int at the same time, even though there is enough space to do so. 
  - It is your responsibility to write the program so that it keeps track of the data type currently being stored in a union.
- **F**
  - A typical use is a table designed to hold a mixture of types in some order
    that is neither regular nor known in advance
  - By using an array of unions, you can create an array of equal-sized units, each of which can hold a variety of data types.
  - Another place you might use a union is in a structure for which the stored information depends on one of the members.

### Mt-Initialization

```c
union hold valA;
valA.letter = 'R';
union hold valB = valA; // initialize one union to another
union hold valC = {88}; // initialize digit member of union
union hold valD = {.bigfl = 118.2}; // C99 designated initializer
```

- In particular, you have three choices: You can initialize a union to another
  union of the same type, you can initialize the first element of a union, or, with C99, you can use a designated initializer:

### Mt-Anonymous Unions (C11)

```c
struct car_data {
char make[15];
int status; /* 0 = owned, 1 = leased */
union {
	struct owner owncar;
	struct leasecompany leasecar;
};
};
```

- **Def:** an anonymous union is an unnamed member union of a structure or union. 



## Enumeration

```c
// Declaration
enum spectrum {red, orange, yellow, green, blue, violet};
enum spectrum color;


```

> - The first declaration establishes spectrum as a tag name, which allows you to use enum spectrum as a type name. 
>   - The identifiers within the braces enumerate the possible values that a spectrum variable can have. These symbolic constants are termed **enumerators**. 
>   -  type int
> - The second declaration makes color a variable of that type. 

- **Def-enumerated type** 

  - By using the `enum` keyword, you can create a new "type"(enumerated type) and specify the values it may have.

- **Obj2**
  - The purpose of enumerated types is to enhance the readability of a program

- **Ab**
  - Actually, `enum` constants are type `int`; therefore, they can be used wherever you would use an int.
  - Note that the enumerated types are for internal use. If you want to enter a
    value of orange for color, you have to enter a 1, not the word orange, or you can read in the string "orange" and have the program convert it to the value orange.

- **Mt-Value**

  - **Default Values**

    - By default, the constants in the enumeration list are assigned the integer values 0, 1, 2, and so on.

  - **Assigned Values**

    - You can choose the integer values that you want the constants to have

      ```c
      enum levels {low = 100, medium = 500, high = 2000};
      enum feline {cat, lynx = 10, puma, tiger};
      ```

    - If you assign a value to one constant but not to the following constants, the following constants will be numbered sequentially.

      - Then cat is 0, by default, and lynx, puma, and tiger are 10, 11, and 12, respectively.

- **F**
  - You can use the enumerated type to <u>declare symbolic names</u> to represent integer constants



## Pointer 指针

### Cc

- **Pointer**
  - **Def:** a pointer is a variable (or, more generally, a data object) whose value is a memory address.
  - **Ab**
    - **Address of Pointer:** Address of pointer itself is different from the address of the object which is pointed to by the pointer.
    - **Size:** 64x > 8 bytes, 32x > 4 bytes
    - **Value:** the address of the <u>object to which it points</u>
    - **Type of pointer itself:** Type point to [particular type]
      - **Att:** Not the type the pointer points to.
    - **Hardware dependent**
      -  **byte addressable** : Each byte in memory is numbered sequentially
  - **Pcp**
    - **Pointer Addition**: Add 1 to a pointer, C adds one storage unit.

### Mt-Declaration

```c
// General
type *ptr;
// e.g.
int *ptr;  // ptr is a pointer to an integer variable
int *ptr1, *ptr2;

// Array and Pointer
char* psa[20];  // psa is an array of 20 pointers to char.
char (*psa)[20];  // psa is a pointer to an array of 20 chars; 2D array
int board[8][8]; // an array of arrays of int
int ** ptr; // a pointer to a pointer to int
int * risks[10]; // a 10-element array of pointers to int
int (* rusks)[10]; // a pointer to an array of 10 ints
int * oof[3][4]; // a 3 x 4 array of pointers to int
int (* uuf)[3][4]; // a pointer to a 3 x 4 array of ints
int (* uof[3])[4]; // a 3-element array of pointers to 4-element arrays of int
char * fump(int); // function returning pointer to char
char (* frump)(int); // pointer to a function that returns type char
char (* flump[3])(int);// an array of 3 pointers to functions that return type char

// pointer-to-constant
const int *ptr;  // The value of pointer could not be changed but the pointer could point to other locations.
int const *ptr;  // Declare and initialize a pointer so that it can’t be made to point elsewhere. Such a pointer can still be used to change values, but it can point only to the location originally assigned to it.
const int const *ptr;  // Neither change values nor change the location to which pointer points.
```

**Lk-Relationships between different address indicators**

- C Primer Plus > Figure 9.5  Declaring and using pointers.

**Pcp**

- The type specification identifies the type of variable pointed to, and the asterisk (*) identifies the variable itself as a pointer.
- The space between the * and the pointer name is optional. Often, programmers use the space in a declaration and omit it when dereferencing a variable.

**Pcp-Pointer-to-constant**

- **Lk:** C Primer Plus > Array and Pointer > More About const
- It's valid to assign the address of either constant data or non-constant data to a pointer-to-constant
- only the addresses of non-constant data can be assigned to regular pointers
  - This is a reasonable rule. Otherwise, you could use the pointer to change data that was supposed to be constant.

**Pcp-Pointer to array**

- The trick to unraveling these declarations is <u>figuring out the order</u> in which to apply the modifiers. 
  - 简而言之，优先级判断从外到内，其意义解释从内到外
- The [], which indicates an array, and the (), which indicates a function, have the same precedence. This precedence is higher than that of the * indirection operator, which means that the following declaration makes risks an array of pointers rather than a pointer to an array: int * risks[10];
- The [] and () associate from left to right. Thus, the next declaration makes goods an array of 12 arrays of 50 ints, not an array of 50 arrays of 12 ints:
  `int goods[12][50]`;
- Both [] and () have the same precedence, but because they associate from left to right, the following declaration groups * and rusks together before applying the brackets. This means that the following declaration makes rusks a pointer to an array of 10 ints: int (* rusks)[10];

### Mt-Assignment

```c
ptr = &var;  // Assign the address of the var variable to the ptr pointer. Or ptr points to var.
*ptr = val;  // Store the value to the address to which pointer points.
```

**Pcp**

- Remember, creating a pointer only allocates memory to store the pointer
  itself; it doesn't allocate memory to store data. Therefore, before you use a pointer, it should be assigned a memory location that has already been allocated.

### Pcp-Pointer Compatibility

- Pointers could be compatible only when the two pointers are the same type. Att: Remind that the pointer to pointer should be considered as different type as pointer to type.

- As we saw earlier, assigning a const pointer to a non-const pointer is not safe, because you could use the new pointer to alter const data. 

- But assigning a non-const pointer to a const pointer is okay, only if you're dealing with just one level of indirection. But such assignments no longer are safe when you go to two levels of indirection. 

  ```c
  const int **pp2;
  int *p1;
  const int * p2;
  const int n = 13;
  // One Indirection
  p2 = p1;
  
  // Two indirection
  pp2 = &p1; // allowed, but const qualifier disregarded
  *pp2 = &n; // valid, both const, but sets p1 to point at n
  *p1 = 10; // valid, but tries to change const n
  ```

- 

### Mt-Use pointers to communicate between functions

```c
int var1, var2;

int func(int * para1, int * para2)
{
   int temp;
   temp = *para1;
   para1 = *para2;
   para2 = temp;
}
```

### Mt-Pointer to Functions

```c
// Declaration
char (* pf) (int);  // pf is a pointer to a function with int argument that returns type char

// Assignment
void ToUpper(char *);
void ToLower(char *);
int round(double);
void (*pf)(char *);
pf = ToUpper;  // valid, ToUpper is address of the function
pf = ToLower;  // valid, ToLower is address of the function
pf = round;  // invalid, round is the wrong type of function
pf = ToLower();  // invalid, ToLower() is not an address

// Access functions
void ToLower(char *);
void (*pf)(char *);
char mis[] = "Nina Metier";
pf = ToUpper;
(*pf)(mis);  // apply ToUpper to mis (syntax 1)
pf = ToLower;
pf(mis);  // apply ToLower to mis (syntax 2)

//*Mt- Use a function pointer as an argument to a function
void show(void (* fp)(char *), char * str);
show(ToLower, mis);  /* show() uses ToLower() function: fp = ToLower */
show(pf, mis); /* show() uses function pointed to by pf: fp = pf */

void show(void (* fp)(char *), char * str)
{
    fp(str);
	(*fp)(str); /* apply chosen function to str */
	puts(str); /* display result */
}
```

- **Def+F:** A pointer to a function can hold the address marking the start of the function code.
- **Ab**
  - After you have a function pointer, you can assign to it the addresses of functions of the proper type. <u>In this context, the name of a function can be used to represent the address of the function</u>.
  - 

- **Mt-Declaration**
  - When declaring a function pointer, you should specify the function signature, that is, the return type for the function and the parameter types for a function.
    - **Trick:** if you want to declare a pointer to a specific type of function, you can declare a function of that type and then replace the function name with an expression of the form (*pf) to create a function pointer declaration.
  - Reading this declaration, you see the first parentheses pair associates the * operator with pf, meaning that pf is a pointer to a function. This makes (*pf) a function, which makes (char *) the parameter list for the function and void the return type.
- **Mt-Access**
  - Each approach sounds sensible. 
  - Here is the first approach: Because pf points to the ToUpper function, `*pf` is the ToUpper function, so the expression (`*pf`)(mis) is the same as ToUpper(mis). Just look at the declarations of ToUpper and of pf to see that ToUpper and(`*pf`) are equivalent. 
  - Here is the second approach: Because the name of a function is a pointer, you can use a pointer and a function name interchangeably, hence pf(mis) is the same as ToLower(mis). Just look at the assignment statement for pf to see that pf and ToLower are equivalent. 

- **Mt- Use a function pointer as an argument to a function.** 
  
- **F**
  - Typically, a function pointer is used as an argument to another function, telling the second function which function to use.
  - Build an array of function pointers to function collections.

### Mt-Pointer to void

```c
void * ptr;
```

- **Ab**
  - C and C++ treat pointer-to-void differently. In both languages, you can assign a pointer of any type to type void *.
    - But C++ requires a type cast when assigning a void * pointer to a pointer of another type, whereas C doesn't have that requirement. 
    - Because the type cast version works in both languages, it makes sense to use it. Then, if you convert the program to C++, you won't have to remember to change that part.

### Mt-Pointer Operations

**Lk:** Pointer-Related Operator

#### Assignment

**Tips:** Assign a address to pointer

```c
int * ptr, * ptr1;
int arr[5] = {0, 1, 2, 3, 4};
ptr = &arr[0];
ptr = arr;
ptr = ptr2;

```

**Pcp**

- Note that the address should be compatible with the pointer type. That is, you can't assign the address of a double to a pointer-to-int, at least not without making an ill-advised type cast.

#### Value finding (Dereference)

```c
int * ptr;
int val;
int arr[5] = {0, 1, 2, 3, 4};
val = *ptr;
```

**Pcp**

- Do not dereference an uninitialized pointer. 

#### Taking a pointer address(Reference)

```c
int * ptr, * address;
address = &ptr;

```

#### Pointer Addition

```c
// Addition
int arr[5] = {0, 1, 2, 3, 4};
int unit;
arr + unit == &arr[unit];
arr++;
```

**Pcp-Addition**

- You can use the + operator to add an integer to a pointer or a pointer to an integer. In either case, the integer is multiplied by the number of bytes in the pointed-to type, and the result is added to the original address.
- The result of addition is undefined if it lies outside of the array into which the original pointer points, except that the address one past the end element of the array is guaranteed to be valid.
- The changes of <u>the value of the pointer(The address of the object the pointer points to)</u> will not modify <u>the address of pointer itself.</u>
- C guarantees that, given an array, a pointer to any array element, or to the position after the last element, is a valid pointer
- The effect of incrementing or decrementing a pointer beyond these limits of array range is undefined.
- Even though a pointer to one past the end element is valid, it's not guaranteed that such a one-past-the-end pointer can be dereferenced.
- A pointer could not be added by another pointer.

#### Pointer Subtraction

```c
// Subtraction
int * ptr, * ptr2;
int sub;
ptr - sub;
ptr--;
ptr - ptr2;
```

**Pcp**

- You can use the - operator to subtract an integer from a pointer; the pointer has to be the first operand and the integer value the second operand.
- The result of subtraction is undefined if it lies outside of the array into which the original pointer points, except that the address one past the end element of the array is guaranteed to be valid.
- `*––ptr` means to decrement the pointer by 1 and use the value found there. `––*ptr`means to take the value pointed to by pc and decrement that value by 1 (for example, H becomes G) (Similarly Increment)
  - `*ptr--` and `*(ptr--)` are similar decrement pointer situations.


### F-Application

#### 传递  Swap

- 主要是用于在不同函数中修改变量值而非调用
- **Lk:** C Primer Plus > swap3.c

#### 偏移

- **Lk:** This note > Pointer Addition and Pointer Subtraction

### Mt-堆栈空间动态内存申请 Heap Stack

**Cc**

- 栈空间：系统设定的类型大小的空间
- 堆空间：变量大小的空间

**`malloc()` function**

```c
ptr = malloc(size);  // malloc returns void potiner type
str_ptr = (char *)malloc(size);  // Change the type of ptr

// Free the memory of stack
free(ptr);
```

- **Def + F:** 
- **Soc:** stdlib.h, stack rather than heap





## Void



# Memory Management System

## Cc-Basic Concepts

- **Data Object/Object**
  - **Def**: Data object is a general term for a region of data storage that can be used to hold values
    - Each entity stored value occupies physical memory. C literature uses the term object for such a chunk of memory.
  - **Ab-区分不同的Objects**
    - Different variable name
    - Different data storage location
      - specify an element of an array, a member of a structure, or use
        a pointer expression that involves the address of the object.
    - Object refers to the actual data storage, but an lvalue is a label used to identify, or locate, that storage

  - **Ab**
    - An object might not yet actually have a stored value, but it will be of the right size to hold an appropriate value.
    - You can describe an object in terms of its storage duration, which means how long it stays in memory 
  - **Cat**
    - **Class object**: Class object encompass both data and permissible operations on the data.
      - The object-oriented programming use the word <u>object</u> to indicate class object.
      - 

- **Identifier**: An identifier is a name, in this case one that can be used to designate the contents of a particular object. 
  - **Ab**
    - You can describe an identifier used to access the object by its scope and its linkage, which together indicate which parts of a program can use it

  - **e.g.**
    - Variable name
      - A variable name isn't the only way to designate an object. 

    - **OPP**
    - `*ptr` 
      - The pointer dereference also designates an object, but `*ptr` is not a identifier since it is not a name. However, `*ptr` is an entity.

- **Entity**: The identifier entity is <u>how the software (the C program) designates the object that's stored in hardware memory</u>. 
  - Entity is an identifier that is an lvalue, and *pt is an expression that is an lvalue.

- **Defining declaration**: The declaration causes storage to be set aside for the variable. It constitutes a definition of the variable

- **Referencing declaration**: The second declaration merely tells the compiler
  to use the tern variable that has been created previously, so it is not a definition




## Cc-Storage and range terms

### Scope

#### Cc-Namespace

- **Def:** C uses the term **namespace** to identify parts of a program in which a name is recognized. 
- **Ab**
  - Structure tags, union tags, and enumeration tags in a particular scope all share the same namespace, and that namespace is different from the one used by ordinary variables. 
  - However, it can be confusing to use the same identifier in two different ways; also, C++ doesn't allow this because it puts tags and variable names into the same namespace.
- **Cat**
  - Scope is part of the concept

#### Cc-Scope

- **Def**: Scope describes the region or regions of a program that can access an identifier. 
  - Scope determines which parts of a program can access the data.


#### Ab

- Scope and linkage describe the visibility of identifiers

#### Cat

##### Block scope

- **Cc-Block**
  - **Block**: A **block** is a region of code contained within an opening brace and the matching closing brace. (even without the function or statements before the braces.)
    - **e.g.**
      - the entire body of a function is a block. 
      - Any compound statement within a function also is a block
    - **Ab**
      - C99 expanded the concept of a block to include the code controlled by a for loop, while loop, do while loop, or if statement, even if no brackets are used. 
      - An entire loop is a sub-block to the block containing it, and the loop body is a sub-block to the entire loop block. Similarly, an if statement is a block, and its associated sub-statement is a sub-block to the if statement.
        - Lk: C Primer Plus > Listing 12.2 The forc99.c Program
      - 
  - **Block scope**: The scope within the block region.
    - **e.g.**
      - A variable defined inside a block has block scope, and it is visible from the point it is defined until the end of the block containing the definition. 
- **Ab**
  - The program examples to date have used block scope almost exclusively for variables.
  - Traditionally, variables with block scope had to be declared at the beginning of a block.
    - C99 relaxed that rule, allowing you to declare variables anywhere in a block. 

  - 

##### Function scope

- **Ab**
  - Function scope applies just to labels used with `goto` statements.
    - This means that even if a label first appears inside an inner block in a function, its scope extends to the whole function
    - 

##### Function prototype scope

- **Ab**
  - Function prototype scope applies to variable names used in function prototypes,
  - Function prototype scope runs from the point the variable is defined to the end of the prototype declaration.
  - 

##### File scope

- **Def:** 
- **Ab**
  - A variable with its definition placed outside of any function has file scope. A variable with file scope is visible from the point it is defined to the end of the file containing the definition.
  - When we describe a variable as having file scope, it's actually visible to the whole translation unit. 
- **Cat**
  - global variable
  - **Translation unit** and file :After C preprocessing, the compiler sees <u>a single file containing information from your source code file and all the header files</u>. This single file is called a <u>translation unit.</u> 

### Linkage

#### Cc-Linkage

- **Def:** Linkage describes the extent to which a variable defined in one unit of a program can be linked to elsewhere.

#### Ab

- Scope and linkage describe the visibility of identifiers

#### Pcp

- how can you tell whether a file scope variable has internal or external linkage? 
  - You look to see if the storage class specifier static is used in the external definition

#### Cat

- **External Linkage/global scope**
  
  - **File scope with external Linkage**
    - A variable with external linkage can be used anywhere in a multifile program. 
  
- **Internal Linkage/program scope**
  
  ```c
  static int id = 3;  // The static means that the identifier is file scope with internal linkage.
  int main(){
      // processes
  }
  ```
  
  - **File scope with internal Linkage**
    - A variable with internal linkage can be used anywhere in a single translation unit.
  
- **No Linkage**
  - **e.g.** Variables with block scope, function scope, or function prototype scope have no linkage. 



### Storage Duration

#### Cc-Storage Duration

#### Def

- 

#### Ab

- Storage duration describes the persistence of the objects accessed by these identifiers.

#### Cat

- **Static storage duration**
  - **Def+F:** If an object has static storage duration, it exists throughout program execution.
  - **Ab**
    - The keyword `static` indicates the linkage type, not the storage duration. All file scope variables, using internal linkage or external linkage, have static storage duration.
    - With the keyword `static`, a variable can have block scope but static storage duration
    - if you initialize a variable with static storage duration (such as static external linkage, static internal linkage, or static with no linkage), you have to use constant values.
  - **e.g.**
    - Variables with file scope have static storage duration. 
      -  the keyword static indicates the linkage type, not the storage duration. 
- **Thread storage duration**
  - **Def+F:** An object with thread storage duration exists from when it's declared until the thread terminates.
    - Such an object is created when a declaration that would otherwise create a file scope object is modified with the keyword `_Thread_local`.
    - When a variable is declared with this specifier, each thread gets its own private copy of that variable.

  - **Space:** Thread storage duration comes into play in concurrent programming, in which program execution can be divided into multiple threads

- **Automatic storage duration**
  - **Def+F:** The object with automatic storage duration means that it  exists within the block or other scopes which have particular borders.
    - The idea is that memory used for automatic variables is a workspace or scratch pad that can be reused.

  - **Ab**
  - **e.g.**
    - Variables with block scope normally have automatic storage duration.
    - These variables have memory allocated for them when the program enters the block in which they are defined, and the memory is freed when the block is exited.

- **Allocated storage duration**
  - **Def+F:** Dynamically allocated memory comes into existence when malloc() or a related function is called, and it''s freed when free() is called.
  - **Ab**
  -  **e.g.**
    - **Lk:**` malloc()` 



## Storage Classes and Storage-class Specifiers

### Cc-Summary and Basic Concepts

| Storage Class                | Duration  | Scope | Linkage  | Declare                                             | Initialization   |
| ---------------------------- | --------- | ----- | -------- | --------------------------------------------------- | ---------------- |
| automatic                    | Automatic | Block | None     | In a block (with the keyword `auto`)                | no default value |
| register                     | Automatic | Block | None     | In a block with the keyword `register`              | no default value |
| static with no linkage       | Static    | Block | Internal | In a block with the keyword `static`                | default 0        |
| static with external linkage | Static    | File  | External | Outside of all functions                            | default 0        |
| static with internal linkage | Static    | File  | Internal | Outside of all functions with the key word `static` | default 0        |

> - The Duration、Scope and Linkage define the Ab of different storage classes
> - static series are initialized just once, at compile time. If not initialized explicitly, its bytes are set to 0.

### Storage-class Specifer

#### Cc

- **Storage-class specifier**: Storage-class specifiers are key words which document that you are intentionally overriding an external variable definition or that it is important not to change the variable to another storage class.

#### Ab

- In particular, in most cases you can use no more than one storage-class specifier in a declaration, so that means you can't use one of the other storage-class specifiers as part of a typedef.
  - The one exception is that _Thread_local may be used together with static and extern.

#### auto

`auto`

- **Def:** The `auto` specifier indicates a variable with <u>automatic storage duration</u>. 
- **F:** It can be used only in <u>declarations</u> of variables <u>with block scope</u>, which already have automatic storage duration, so its main use is documenting intent.

#### register

`register`

- **Def:** The register specifier also can be used only with variables of block scope. It puts a variable into the register storage class.
- **F:**  Using`register` variable amounts to a request to minimize the access time for that variable. It also prevents you from taking the address of the variable.

#### static

`static`

- **Def+F:** The `static` specifier creates an object with static storage duration, one that's created when the program is loaded and ends when the program terminates. 

- **F:**
  - If static is used with a file scope declaration, scope is limited to that one file. (Static variable with internal linkage)
  
  - If static is used with a block scope declaration, scope is limited to that block. (Static variable with no linkage) 
  
  - Thus, the object exists and retains its value as long as the program is running, but it can be accessed by the identifier only when code within the block is being executed. 
  
  - Memory for a variable with static storage duration is allocated at compile time and lasts as long as the program runs. If uninitialized, such a variable is set to 0.
  
  - <u>The new use</u> is to tell the compiler how a formal parameter will be used. For example, consider this prototype 
  
    - This use of static indicates that the actual argument in a function call will be a pointer to the first element of an array having at least 20 elements.
  
      ```c
      double func(double ar[static 20]);
      ```
  
    - **F**
  
      - The purpose of this is to enable the compiler to use that information to optimize its coding of the function.
      - One is the compiler, and it tells the compiler it is free to make certain assumptions concerning optimization. The other audience is the user, and it tells the user to only provide arguments that satisfy the static requirements.
  
    

#### extern

`extern`

- **Def:** The extern specifier indicates that you are declaring a variable that has been defined elsewhere.
- **F:**
  - If the declaration containing extern has file scope, the variable referred to must have external linkage. (static variable with external linkage)
  - If the declaration containing extern has block scope, the referred-to variable can have either external linkage or internal linkage, depending on the defining declaration for that variable. (referencing declaration)

#### _Thread_local

`_Thread_local`

- **Def:**
- **F:**

#### typedef

`typedef`

```c
// Create an alias for an existing type
typedef original_name alias;
alias variable;
```

- **Def:** typedef mechanism

- **F:**

### Automatic Variables

#### Cc

```c
int func(para)
{
    auto int auto_var;
}
```

- **Def**
  -  A variable belonging to the automatic storage class has automatic storage duration, block scope, and no linkage.
- **Ab**
  - Default
  - Block scope and no linkage imply that only the block in which the variable is defined can access that variable by name.
  - A automatic variable is known only to the block in which it is declared and to any block inside that block
  - You can use `auto` for function parameters
  - **Space:** Computer memory
  - **t:** Automatic storage duration

#### Mt-Storage-class specifier

- `auto`
- **Problem**
  -  C++ has repurposed the `auto` keyword for a quite different use, so simply not using auto as a storage-class specifier is better for C/C++
    compatibility.

#### Mt-Initialization

```c
{
	int auto_var = 2;
	auto int auto_var = 3;    
}

```

- Automatic variables are not initialized unless you do so explicitly
- You can initialize an automatic variable with a <u>non-constant</u> expression, provided any variables used have been defined previously:

#### Mt-Hide

```c
int main()
{
    int hide_var = 1;
    printf("Outside Variable: %d\n", hide_var);
    {
        int hide_var = 2;
        printf("Inner Variable: %d\n", hide_var);
    }
    printf("OUter Variable: %d\n", hide_var);
}
```

- **Lk:** C Primer Plue > Chapter 12 Storage Classes, Linkage and Memory Management > Listing 12.1 The hiding.c Program.

- **Def:** The local variable shadows the outer variable.
  - What if you declare a variable in an inner block that has the same name as one in the outer block? Then the name defined inside the block is the variable used inside the block. We say it **hides the outer definition.** 


### Register Variables

#### Cc

- **Def**
  - A variable declared in a block (or as a parameter in a function header) with the `register` storage class modifier belongs to the register storage class.

- **Ab**
  - Because a register variable may be in a register rather than in memory, you can't take the address of a register variable. 
  - You can use register for function parameters

- **Space**
  - Register variables are stored <u>in the CPU registers</u> or, more generally, in <u>the fastest memory available</u>, where they can be accessed and manipulated more rapidly than regular variables.

#### Mt-Storage-class specifier

- `register`
- declaring a variable as a register class is more a request than a direct order.
  - The compiler has to weigh your demands against the number of registers or amount of fast memory available, or <u>it can simply ignore the request, so you might not get your wish.</u>
  - The types that can be declared register may be restricted. 
    - For example, the registers in a processor might not be large enough to hold type double.

#### Mt-Initalization

```c
register int x = 1;
```

- Its value, if uninitialized, is not undetermined.

### Static Variables with no linkage/with block scope

#### Cc

- **Def**: variables have block scope, no linkage, but static storage duration

  - **Alias**
    - Local static variable
    - Internal static storage class (The internal term means that the variables are within the block rather than the internal linkage.)

- **Ab**

  - After initialization, static variables and external variables are already in place after a program is loaded into memory rather than be a part of the block scope.

  - You can't use static for function parameters

#### Mt-Storage-class specifier

- `static`
-  within the block

#### Mt-Initialization

```c
{
	static int test;
	static int test = 1;    
}

```

- Static variables are initialized to zero if you don't explicitly initialize them to some other value.
- A static variable is initialized just once, when its block is compiled



### Static Variables with External Linkage

#### Cc

- **Def**
  - A static variable with external linkage has file scope, external linkage, and static storage duration.

- **Ab**
  -  **Space-The scope of external variables:** from the point of declaration to the end of the file and possibly other translation units/source files including the file.
  - **Storage Duration**: Static storage duration
- **F**
  - Sometimes multiple c source files might need to share an external variable. The C way to do this is to have a defining declaration in one file and referencing declarations in the other files. 
    - That is, all but one declaration (the defining declaration) should use the `extern` keyword, and only the defining declaration should be used to initialize the variable.
    - Note that an external variable <u>defined</u> in one file is not available to a second file unless it is also <u>declared</u> (by using extern) in the second file. An external declaration by itself only makes a variable potentially available to other files.



#### Mt-Storage-class specifier

- `extern`
- **F:**  The storage class specifier extern tells the compiler that any mention of units in this particular function refers to a variable defined outside the function, perhaps even outside the file.
  - **Att:** The keyword extern indicates that a declaration is not a definition because it instructs the compiler to look elsewhere.
  -  Therefore, don't use the keyword `extern` to create an external definition; use it only to refer to an existing external definition.

- If a particular external variable is defined in one source code file and is used in a second source code file, declaring the variable in the second file with extern is mandatory
- The group of extern declarations inside main() can be omitted entirely because external variables have file scope, so they are known from the point of declaration to the end of the file. 
- 

#### Mt-External names

- The C99 and C11 standards require compilers to recognize the first 63 characters for local identifiers and the first 31 characters for external identifiers.
- The reason the rules for names of external variables are more restrictive than for local variables is that external names need to comply with the rules of the local environment, which may be more limiting.

#### Mt-Initialization

```c
// File 1
int id = 3;  // external

int main(void)
{
   extern int id; // The same as var id above
   
    // processes
}

// File 2 or header.h
extern int id;
```

- Like automatic variables, external variables can be initialized explicitly. 
- Unlike automatic variables, external variables are initialized automatically to zero if you don't initialize them. 
  - applies to elements of an externally defined array, too
-  you can use only constant expressions to initialize file scope variables without variables even defined previously.
- An external variable can be initialized only once, and that must occur when the variable is defined.

### Static Variables with Internal Linkage

#### Cc

- **Def:** Variables of this storage class have static storage duration, file scope, and internal linkage
  - **Alias:** External static variables
- **Ab**
  - **Space:** File scope within the file where the static variables with internal linkage being defined.

#### Mt-Initialization

```c
int id_ex = 2;  // external
static int id = 3;  // internal
int main()
{
    extern int id_ex;  // external linkage
    extern int id;  // internal linkage but externa referencing declaration, the same as the defining declaration before main()
    int id_ex;  // directly use the file scope variable is also permitted.
}
```



### Storage Classes and Functions

#### external

```c
extern int func(para);  // function prototype

extern int func(para)
{
    // processes
}
```

- **Def**: An external function can be accessed by functions in other files.
- **Ab**
  - Default
- **F**
  - The usual practice is to use the extern keyword when declaring functions defined in other files.
    - This practice is mostly a matter of clarity because a function declaration is assumed to be `extern` unless the keyword `static` is used.

#### static

```c
static int func(para);  // function prototype

static int func(para)
{
	//processes
}
```

- **Def**: a static function can be used only within the defining file. 
- **F**: One reason to use the static storage class is to create functions that are private to a particular module, thereby avoiding the possibility of name conflicts.

#### inline



## Memory Management

### Memory Allotment Functions

**`malloc()` Function**

```c
// General
ptd = (type_cast_ptr) malloc(bytes_size);
// e.g.
int var = 10;
double * ptr;
ptd = (double *) malloc(var * sizeof(double));
```

> - The typecast to (double *) is optional in C but required in C++, so using the - typecast makes it simpler to move a program from C to C++.

- **Soc**
  - stdlib.h
- **Cpn-Argument**
  -  takes one argument: the number of bytes of memory you want.
- **Ab**
  - The memory is anonymous; that is, `malloc()` allocates memory but it doesn't assign a name to it
  - **Type:**  Because char represents a byte, `malloc()` has traditionally been defined as type <u>pointer-to-char.</u>
    - Since the ANSI C standard, however, C uses a new type: <u>pointer-to-void</u>
  - The duration of allocated memory is from when malloc() is called to allocate the memory until free() is called to free up the memory so that it can be reused. 
- **F**
  -  allocate more memory as a program run
- **F-Return**
  - Return the address of the first byte of that block
  -  The `malloc()`function can be used to return pointers to arrays, structures, and so forth, so normally the return value is typecast to the proper value.
    - malloc() returns a pointer-to-char in its pre-ANSI version and a pointer-to void under ANSI.
    - Assigning a pointer-to-void value to a pointer of another type is not considered a type clash. 
    - You should use the cast operator if you want to store a different type.
  - If `malloc()` fails to find the required space, it returns the null pointer.

**`free()` Function**

```c
free(ptd);
```

- **Soc**
  - stdlib.h
- **Ab**
  -  The argument to `free()` should be a pointer to a block of memory allocated by `malloc()`; 
    - You can't use `free()` to free memory allocated by other means, such as declaring an array.
- **F**
  - The `free()` function takes as its argument an address returned earlier by `malloc()` and frees up the memory that had been allocated.
  - Some operating systems will free allocated memory automatically when a program finishes, but others may not. So use free() and don't rely on the operating system to clean up for you.
- **Problem**
  - **Memory leak**
    - The program may have run out of memory pool, for example, because of the lost of `free()`.

**`exit()` function**

```c
exit(EXIT_SUCCESS);
exit(EXIT_FAILURE);
```

- **Soc:** prototyped in stdlib.h
- **Cpn-Argument**
  - Return value
-  **F**
  - The exit() function causes the program to terminate, even in functions other than main(). 
  - It flushes all output streams, closes all open streams, and closes temporary files created by calls to the standard I/O function tmpfile(). 
  - Then exit() returns control to the host environment and, if possible, reports a termination status to the environment.
- **F-Return**
  - EXIT_SUCCESS: value 0, indicate normal program termination
  - EXIT_FAILURE:  accept additional integer values denoting particular forms of failure, indicate abnormal termination
    - Different exit values can be used to distinguish between different causes of failure, and this is the usual practice in Unix and DOS programming.
    - However, not all operating systems recognize the same range of possible return values. Therefore, the C standard mandates a rather
      restricted minimum range. 
    - Under ANSI C, using return in the <u>initial call</u> to main() has the same effect as calling exit(). Therefore, in main(), the statement `exit(0);` is equivalent to `return 0;`
      - However,  If you make main() into a recursive program, exit() still terminates the program, but return passes control to the previous level of recursion until the original level is reached. Then return terminates the program
      - exit() terminates the program even if called in a function other than main().

**`atexit()` Function**

```c
// Prototype

// General
atexit(ptr_func);

//e.g.
atexit(too_bad);
```

- **Def + F:** The atexit() function provides a method that you can specify particular functions to be called when exiting by registering the functions to be called on `exit()`
- **Cpn**
  - ptr_func: the atexit() function takes a function pointer as its argument.
- **Ab**
  - ANSI guarantees that you can place at least 32 functions on the list.
  - Each function is added sequentially with a separate call to atexit()
  - When the exit() function is finally called, it executes these functions, with the last function added being executed first.
  - The functions registered by atexit() should be type void functions taking no arguments.
  - Note that registered functions are called even when exit() is not called explicitly; that's because <u>exit() is called implicitly when main() terminates.</u>
- **F-Return**
  - `_Noreturn`
- **F**
  - atexit() registers that function in a list of functions to be executed when exit() is called.

**`alloc` Function**

```c
// General
ptd = (type_cast_ptr) malloc(bytes_size);
// e.g.
int var = 10;
long * ptr;
ptr = (long *) calloc(var, sizeof(long));
```

**Cpn-Arguments**

- size_t, The first argument is the number of memory cells you want.
- size_t, The second argument is the size of each cell in bytes.

**Ab**
- It sets all the bits in the block to zero.
  (Note, however, that on some hardware systems, a floating-point value of 0 is not represented by all bits set to 0.)

**F-Return**
-  calloc() returns a pointer-to-char in its pre-ANSI version and a pointer-to-
  void under ANSI.
- You should use the cast operator if you want to store a different type.

### Memory Usage

- The amount of <u>static memory</u> is fixed at compile time
- The amount of memory used for <u>automatic variables</u> grows and shrinks automatically as the program executes.
- The amount of memory used for <u>allocated memory</u> just grows unless you remember to use free()

### Dynamic Memory Allocation

**Lk**: VLA and malloc and calloc

#### Create an array whose size is determined during runtime

```c
// Array
int var;
int list[var];  // VLA

int ptr;
ptr = (double *) malloc(var * sizeof(double));
ptr = (double *) calloc(var, sizeof(double));

// Multiple-dimensional Array
int rows;
int cols;
double mat[rows][cols];
double (*mat)[cols];

ptr = (double *[cols]) malloc(rows * cols * sizeof(double));

// Extract elements
mat[1][2] == ptr[1][2];

```

**Ab-Difference between VLA and malloc/calloc**

- One difference is that the VLA is automatic storage. On the other hand, the array created using malloc() needn't have its access limited to one function.
  - the calling function could call free() when it is finished. It's okay to use a different pointer variable with free() than with malloc(); what must agree are the addresses stored in the pointers. 
  - However, you should not try to free the same block of memory twice.

#### Relationships between storage classes and dynamic memory allocation

- Typically, a program uses different regions of memory for <u>static objects, automatic objects, and dynamically allocated objects.</u>
- You can think of a program as dividing its available memory into three separate sections: 
  - **Storage Class:** one for static variables with external linkage, internal linkage, and no linkage
    - **Duration:** The data stored in this section is available as long as the program runs.
    - **Memory Allocation:** The amount of memory needed for the static duration storage classes is known at compile time
  - **Storage Class:** one for automatic variables;
    - **Duration:** An automatic variable, however, comes into existence when a program enters the block of code containing the variable's definition and expires when its block of code is exited. 
    - **Memory Allocation:** Therefore, as a program calls functions and as functions terminate, the amount of memory used by automatic variables grows and shrinks. 
      - This section of memory is typically handled as a **stack**. That means new variables are added sequentially in memory as they are created and then are removed in the opposite order as they pass away.
    - **Space**: stack 栈空间
  - **Storage Class:** one for dynamically allocated memory.
    - **Duration:** dynamically allocated memory comes into existence when malloc() or a related function is called, and it''s freed when free() is called. 
    - **Memory Allocation:** Memory persistence is controlled by the programmer, not by a set of rigid rules, so a memory block can be created in one function and disposed of in another function.
      - Because of this, the section of memory used for dynamic memory
        allocation can end up **fragmented**—that is, unused chunks could be interspersed among active blocks of memory.
      - Also, using dynamic memory tends to be a slower process than using stack memory.
    - **Space:** memory heap 堆空间 / free store
  - **Remind:** Register variable is stored in the register memory rather than the common-used memory.



### Move/Modify the memory

`memcpy()` and `memmove()`

```c
// Prototypes
void *memcpy(void * restrict s1, const void * restrict s2, size_t n);
void *memmove(void *s1, const void *s2, size_t n);
```

**Soc:** `string.h`

**Def + F:** The `memcpy()` and `memmove()` functions offer more generic methods to copy different kinds of arrays.

- strings have better methods like `strcpy`

**Cpn**

- s1: The pointer to void which points to the target
- s2: The pointer to void which points to the source
- n: the number of bytes to be copied

**Ab**

- The difference between the two is indicated by the keyword restrict 
  - The memcpy() is free to assume that there is no overlap between the two memory ranges(sole initial means of accessing a data object). 
  - The memmove() function doesn't make that assumption, so copying takes place as if all the bytes are first copied to a temporary buffer before being copied to the final destination
- memcpy() doesn't know or care about data types; it just copies bytes from one location to another. 
- there is no data conversion

**F**

- Both of these functions copy n bytes from the location pointed to by s2 to the location pointed to by s1, and both return the value of s1. 

#### Type Qualifiers

`const` Modifier

**Cc**

- `const` is a method that creates an object, but one whose value cannot be changed.

**Mt**

```c
const int C_VAR = 1;
```

**F**

-  a second way to create symbolic constants—using the `const` keyword to convert a declaration for a variable into a declaration for a constant
-  it lets you declare a type, and it allows better control over which parts of a program can use the constant



`volatile`

```c
volatile int loc1;  /* loc1 is a volatile location */
volatile int * ploc;  /* ploc points to a volatile location */
```

- **Def:** (Volatility) The`volatile` qualifier tells the compiler that a variable can have its value altered by <u>agencies other than the program</u>.
- **Cc-Related**
  -  **Caching**: A smart (optimizing) compiler might notice that you use x twice without changing its value. It would temporarily store the x value in a register.

- **Ab**
  - A value can be both `const` and `volatile`, which means that the value of  the variable can not be changed by this program but could be altered by other agencies.

- **F**
  - `volatile` is designed to facilitate compiler optimization
  - It is typically used for <u>hardware addresses and for data shared with other programs or threads running simultaneously.</u>



`restrict`

```c
restrict int * ptr;
int arr[restrict];
```

- **Def + F:** `restrict`can be applied <u>only to pointers</u>, and it indicates that <u>a pointer is *the sole* initial means of accessing a data object</u>. 
- **Ab**
  - Only to pointer variables
  - The `restrict` pointer has no overlap with other pointers/blocks.
- **F**
  - The `restrict` keyword enhances computational support by giving the <u>compiler</u> permission to optimize certain kinds of code. 
  - You can use the restrict keyword as a qualifier for function parameters that are pointers
    - This means that the compiler can assume that no other identifiers modify the pointed-to data within the body of the function and that the compiler can try optimizations it might not otherwise use.
  - it tells the user to use only arguments that satisfy the restrict requirements.

`_Atomic`

- **Soc:** `stdatomic.h` (Add: `threads.h`)
- **Def+F:** One aspect is the concept of an atomic type for which access is controlled by various <u>macro functions</u>.
- **F**
  - While <u>a thread</u> performs an atomic operation on an object of atomic type, <u>other threads won't access that object.</u>


















# Input/Output

## Cc-I/O

- **Whitespace** consists of spaces, tabs, and newlines. 
  - **F**
    - C uses whitespace to separate tokens from one another
    - scanf() uses whitespace to separate consecutive input items from each other.

- **Echoing the input**: fetch characters from keyboard input and send them to the screen. 
  - **Echoed input** means the character you type shows on screen
  - **Unechoed input** means the keystrokes don't show

- **Low-level I/O**: On one level, it can deal with files by using the basic file tools of the host operating system. 
  - it is impossible to create a standard library of universal low-level I/O functions in different systems, and ANSI C does not attempt to do so

- **Standard I/O package**: higher level I/O creating a standard model and a standard set of I/O functions for dealing with files. C features a family of functions, called the s<u><u>tandard I/O package, that treats different file forms</u>
  on different systems in a uniform manner.</u> 

  - **Cc**
    - **stdin**: Keyboard input is represented by a stream called stdin
    - **stdout**: Output to the screen (or teletype or other output device) is represented by a stream called stdout.
  
  - **Ab**
    
      - C treats input and output devices the same as it treats regular files on storage devices
      - One implication of all this is that you can use the same techniques with keyboard input as you do with files.
      
      - **F** 
          - <u>Differences between systems</u> are handled by specific C implementations so <u>that you deal with a uniform interface.</u>
      
  - **Cat**
      - The getchar(), putchar(), printf(), and scanf() functions are all members of the standard I/O package, and they deal with these two streams
  
- **Stream**: A stream is an idealized flow of data to which the actual input or output is mapped.
  - Conceptually, the C program deals with a stream instead of directly with a file. 
  - That means various kinds of input with differing properties are represented by streams with more uniform properties

- **Active position**

  - **Def:** the active position is a generalization of the screen cursor with which you are probably accustomed. 
    - the standard means the location on the display device

  - 






## Cc-Buffer

**Cc**

- **Buffer:** an intermediate storage area for output

  - **Mt-flushing the buffer:** Sending the output from the buffer to the screen or file

    - It is sent when the buffer gets full, when a newline character is encountered, or when there is impending input

    

- **Unbuffered (or direct) input**: The characters you type are immediately made available to the waiting program

  - **Ab-Advantage of unbuffer**
    - Unbuffered input, on the other hand, is desirable for some interactive programs
  - e.g. The immediate echoing of input characters 

- **Buffered input**: the characters you type are collected and stored in an area of temporary storage called a buffer. Pressing Enter causes the block of characters you typed to be made available to your program. 

  - **Ab-Advantage of buffer**
    - less time-consuming to transmit several characters as a block
      than to send them one by one
    - if you mistype, you can use your keyboard correction features to fix your mistake.
  - **Cat**
    - With Unix, you use the `ioctl()` function (part of the Unix library but not part of standard C) to <u>specify the type of input you want</u>, and getchar() behaves accordingly.
    - In ANSI C, the setbuf() and setvbuf() functions supply some control over buffering, but the inherent limitations of some systems can restrict the effectiveness of these functions.
    - **fully buffered I/O**: The buffer is flushed (the contents are sent to their destination) when it is full.
      - **Ab**
        - usually occurs with file input.
        - buffer size depends on the system, but 512 bytes and 4096 bytes are common values
    - **line-buffered I/O**: The buffer is flushed whenever a newline character shows up
      - **Ab**
        - usually occurs with Keyboard input
  - e.g. delayed echoing







## Mt-Redirection

### Cc-Redirection

**Ab**

- One major problem with redirection is that it is associated with the operating system, not C.

**F**

- Redirection enable your program to use a file instead of the keyboard for input, and redirecting output enables it to use a file instead of the screen for output.

**Cat+Mt**-Redirection

- explicitly use special functions that open files, close files, read files, write in files, and so forth
- use a program designed to work with a keyboard and screen, but to redirect input and output along different channels—to and from files
- The 2nd approach (redirection) is more limited in some respects than the first, but it is much simpler to use, and it allows you to gain familiarity with common file-processing techniques.

### Unix, Linux, and Windows Command Prompt Redirection

- **Lk:** TLCL or the LinuxShell.ipynb in the iToolkits





## Mt-User Interface

### Pcp

- You should, of course, provide clear instructions to the user, but no matter how clear you make them, someone will always misinterpret them and then blame you for poor instructions.
- When you write interactive programs, you should try to anticipate ways in which users might fail to follow instructions. Then you should design your program to handle user failures gracefully. Tell them when they are wrong, and give them another chance.



## Mt-Input Validation

- Input types should be validated.
- Input requirement for the practical usage should be validated.





## Mt-Input/Output Functions

### General I/O functions

`printf()` **function**

```c
// printf(Control-string, items);
printf("Print var %d, number %d, equation %d.\n", var1, num2, equation3);

```

**Def:** `printf`  is a <u>formatting</u> print function

**Mt** 

- Transfer different types to string and store in the stdout buffer, and then print the output when it flush buffer.

- The default output should be right alignment.

**F-Return**

-  `printf`returns the number of characters it printed
  - Note that the count includes all the printed characters, including the spaces and the unseen newline character.
- If there is an output error, `printf()` returns a negative value
- The return value for `printf()` is incidental to its main purpose of printing output, and it usually isn't used.
- One reason you might use the return value is to check for output errors. This is more commonly done when writing to a file rather than to a screen.

**Cpn-Specifier/Format specifier/Conversion specifications**

- **Lk:** C Primer Plus > Table 4.3

- **Def:** conversion specifications specify how the data is to be converted into displayable form

- **Ab**

  - The  specifiers determine how data is displayed, not how it is stored

- **F**

  - what a conversion specification converts?
    - It converts a value stored in the computer in some binary format to a series of characters (a string) to be displayed.

- **Cpn**

- `%`  is a <u>placeholder</u> to show where the value of variable is to be printed, which alerts the program that a list of variables is to be printed at that locations <u>sequentially</u>.

  - `%3d` means that the variable value could be used as <u>decimal integer</u> with 3 number space for presentation.

    - `%ld`: `long` type
    - `%lld`: `long long` type
    - `%hd`: `short` type
    - `%hu`: `unsigned short` type
    - `%i`: `%i` is the same as `%d`

  - `%o` : Display an integer <u>in octal notation</u> transferred from decimal number

    - `%#o`: Generate the `0` prefix
    - `%lo`: `long` type

  - `%x` : Display an integer i<u>n hexadecimal notation</u> transferred from decimal number

    - `%#x`: Generate the `0x` prefix

    - `%#X`: Generate the `0X` prefix

      `%lx`: `long` type

  - `%5.2f` means <u>floating-point and double value.</u> 
    - The 5.2 modifier to the %f specifier fine-tunes the appearance of the output so that it displays two places to the right of the decimal with 5 number space.

    - `%e` to print them in exponential notation

    - `%a` or `%A`: Display an hexadecimal float or double number

    - `%Lf` or `%Le` or `%La` to print the `long double` type.

  - `%c`: `char` type

  - `%s`:  character strings

  - `%u`: `unsigned`  adjective type

  - `%zd`: for the result of `sizeof` and `strlen`

  - `%g`: Use %f or %e, depending on the value. The %e style is used if the exponent is less than −4 or greater than or equal to the precision.

  - `%p`: pointer/address

  - `%%`: Prints a percent sign.
  
  - `*`: Awaiting argument
  
    - Example
  
      ```c
      printf("%*d", width, number);
      ```
  

**Cpn-Conversion specifications modifier**

- **Lk:** C Primer Plus > Table 4.4 and 4.5
- **Def+F:** Modifiers which could change the presentation of original conversion specification.
- **Space:** 
  - Inserting modifiers between the % and the defining conversion character.
  - If you use more than one modifier, they should be in the same order as they appear in Table 4.4.
- **Pcp**
  - Not all combinations are possible.
- **Cpn**
  - **flags**
    - `-`: left alignment/ left-justified
      - e.g. `%-d`
    - `+`:  Signed values are displayed with a plus sign, if positive, and with a minus sign, if negative.
      - Example: "%+6.2f".
    - `space` or : Signed values are displayed <u>with a leading space (but no sign) if positive</u> and <u>with a minus sign if negative</u>. 
      - **Att:** A `+` flag overrides a space
      - Example: "% 6.2f"
    - `#`
      - `#`: Use an alternative form for the conversion specification. Produces an initial 0 for the %o form and an initial 0x or 0X for the %x or %X form, respectively. For all floating-point forms, # guarantees that a decimal-point character is printed, even if no digits follow. For %g and %G forms, it prevents trailing zeros from being removed.
        - e.g. "%#o", "%#8.0f", and "%+#10.3E".
    - `0`
      - `0`: For numeric forms, pad the field width with leading zeros instead of with spaces. This flag is ignored if a `-` flag is present or if, for an integer form, a precision is specified.
        - e.g.  "%010d" and "%08.3f"
  - **digit(s)**
    - `5`: The minimum field width.
      - e.g. `%5f` 
  - **.digit(s)**
    - `.2`: Precision. Detail > Lk > Table 4.4
      - e.g. `%.2f`
  - `h`
    - `h`: Used with an integer conversion specifier to indicate a short int or unsigned short int value
      - e.g. "%hu", "%hx", and "%6.4hd"
  - `hh`
    - `hh`:Used with an integer conversion specifier to indicate a signed char or unsigned char value.
      - e.g. "%hhu", "%hhx", and "%6.4hhd"
  - `j`
    - `j`:Used with an integer conversion specifier to indicate an intmax_t or uintmax_t value; these are types defined in stdint.h.
    - 
      - e.g."%jd" and "%8jX"
  - `l`
    - `l`:Used with an integer conversion specifier to indicate a long int or unsigned long int.
      - e.g."%ld" and "%8lu".
  - `ll`
    - `ll`:Used with an integer conversion specifier to indicate a long long int or unsigned long long int. (C99).
      - e.g. "%lld" and "%8llu"
  - `L`
    - `L`: Used with a floating-point conversion specifier to indicate a long double value.
      - e.g. "%Lf" and "%10.4Le".
  - `t`
    - `t`:Used with an integer conversion specifier to indicate a `ptrdiff_t` value. This is the type corresponding to the difference between two pointers. (C99).
      - e.g."%td" and "%12ti".
  - `z`
    - `z`: Used with an integer conversion specifier to indicate a size_t value. This is the type returned by sizeof. (C99)
      - e.g. "%zd" and "%12zx".
    - **Add+Pcp-Type Portability**
      - **Lk:** C Primer Plus > Type Portability
      - First, the stddef.h header file (included when you include stdio.h) defines size_t to be whatever the type your system uses for sizeof; this is called the **underlying type**. 
      - Second, printf() uses the z modifier to indicate the corresponding type for printing.

**Problem-Mismatched Conversions**

**Lk:** C Primer Plus > C4 > Mismatched Conversions

- `signed` and `unsigned` values
  - this results from the way that signed short int values are represented on our reference system. First, they are 2 bytes in size. Second, the system uses a method called the **two's complement** to represent signed integers.
  - Not all systems use this method to represent negative integers. Nonetheless, there is a moral: Don't expect a %u conversion to simply strip the sign from a number.
  - e.g. (简而言之，相同的数字在unsigned和signed中因为储存方式的考量，signed负数部分其实是从unsigned的最大数值处往signed最大正数部分递减) In this method, the numbers 0 to 32767 represent themselves, and the numbers 32768 to 65535 represent negative numbers, with 65535 being −1, 65534 being −2, and so forth. Therefore, −336 is represented by 65536 - 336, or 65200.

- `short int` and `char` values
  - On this system, a short int is 2 bytes and a char is 1 byte. When printf() prints 336 using %c, it looks at only 1 byte out of the 2 used to hold 336. This truncation (see Figure 4.8) amounts to dividing the integer by 256(1 byte) and keeping just the remainder.
  - **modulo 256** means using the remainder when the number is divided by 256.

- `float` and `int`
  - using a %e specifier does not convert an integer to a floating-point number.
  - So even if n3 had the correct number of bits, they would be interpreted differently under %e than under %ld. The net result is nonsense.

- Passing Arguments might also cause mismatches.
  - **Lk:** C Primer Plus > C4 > Passing Arguments
  - 由于读取变量长度是按照变量类型进行并依次叠放入stack中而将变量值转化为字符是按照conversion specification指定的类型依次从stack中取出，因此当前面的一部分变量与conversion specification不同时，整个读取顺序/量就会出现问题。




`scanf()` **function** 

```c
// scanf("control_string", arguments);
scanf("%f", &num);
scanf("%d%f %c", &d_var, &f_var, c_var);  // there should be a space before `%c`
ret = scanf("%d%f %c", &d_var, &f_var, c_var);  // `ret` could be used to collect the return value of scanf, which is the number of match variables.
scanf("%s", input_string);  // handle the input and output of the string without &
```

**Def:** `scanf()` function provides keyboard input to the program. 

**Pcp**

- **Assignment:** The `&weight` tells `scanf()` to assign the input value to the variable named `weight`. The `scanf()` function uses the `&` notation to indicate where it can find the weight variable. 
- The `scanf()` function uses whitespace (newlines, tabs, and spaces) to decide how to <u>divide the input into separate fields</u>.
-  The `scanf()` function uses pointers to variables as its arguments
  - except %c and %s

**F**

- convert the string character-by-character to a numerical value

**F-Return**

- the number of matched inputs.
- If it reads no items, scanf() returns the value 0. 
- It returns EOF when it detects the condition known as “end of file.”

**Cpn-Conversion specifications**

- **Lk:** C Primer Plus > Table 4.6
- **Lk:** `printf` function > Cpn-Conversion specifications
  - The main difference is that printf() uses %f, %e, %E, %g, and %G for both type float and type double, whereas scanf() uses them just for type float, requiring the l modifier for double.
- `digit(s)`: Maximum field width. Input stops when the maximum field width is reached or when the first whitespace character is encountered, whichever comes first.
  - Example: "%10s".
- `%s`: Interpret input as a string. Input begins with the first non-whitespace character and includes everything up to the next whitespace character
  - %s results in scanf() reading a single word that is, a string with no whitespace in it.
  - When scanf() places the string in the designated array, it adds the terminating '\0' to make the array contents a C string
  - when using `%10s`, it means that the scanf() collects up to 10 characters or up to the first whitespace character, whichever comes first.
- `%c`
  - If you use a %c specifier, all input characters are fair game. If the next input character is a space or a newline, a space or a newline is assigned to the indicated variable; <u>whitespace is not skipped.</u>
    - Thus, use scanf(" %c", &ch) reads the first non-whitespace character encountered.

**Cpn-Conversion specification modifier**

- **Lk:** C Primer Plus > Table 4.7
- **Lk:** `printf` function > Cpn-Conversion specification modifier
- `*`: Suppress/Skip corresponding inputs
  - Example: "%*d".
- `l`: 
  - "%le", "%lf", and "%lg" indicate that the value will be stored in type double.
  - Using L instead of l with e, f, and g indicates that the value will be stored in type long double.
  - In the absence of these modifiers, d, i, o, and x indicate type int, and e, f, and g indicate type float.

**Mt-scanf reading process**

- For a `%d` 
- The `scanf()` function begins reading input a character at a time. It <u>skips over whitespace characters (spaces, tabs, and newlines) until</u> it finds a non-whitespace character. 
- scanf() continues reading and saving characters until it encounters a non target value. It then concludes that it has reached the end of a value.
  - If you use a field width, scanf() halts at the field end or at the first whitespace, whichever comes first.
- scanf() places the nondigit back into the input. This means that the next time the program goes to read input, it starts at the previously rejected, nondigit
  character.
- Finally, scanf() computes the numerical value corresponding to the digits (and
  possible sign) it read and places that value in the specified variable.
- **Fail**
- Also, if you use a scanf() statement with several specifiers, C requires the function to stop reading input at the first match failure.
- When scanf() fails to read the specified form of input, it leaves the nonconforming input in place to be read the next time.

**Mt-Regualar character in the format string**

```c
scanf("%d test %d", &num1, &num2);
// Then your input should be something like `5 test 4`
```

- The scanf() function does enable you to place ordinary characters in the format string. Ordinary characters other than the space character must be matched exactly by the input string.



### Character Oriented I/O functions

**Tips:** Single-Character I/O

`getchar()` 

```c
var_char = getchar();
```



- **Def+F:** `getchar()` function reads the next input character, so the program has to wait for input.
  - The getchar() function takes no arguments, and it returns the next character from input

- **Ab**
  - preprocessor macros

  - The fact that getchar() is type int is why some compilers warn of possible data loss if you assign the getchar() return value to a type char variable.

    You don't have to worry about the actual value of EOF, because the #define statement in stdio.h enables you to use the symbolic representation EOF. You shouldn’t write code that assumes EOF has a particular value.f

- **F-Return**

  - the value of an assignment expression is the value of the left member, the `ch = getchar();`
  - Return the input character/a value in the range 0 through 127, because those are values corresponding to the standard character set. or 0-255 for extended character set.
  - In either case, <u>the value -1</u> does not correspond to any character, so it can be used to <u>signal the end of a file(EOF).</u>
    - Some systems may define EOF to be a value other than -1, but the definition is always different from a return value produced by a legitimate input character.


`putchar()` 

```c
putchar(var_char);
```

- **Def+F:** `putchar()` function prints its argument.
- **Ab**
  - preprocessor macros 

`isalpha() `

- **Def+F:** returns a nonzero value if its argument is a letter

`fgets()` function

- 



## Mt-Common Tips

### Skip the rest of the input line

```c
// Skip the white spaces before valid inputs
if (getchar() == ' ')
    continue;

// At the end of the loop to skip the rest of the input line
while (getchar() != '\n')
	continue;

// dispose of non-integer input
scanf("%*s");
```



## File Input/Output

### Cc-File Input and Output

- **File**: A file is an area of memory in which information is stored.
  - **Def:** A file is a named section of storage, usually on a disk, or, more recently, on a solid-state device.
  - **Ab**
    - C views a file as a continuous sequence of bytes, each of which can be read individually.

  - **Ab-End of file**
    - a modern text file may or may not have an embedded Ctrl+Z, but if it does, the operating system will treat it as <u>an end-of-file marker.</u>
    - A second approach to mark the end of files is for the operating system to store information on <u>the size of the file</u>. 
  - **Cat**
    - Text file
      - A text file is one containing text—that is, data stored as human-readable characters.
      - A file containing machine language instructions, such as the file holding the executable version of a program, is not a text file. 

- **EOF (end of file)**: C handles this variety of methods by having the getchar() function return a special value when the end of a file is reached, regardless of how the operating system actually detects the end of file. The value is EOF.
  - **Def:**  EOF is a signal (a special value) returned by getchar() and scanf() to indicate that they have detected the end of a file.
  - **Soc:** Defined from stdio.h file.
  - **Ab**

    - The important point is that EOF represents a value that signals the end of a file was detected; <u>it is not a symbol actually found in the file.</u>
  - **Mt-How to type EOF with keyboard input**

    - **Simulated EOF**
      - **Space:** Command-line environment
      - the user interacts with a program through keystrokes, and the operating system generates the EOF signal.
      - After the EOF signal, the functions will not fetch information from stdin. (Not sure but exist).
    - **Pc+Cat**
      - The program behavior on encountering a simulated EOF depends on the compiler and project type.
      - On most Unix and Linux systems, for example, pressing Ctrl+D at the beginning of a line causes the end-of-file signal to be transmitted. 
      - Many micro computing systems recognize Ctrl+Z at the beginning of a line as an end-of-file signal
      - some interpret a Ctrl+Z anywhere as an end-of-file signal.
  - **F-Value/Return Value:** -1
  - **Cat**: getchar() and scanf() will return this value when they detect the end of a file.
- **fpos_t (for file position type)**
  - **Def:** Instead of using a long value to represent a position, C uses a new type, called fpos_t (for file position type) for giant file location. 
  - **Obj2**
    - fgetpos() and fsetpos() Functions

  - **Ab**
    - The fpos_t type is not a fundamental type; rather, it is defined in terms of other types. 
    - A variable or data object of fpos_t type can specify a location/pointer within a file
    - It cannot be an array type, but its nature is not specified beyond that.




### Mode

#### Pcp-General

- <u>All</u> file content is in binary form (zeros and ones). (Text content and binary content are different from the representation rather than their form.)
- Unix uses the same file format for both kinds of content.
-  In general, however, you use the binary mode to store binary data in a binary format file. Similarly, you most often use text data in text files opened in the text format. 

#### Pcp-Difference

- The distinction between <u>a binary file and a text file</u> is a system-dependent difference between file formats. 
- The distinction between <u>a binary stream and a text stream</u> consists of translations performed by the program as it reads or writes streams. (A binary stream has no translations; a text stream may convert newline and other characters.)

#### The Text Mode

**Cc**

- **Text content**
  - **Text File:** If a file primarily uses the binary codes for characters (for instance, ASCII or Unicode) to represent text, much as a C string does, then it is a <u>text file</u>
  - **Text Content:** A file primarily <u>uses the binary codes for characters (for instance, ASCII or Unicode) to represent text content</u>, and then it contains <u>text content</u>.
- **Text file formats**
  - 
- **Text mode**

**Ab**

- In the text mode, however, what the program sees can differ from what
  is in the file.
- The local environment's representation of such things as the end of a line or end-of-file are mapped to the C view when a file is read. Similarly, the C
  view is mapped to the local representation of output.

**Cpn**

- 

#### The Binary Mode

**Cc**

- **Binary content**
  - **Binary Content and Binary File:** If, instead, the binary values in the file <u>represent machine language code or numeric data</u> (using the same internal representation as, say, used for long or double values) <u>or image or music encoding</u>, <u>the content is binary</u>.
- **Binary file formats**
  - 
- **Binary mode**

**Ab**

- In the binary mode, each and every byte of the file is acces-
  sible to a program

**Cpn**

- 

**F**

- bring some regularity to the handling of text files



### Level of I/O

**Cc**

-  **Low-level I/O**
  - **Def:** Low-level I/O uses the fundamental I/O services provided by the operating system.
- **Standard high-level I/O**
  - **Def:** Standard high-level I/O uses a standard package of C library functions and `stdio.h` header file definitions.
  - **Mt-How standard I/O works**
    - Normally, the first step in using standard I/O is to use fopen() to open a file.
      - Lk: fopen() F
      - Lk: fopen() F-Return
    - The next step is to call on one of the input functions declared in stdio.h, such as fscanf(), getc(), or fgets(). 
      - Calling any one of these functions causes a chunk of data to be copied from the file to the buffer. The buffer size is implementation dependent, but it typically is 512 bytes or some multiple thereof, such as 4,096 or 16,384.
      - In addition to filling the buffer, the initial function call sets values in the structure pointed to by fp. In particular, the current position in the stream and the number of bytes copied into the buffer are set. Usually the current position starts at byte 0.
    - After the data structure and buffer are initialized, the input function reads the requested data from the buffer.
      - As it does so, the file position indicator is set to point to the character following the last character read.
      - Because all the input functions from the stdio.h family use the same buffer, a call to any one function resumes where the previous call to any of the functions stopped.
    - When an input function finds that it has read all the characters in the buffer, it requests that the next buffer-sized chunk of data be copied from the file into the buffer.
      - In this manner, the input functions can read all the file contents up to the end of the file. 
      - After a function reads the last character of the final buffer's worth of data, it sets the end-of-file indicator to true. The next call to an input function then returns EOF.
    - In a similar manner, output functions write to a buffer. When the buffer is filled, the data is copied to the file.
  - **F**
    - The standard I/O package has two advantages, besides portability, over low-level I/O.
      1. it has many specialized functions that simplify handling different I/O problems
      2. input and output are buffered. That is, information is transferred in large chunks (typically 512 bytes at a time or more) instead of a byte at a time. 
    - 

**Pcp**

- The C standard supports only the standard I/O package because there is no way to guarantee that all operating systems can be represented by the same low-level I/O model. 
- Particular implementations may also provide low-level libraries, but, because the C standard establishes a portable I/O model, we will concentrate on it.



### Standard Files

**Cc**

- **Standard input**
  - **Def:** The standard input, by default, is the normal input device for your system, usually your keyboard. 
  - **Ab**
    - File pointer: stdin from stdio.h
      - Type: pointer-to-FILE
  - **F**
    - The standard input, naturally, provides input to your program.
  
- **Standard output**
  - **Def:** Both the standard output and the standard error output, by default, are the normal output device for your system, usually your display screen.
  - **Ab**
    - File pointer: stdout from stdio.h
      - Type: pointer-to-FILE
  - **F**
    -  The standard output is where normal program output goes.
  
- **Standard error output**
  - **Def:** Both the standard output and the standard error output, by default, are the normal output device for your system, usually your display screen.
  - **Ab**
    - File pointer: stderr from stdio.h
      - Type: pointer-to-FILE
  - **F**
    - The purpose of the standard error output file is to provide a logically distinct place to send error messages.



### Mt-File Input/Output Functions

#### File Stream

**`fopen()` function**

```c
FILE *fp;
fp = fopen(argv[1], "r");
```

- **Def+F:**  

  - The fopen() function opens a file for standard I/O and creates a data structure designed to hold information about the file and the buffer.

- **Soc:** stdlib.h

- **Cpn-Argument**

  - The first argument is <u>the name of the file to be opened</u>; more exactly, it is <u>the address of a string containing that name</u>. 

  - The second argument is <u>a string</u> identifying the <u>mode</u> in which the file is to be opened. 

    | Mode String                                                | Meaning                                                      |
    | ---------------------------------------------------------- | ------------------------------------------------------------ |
    | "r"                                                        | Open a text file for reading.                                |
    | "w"                                                        | Open a text file for writing, <u>truncating</u> an existing file to zero length, or creating the file if it does not exist. |
    | "a"                                                        | Open a text file for writing, <u>appending</u> to the end of an existing file, or creating the file if it does not exist. |
    | "r+"                                                       | Open a text file for update (that is, for both reading and writing). |
    | "w+"                                                       | Open a text file for update (reading and writing), first <u>truncating</u> the file to zero length if it exists or creating the file if it does not exist. |
    | "a+"                                                       | Open a text file for update (reading and writing), appending to the end of an existing file, or creating the file if it does not yet exist; the whole file can be read, but writing can only be appended. |
    | "rb" , "wb", "ab","ab+", "a+b", "wb+", "w+b", "ab+", "a+b" | Like the preceding modes, except they use <u>binary mode</u> instead of text mode. |
    | "wx" , "wbx","w+x", "wb+x" or "w+bx"                       | (C11) Like the non-x modes, except they fail if the file already exists and they open a file in exclusive mode. <br>- if possible.  the modes with x cause fopen() to fail instead, leaving the file unharmed.<br>- to the extent that the environment allows, the exclusivity feature of the x modes keeps other programs or threads from accessing the file until the current process closes the file. |

    - For systems such as Unix and Linux that have just one file type, the modes with the b are equivalent to the corresponding modes lacking the b.

- **Ab**

  - 

- **F**

  - The fopen() function is said to "open a stream." If the file is opened in the text mode, you get a text stream, and if the file is opened in the binary
    mode, you get a binary stream.
  - The fopen() function not only <u>opens a file</u> but sets up a buffer (two buffers for read-write modes), and <u>it sets up a data structure containing data about the file and about the buffer.</u> 
    - The data structure typically includes a file position indicator to specify the current position in the stream. It also has indicators for errors and end-of-file, a pointer to the beginning of the buffer, a file identifier, and a count for the number of bytes actually copied into the buffer.

- **F-Return**

  - fopen() returns a file pointer to the data structure and the pointer is used by the other I/O functions to specify the file.
    - the file pointer's type is pointer-to-FILE
      - FILE is a derived type defined in stdio.h. The pointer fp doesn't point to the actual file. Instead, it points to a data object containing information about the file, including information about the buffer used for the file's I/O. 
    - The fopen() function returns the null pointer (also defined in stdio.h) if it cannot open the file.
      - This program exits if fp is NULL.

**`fclose()` Functions**

```c
fclose(fp);
```

- **Def+F:** The fclose(fp) function closes the file identified by fp, flushing buffers as needed. 
- **Cpn-Argument**
  - the file pointer needed to be closed
- **F-Return**
  - The function fclose() returns a value of 0 if successful, and EOF if not

**`rewind()` Function**

```c
rewind(fp);  /* go back to beginning of file */
```

- **Def+F:** The rewind() command takes the program to the file beginning so that the final while loop can print the file contents.

---

#### Textual I/O

 **`getc()` Function**

```c
char ch;
ch = getc(fp);
ch = getc(stdin);  // equal to getchar()
```

- **Def+F:** `getc` get a character from the file identified by fp
- **Cpn-Argument**
  - file pointer

- **F-Return**
  - The character it reads from the file
  - The getc() function returns the special value EOF if it tries to read a character and discovers it has reached the end of the file.
    - only after it tries to <u>read past the end of the file</u> ((This is unlike the behavior of some languages, which use a special function to test for end-of-file before attempting a read.)

**`putc()` Function**

```c
vchar ch;
ch = getc(fp);
putc(ch, stdout);  // equal to putchar()
putc(ch, fp);
```

- **Def+F:** `putc` put the character ch into the file identified by the FILE pointer fp
- **Cpn-Argument**
  - character
  - file pointer



**`fprintf()` Function**

```c
fprintf(fp,"Can't create output file.\n");

double f_num = 10.2;
fprintf(fp,"some thing is %f", f_num);
```

- **Def+F:** fprintf prints the formatted string into the file pointer.
- **Ab**
  - In general, fprintf() converts numeric values to character data, possibly altering the value
  - if the fp is stdin, fprintf is similar with printf.
  - 

**`fscanf()` Function**

```c
fscanf(fp, "%40s", words);
```

---

#### Search Location

**`fseek()` Function**

```c
// General
fseek(fp, offset, mode); 

fseek(fp, offset, SEEK_END); 
```

- **Def+F:** The fseek() function enables you to treat a file like an array and move directly to any particular byte in a file opened by fopen()
- **Soc:** stdio.h
- **Cpn-Argument**
  - **fp**: pointer-to-file
  - **offset**: This argument tells how far to <u>move from the starting point</u> (see the following list of mode starting points). 
    -  The argument must be a `long` value.
    - It can be positive (move forward), negative (move backward), or zero (stay put).

  - **mode**: the mode identifies <u>the starting point.</u> 
    - Since the ANSI standard, the stdio.h header file specifies the following manifest constants for the mode.
    - SEEK_SET
      - Beginning of file

    - SEEK_CUR
      - Current position

    - SEEK_END
      - End of file

    - Older implementations may lack these definitions and, instead, use the numeric values 0L, 1L, and 2L, respectively, for these modes. Recall that the L suffix identifies type long values.

- **Ab**
  - Ideally, fseek() and ftell() should conform to the Unix model. However, differences in real systems sometimes make this impossible. 
  -  In the binary mode, implementations need not support the SEEK_END mode.
  - In the text mode, the only calls to fseek() that are guaranteed to work are these:
    - `fseek(file, 0L, SEEK_SET)` Go to the beginning of the file.
    - `fseek(file, 0L, SEEK_CUR)` Stay at the current position.
    - `fseek(file, 0L, SEEK_END)` Go to the file's end.
    - `fseek(file,ftell-pos, SEEK_SET)` Go to position ftell-pos from the beginning; ftell-pos is a value returned by ftell().
- **F-Return**
  - an int value.
    - The value returned by fseek() is 0 if everything is okay, and -1 if there is an error, such as attempting to move past the bounds of the file.


**`ftell()` Function**

```c
ftell(fp);
```

- **Def+F:** The ftell() function is type long, and it returns the current file location.
- **Soc:** stdio.h
- **F-Return**
  - Return the current position in a file <u>as a long value</u>
    - The long value is the number of bytes from the beginning, <u>with the first byte being byte 0</u>, and so on.
    - Under ANSI C, this definition applies to files opened in the binary mode, but not necessarily to files opened in the text mode.
    -  ANSI C states that, for the text mode, ftell() returns a value that can be used as the second argument to fseek()

**`fgetpos()` Function**

```c
// Prototype
int fgetpos(FILE * restrict stream, fpos_t * restrict pos);
```

- **Def+F:** When called, it places an fpos_t value in the location pointed to by pos; the value describes a location in the file.
- **F-Return**
  - The function returns zero if successful and a nonzero value for failure.

**`fsetpos()` Function**

```c
// Prototype
int fsetpos(FILE *stream, const fpos_t *pos);
```

- **Def+F:** When called, it uses the fpos_t value in the location pointed to by pos to set the file pointer to the location indicated by that value. 
- **Cpn**
  - fpos_t *pos: The fpos_t value should have been obtained by a previous call to fgetpos().
- **F-Return**
  - The function returns zero if successful and a nonzero value for failure. 

---

#### Backspace

**`ungetc()` Function**

```c
// Prototype
int ungetc(int c, FILE *fp);
```

- **Def+F:** The int ungetc() function pushes the character specified by c back onto the input stream.
  - If you push a character onto the input stream, the next call to a standard input function reads that character (see Figure 13.2). 
- **Ab**
  - The ANSI C standard guarantees only one pushback at a time.
  - If an implementation permits you to push back several characters in a row, the input functions read them in the reversed order of pushing.

---

#### Buffer-Related Functions

**`fflush()` Function**

```c
// Prototype
int fflush(FILE *fp);

// General
fflush(stdin);  
fflush(fp);
```

- **Def+F:** Calling the fflush() function causes any unwritten data <u>in the output buffer to be sent to the output file identified by fp</u>. This process is called **flushing a buffer**.
- **Cpn-Argument**
  - **fp**: a pointer to file
    - If fp is the null pointer, all output buffers are flushed. 

**`setvbuf()` Function**

```c
// Prototype
int setvbuf(FILE * restrict fp, char * restrict buf, int mode, size_t size);
```

- **Def+F:** The setvbuf() function sets up an alternative buffer to be used by the standard I/O functions.
- **Cpn**
  - **fp** identifies the stream
  - **buf** points to the storage to be used.
    - If the value of buf is not NULL, you must create the buffer.
    - However, if you use NULL for the value of buf, the function allocates a buffer itself.
  - **mode**
    - The mode is selected from the following choices: 
    - _IOFBF means fully buffered (buffer flushed when full)
    - IOLBF means line-buffered (buffer flushed when full or when a
      newline is written),
    - _IONBF means nonbuffered. 
  - **size:** The size variable tells setvbuf() how big the array is.
- **Ab**
  - **t:** It is called after the file has been opened and before any other operations have been performed on the stream.
- **F**
  - You could use setvbuf() to create a buffer whose size is a multiple of the data object's size.
- **F-Return**
  - The function returns zero if successful, nonzero otherwise.

****

#### Binary I/O

**Cc**

- **binary form**: When data is stored in a file using the same representation that the program uses, we say that the data is stored in binary form. 

**Mt-Functions**

**`fread()` Function**

```c
// Prototype
size_t fread(void * restrict ptr, size_t size, size_t nmemb, FILE * restrict fp);

// General
double earnings[10];
fread(earnings, sizeof (double), 10, fp);  // This call copies 10 size double values into the earnings array.
```

- **Def+F:** 
- **Soc:** Binary I/O
- **Cpn-Argument**
  - ptr: ptr is the address of the memory storage into which file data is read
  - size: size represents the size, in bytes, of each chunk to be written
  - nmemb represents the number of chunks to be written.
  - fp: fp identifies the file to be read.

- **F-Return**
  - The fread() function returns the number of items successfully read. Normally, this equals nmemb, but it can be less if there is a read error or if the end-of-file is reached.


**`fwrite()` Function**

```c
// Prototype
size_t fwrite(const void * restrict ptr, size_t size, size_t nmemb, FILE * restrict fp);

// General
char buffer[256];
fwrite(buffer, 256, 1, fp);

double earnings[10];
fwrite(earnings, sizeof (double), 10, fp);
```

- **Def+F:** The fwrite() function writes binary data to a file
- **Soc:** Binary I/O
- **Cpn-Argument**
  - ptr: The pointer ptr is the address of the chunk of data to be written. 
    - One problem with fwrite() is that its first argument is not a fixed type.
    - Under ANSI C function prototyping, these actual arguments are converted to the pointer-to-void type, which acts as a sort of catch-
      all type for pointers. (Pre-ANSI C uses type char * for this argument, requiring you to typecast actual arguments to that type.)

  - size: size represents the size, in bytes, of each chunk to be written
  - nmemb represents the number of chunks to be written.
  - fp: As usual, fp identifies the file to be written to.

- **F-Return**
  - size_t type, The fwrite() function returns the number of items successfully written. Normally, this equals nmemb, but it can be less if there is a write error.

---

#### Error Report


**`feof()` Function**

```c
// Prototype
int feof(FILE *fp) 
```

- **Def+F:** The feof() and ferror() functions enable you to distinguish between the EOF which indicate the actual end of the file and EOF produced by read errors.
- **F-Return**
  - The feof() function returns a nonzero value if the last input call detected the end-of-file, and it returns zero otherwise. 

**`ferror()` Function**

```c
// Prototype
int ferror(FILE *fp)
```

- **Def+F:** The feof() and ferror() functions enable you to distinguish between the EOF which indicate the actual end of the file and EOF produced by read errors.
- **F-Return**
  - The ferror() function returns a nonzero value if a read or write error has occurred, and it returns zero otherwise.









# Function

## Cc-Function

- **Function**
  
  ```c
  func_type func_name(arg_type argument, arg_type argument){
      function body
  }
  ```
  
  - **Def:** A function is a self-contained unit of program code designed to accomplish a particular task.
  - **Cpn**
    - **function header:** The line contains <u>the function name</u> along with information about t<u>he type of information/arguments passed to the function and returned by the function</u>
      - **Function name**
        - a function name can be used: in defining a function, in declaring a function, in calling a function, and as a pointer.
      - **Function Signature:** The function return type together with the function parameter list constitute the **function signature.**
        - return type/function type (Output)
        - argument declaration list (Input)
    - **function body:** The block construct the usage of the function with proper statements within the braces
  - **Ab**
    - **Function Type:** Like variables, functions have types. Any program that uses a function should declare the type for that function before it is used. 
      - If the returned value is of a type different from the declared return type, the value is type cast to the declared type.
  - **F-Return**
    - The actual return value is what you would get if you assigned the indicated return value to a variable of the declared return type.
  
- **Arguments**
  - **Def:** The information/items passed to a function
  - **Space:** Enclosed in the parentheses
  - **Ab**
    - Arguments are local variables private to the function.
  - **Pcp**
    - C uses commas to separate arguments to a function. 
    - unlike the case with regular declarations, you can't use a list of variables of the same type in the ();
  - **Cat**
    - In short, the formal parameter is a variable in the called function, and the actual argument is the particular value assigned to the function variable by the calling function.
    - **Formal Argument/formal parameter:** a variable in the function used to hold the value; Declaring an argument creates a variable
      - **Def:** The formal parameter is a variable declared in the header of a function definition.
    - **Actual argument or the Actual parameter**: the function call passes a value, and this value is called the actual argument or the actual parameter
      - **Def:** The actual argument is an expression that appears in the parentheses of a function call. 
      - a constant, a variable, or an even more elaborate expression1
  - **Pcp-Arguments and Parameters**
    - With this C99 convention, we can say that <u>parameters are variables</u> and that <u>arguments are values provided by a function call and assigned to the corresponding parameters</u> 
      - e.g. `int func(para)` `func(arg)`
    - a name defined in one function doesn't conflict with the same name defined elsewhere. 
    - In C language, actual arguments swaps <u>values</u> to formal parameters rather than the actual arguments themselves. 即C语言是值传递而非变量传递。
  
- **Driver**: A program designed to test functions to check to see whether functions works is sometimes called a driver.

- **Block**: The section of program between and including the braces is called a block. 

- 



## Pcp

- Thinking of functions in this manner helps you concentrate on the program's overall design rather than the details. Think carefully about what the function should do and how it relates to the program as a whole before worrying about writing the code.
- Module, Reuse.
- Each can call any other function or be called by any other function. Nested function scene is not that important in C language.
- Each function should have a single, well-defined purpose. One function one task



## Mt-Function Prototype

**Tips:** forward declaration/function declaration

```c
#include <stdio.h>

int proto(int numx);  // Prototype informs the compiler that the function called proto() will be used
int proto(int); // only type of the argument could also be accepted.

int proto();  // Forgo function prototype and the check of args

int proto(void);  // function without args

int proto(int arg, ...);  // partial prototyping,  there may be further arguments of an unspecified nature.

int * proto_ptr(int * ptr);  // Pointer


....


int proto(int num_proto){  // Function definition
    printf("%d", num_proto);
}
```

**Soc-History**

- Before ANSI C, C used function declarations that weren't prototypes; they just indicated the name and return type but not the argument types. 

**Cc**

- **Prototype/Function declaration**
  - **Def:** A prototype is a function declaration to the compiler that you are using a particular function and specifies properties, such as return type, its arguments with corresponding types,  of the function.
  - **Space**
    - The function prototype could be used inside other functions or outside the function.
    - In either case, your chief concern should be that <u>the function declaration appears before the function is used.</u>
  - **Pcp**
    - When a function takes arguments, the prototype indicates their number and type by using a comma-separated list of the types. If you like, you can omit variable names in the prototype.(For the reason of function prototype scope)
      - One case in which the names matter a little is with variable-length array parameters. If you use names in the array brackets, they have to be names declared earlier in the prototype.
      - Using variable names in a prototype doesn't actually create variables. It merely clarifies the fact that char means a char variable, and so on.
      - Remember that the variable names in function prototypes are dummy names and don't have to match the names used in the function definition.
    - When using function in multiple files, function prototype is mandatory for the file which use that function but does not contain the function definition.
    - If there is a type mismatch and if both types are numbers, the compiler converts the values of the actual arguments to the same type as the formal arguments(if convertable)
    - if there are nothing within the parentheses, an ANSI C compiler will assume that you have decided to forgo function prototyping, and it will not check arguments. `type func();`
  - **F**
    - In this case, the prototype informs the compiler that proto() expects an int argument. In response, when the compiler reaches the proto(var) expression, it <u>automatically applies a type-cast to the  argument, converting it to a correct type for this argument.</u>
    - a prototype specifies both the type of value <u>a function returns and the types of arguments it expects.</u> Collectively, this information is called <u>the signature of the function.</u> 



## Mt-Function Definition

```c
int main(int para1, float para2){
    //function statements
    return para1;
}
```

> - The parentheses identify `main()` as a function name.
> - The `int` indicates that the `main() `function's return type should be an integer
> - The `void` indicates that `main()` doesn't take any arguments passed along to the function.

- When you define a function, state the type of value it returns.
- Use the keyword return to indicate the value to be returned.



## Mt-Function Call/Invoke/Summon

```c
printf("I am a simple ");
```

**Cc**

- **Calling function**: The original function before invoking another function
- **Called function**: It is the function which be invoked by the calling function.

**Pc**

- Whenever the computer reaches a function calling statement, it looks for the targeted function and follows the instructions there. 
  - The calling function places its arguments in a temporary storage area called the stack, and the called function reads those arguments off the stack.

- When finished with the code within function, the computer returns to the next line of the original calling function—main()
- 



## Return Statement

**Cc-Return**

- **Def+F:** The keyword return causes the value of the following expression to be the return value of the function.

**Return Void**

```c
return;
```


It causes the function to terminate and return control to the calling function. Because no expression follows return, no value is returned, and this form should be used only in a type void function.



## Mt-Recursion

**Cc**

- **Recursion:** C permits a function to call itself.

**Pcp-Compared with loop**

- Recursive solutions tend to be more elegant and less efficient than loop solutions. Recursion often can be used where loops can be used. Normally, the loop is the better choice. 
- First, because each recursive call gets its own set of variables, recursion uses more memory; each recursive call places a new set of variables on the stack. Space restrictions in the stack can also limit the number of recursive calls.
- Second, recursion is slower because each function call takes time.

**Pcp-Recursion**

- Note that each level of recursion/each level of function call has its own private variables. You can tell this is so by looking at the address values. 
- Each function call is balanced with a return. When program flow reaches the return at the end of the last recursion level, control passes to the previous recursion level. (Just like calling function)
- The statements in a recursive function that <u>come before the recursive call</u> are executed in <u>the same order</u> that the functions are called. The statements in a recursive function that <u>come after the recursive call</u> are executed in the <u>opposite order</u> from which the functions are called.
- Although each level of recursion has its own set of variables, <u>the code itself is not duplicated.</u> The code is a sequence of instructions, and a function call is a command to go to the beginning of that set of instructions.
- it's vital that a recursive function contain something to <u>halt the sequence of recursive calls.</u> 

**F-Advantages**

- One good point is that recursion offers the simplest solution to some programming problems. 

**Problems-Disadvantages**

- One bad point is that some recursive algorithms can rapidly exhaust a computer's memory resources.
- Recursion can be difficult to document and maintain

**Cat**

- **Tail recursion, or end recursion**: In the simplest form of recursion, the recursive call is at the end of the function/the last executable statement, just before the return statement.
  - **Ab**
    - Simply like a loop, the tail recursion will execute all statements before the recursive call until reach the test condition.
- **Double recursion:** the function calls itself twice. And that leads to a weakness of computer power.



## Mt-Jump to particular function definition

- `Ctrl + LeftClick` on the function name.



## Mt-Inline Function(C99)

### Cc-Inline Function

- **Def**  
  - Making a function an inline function suggests that calls to the function be as fast as possible. The extent to which such suggestions are effective is
    implementation-defined
- **Ab**
  - Because an inline function doesn't have a separate block of code set aside for it, you can't take its address. (Actually, you can take the address, but then the compiler will generate a non-inline function.) 
  - Also, an inline function may not show up in a debugger.
  - An inline function should be short. For a long function, the time consumed in calling the function is short compared to the time spent executing the body of the function, so there is no great savings in time using an inline version.
  - For the compiler to make inline optimizations, it has to know the contents of the function definition. This means the inline function definition has to be in the same file as the function call. 
    -  For this reason, an inline function ordinarily has internal linkage.
      - C, unlike C++, also allows a mixture of inline definitions with external definitions (function definitions with external linkage). 
    - Therefore, if you have a multifile program, you need an inline  definition in each file that calls the function.
    - The simplest way to accomplish this is to put the inline function definition in a header file and then include the header file in those files that use the function.
  - An inline function is an exception to the rule of not placing executable code in a header file. Because the inline function has internal linkage, defining one in several files doesn't cause problems.
- **F**
  - making a function an inline function may cause the compiler to replace the function call with inline code and/or perform some other sorts of optimizations, or it may have no effect

### Mt-Create inline functions

```c
inline static void eatline() // inline definition/prototype
{
   // Statements 
}
```

- The standard method says that a f<u>unction with internal linkage can be made inline</u> and that the definition for <u>the inline function must be in the same file in which the function is used</u>. So a simple approach is to <u>use the inline function specifier along with the static storage-class specifier.</u> 





## Mt-_Noreturn Functions (C11)

```c

```

- **Def**
  - `_Noreturn`, to indicate a function that, upon completion, does not return to the calling function. 
- **F**
  - The purpose of _Noreturn is to inform the user and the compiler that a particular function won't return control to the calling program. 
  -  Informing the user helps to prevent misuse of the function, and informing the compiler may enable it to make some code optimizations.
- **e.g.**
  - exit()




## Cc-General Functions

**`main()` function**

```c
int main(void){  // function header
	statements;  // function body
    return 0;
}
```

- **Ab**

  - C program always begins execution with the function called `main()`, which could normally provides the basic framework for a program.
  - All C programs begin execution with `main()`, no matter where main() is located in the program files. 
  - Even main() can be called by itself recursively or by other functions,
    although this is rarely done.

**`qsort()` function**

```c
// Prototype
void qsort (void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *));

// General
qsort(vals, NUM, sizeof(double), mycomp);

/* sort by increasing value */
int mycomp(const void * p1, const void * p2)
{
    /* need to use pointers to double to access values*/
    const double * a1 = (const double *) p1;
    const double * a2 = (const double *) p2;

    if (*a1 < *a2)
        return -1;
    else if (*a1 == *a2)
        return 0;
    else
        return 1;
}

```

**Soc**

- `stdlib.h`

**Def+F**

- The qsort() function sorts an array of data objects by quick sort algorithm.

**Cpn**

- base: The first argument is a pointer to the beginning of the array to be sorted
  - ANSI C permits any data pointer type to be typecast to a pointer-to-void, thus permitting the first actual argument to qsort() to refer to any kind of array.
  - Because qsort() converts its first argument to a void pointer, qsort() loses track of how bigeach array element is.
- nmemb: The second argument is the number of items to be sorted.
- size: tell qsort() explicitly the size of the data object. 
  - For example, if you are sorting an array of type double, you would use sizeof(double) for this argument.
- compar: Finally, qsort() requires a pointer to the function to be used to determine the sorting order. 
  - The comparison function should take two arguments: pointers to the two items being compared.
  - According to the prototype, the comparison function should return a positive integer if the first item should follow the second value, zero if the two items are the same, and a negative integer if the second item should follow the first. 

**Ab**

- Because qsort() converts its first argument to a void pointer, qsort() loses track of how big each array element is. 

**Mt-How to define the comparison function**

- 

**F-Return**

- 

**`time()` function**

```c
time();
```

- **F**
  - Return the system time











# Operators 运算符

## Cc-Operators and Operands

- **Operator**
  - **Def:** An operator is a built-in language element that operates on one or more items to produce a value. 
- **Operand 操作数**
  - **Def:** Operands are what operators operate on.
  - **Cat**
    - **Unary operator:** meaning that it takes just one operand
    - **Binary, or dyadic, operators**: operators that require two operands.
    - **Sign Operators:**
      - **Def:** The minus sign can also be used to <u>indicate or to change the algebraic sign of a value.</u>
      - **e.g.** – and +
        - **Att:** plus sign doesn't alter the value or sign of its operand;
          it just enables you to use statements without getting a compiler complaint.
- **Lvalue/left value**
  - **Def**: C uses the term lvalue to mean any such name or expression that <u>identifies a particular data object.</u> 
  - **Ab+Pcp**
    - Object refers to the actual data storage, but an lvalue is a label used to identify, or locate, that storage
    - It specified an object, hence referred to an address in memory.
    - It could be used on the left side of an assignment operator, hence the "l" in lvalue.
    - At this point the standard continued to use lvalue for any expression identifying an object, even though some lvalues could not be used on the left side of an assignment operator.
  - **Cat**
    - **modifiable lvalue/object locator value **: identify an object whose value can be changed
      - **Ab**
        - can be used either on the left side or the right side of an assignment operator
    - **non-modifiable lvalue:** dentify an object whose value can not be changed
      - **e.g.** symbolic constants
- **Rvalue/right value/value of an expression**
  - **Def:** quantities that <u>can be assigned to modifiable lvalues</u> but which are not themselves lvalues
  - **Cat**
    - constants
    - expressions



## Cc-Operator Precedence and Associativity

**Cc**

- **Expression Tree**:  represent the precedence order of evaluation with a type of diagram

**Pcp**

- C does this by setting up an <u>operator pecking order</u>. Each operator is assigned <u>a precedence level</u>
- If two operators that have the same precedence share an operand, they are executed according to t<u>he order in which they occur in the statement.</u>
- For most operators, the order is from left to right. (The = operator was an exception to this.)

**Cpn-Detailed Precedence**

- **Lk:** C Primer Plus > Table 5.1 Operators in Order of <u>Decreasing Precedence</u>
- **Pcp:** 
  - The associativity column tells you how an operator associates with its operands
  -  The association rule applies for operators that share an operand. The left-to-right rule applies in this case that the choice does matter.
    - e.g. `1*2/3` the center number `2` is the shared operand and the left-to-right rule should be applied.
  - What precedence does not establish is which of these two multiplications occurs first. C leaves that choice to the implementation since the choice doesn't affect the final value for this particular example.
    - e.g. `1*2 + 2*3` the first two multiplications do not have precedence.

| Operators(High to Low Precedence)                            | Associativity |
| ------------------------------------------------------------ | ------------- |
| ++ (postfix) --(postfix) ( ) (function call) [ ] {} (compound literal) . -> | Right to Left |
| ++ (prefix) --(prefix) - + ~ ! *(dereference)&(address)      | R-L           |
| sizeof_Alignof  (type) (all unary)                           | Left to right |
| (type name)                                                  | L–R           |
| * / %                                                        | L–R           |
| +(binary) -(binary)                                          | L–R           |
| << >>                                                        | L–R           |
| < > <= >=                                                    | L–R           |
| == !=                                                        | L–R           |
| &                                                            | L–R           |
| ^                                                            | L–R           |
| \|                                                           | L–R           |
| &&                                                           | L–R           |
| \|\|                                                         | L–R           |
| ? :(conditional expression)                                  | R-L           |
| = *= /= %= += -= <<= >>= &= \|= ^=                           | R-L           |
| , (comma operator)                                           | L–R           |

**Add**

- The unary operators * and ++ have the same precedence but associate from right to left. Thus `*arr++` will get the next unit of array rather than get the value of the address add 1.



## Assignment Operator 赋值运算符

`=`

```c
var = num2
```

**Pcp**

- the left side/operand of an assignment operator should be a modifiable lvalue
- The assignments are made from right to left

**F**: assignment statement. Its purpose is to store a value at a memory location.



`+=`

```c
x += 1
```

scores += 20 is the same as scores = scores + 20 .



` -=`

```c
y -= 1 
```

dimes -= 2 is the same as dimes = dimes - 2.



 `*=`

bunnies *= 2 is the same as bunnies = bunnies * 2.



`/=`

time /= 2.73 is the same as time = time / 2.73.



`%=`

reduce %= 3 is the same as reduce = reduce % 3.



## Arithmetic Operators 算术运算符

### Basic

```c
int num1, num2;
num1 + num2;  // Plus
num1 - num2;  // Minus
num1 * num2;  // Multiplication
num1 / num2;  // Division

#include <math.h>
pow(num1, num2); // exponentiating operator
```

### Pcp

- **Truncation**: Division works differently for integer types than it does for floating types. Floating-type division gives a floating-point answer, but integer division yields an integer answer.  <u>In C, any fraction resulting from integer division is discarded. This process is called truncation.</u>
  - Notice how integer division <u>does not round to</u> the nearest integer, but always truncates (that is, <u>discards the entire fractional part</u>). 
- **Truncating toward zero** : deciding how integer division with negative numbers worked
  - **Def:** the integer division rounding process is that it just dumps the fractional part when the result of the integer division comes with a fraction part and negative sign.
-  **Mixed Types:** When you mixed integers with floating point, the answer
  came out the same as floating point. e.g. 6.3/2
  - Actually, the computer is not really capable of dividing a floating-point type by an integer type, so the compiler converts both operands to a single type.
  - normally you should avoid mixing types.

---

**`%` Modulus Operator**

```c
5 % 2 == 1
```

- **Def+F**:  `%` gives the <u>remainder</u> that results when the integer to its left is divided by the integer to its right
- **Ab**
  - integer arithmetic
- **F**
  - One common use is to help you control the flow of a program. 
  - With the C99 rule in place, you get a negative modulus value <u>if the first operand is negative(Not the second one)</u>, and you get a positive modulus otherwise.
    - Different from the `%`, `/` obey the basic arithmetic rule about division.
    - `x%y = x - (x/y)*y`





## Increment and Decrement Operators

**Tips:** **`++` and `--`Increment and Decrement Operators**

```c
x++;
++x;
y--;
--y;
```

**Def+F**

- Operators which could increments (increases) the value of its operand by 1.

**Ab-Mode**

- The two modes differ with regard to the precise time that the incrementing takes place.
- **prefix mode**: the ++ come before the affected variable
  - `++x ` is the same as `x = x + 1`
  - `++x` is changed <u>before its value is used/evaluated</u>
- **postfix mode**: the ++ after the affected variable
  - `x++` is changed <u>after its value is used/evaluated</u>

**Ab-Precedence**

- The increment and decrement operators have a very high precedence of association; only parentheses are higher. 
- Don't confuse precedence of these two operators with the order of evaluation. The postfix mode will not conduct after the variable is used.
  - Precedence tells us that the ++ is attached only to the n, not to y + n. It also tells us when the value of n is used for evaluating the expression, but the nature of the increment operator determines when the value of n is changed.

**Problem**

- The problem is that when printf() goes to get the values for printing, it might evaluate the last argument first and increment variable before getting to the other argument. 
  - e.g. C Primer Plus > P166


- **Pcp**

- Don't use increment or decrement operators on a variable that is <u>part of more than one argument of a function.</u>
- Don't use increment or decrement operators on a variable that <u>appears more than once in an expression.</u>



## Relational Operator 关系运算符

**`==` equality operator**

```c
1 == 0;
```



`<` , `>` , `<=`, `>=` 

**Pcp**

- 当使用关系运算符时，通过将常量写在变量前面`3 == i`可以保证查出潜在的`i = 3` 之类的错误

**F-Return**

- check to see whether th relational expression is true or false

**Att**

- You should limit yourself to using only < and > in **floating-point comparisons.** The reason is that round-off errors can prevent two numbers from being equal, even though logically they should be.
  - The `fabs()` function, declared in the math.h header file, can be handy for floating-point tests, which returns the absolute value of a floating-point value—that is, the value without the algebraic sign



## Logical Operators 逻辑运算符

`!` Not

```c
!true == false
```

**Ab**

- **Affinity:** R to L
- 

e.g.

- `!=` Not equal to

`&&` And

```c
true && false == false
```

`||` Or

```c
true || false == true
```

### Ab-Precedence

- The ! operator has a very high precedence—higher than multiplication, the same as the increment operators, and just below that of parentheses. 
- The && operator has higher precedence than ||, and both rank below the relational operators and above assignment in precedence.

### Ab-Affinity

- Although other operators could be ambiguous when facing the evaluation order problem like the order of two additions in `a = (1+2) * (2+3)`, <u>C guarantees that logical expressions are evaluated from left to right</u>.

### Ab-Sequence Point

- The && and || operators are <u>sequence points</u>, so all side effects take place before a program moves from one operand to the next.
- The fact that the && and || operator is a sequence point guarantees that the expressions on the left side will be executed with side effects before the expression on the right is evaluated.

### Ab+Mt-短路运算/逻辑中断

```c
if (condition_1 && condition_2){
    printf("Only the condition_1 passing could lead to the judgement of condition_2.");
}
```

**Def:** When the operator could definitely get the only value, it will not execute the afterwards expression.

- C guarantees that as soon as an element is found that invalidates the expression as a whole, the evaluation of logical operator will stop.

**e.g.**

- `0 && expression`, the expression will not be executed since the false and relational statement should always be false.
- `1 || experssion`, similarly, the expression will not be executed as the true or relational statement should always be true.





## Comma Operator

`,`

```c
for (ounces=1, cost=FIRST_OZ; ounces <= 16; ounces++, cost += NEXT_OZ)
	printf("%5d $%4.2f\n", ounces, cost/100.0);
```

**Def+F**

- The comma operator links two expressions into one and guarantees that the leftmost expression is evaluated first.

**Ab**

- The comma is a sequence point, so all side effects to the left of the comma take place before the program moves to the right of the comma.
- the value of the whole comma expression is the value of the right-hand member of comma.
- The comma also is used as a separator when using for assignment or function argument. These commas are separators, not comma operators.



## Conditional Operator

`? : `

```c
(condition) ? (true statements) : (false statement)
```

**Def+F:**  a shorthand way to express one form of the if else statement

**Ab**

- conditional expression
- ternary operator

**F-Return**

- if condition is true, the whole conditional expression has the same value as true statements. Instead, if the condition is false, the value will be the false statements.

**e.g.**

- ```c
  x = (y > 0) ? y : -y;
  
  // The conditional expression is equal to the if else statement
  if (y > 0)
  {
      x = y;
  }
  else
  {
      x = -y;
  }
  ```

  

##  Structure and Union Operators

### The Membership Operator `.`

```c
name.field;
```

- **Def:** This operator is used with a structure or union name to specify a member of that structure or union. 
- **F:** If name is the name of a structure and member is a member specified by the structure template, it will identify that member of the structure.

### The Indirect Membership Operator `->`

```c
ptrstruct -> member;
```

- **Def:** This operator is used with a pointer to a structure or union to identify a member of that structure or union. 
- **F:** `->` identifies that member of the pointed-to structure.







## Pointer-related Operators

**Pcp**

- The statements ptr = &bah; and val = *ptr; taken together amount to the following statement: val = bah;
  - Address + Indirection = Assignment



**Address operator(`&`)**

```c
ptr = &var;
```

- **Def+F:** The unary & operator gives you the address where a variable is stored.
- **Ab**
  - unary



**Indirection operator / Dereferencing operator/ Dereference(`*`)**

```c
var = 2;
ptr = &var;
val = *ptr;  // Similar to Assignmnet: val = var;

// Change the value of the pointer address
*ptr1 = *ptr2;
```

- **Def+F:**  The indirection operator could be used to find the value stored in the address being pointed to. When followed by a pointer name or an address, `*` gives the value stored at the pointed-to address.
- **Ab**
  - Unary, the binary multiplication operator has the same symbol but different syntax from unary indirection operator.
  - Using the address and indirection operators is a rather indirect way of accomplishing the assignment process, hence the name “indirection operator.”





## Miscellaneous Operators

### The Cast Operator 强制类型转换

```c
// Cast
float float_result = (float)int_l/int_r;  // Cast operator
```

**Cc**

- **Cast/Type Cast**
  - **Def+F**
    - The precise type conversion that you want or else document that you know you're making a type conversion
- **Cast Operator**
  - **Def+F**
    - preceding the quantity with the name of the desired type in parentheses. The parentheses and type name together constitute a cast operator.
  - **St**
    - `(type)var` 

**Pcp**

- 强制类型转换并不会直接转换原本的变量类型，应当作是一种运算符作用于变量
- 

**e.g.**

- 因为左右操作数同时为`int`，即便结果在数学上是浮点数，但实际上会进行整数运算（例如整除），导致结果只有整数部分，需要通过强制类型转换将其左右操作数中的一个转变为`float`



### Sizeof Operator 求字节运算符

```c
sizeof(type);
sizeof value;
sizeof var;
```

- **Def+F:** `sizeof` gives the memory size of a type/particular value.
- **Obj2-Operand**
  - The operand can be a specific data object, such as the name of a variable, or it can be a type. 
    - If it is a type, such as `float`, the operand must be enclosed in parentheses.
- **Pcp**
  - 其实全用带上括号的就好。Whether you use parentheses depends on whether you want the size of a type or the size of a particular quantity. Parentheses are required for types but are optional for particular quantities.
- **F-Return**
  - `sizeof` returns a value of type `size_t`
  - `size_t` is an unsigned integer type
  - the C header files system can use `typedef` to make `size_t` a synonym for `unsigned int` on one system or for `unsigned long` on another. **Thus, when you use the `size_t` type, the compiler will substitute the standard type that works for your system**






# Debugging

## Pcp

- Comprehend, learn, collect and remind when errors appear.
- 



## Cc-Errors

### Syntax Errors

**Def:** C syntax errors use valid C symbols in the wrong places.



### Semantic Errors

**Def:** Semantic errors are errors in meaning. A program does not executes as what you expected it to do.



### Cc-Compile Error



### Cc-Executable Error





## Mt-Debugging

### Program State

**Def:** The program state is simply the set of values of all the variables at a given point in program execution. 

**Ab:** the current state of computation

**Mt-Trace the program state step-by-step**

* Pretend you are the computer and to follow the program steps one by one
* sprinkle extra `printf()` statements throughout to monitor the values of selected variables at key points in the program

**Mt-Trace by the IDE debugger**

* **Debugger:** a program that enables you to run another program step-by-step and examine the value of that program's variables



### Mt-Breakpoint 断点调试

- Click the line number on the left of your code block and there will be a red point(**Breakpoint**) which could pause the code execution on the line.
- Debug  the C/C++ file(Not Run the C/C++ file) and the program will pause when it meet the breakpoint.





## Cc-Linter

**Def**

- a linter is a tool to help you improve your code.
- For compiled language like C, linter is mostly static analysis program.

**Cat-C**

- Clang: https://clang.llvm.org/
- cppcheck: http://cppcheck.net/
- PC lint Plus: https://pclintplus.com/pc-lint-plus/





# Bit Fiddling

## Obj2 + F

- With C, you can manipulate the individual bits in a variable.

- High-level languages generally don't deal with this level of detail;
  C's ability to provide high-level language facilities while also being able to work at a level typically reserved for assembly language makes it a preferred language for writing device drivers and embedded code.



## Cc-Bits, and Bytes

### Bit: bit, byte, and word

**bit** 比特/位

- **Def+F:** The smallest unit of memory which hold one of two values: 0 or 1

**byte** 字节
- **Def+F:** The usual unit of computer memory
  - C, remember, uses the term byte to denote the size used to hold a system's character set, so a C byte could be 8 bits, 9 bits, 16 bits, or some other value.
  - the **8-bit byte / octet** is the byte used to describe memory chips and the byte used to describe data transfer rates. 
    - You can think of these 8 bits as being numbered from 7 to 0, left to right. <u>Bit 7 is called the high-order bit, and bit 0 is the low- order bit in the byte.</u>
    - The smallest binary number would be 00000000, or a simple 0. 
    - The largest number this byte can hold is 1, with all bits set to 1: 11111111. The value of this binary number is as follows: 128 + 64 + 32 + 16 + 8 + 4 + 2 + 1 = 255
    - A byte can store numbers from 0 to 255, for a total of 256 possible values. ($2^{8} = 256$ )(unsigned int). Or, by interpreting the bit pattern differently, a program can use a byte to store numbers from –128 to +127. (signed char)
- **Pcp-General:** 1 byte = 8 bits
  -  a C byte is defined as the size used by the `char` type
- **F:** Other memory measurement is based on the byte unit.
  - 1K = 1024 bytes
  - 1M = 1024 K
  - 1G = 1024 M
  - 1T = 1024G
  - etc.

**word**
- **Def:** The natural unit of memory for a given computer design. 

**F-All**

- bit, byte, and word describe units of computer data or to describe units of computer memory

## Cc-Base: decimal, binary, octal and hexadecimal

### Decimal / Base 10

- **Def:** Because our system of writing numbers is based on powers of 10, we say that 2157 is written in base 10.
- **Space:** $x \in 0-9$, $\sum x \times 10^{n}(n \geq 0)$

### Binary / Base 2

- **Def:** Binary refers to a base 2 system. 
- **Space:** $x \in 0-1$, $\sum x \times 2^{n}(n \geq 0)$
- **e.g.**
  - `0111 1011`

### Octal

- **Def:**  Octal refers to a base 8 system.

- **Space:** $x \in 0-7$, $\sum x \times 8^{n}(n \geq 0)$

- **Ab**

  - A handy thing to know about octal is that each octal digit corresponds to three binary digits.

- **Mt-Binary Equivalents for Octal Digits**

  - Tips: Transfer three by three

  | Octal Digit | Binary Equivalent |
  | ----------- | ----------------- |
  | 0           | 000               |
  | 1           | 001               |
  | 2           | 010               |
  | 3           | 011               |
  | 4           | 100               |
  | 5           | 101               |
  | 6           | 110               |
  | 7           | 111               |

  - The only awkward part is that a 3-digit octal number might take up to 9 bits in binary form, so an octal value larger than 0377 requires more than a byte. 
  - Note that internal 0s are not dropped: 0173 is 01 111 011, not 01 111 11

- **e.g.**
  - `173` 

### Hexadecimal

- **Def:** Hexadecimal (or hex) refers to a base 16 system. 

- **Space:** $x \in 0-9+A-F$ , $\sum x \times 16^{n}(n \geq 0)$

  - A represents 10 and  F represents 15
  - In C, you can use either lowercase or uppercase letters for the additional hex digits.

- **Ab**

  - Each hexadecimal digit corresponds to a 4-digit binary number, so two hexadecimal digits correspond exactly to an 8-bit byte.
  - For the 8-bit bytes, the first hex digit represents the upper 4 bits, and the second digit the last 4 bits. This makes hexadecimal a natural choice for representing byte values.

- **Mt-Decimal, Hexadecimal, and Binary Equivalents**

  | Decimal Digit | Hexadecimal Digit | Binary Equivalent |
  | ------------- | ----------------- | ----------------- |
  | 0             | 0                 | 0000              |
  | 1             | 1                 | 0001              |
  | 2             | 2                 | 0010              |
  | 3             | 3                 | 0011              |
  | 4             | 4                 | 0100              |
  | 5             | 5                 | 0101              |
  | 6             | 6                 | 0110              |
  | 7             | 7                 | 0111              |
  | 8             | 8                 | 1000              |
  | 9             | 9                 | 1001              |
  | 10            | A                 | 1010              |
  | 11            | B                 | 1011              |
  | 12            | C                 | 1100              |
  | 13            | D                 | 1101              |
  | 14            | E                 | 1110              |
  | 15            | F                 | 1111              |
  
  
  
  - 
  
- **e.g.**

  - `A3F`, `0xA3F` in C

### Mt-Transfer between different bases based on base 10

##### n -> 10

- $\sum number * n^{power}$ 

- **e.g.**

  -  1101 base 2 to base 10

  $$
  1 \times 2^{3} + 1 \times 2^{2} + 0 \times 2^{1} + 1 \times 2^{0} = 13
  $$

##### 10 -> n

- num % n and collect the remainder until $num \leq n$
- collect the remainder from bottom(right) to top(left) since the higher power should be on the right/bottom side.
- add the prefix.
- **e.g.**
  - 13 base 10 to base 2
    - 13 / 2 = 6%**1**(remainder)  =>  6 / 2 = 3%**0** => 3 / 2 = **1**%**1**
    - 1101



## Binary Numbers

### Binary Integers

- **Unsigned Integer**: **Lk:** This markdown notebook > 8-bit byte / octet

**Signed Integer Representation methods**

- The representation of signed numbers is determined by the hardware, not by C
- **Sign-magnitude representation**
  -  **Def:** In a 1-byte value, this leaves 7 bits for the number itself. In such a **sign-magnitude representation**, 10000001 is –1 and 00000001 is 1. 
  - **Ab**
    - **Range:** The total range, then, is –127 to +127.
    - One disadvantage of this approach is that it has two zeros: +0 and –0. This is confusing,
  - **e.g.**
    - +127: 01111111
    - +0: 00000000
    - -0:  10000000
    - -127: 11111111
- **Two's-complement method**
  - It also uses up two bit patterns for just one value. The **two's-complement method** avoids that zero problem and is the most common system used today.
    - **Ab**
      - **Range:** The method represents numbers in the range –128 to +127.
      - In that context, the values 0 through 127 are represented by the last 7 bits, with the high-order bit set to 0, which means that the value is positive.
      - if the high-order bit is 1, the value is negative. The difference comes in determining the value of that negative number. <u>Subtract the bit-pattern for a negative number from the 9-bit pattern 100000000 (256 as expressed in binary), and the result is the magnitude of the value</u>. 
      - The simplest method for <u>reversing the sign</u> of a two's-complement binary number is to <u>invert each bit (convert 0s to 1s and 1s to 0s) and then add 1.</u> 
    - **e.g.**
      - +127 => 011111111
      - 0 => 00000000
      - -1 => 11111111
      - -127 => 10000001
      - -128 => 10000000
-  **One's-complement method** 
  - **Def:** The one's-complement method forms the negative of a number by inverting each bit in the pattern. 
  - **Ab**
    - **Range:** Its range (for a 1-byte value) is –127 to +127.
  - **e.g.**
    - +0: 00000000
    - +1: 00000001
    - +127: 01111111
    - -127: 10000000
    - -1: 11111110
    - –0: 11111111



### Binary Floating Point

**Mt-Representation**

- Floating-point numbers are stored in two parts: a binary fraction and a binary exponent.
- **Binary Fractions**
  - **Def:** Because our system of writing numbers is based on powers of 10, we say that 2157 is written in base 10.
  - **Space:** $x \in 0-1$ , $\sum x \times (1/2)^{n}(n \geq 0)$
  - **Ab**
    - Many fractions, such as 1/3, cannot be represented exactly in decimal notation.
    - Indeed, the only fractions that can be represented exactly are combinations of multiples of powers of 1/2. 
- **Binary Exponent**
- **Floating-Point Representation**
  - To represent a floating-point number in a computer, a certain number of bits (depending on the system) are set aside to hold a binary fraction. Additional bits hold an exponent.
  - In general terms, the actual value of the number consists of the binary fraction times 2 to the indicated exponent. 
  - Multiplying a floating-point number by, say, 4, increases the exponent by
    2 and leaves the binary fraction unchanged. 
  - Multiplying by a number that is not a power of 2 changes the binary fraction and, if necessary, the exponent.



## Bitwise Operators

### Ab

- The bitwise operators have lower precedence than ==, so when using ==, the parentheses are somehow needed.

### Bitwise Logical Operators

**Att**

-  Don't confuse them with the regular logical operators (&&, ||, and !), which operate on values as a whole.

**Obj1**

-  integer-type data, including char

**Ab**

- They are called `bitwise` because they operate on each bit independently of the bit to the left or right. 

#### unary operator ~

```c
~(10011010);  // expression
(01100101);  // resulting value
    
val = ~val;
```

- **Def+F:** unary operator ~ changes each 1 to a 0 and each 0 to a 1
- **Ab**
  - Note that the operator does not change the value of val, just as 3 * val does not change the value of val; val is still 2, but it does create a new value that can be used or assigned elsewhere

#### Bitwise AND: &

```c
(10010011) & (00111101);  // expression
(00010001);  // resulting value

val &= 0377;
val = val & 0377;
```

- **Def+F:** The binary operator & produces a new value by making a bit-by-bit comparison between two operands.
- **F:** For each bit position, the resulting bit is 1 only if both corresponding bits in the operands are 1. (In terms of true/false, the result is true only if each of the two bit operands is true.) 

#### Bitwise OR: |

```c
(10010011) | (00111101);  // expression
(10111111);  // resulting value

val | = 0377;
val = val | 0377;
```

- **Def+F:** The binary operator | produces a new value by making a bit-by-bit comparison between two operands.
- **F:** For each bit position, the resulting bit is 1 if either of the corresponding bits in the operands is 1. (In terms of true/false, the result is true if one or the other bit operands are true or if both are true.) 

#### Bitwise EXCLUSIVE OR: ^

```c
(10010011) ^ (00111101) // expression
(10101110) // resulting value

val ^= 0377;
val = val ^ 0377;
```

- **Def+F:** The binary operator ^ makes a bit-by-bit comparison between two operands. 
- **F:** For each bit position, the resulting bit is 1 if one or the other (but not both) of the corresponding bits in the operands is 1. (In terms of true/false, the result is true if one or the other bit operands—but not both— is true.) 

#### Mt-Usage: Masks

**Lk:** C Primer Plus > Usage: Masks

**Cc+F**

- **Mask**: A mask is a bit pattern with some bits set to on (1) and some bits to off (0). 

- The expression flags & MASK is like covering the flags bit pattern with
  the mask; only the bits under MASK's 1s are visible

**Obj1**

-  bitwise AND operator
- MASK

**Mt**

- flags & Mask

**Pc + e.g.**

- flag: (00001111) 
- mask: (10110110)
- flags I MASK: (00000110)

#### Mt-Usage: Turning Bits On (Setting Bits)

**Lk:** C Primer Plus > Usage: Turning Bits On (Setting Bits)

**F** 

- Turn on particular bits in a value while leaving the remaining bits unchanged.

**Obj1**

- bitwise OR operator
- MASK

**Mt**

- flags I MASK or flags | = MASK

**Pc + e.g.**

- flag: (00001111) 
- mask: (10110110)
- flags I MASK: (10111111)

#### Mt-Usage: Turning Bits Off (Clearing Bits)

**Lk:** C Primer Plus > Usage: Turning Bits Off (Clearing Bits)

**F** 

- turn off bit 1 in the variable flags

**Obj1** 

- unary operator
- bitwise AND operator
- MASK

**Mt**

- flags & ~MASK or flags &= ~MASK

**Pc + e.g.**

- flag: (00001111) 
- mask: (10110110)
- ~mask: (01001001)
  - Because MASK is all 0s except for bit 1, ~MASK is all 1s except for bit 1
  - Thus the bit 1 in mask will be 0 in ~mask and that is the bits that should be turned off.
- flags & ~MASK: (00001001)

#### Mt-Usage: Toggling/Invert Bits

**Lk:** C Primer Plus > Usage: Toggling Bits

**F**

- Toggling a bit means turning it off if it is on, and turning it on if it is off. 

**Obj1**

- bitwise Exclusive operator
- MASK

**Mt**

- flags = flags ^ MASK;
  flags ^= MASK;

**Pc + e.g.**

- 
  flag: (00001111) 
- mask: (10110110)
- flag ^ mask : (10111001)
  - All the bits that are set to 1 in MASK result in the corresponding bits of flags being toggled. All the bits in flags that corresponded to 0 bits in MASK are left unchanged.

#### Mt-Usage: Checking the Value of a Bit

**Lk:** C Primer Plus > Usage: Checking the Value of a Bit

**Mt**

```c
if ((flags & MASK) == MASK)
	puts("Equal");
```

**F**

- Check whether the value in bit n is 1

**Pcp**

- You must first mask the other bits in flags so that you compare only bit 1 of
  flags with MASK
- The bitwise operators have lower precedence than ==, so the parentheses around flags & MASK are needed.
- To avoid information peeking around the edges, a bit mask should be at least as wide as the value it's masking.

**e.g.**

- flag: (00001011) 
- flag_false: (11010000)
- mask used to check whether the bit 1 is 1: (00000010)
- flag & mask: (00000010)
- flag_false & mask: (0000000)
- flag & mask == mask > true
- flag_false & mask == mask > false

**Wrong method**

```c
if (flags== MASK)
	puts("Equal");
```

- flag: (00001011) 
- flag_false: (11010000)
- mask used to check whether the bit 1 is 1: (00000010)
- flag == mask > false since they are not the same value actually
  - Even if bit 1 in flags is set to 1, the other bit setting in flags can make the comparison untrue.
- flag_false == mask > false

### Bitwise Shift Operators

#### F

- The bitwise shift operators shift bits to the left or right.

#### Left Shift: <<

```c
(10001010) << 2 // expression
(00101000) // resulting value
    
int stonk = 1;
int onkoo;
onkoo = stonk << 2; /* assigns 4 to onkoo */
stonk << = 2; /* changes stonk to 4 */
```

- **Def+F:** The left shift operator (<<) shifts the bits of the value of the left operand to the left by the number of places given by the right operand. 
- **Ab**
  - The vacated positions are filled with 0s, and bits moved past the end of the left operand are lost.
  - This operation produces a new bit value, but it doesn't change its operands.
  - You can use the left-shift assignment operator (<<=) to actually change a variable's value.

- **F**
  - number << n  Multiplies number by 2 to the nth power

#### Right Shift: >>

```c
(10001010) >> 2 // expression, signed value
(00100010) // resulting value, some systems
(10001010) >> 2 // expression, signed value
(11100010) // resulting value, other systems, think about the one's complement and two's complement
    
(10001010) >> 2 // expression, unsigned value
(00100010) // resulting value, all system
    
int sweet = 16;
int ooosw;
ooosw = sweet >> 3;  // ooosw = 2, sweet still 16
sweet >>=3;  // sweet changed to 2
```

- **Def+F:** The right-shift operator (>>) shifts the bits of the value of the left operand to the right by the number of places given by the right operand.
- **Ab**
  - For unsigned types, the places vacated at the left end are replaced by 0s. 
  - For signed types, the result is machine dependent. 
    - The vacated places may be filled with 0s, or they may be filled with copies of the sign (leftmost) bit

  - This operation produces a new bit value, but it doesn't change its operands.
  - You can use the right-shift assignment operator (>>=) to actually change a variable's value.

- **F** 
  - number >> n Divides number by 2 to the nth power if number is not negative


#### Mt-Usage: Bitwise Shift Operators

##### Power 2

- The bitwise shift operators can provide swift, efficient (depending on the hardware) multiplication and division by powers of 2
- `number << n`  Multiplies number by 2 to the nth power
- `number >> n` Divides number by 2 to the nth power if number is not negative

##### Extract

- The shift operators can also be used to extract groups of bits from larger units
- e.g. color palette

##### Binary representation

- **Lk:** C Primer Plus > Listing 15.1 The binbit.c Program
- **F:** a program to convert numbers to a binary representation.
- 

**Invert**

- **Lk:** C Primer Plus > Listing 15.2 The invert4.c Program
- **F:** write a function that inverts the last n bits in a value, with both n and the value being function arguments.



## Field Data Form

### Cc-Bit Field

- **Def:** bit field, which is just a set of neighboring bits <u>within a signed int or an unsigned int</u>. (C99 and C11 additionally allow <u>type _Bool bit fields</u>.) 
- **St**
  - A bit field is set up with a structure declaration that <u>labels</u> each field and determines its <u>width</u>(The number of <u>bit</u> for each field).
- **Pcp + Ab**
  - make sure the value doesn't exceed the capacity of the field.
  - Keep in mind, however, that C uses <u>unsigned int(4 bytes) as the basic layout unit</u> for structures with bit fields
    - So even if the sole member of a structure is a single 1-bit field, the structure will have the same size as an unsigned int
  - What if the total number of bits you declare exceeds the size of an unsigned int? 
    - Then the next unsigned int storage location is used. 
    - However, a single field is not allowed to overlap the boundary between two unsigned ints. 
      - The compiler automatically shifts an overlapping field definition so that the field is aligned with the unsigned int boundary. When this occurs, it leaves an unnamed hole in the first unsigned int.
  - One important machine dependency is the order in which fields are placed into an int. 
    - On some machines, the order is left to right; on others, it is right to left. 
  - Also, machines differ in the location of boundaries between fields. For these reasons, bit fields tend not to be very portable. 
    - Typically, however, they are used for non-portable purposes, such as putting data in the exact form used by a particular hardware device.
  - The correspondence between bit fields and bit positions is implementation dependent.

### Mt-Set up bit field

```c
// Declaration
struct
 {
    unsigned int autfd : 1;
    unsigned int bldfc : 1;
    unsigned int undln : 1;
    unsigned int itals : 1;
} prnt;

// Assign values to individual fields
prnt.itals = 0;
prnt.undln = 1;
```

> This definition causes prnt to contain four 1-bit fields.

### Mt-Pad A field structure

```c
struct {
unsigned int field1 : 1;
unsigned int        : 2;  // padding
unsigned int field2 : 1;
unsigned int        : 0;  // padding
unsigned int field3 : 1;
} stuff;
```

> - Here, there is a 2-bit gap between stuff.field1 and stuff.field2, and stuff.field3 is stored in the next int.

- You can pad a field structure with unnamed holes by using unnamed field widths. Using an unnamed field width of 0 forces the next field to align with the next integer

### F

- gives you access to bits within an `int`
- Structures with bit fields provide a handy way to keep track of settings.
  - You could use a separate variable or a full-sized structure member for each property, but that is a bit wasteful of bits. 

- A structure with bit fields allows you to store several settings in a single unit.

### Bit Field and Bitwise Operators

- One difference between the bit-field and bitwise views is that the bitwise view needs positional information
- using bitwise operators to change settings is more complicated. 
- 





## Alignment Features (C11)

**Soc**

- ` stdalign.h`: the stdalign.h header file allows you to use alignas and alignof for _Alignas and _Alignof

**Cc-Alignment**

- **Def:** Alignment, in this context, refers to how objects are positioned in memory.
  - Numerically, alignment means that the number of bytes between consecutive addresses for storing values of that type. 

- **Ab**
  -  In general, alignment values should be a non-negative integer power of two.
  - Bigger alignment values are termed stricter or stronger than smaller ones, while smaller ones are termed weaker.


**`Alignof` operator**

```c
size_t d_align = _Alignof(float);
```

- **Def+F**: The _Alignof operator yields the alignment requirement of a type.
- **F-Return**
  - The alignment of the type
- **F:** A value of, say, 4 for d_align means float objects have an alignment requirement of 4. <u>That means that 4 is the number of bytes between consecutive addresses for storing values of that type</u>. 

**`_Alignas` specifier** 

```c
_Alignas(double) char c1;
_Alignas(8) char c2;
unsigned char _Alignas(long double) c_arr[sizeof(long double)];
```

> - This specifier is used as part of a declaration, and it’s followed by parentheses containing either an alignment value or a type:

- **Def+F:**  use the _Alignas specifier to request a specific alignment for a variable or type. 
- **Ab**
  - But you shouldn't request an alignment weaker than the fundamental alignment for the type. 
- **F-Return**
  - The alignment of the type



`aligned_alloc`

```c
// Prototype
void *aligned_alloc(size_t alignment, size_t size);
```

- C11 also brings alignment capability for allocated memory by adding a new memory allocation function to the stdlib.h library.
- The first parameter specifies the alignment required, and the second parameter requests the number of bytes required; it should be a multiple of the first parameter.
- As with the other memory allocation functions, use free() to release the memory once you are done with it.

**F**

- Some situations may benefit from alignment control, for example, transferring data from one hardware location to another or invoking instructions that operate upon multiple data items simultaneously.













# Pseudo code

## Cc

- **Pseudocode:** the art of expressing a program <u>in simple English that parallels the forms of a computer language</u>



## F

- working out the logic of a program



## e.g.

```c
initialize sum to 0
prompt user
read input
while the input is an integer,
	add the input to sum,
	prompt user,
	then read next input
after input completes, print sum
```

```
1 2 3 4 5
5 4 3 2 1
15 14 13 12 11
```

initialization



# Command-line

Lk: C Primer Plus > 11 Character String > Command-Line Arguments

```shell
$ program_name arg1 arg2

$ program_name "arg1" arg2
```

```c
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    process;
}
```

**Pcp**

- C compilers allow main() to have no arguments or else to have two arguments. (Some implementations allow additional arguments, but that would be an extension of the standard.)
  - the first argument is the number of strings in the command line. By tradition (but not by necessity), this int argument is called `argc` for argument count.
  - The program stores the command line strings in memory and stores the address of each string in an array of pointers. By convention, this pointer to pointers is called `argv`, for argument values. The address of this array is stored in the second argument. 
    - argv[0] stores the program name
      - Unix and Linux support.
      - But beware—some operating systems may not recognize argv[0], so this usage is not completely portable.
    - argv[1] stores the strings and so on.
  - The program name will also be passed into the program.





# Others

C相关笔记

scanf 因为数组名a中本身就存储了数组的起始地址，所以不需要&a，虽然这里&a也可以

struct 结构体对齐第一条：结构体的大小必须是其最大成员的整数倍 第二条：两个小的加一起，如果小于最大成员，那就结合在一起，代码内有备注这一点，针对其他场景，考研不会出的

在删除第i个结点时，L为什么不加引用？（听风助教）答：因为在删除第i个节点的时候，L的值并没有改变。L是指向头结点的指针，这个在删除第i个节点的时候没发生变化。

查看内存那块就会有很多疑问，我们在clion查看内存，它是小端存储数据，低位在前，高位在后。注意大端存储和小端存储都是我们必考的内容

关于什么时候加&再总结（强哥助教）我们前面已经总结过什么时候加&的问题，但是大家还是有些不理解，这里我再给大家说一下，我们定义了L以后，函数该不该加&，这里有一个简单的判断方法，如果我们需要给L申请地址空间，这样改变了L指向，所以要加&。其余的像改变L->next，它没有改变L本身指向，所以不需要加引用。当然像出栈，入栈，出队，入队都改变了其结构，所以需要加&。希望我这次的总结能够大家一些启示。



​	

