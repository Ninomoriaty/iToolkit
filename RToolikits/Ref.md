# Ref

- Coursera Data Science Specialization



# Pcp-编辑原则

1. 这份笔记主要是用来整合所有的R语言编程书籍以及个人编程中遇到的各种问题+解决方案，从而形成一份带有个人经验性质的工具笔记。【从这个层面上来说，可能工具笔记这方面还会个人笔记更加好，博客的话一直需要考虑读者的角度，这个反而在有的时候会让人感到束手束脚。】
2. 目前需要整合的内容有：
   1. ![img](data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==)Meskit开发过程中的经历
   2. ![img](data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==)R in Action 3rd
   3. ![img](data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==)Tidyverse Manual for R in Action in 3rd.
   4. ![img](data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==)SDA course materials
3. 同时需要注意的是由于R语言目前是4.0版本，因此很多教程的内容可能都需要在一定程度上进行更新了。
4. (2021.3.18) Make this Wiznote more like a integrated guideline and replace those Rmarkdown documents with GitHub linkage.



# Recommanded R Packages and Installation Instrunction

## Installation and Manage R packages

install.packages(dependency = T, repo=)

## Data processing

1. typrverse
2. dplyr
3. tidyr

## Visualisation

1. ggplot2
2. plotly