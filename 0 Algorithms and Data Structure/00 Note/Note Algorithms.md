Guideline level

<table><tr><td  style="background: red !important;"><center><font style="font-size:45px; color:white !important;"><b>
    Algorithms
    </b></font></center></td></tr></table>

# Ref

**Courses**

- [ ] MSc Bioinformatics Algorithm Course
- [ ] 王道 408 数据结构


**Textbooks**

- [ ] Introduction to Algorithm
  - Lecture: MIT《Introduction to Algorithms》6.006

- [ ] Algorithms 4th-Edition
  - Lecture: Coursera Algorithm Course is also provided for Part I and Part II.
    - Princeton《Algorithms, Part I & II》- Robert Sedgewick
  - Website: [Princeton-Algs4](https://algs4.cs.princeton.edu/home/)

- [ ] 挑战程序设计竞赛（第2版）, 秋叶拓哉

**GitHub**

- iToolkits

**题库**

- **LeetCode**：[全球题库](https://leetcode.com/)（适合面试）
- **Codeforces**：[竞赛平台](https://codeforces.com/)（适合ACM/ICPC）
- **牛客网**：[国内题库](https://www.nowcoder.com/)（含企业真题



# Obj2-数据结构

- 如何用代码程序将现实问题信息化
- 如何使用计算机高效处理信息并回答现实问题





# Cc-数据结构

## Cc-数据 Data

**Cc**

- **Def:**  <u>数据是信息的载体</u>，是描述客观事物属性的数、字符以及<u>所有能输入到计算机中并被计算机程序识别和处理的符号的集合</u>。
  - The data constitutes the information a program uses and processes.



## Cc-数据元素 Element

**Def:** 数据元素是数据的基本单位

**Cpn**
- **数据项**：一个数据元素可由若干数据项组成，数据项是构成数据元素不可分割的最小单位。
- **组合项**：多个数据项组合而成的一个数据项

**e.g.**
- struct 结构体是一种数据元素，而结构体内的各项数据则是数据项，
- 如果其中有数组，则该数组可以认为是一个**组合项**，即由多个数据构成的数据项



## Cc-数据对象 Object 

**Def:**  数据对象是具有<u>相同性质</u>的数据元素的集合

**Ab:** 数据对象往往是数据的一个子集，毕竟只要有相同的性质即可



## Cc-数据类型 Type

**Def:** <u>一个值的集合</u>和<u>定义在此集合上的一组操作</u>的总称

**Cat**

- **原子类型**：其值不可再分的数据类型
- **结构类型**：其值可以分解的数据类型
- **Abstract Data Type(ADT) 抽象数据类型**：抽象数据组织以及对应操作
  - 用数学化的语言定义数据的<u>逻辑结构和数据运算</u>，与具体的存储结构、实现无关
    - 即数据对象+数据关系+基本操作集

  - 抽象数据类型可以用于定义一个完整的数据结构




## St-数据结构 Data Structure

### Cc

- **结构**：各个数据元素之间的关系
- **数据结构**：相互存在一种或多种特定<u>关系</u>的数据元素的集合
  - **Ab:** 注意数据结构强调数据元素之间的关系，如果数据元素之间没有关系那么这些数据元素并不能认为是数据结构
  - 数据结构的三要素是逻辑结构、存储结构和数据运算


### Cpn

#### 逻辑结构

**Def:** 数据元素之间的逻辑关系，从逻辑关系上描述数据。

**Ab**
- 逻辑结构与数据的存储无关，独立于计算机、储存结构

**Cpn**

- 包含数据对象、数据关系

**Cat**

- **线性结构**：结构中的数据元素之间<u>只存在一对一</u>的关系，即除了第一个元素所有元素都有<u>唯一前驱</u>； 除了最后一个元素，所有元素都有<u>唯一后继</u>
  - **前驱**：前一个数据元素
  - **后继**：后一个数据元素
  - 一般**线性表**
  - 受限线性表
    - **栈、队列**
    - **串**
  - 线性表推广
    - **数组**
- **非线性结构**：结构中的数据元素之间存在<u>多种一对一、一对多、多对多</u>的对应关系
  - **集合**：数据结构中的数据元素除了同属于一个集合外没有其他关系
  - **树形结构**（有根）：结构中的数据元素之间存在一对多的关系
  - **图状结构/网状结构**（无根）：结构中的数据元素之间存在多对多的关系

#### 物理结构/存储结构

**Def:** 存储结构是指数据结构在计算机中的表示/映像

**Ab**

- 依赖计算机语言
- 存储结构会影响存储空间分配的方案和对应效率
- 存储结构会影响各种数据运算的效率 

**Pcp-存储结构与逻辑结构的关系**

- 数据的存储结构是用计算机语言实现的逻辑结构

**Cat**: 参考各个数据结构中的存储结构部分

- **顺序存储**：把逻辑上相邻的元素储存在物理位置上也相邻的储存单元中，元素之间的关系由储存单元的邻接关系来表示

  - **Lk:** 顺序表
  - **Ab**
    - 数据元素在物理上必须是连续的
    - 最基本的存储结构其一
    - 优点
      - 可以实现随机存取
      - 每个元素占用最少的储存空间
    - 缺点
      - 只能使用相邻的一整块存储单元
      - 可能产生较多的外部碎片
  - **e.g.**
    - 数组

- **链式存储**：不要求逻辑相邻的元素也物理相邻，借助指示元素储存地址的<u>指针</u>来表示元素之间的逻辑关系

  - **Lk:** 链表
  - **Ab**
    - 数据元素在物理上可以是分散的
    - 最基本的存储结构其二
    - 相邻结点可以不连续，但是结点内的存储单元地址一定连续
    - 优点：
      - 不会出现碎片现象，能充分利用所有存储单元
    - 缺点：
      - 每个元素因存储指针而占用额外的存储空间，且只能实现顺序存取
  - **e.g.**
    - 链表

- **索引存储**：在存储元素信息的同时，还建立附加的索引表。索引表中的每一项称为索引项，通过索引项来联系储存数据元素。

  - **Lk:** 线性结构
  - **Ab**
    - 数据元素在物理上可以是分散的
    - 可以使用链式存储和顺序存储表示
    - 优点：
      - 检索速度快
    - 缺点：
      - 附加的索引表额外占用存储空间。
      - 增删数据元素时需要修改索引表，实际维护时间消耗较长
  - **例如：**
    - 关键字【这里是指唯一表示索引项的标识，用以区分不同的数据项】+ 地址

  **散列存储/哈希存储**：根据数据元素的关键字通过散列函数直接计算出该元素的存储地址

  - **Lk:** 线性结构
  - **Ab**
    - 数据元素在物理上可以是分散的
    - 可以使用链式存储和顺序存储表示
    - 优点：
      - 检索、增加和删除结点的操作速度都很快
    - 缺点：
      - 如果散列函数不好，可能出现元素存储单元的冲突，此情况出现也会增加时间和空间的消耗。

#### 数据运算

- **Lk:** Mt-操作、方法 Methods


### Pcp-数据结构、逻辑结构、存储结构的关系

- 数据结构的三要素是逻辑结构、存储结构和数据运算，都是不可或缺的
- 便于理解方面，逻辑结构可以理解为数据结构的基本结构分类，存储结构可以理解为选定逻辑结构所代表的分类后的计算机实现方案，然后才是写代码对其进行具体实现
- 数据的逻辑结构独立于其存储结构，但存储结构依赖于其数据结构
- 一种逻辑结构可以对应多种存储结构，一种存储结构也可以是多种逻辑结构的组合

## Mt-操作、方法 Methods

- **数据运算**
  - **Def:** 施加在数据上的运算包括运算的定义和实现
  - **Pcp**
    - 运算的定义是针对逻辑结构的，指出运算的功能
    - 运算的实现是针对存储结构的，指出运算的具体操作步骤
- **数据结构的基本操作**
  - **Def:** 数据结构的基本操作是最核心、最基本的操作





## 应用接口 Applications programming interface (API)

- **Def:**





# Cc-算法概念

## Cc-算法

- **程序**
  - **Def:** 通过数据结构和算法组合而成用于求解问题的计算机程序
  - **Pcp-程序设计**
    - 如何将问题信息化，转变为合适的数据结构，然后可以使用算法高效解决的问题？
    - 一个算法的设计取决于所选的逻辑结构，而算法的实现依赖于所用的储存结构。
- **算法**
  - **Def:** 算法是对特定问题求解步骤的一种描述，它是指令的有限序列，其中每个指令代表一个或多个操作
    - The algorithms are the methods the program uses
  - **Ab-必备特性**
    - 有穷性：一个算法必须在执行一定的/有穷的步数后结束，且每一步完成所需的时间应该是有限的/有穷的
      - 算法是有穷的，而程序可以是无穷的
    - 确定性：算法中的每一个指令都必须有确切的含义，对于相同的输入只能得到相同的结果
      - e.g.
        - 例如排序中如果出现具有相同值的数据项的不同数据元素，那么对于同一个算法而言其排序结果是一定的，然而对于不同的算法而言却可能是不同的。
    - 可行性：算法中描述的操作都可以通过已经实现的基本运算操作执行有限次来实现
    - 输入：一个算法有零或多个输入
    - 输出：一个算法有一或多个输出，输出与输入具有某种特定关系
  - **Pcp-算法实现要求**
    - 正确性：算法能正确地解决求解问题
    - 可读性：算法应当便于理解
    - 健壮性 Robust：算法应该在输入检查和求解结果上作出对应要求，保证其结果相对稳定
    - 高效率：算法执行时间，时间复杂度低
    - 低储存量要求：算法执行过程所需要的最大的存储空间，空间复杂度低





## Cc-Others

- **随机存取 Random Access**: you can use the array index to access any element immediately. This is called random access.
  - e.g.: array
- **顺序存取 Sequential Access**: you have to start at the top of the list and then move from node to node until you get to the node you want, which is termed sequential access
  - e.g.: linked list



## Mt-算法效率评估

### Pcp-Analyzing algorithm performance

- scientific method: we develop hypotheses about performance, create mathematical models, and run experiments to test them, repeating the process as necessary.

### ----- 算法效率的度量 -----

### 时间复杂度 $T(n)$

#### Cc

- **时间复杂度**
  - **Def:** 事先预估算法时间开销($ T(n) $)和问题规模($n$)关系的计量
    - 算法中所有语句的频度/执行次数之和

#### Pcp

若$ T(n) $和$f(n)$ 属于正整数集合上的两个函数，则存在正常数C和$n_{0}$使得当$n >= n_{0}$时，满足$ 0 \leq T(n) \leq Cf(n)$。
$$
T(n) = O(f(n))
$$

> - $f(n)$ 是基本运算的频度，频度是指该语句在算法中被重复执行的次数
> - $ T(n) $是算法中所有语句的频度之和
>   - 算法中基本运算（最深层循环中的语句）的频度与$ T(n) $同数量级
>   - 因此常用基本运算的频度来分析算法的时间复杂度
> - $O()$表示同阶无穷小，即()里面的函数在$n \rightarrow \infty$时二者之比为常量，同阶无穷小的概念可以参考**Lk:** Note Mathematical Analysis
>   - 算法的复杂度可以保留最高数量级而忽视一部分的常量、低数量级的部分
>   - 同时也表明该算法的执行时间与$f(n)$成正比新

- **加法规则**

  算法的复杂度可以保留算法基本运算中最高数量级的部分
  $$
  T(n) = O(f(n)) + O(g(n)) = O(max(f(n), g(n)))
  $$

- **乘法规则**
  $$
  T(n) = O(f(n)) \times O(g(n)) = O(f(n)  \times g(n))
  $$

- **常见的时间复杂度**

  常对幂指阶
  $$
  O(1) < O(\log_2n) < O(n) < O(n\log_2n) < O(n^2) < O(2^n) < O(n!) < O(n^n)
  $$


- 但注意，时间复杂度不仅跟问题规模有关，也与待输入数据的性质有关，因此对于同一个算法，其时间复杂度可能有多种情况

  - 最坏时间复杂度：一般总是考虑最坏情况
  - 平均时间复杂度：所有可能的输入实例在等概率的情况下，算法的期望运行时间
  - 最好时间复杂度


#### Mt-计算时间复杂度的方法

- 循环主体中的变量参与循环条件的判断
  - 设执行次数、频度为t，其数据规模为$n$
  - 根据加法原则找到程序中执行频度最高的部分/section
  - 找到与执行次数t相关的变量：相同或者成正比的循环变量，与执行次数有关的数值，此时一般考虑参与循环条件的变量的关系
  - 写出带有执行次数t和数据规模n的公式、不等式，或公式组
  - 求解得到仅有t为单边的公式
  - 找公式中n的那一侧的公式的同阶无穷小，即$O(n)$，得解
- 递归程序
  - 每一次递归中的执行次数之和
  - 一般为$O(n)$

**e.g.-常见时间复杂度的一些例子**

$O(1)$

```cpp
int n = 2;
printf("%d", n);
```

- 相当于是计算频次是常数，与变量$n$无关，因此复杂度为常数量级，即$O(1)$

$O(\log_2n)$

```cpp
int x = 2;
while (x < n/2)
    	x = 2*x;
```

- 根据加法原则，可知复杂度为该代码中执行频次最高的循环部分

- 设该循环语句执行了$t$次，则结束时应该满足$2^{t+1} < n/2$，由此可以得到$t$与$n$​的关系：
  $$
  2^{t+1} < n/2 \\
  t+1 < \log_2 n - \log_2 2 \\
  t < \log_2 n - 2
  $$
  
- 因此取其同阶无穷小可知
  $$
  T(n) = O(\log_2n)
  $$
  

$O(n)$

### 空间复杂度

#### Cc

- **空间复杂度**：该算法所耗费的存储空间
  - 

#### Pcp

$$
S(n) = O(g(n))
$$

> - $O(1)$ 是指算法原地工作，即算法所需要的辅助空间为常量
> - 其余原则与时间复杂度类似，都是取其同阶无穷小



# 程序设计思路

### Mt-A systematic approach to defining types

**Pcp**

- What constitutes a type? 
  - A type specifies two kinds of information: a set of properties and a set
    of operations
  - Thus, firstly you have to provide a way to store the data, perhaps by designing a structure. Second, you have to provide ways of manipulating the data.

**Pc**

1. Provide an abstract description of the type's properties and of the operations you can perform on the data type which meets these requirements. 
   - This description shouldn't be tied to any particular implementation. It shouldn't even be tied to a particular programming language. Such a formal abstract description is called an abstract data type (ADT).
   - The ADT version is oriented to the end user's concerns and is much easier to read, which expresses the program in terms of the problem to be solved, not in terms of the low-level tools needed to solve the problem.
   - The ADT interface should hides these details and expresses the program in a language that relates directly to the tasks.
   - **F:** Another important point is that the user interface is defined in terms of abstract list operations, not in terms of some particular set of data representations and algorithms. <u>This leaves you free to fiddle with the implementation without having to redo the final program</u>. 

2. Develop a programming interface that implements the ADT. 

   - That is, indicate how to store the data and describe a set of functions that perform the desired operations. 

   - Our claim is that you should be able to use this interface to write a program without knowing any further details—for example, without knowing how the functions are written. 

   - In C, for example, you might supply a structure definition along with prototypes for functions to manipulate the structures. These functions play the same role for the user-defined type that C's built-in operators play for the fundamental C types. Someone who wants to use the new type will use this interface for her or his programming.


3. Write detailed code to implement the interface. 
   - This step is essential, of course, but the programmer using the new type need not be aware of the details of the implementation.
   - The interface design should parallel the ADT description as closely as possible.
   - The complete program, then, consists of three files:
     - list.h, which defines the data structures and provides prototypes for the user interface
     - list.c, which provides the function code to implement the interface
       - the list.h and list.c files together constitute a reusable resource. 
     - films3.c, which is a source code file that applies the list interface to a particular programming problem.
       - ADT interface

### Mt-Data hiding

- The art of concealing details of data representation from the higher levels of programming.

### Mt-

- 

### Mt-Divide and Conquer Strategy 分治法

- 分治策略：对于一个规模为n的问题，若该问题可以容易的解决(比如规模n较小)则直接解决，否则将其分解为k个规模较小的子问题，这些子问题互相独立且与原问题形式相同，递归地解决这些子问题，然后将各个子问题的解合并得到原问题的解。
- 一般常见于使用function recursion的场合，其他时候也可以作为一种常用的分析问题手段
- **Tips:** 讲道理，感觉这个H2都应该放在另一个文件里，跟program design相关的书之后补充记得转移一下

**Top-down design** 

- the idea is to break a large program into smaller, more manageable tasks. If one of these tasks is still too broad, you divide it into yet smaller tasks.You continue with this process until the program is compartmentalized into small, easily programmed modules
- 这个跟分治法很类似

**Bottom-up programming**

- The process of going from a lower level of organization, such as classes, to a higher level, such as program design, is called bottom-up programming.
- 这个恰反

### Mt-About Function Comments

```c
// operation: initialize a list
// preconditions: plist points to a list
// postconditions: the list is initialized to empty
```

- Operation: F of the function
- Precondition: The comments outline preconditions—that is, conditions that should hold before the function is called. 
- Postcondition: The comments outline postconditions—that is, conditions that should hold after the function executes.





# Fundamentals





## St-线性表 Linear List

### Cc-线性表

**Def-线性表**

- 线性表是表示<u>具有相同数据类型</u>的$n(n \geq 0)$个<u>数据元素</u>的<u>有限序列</u>的<u>逻辑结构</u>，其中n为表长
  $$
  L = (a_1, a_2, ..., a_i, a_{i+1}, ..., a_n)
  $$

  > - 当$n = 0$时，线性表是一个空表
  > - $a_1$称为表头元素，$a_n$是表尾元素，对于一个线性表来说它们都是唯一的。
  > - $a_{i-1}$是$a_i$的前驱/直接前驱，$a_{i+1}$是$a_i$的后继/直接后继，每一个元素有且仅有一个前驱和后驱（除了表头和表尾），是线性有序的

### Ab-线性表

- 线性表是一种逻辑结构
  - 类似地比较，顺序表、单链表、哈希表属于数据结构，它们同时描述了逻辑结构、存储结构和数据运算

- 元素个数有限
- 元素具有逻辑上的顺序性，表中元素有对应的先后次序，而表示其位置的数字为位序
  - **Att:** 位序是从1开始的，而数组下标往往是从0开始的

- 元素是单个数据元素
- 元素数据类型相同，具有相同大小的存储空间
- 元素具有抽象性，可以仅讨论元素之间的逻辑关系而不考虑元素具体内容



### Mt-基本操作

- `InitList(&L)` 初始化表，构造空表，分配其内存空间
- `Length(L)`  求表长，指元素个数
- `LocateElem(L, value)` 按值查找，在L表中查找具有value值的元素
- `GetElem(L, location)` 按位查找，在L表中查找第location个位置的元素
- `ListInsert(&L, location, value) ` 插入操作，在L表中第location个位置插入元素value
- `ListDelete(&L, location, &value)` 删除操作，在L表中第location个位置删除元素，并用value返回删除元素的值
- `PrintList(L) `输出操作，按顺序输出L表的所有值
- `Empty(L) `判空操作，如果表L为空，则true，反之为false
- `Destroy(&L)` 销毁操作，销毁线性表，释放其内存空间

### ---------- St-数据结构 ----------

### 顺序表 Sequential List

#### Cc

**Def-顺序表**

- 线性表的顺序存储，使用一组地址连续的存储单元依次存储线性表中的数据元素，从而使得逻辑上相邻的两个元素在物理位置上也相邻

#### Ab

- 存储结构
- 随机存取/随机访问：顺序表最主要的特点，通过首地址和位序可以在时间$O(1)$内找到指定的元素
  - **Att:** 顺序表反而是随机存取的

- 表中元素的逻辑顺序与物理顺序相同
  - 存储密度高
  - 插入和删除需要移动大量的元素
- 顺序表所占用的存储空间大小相关因素
  - 顺序表所占的存储空间=表长 * sizeof (元素的类型)
  - 表长
    - 若长度为n的非空线性表采用顺序存储结构，在表的第i个位置插入一个数据元素，i的合法值应该是$1 \leq i \leq n+1$
    - 顺序表位序从1开始插入，n是表尾，在n+1插入就是在表尾新加一个元素(**Att：**纯粹的文字游戏，不建议给同事添乱)

  - 元素类型和大小（包括结构体内的各个字段的大小）

- **Add**
  - 顺序表并不能方便地运用于各种逻辑结构的存储表示
  - 顺序表的插入算法中,当 n 个空间已满时,可再申请增加分配 m 个空间,若申请失败,
    则说明系统没有n+m个可分配的存储空间。
    - 顺序存储需要连续的存储空间,在申请时需申请 n +m 个连续的存储空间,然后将线性表原来的 n 个元素复制到新申请的 n+m 个连续的存储空间的前 n 个单元。


#### Pcp-数据结构选择

- 需要按序号随机存取/按位查找时，顺序表时间复杂度为$O(1)$，比多数$O(n)$的存储结构效率高
  - 若线性表最常用的操作是存取第 i 个元素及其前驱和后继元素的值,为了提高效率,应采用<u>顺序表</u>存储方式
  - 一个线性表最常用的操作是存取任意一个指定序号的元素并在最后进行插入、删除操作,则利用<u>顺序表</u>存储方式可以节省时间。

#### Mt-内存分配

**静态分配**：顺序表的空间、长度、元素大小在数组确定后就无法改变。

```c++
#define MAXSIZE 50;  // 静态分配的最大长度
typedef struct
{
    ElemType data[MAXSIZE];  // 顺序表元素
    int length;  // 顺序表当前长度
}SqList;  // 顺序表的类型定义
```

- **Problem** 
  - 一旦空间占满，新加入数据会导致溢出，导致程序出现错误
  - 难以确定存储空间的容量
  - 存储分配需要一整段连续的存储空间，拓展灵活性稍差
- e.g. 数组

**动态分配**：顺序表的空间是在程序执行过程中通过动态存储分配语句分配的，可以动态扩充，不需要一次性划分空间

```c++
#define INITMAXSIZE 50;  // 初始定义的长度，后续可改
typedef struct
{
    ElemType *data;  // 动态分配顺序表首地址、指针
    int MAXSIZE， length;  // 动态分配最大长度和顺序表当前长度
}SqList;  // 顺序表的类型定义
```

- **Att:** 注意如果增加动态分配空间，是申请一个更大的空间存储之前的数据并释放之前的空间，因此依然是一个连续的存储空间而非链表那样的断开的存储空间
- e.g. VLA 或者 malloc/realloc

#### Cat

- 数组

#### Mt-基本操作的实现

**插入**

```c
bool Listinsert(SqList &L , int i,ElemType e) {
	if(i < l || i > L.length + 1)  //判断 l 的范围是否有效
		return false;
	if(L.length>=MaxSize)  //当前存储空间已满 ,不能插入
		return false;
	for(int j = L.length;j >= i; j--)  //将第 1 个元素及之后的元素后移
		L.data[j] = L.data[j-1];
    L.data[i-l]=e;  //在位置i处放入 e
	L.length++;  //线性表长度加 1
	return true;
}
```

> - **Cpn**
>
>   - 其中`i` 为元素位置，`e`为插入元素
>
> - **Ab**
>
>   - 时间复杂度
>
>     - 最好情况 : 在表尾插入(即 l=n+l) ,元素后 移语句将不执行,时间复杂度为 0(1 ) 。
>       最坏情况 : 在表头插入(即 i = 1) ,元素后 移语句将执行 n 次,时间复杂 度为 O(n) 。
>       平均情况 : 假设 p; (p; = 1/(n + 1) ) 是在第 i 个位置上插入一个结点的概率,则在长度为 n 的
>       线性表中插入 一 个结点时,所需移动结点的平均次数为
>       $$
>       \sum_{i=1}^{n+1}p_i (n-i+1) = \sum_{i=1}^{n+1} \frac{n-i+1}{n+1} = \frac{1}{n+1}\frac{n(n+1)}{2} = \frac{n}{2}
>       $$
>       因此,顺序表插入算法的平均时间复杂度为 O(n) 。
>
> - **F**
>   - 在顺序表 L 的第 i (l<=i<=L.length+1 ) 个位置插入新元素 e 

**删除**

```c
bool ListDelete(SqList &L,int i,Elemtype &e) {
	if (i < 1 || i > L.length)  //判断i 的范围是否有效
		return false;
	e=L.data[i-1];  //将被删除的元素赋值给e
    for(int j=i;j<L.length;j ++)
        L.data[j - 1] = L.data[j];  //将第 i 个位置后的元素前移
    L.length--; //线性表长度减 1
    return true;
}
```

> - **Ab**
>   
>   
>   
>   - 时间复杂度
>   
>     - 最好情况: 删除表尾元素(即 i= n) ,无须移动元素,时间复杂度为 0(1) 。
>   
>     - 最坏情况: 删除表头元素(即 i = 1) ,需移动除表头元素外的所有元素,时间复杂度为 O(n) 。
>   
>     - 平均情况: 假设 p1 (pl=l/n) 是删除第 i 个位置上结点的概率,则在长度为 n 的线性表中删除一个结点时, 所需移动结点的平均次数为
>       $$
>       \sum_{i=1}^{n}p_i (n-i) = \sum_{i=1}^{n} \frac{n-i}{n} = \frac{1}{n}\frac{n(n-1)}{2} = \frac{n-1}{2}
>       $$
>       因此,顺序表删除算法的平均时间复杂度为 O(n) 。
>   
> - **F**
>   
>   - 删除顺序表 L 中第 i (1< = 区=L . length) 个位置的元素,用引用变量 e 返回

**按位查找**

```c
// 静态分配 动态分配 都一样
ElemType GetElem(SqList L, int i)
{
   if (i < 1 || i > L.length)
   {
       return 0; // error
   } 
    return L.data[i - 1];
}

```

> - **Cpn**
>   - 查找位序为i，数组下标为i-1的元素的值
> - **Ab**
>   - 时间复杂度：$O(1)$
>   - 

**按值查找**

```c
int LocateElem(SqList L,ElemType e) {
    int i;
    for(i = O; i < L.length; i++)
        if(L.data[i] == e)
            return i + 1;  //下标为 i 的元素值等于e,  返回其位序
    return 0;  //退出循环,说明查找失败
}
```

> - **Cpn**
>
>   - `i` 数组下标
>   - `i + 1` 位序
>
> - **Ab**
>
>   - 时间复杂度
>
>     - 最好情况: 查找的元素就在表头,仅 需 比较 一 次,时间复杂度为 0(1) 。
>
>     - 最坏情况: 查找的元素在表尾(或不存在 )时, 需要比较 n 次,时间复杂度为 O(n) 。
>
>     - 平均情况: 假设 $p_i (p_i=1/n)$ 是查找的元素在第 i ($1 <= i <= L.length$) 个位置上的概率，则在长度为 n 的线性表中查找值为 e 的元素所需比较的平均次数为：
>       $$
>       \sum_{i=1}^n(p_i \times i) = \sum_{i=1}^n \frac{1}{n} \times i = \frac{1}{n}\frac{n(n+1)}{2} = \frac{n+1}{2}
>       $$
>       因此,顺序表按值查找算法的平均时间复杂度为 O(n) 。
>
> - Pc
>
> - **F**
>
>   - 在顺序表 L 中查找第一个元素值等于 e 的元素, 并返回其位序。
>
> - **Att**
>
>   - 不同结构体类型无法使用`==`进行比较，因此也无法使用这个方法。对于结构体需要定义对应的函数对每一个数据项进行比较。
>
> 

### 链表/单链表 Linked List

#### Cc

**Def**

- linked list—a list in which each item contains information describing where to find the next item.
- 线性表的链式存储又称**单链表**,它是指通过一组任意的存储单元来存储线性表中的数据元素。
- 

#### Ab

- **顺序存取/非随机存取**：只能从表头依次查找的存取方式
  - 由于单链表的元素离散地分布在存储空间中,所以单链表是非随机存取的存储结构,即不能直接找到表中某个特定的结点。查找某个特定的结点时, 需要从表头开始遍历，依次查找。
- 链式通过 “链”建立起元素之间的逻辑关系，不需要使用地址连续的存储单元,即不要求逻辑上相邻的元素在物理位置上也相邻
- 插入、删除运算方便，只需修改指针
- 利用单链表可以解决顺序表需要大量连续存储单元的缺点,但单链表附加指针域也存在浪费存储空间的缺点
- 单链表是整个链表的基础,读者一定要熟练掌握单链表的基本操作算法。在设计算法
  时,建议先通过图示的方法理清算法的思路,然后进行算法的编写。

#### Cpn

- **Node 结点**: In a linked list implementation, each link is called a node.
  - **Ab**
    - Each node contains information that forms the contents of the list along with a pointer to the next node. 
  - **Cat**
    - **头结点**
      - **Def + F:** 为了操作上的方便, 在单链表第一个结点之前附加一个结点, 称为头结点 。 
      - **Ab**
        - 头结点的数据域可以不设任何信息,也可以记录表长等信息。
        - 头结点的指针域指向线性表的第一个元素结点
        - 引入头结点后,可以带来两个优点:
          - 由于第一个数据结点的位置被存放在头结点的指针域中,因此在链表 的第 一个位置上的操作和在表的其他位置上的操作一致,无须进行特殊处理 。
          - 无论链表是否为空 ,其头指针都是指向头结点的非 空指针(空表 中头结点的指 针域为空), 因此空表和非空表的处理也就得到了统一。
      - **Pcp**
        - 头结点和头指针的区分 : 不管带不带头结点,头指针都始终指 向链表 的 第一个结点,而头结点是带头结点的链表中的第 一个结点,结点内通常不存储信息 。
          
  
- **Head pointer 头指针**: You can do this by assigning its address to a separate pointer that we’ll refer to as the head pointer. The head pointer points to the first item in a linked list of items.
  - **F:** 通常用头指针来标识一个单链表
    - 头指针为 NULL 时表示一个空表。


#### Mt-Implementation

- **Lk:** C Primer Plus > Listing 17.2 The films2.c Program
- **Lk:** C Primer Plus > Listing 17.4 The films3.c Program

**结点**

```c
typedef struct LNode { //定义单链表结点类型
	ElemType data; //数据域
	struct LNode *next; //指针域
}LNode, *LinkList;

```

- 一般使用LNode强调使用的是一个单链表节点，而 * LinkList强调使用的是一个单链表。

**头插法建立单链表**

```c
int END = 9999;

LinkList List_HeadInsert(LinkList &L) {//逆向建立单链表
    LNode *s; int x ;
    L=(LinkList)malloc(sizeof(LNode)); //创建头结点
    L - >next=NULL ;  //初始为空链表
    scanf( "%d ", &x);  //输入结点的值
    while (x ! = END) 
    { 
        s=(LNode* ) malloc(sizeof(LNode) ); //创建新结点(j)
        s->data=x;
        s->next=L->next;
        L->next=s; //将新结点插入表中, L 为头指针
        scanf ("%d ", &x);
    }
    return L;
}
```

- **Pc:** 该方法从一个空表开始,生成新结点,并将读取到的数据存放到新结点的数据域 中 ,然后<u>将新结点插入到当前链表的表头,即头结点之后</u>
- **Ab**
  - **时间复杂度：**每个结点插入的时间为 $O(1)$ ,设单链表长为 n, 则总时间复杂度为 $O(n)$ 。
  - 采用头插法建立单链表时 , 读入数据的顺序与生成的链表中的元素的顺序是相反。

**采用尾插法建立单链表**

```c
LinkList List_TailInsert(LinkList &L){//正向建立单链表
    int x; //设元素类型为整型
    L=(LinkList)malloc(sizeof(LNode));
    LNode *s,*r=L; //r 为表尾指针
    scan f ("%d", &x); //输入结点的值
    while(x ! =9999 )  //输入 9999 表示结束
    {
        s=(LNode *)malloc(sizeof(LNode));
        s->data=x;
        r->next=s;
        r=s; // 指向新的表尾结点
        scanf ("%d", &x) ;
        r->next=NULL;//尾结点指针置空
    }
    return L;
}
```

- **Pc**
  - 该方法将新结点插入到当前链表的表尾,为此必须增加一 个尾指针r使其始终指向当前链表的尾结点
- **Ab**
  - **时间复杂度：**每个结点插入的时间为 $O(1)$ ,设单链表长为 n, 则总时间复杂度为 $O(n)$ 。
  - 采用尾插法建立单链表时 , 读入数据的顺序与生成的链表中的元素的顺序一致。



**按序号查找结点**

```c
LNode *GetElem(LinkList L,int i) {
    if(i < 1)
        return NULL; //若 i 无效,则返回 NULL
    int j = 1; //计数 ,初始为 1
    LNode *p=L->next; //第 1 个结点指针赋给 p
    while (p != NULL && j < i)  //从第 1 个结点开始找,查找第 i 个结点
    {
        p=p->next;
        j++;
    }
    return p;  //返回第 i 个结点的指针,若工大于表长,则返回 NULL
}
```

- **Pc**
  - 在单链表中从第一个结点出发,顺着指针next域逐个往下搜索,直到找到第 l 个结点为止,否则返回最后一个结点指针域 NULL 。
- **Ab**
  - **时间复杂度:** $O(n)$

**按值查找表结点**

```c
LNode *LocateElem (LinkList L, ElemType e )
{
    LNode *p=L->next ;
    while (p ! =NULL&&p->data ! =e) //从第 1 个结点开始查找 data 域为 e 的结点
    	p = p -> next;
	return p; //找到后返回该结点指针,否则返回 NUL:...
}
```

- **Pc**
  - 从单链表的第 一 个结点开始,由前往后依次比较表中各结点数据域的值 ,若某结点数据域的值等于给定值 e, 则返回该结点的指针;若整个单链表中没有这样的结点,则返回 NULL 。

- **Ab**
  - **时间复杂度:** $O(n)$

**插入结点操作**

后插操作

```c
p=GetElem(L, i-1) ; //查找插入位置的前驱结点
s->next=p->next; // 待插入节点的后继指针指向p本身的后继节点
p -> next=s; // 待插入节点的前驱节点的指针指向待插入节点
```

- **Pc**
  - 插入结点操作将值为 x 的新结点插入到单链表的第 1 个位置上 。 先检查插入位置 的合法性, 然后找到待插入位置的前驱结点,即第 i -1 个结点,再在其后插入新结点 。
- **Ab**
  - **时间复杂度:** 
    - 本算法主要的时间开销在于查找第 i-1 个元素,时间复杂度为 O(n) 。若在给定的结点后面插入新结点, 则时间复杂度仅为 O(1) 。

后插转前插操作

```c
//将* s 结点插入到* p之前的主要代码片段
s->next = p -> next; //修改指针域,不能颠倒
p->next = s;
temp = p->data;  //交换数据域部分
p->data = s->data;
s->data = temp;
```

- **Pc**
  - 此外,可采用另 一 种方式将其转化为后插操作来实现 , 设待插入结点为* s, 将* s 插入到* p的前面 。我们仍然将* s 插入 到 * p 的后面,然后将 p ->data 与 s ->data 交换，这样既满足了逻辑关系,又能使得时间复杂度为 0(1) 。
- **Ab**
  - 对结点的前插操作均可转化为后插操作,前提是从单链表的头结点开始顺序查找到其前驱结点，时间复杂度为 $O(n)$ 。

**删除结点操作**

```c
p = GetElem(L,i-1); //查找删除位置的前驱结点
q = p->next; //令 q 指向被删除结点
p -> next = q -> next; //将* q 结点从链中“断开“
free{q); //释放结点的存储空间°
```

- **Pc**
  - 删除结点操作是将单链表的第 i 个结点删除。先检查删除位置的合法性,后查找表中第 i-1个结点,即被删结点的前驱结点,再将其删除。
  - 假设结点* p 为找到的被删结点的前驱结点,为实现这一操作后的逻辑关系的变化,仅需修改* p 的指针域,即将* p 的指针域 next 指向* q 的下一结点。
- **Ab**
  - **时间复杂度:** $O(n)$

```c
q = p->next //令q指向*p的后继结点
p -> data = p -> next -> data; //用后继结点的数据域覆盖
P -> next = q -> next; //将* q 结点从链中“断开“
free(q); //释放后继结点的存储空间
```

- **Pc**
  - 要删除某个给定结点* p, 通常的做法是先从链表的头结点开始顺序找到其前驱结点,然后执行删除操作,<u>算法的时间复杂度为 O(n)</u> 。
  - 其实,删除结点* p 的操作可用删除* p 的后继结点操作来实现,实质就是将其后继结点的值赋予其自身,然后删除后继结点* q,也能使得<u>时间复杂度为 O(1)</u> 。
    实现上述操作的代码片段见上

- 

**求表长操作**

```c

```

- **Pc**
  - 求表长操作就是计算单链表中数据结点(不含头结点)的个数,需要从第一个结点开始顺序依次访问表中的每个结点,为此需要设置一个计数器变量,每访问一个结点,计数器加 1, 直到访问到空结点为止。
  - 需要注意的是,因为单链表的长度是不包括头结点的,因此不带头结点和带头结点的单链表在求表长操作上会略有不同。对不带头结点的单链表,当表为空时,要单独处理。

- **Ab**
  - **时间复杂度:** $O(n)$



### 双链表 Doubly linked list

#### Cc

- **Def:** 双链表,双链表结点中有两个指针 piror 和 next, 分别指向其前驱结点和后继结点

#### Ab

- 双链表中的 按值查找和按位查找的操作与单链表的相同 。 
- 但双链表在插入和删除操作的实现上,与单链表有着较大的不同 
  - 这是因为“链“变化时也需要对 prior 指针做出修改,其关键是保证在修改的过程中 不断链 。 
  - 此外,双链表可以很方便地找到其前驱结点,因此,插入、删除操作的时间复杂度仅为 $O(1)$ 。

#### Mt-Implementation

**Lk:** 链表 Linked List > Mt-Implementation

**Node**

```c
typedef struct DNode{ //定义双链表结点类型
ElemType data; //数据域
struct DNode *prior , *next; //前驱和后继指针
}DNode , *DLinklist;
```

**插入**

- **Pcp**
  - 设定好插入节点的前驱结点和后继结点
  - 重新将插入节点两侧节点对应变化的前驱节点和后继结点设定好
  - 代码的语句顺序不是唯一的,但也不是任意的,设定插入节点两步必须在修改已知节点的前/后躯节点信息之前 , 否则`*p` 的后继结点的指针就会丢掉,导致插入失败 。
- **Ab**
  - **时间复杂度:** $O(1)$

后插

```cpp
s -> next = p -> next; //将结点* s 插入到结点* p 之后
s -> prior = p;
p -> next -> prior = s;
p -> next = s;
```

- 在双链表中结点`*p`之后插入结点`*s`

前插

```cpp
s -> next = p; //将结点* s 插入到结点* p 之前
s -> prior = p -> prior;
p -> prior -> next = s;
p -> next = s;
```

- 在双链表中结点`*p`之前插入结点`*s`

**删除**

- **Ab**
  - **时间复杂度:** $O(1)$

```cpp
p -> next = q -> next;
q -> next -> prior = p;
free(q);
```

- 删除双链表中结点`*p` 的后继结点`*q`



### 循环链表





### 静态链表



### --------------------



## St-Bag



**Mt-ADT**

- ADT: array



## St-队列 Queue

**Def+Ab**

- A queue is a list with two special properties. 
  - First, new items can be added only to the end of the list. In this respect, the queue is like the simple list. 
  - Second, items can be removed from the list only at the beginning. 
  - Thus, A queue is a first in, first out (FIFO) data form

**Mt-ADT**

- ADT: resizing array

**Cat**

循环队列

- 循环队列是一种数据结构

## St-栈 Stack

**Ab**

- 栈是一种抽象数据类型，逻辑结构

**Mt-ADT**

- ADT: linked list





# Sorting





# Searching

## Sequential Search

**Cc**

- **Sequential search**: Suppose you want to search a list for a particular item. One algorithm is to start at the beginning of the list and search through it in sequence, called a sequential search.

**Pcp**

- You can improve the sequential search by sorting the list first.
  - That way, you can terminate a search if you haven’t found an item by the time you reach an item that would come later.



## Binary Search

**Ab**

- 时间复杂度：$O(2^{n})$
  - In general, n comparisons let you process an array with 2n−1 members, so the advantage of a binary search over a sequential search gets greater the longer the list is.

**Mt**

- First, call the list item you want to find the target and assume the list is in alphabetical order. 
- Next, pick the item halfway down the list and compare it to the target.
  -  If the two are the same, the search is over. 
  - If the list item comes before the target alphabetically, the target, if it’s in the list, must be in the second half. 
  - If the list item follows the target alphabetically, the target must be in the first half. 
  - Either way, the comparison rules out half the list as a place to search.
- Next, apply the method again.
  - That is, choose an item midway in the half of the list that remains. Again, this method either finds the item or rules out half the remaining list.
- Proceed in this fashion until you find the item or until you've eliminated the whole list. 





# Graphs





# Strings





# Context

















# Cc-Bioinformatics Algorithm

1. **Algorithm**: The methods for solving problems that are suitable for computer implementation.
2. **Implementation of Algorithm**:
3. **In-memory algorithms**: all the programme & resources are loaded into computer memory before computation





# Ab-Algorithm Validation

1. Performance/Efficiency/Speed of execution
   1. Balance Hardware work load between memory/storage requirements and CPU performance
   2. Complexity/Order of notation(Big-O Notation):
      1. Def: performance will change with the change of inputs N following function f
      2. Obj1: The number of inputs (N)
      3. Pcp: O(f(N))
      4. e.g.: O(N<sup>2</sup>), O(log(N)), O(N log(N))
      5. Att: Big-O is an index of complexity but not the real performance when dealing with real world data.
2. Re-usability
3. Data structure 
   1. The format storing data can affect the performance of algorithm
4. Heuristics
   1. Def: A class of method that trade accuracy/precision against the computational cost of finding a solution.
   2. Obj1: When problems are computationally hard to solve
   3. Obj2: not longer finding the best solution for a given problem,
      but finds a good solution instead
   4. Disadvantage:
      1. potentially not being guaranteed to find the best solution
   5. Advantage:
      1. a solution can be found, even for problems that would
         otherwise be unsolvable





# Pcp

1. **Data structure** is important for the application of algorithm and you should understand more about how to choose the best-matched data structure to model your scenario.
2. Then **Select proper algorithm** and compare their performance characteristics.





# Sorting

**Pcp**

- Elements should be in a ordered container/data structure and elements are comparable.
- 

## Pushdown Stacks

## Selection Sort

**Pc**

1. Swap the minimum elements from the first elements to the last elements and then get the 1st min, 2nd min ... Nnd min values in every iteration
2. 

## Bubble Sort

1. Swap the value in every two elements from the first elements every iteration until there are not any swaps.

**Ab**

- Best Stituation: Sorted list O(N) 
- Worst Situation: Reverse sorted list. which you need to 



## Quick Sort

**Pcp**

1. Pick the pivot is important for 

**Ab**

- Best Case: sorted list, O(N)
- Normal Case: O(Nlog(N))
- Worst Situation: Pivot is at one end of the array(Start/End), need to spilt each elements in the array, O(N<sup>2</sup>)



# Trees

## Binary Search Trees

**Cc**

- **binary search tree**: The binary search tree is a linked structure that incorporates the binary search strategy. 

**Ab**

- **Hierarchical Organization**: A tree is a **hierarchical organization**, meaning that the data is organized in ranks, or levels, with each rank, in general, having ranks above and below it
  - This relationship holds for every node with children. 
  - All items that can trace their ancestry back to a left node of a parent contain items that precede the parent item in order, and every item descended from the right node contains items that follow the parent item in order.
  - each level has twice as many nodes as the level above it.
- **Efficiency**: The programming price is that putting a tree together is more involved than creating a linked list.
- binary search tree has no duplicate items

**Cpn**

- **Root**
  - **Def:** The top of the tree
- **Child nodes**
  - **Def:** Each node in the tree contains an item and two pointers to other nodes, called child nodes.
- **Subtree**
  - **Def:** Each node in the binary search tree is itself the root of the nodes descending from it, making the node and its descendants a subtree.
- **Leaf**
  - **Def:** The node to be deleted has no children. Such a node is called a leaf. 






# Hash Tables

**Pcp**

- String are converted into hash value 

**Space**: Hash elements are often stored in lists as hash keys are not guaranteed to be unique



