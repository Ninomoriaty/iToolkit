# Python 常用库

# Pip

```shell
pip install <name of packages>
pip install -U <name>  # 更新
pip uninstall <>  # 卸载
pip download <>  # 下载但是并不安装 

pip -h 或者 pip help# 可以找到相关的command，较为常用的参考下图
```

# Anaconda

# Tips

**Lk:** http://blog.konghy.cn/2017/04/24/python-entry-program/

# Pcp

- 常用库.包.模块
  注意常用的语法组成：
  package.子库.函数(对象/变量参数)
  函数.方法(对象, 变量, 参数)
  对象.方法(变量, 参数)

# Mt

`import`

```python
# 调用模块
import module

# 直接调用模块内的函数,使用该函数时不需要增加模块前缀，但是容易出现不同库之间的冲突、重名
from turtle import fd
from turtle import* # 全部函数调用

# 避免上述问题(虽然也没啥区别)，注意此时原名反而无法使用
import turtle as nickname
nickname.fd()
```



# Cat

`os`

```python
# os库 系统交互
import os

# 路径
os.path() 相当于调用输入的路径生成了一个对象
os.path.abspath('2.py')  # 返回绝对路径
os.path.normpath('D:\\2.py')  # 归一化路径
os.path.relpath('2.py')  # 返回文件与当前python程序之间的相对路径
os.path.dirname('D:\\2.py')  # 返回其中的目录部分
os.path.basename('D:\\2.py')  # 返回最后的文件名
os.path.join('D:\\','2.py')  # 组合字符串路径名

os.path.exists('D:\\2.py')  # 判断文件或目录是否存在
os.path.isfile('D:\\2.py')  # 判断路径是否指向文件
os.path.isdir('D:\\2.py')  # 判断路径是否是目录

os.path.getatime('D:\\2.py')  # 返回文件上一次访问的时间
os.path.getmtime('D:\\2.py')  # 返回文件上一次修改的时间
os.path.getctime('D:\\2.py')  # 返回文件上一次创建的时间

os.path.getsize('D:\\2.py')  # 返回路径的字节长度

# 进程管理 
os.system('D:\\2.exe \ D:\\2.py')  # 调用特定程序并执行其后的路径的文件 相当于cmd命令行的直接使用 最终返回的0代表正常执行

# 环境参数
os.chdir('D:\\2.py')  # 改变当前程序操作的目录
os.getcwd('D:\\2.py')  # 返回程序当前的路径
os.getlogin()  # 获取系统登录的用户名称
os.cpu_count()  # 返回cpu数量
os.urandom(n)  # 返回n个字节的随机字符串 进行加密


```

`turtle`

```python
# 画笔控制函数
import turtle as t

# 绘制标题栏的信息
t.title('huaq')

# 布局 左上角（0,0）
t.setup(0, 0)

# RGB颜色表示方式设定
t.colormode(1)
t.colormode(255)

# 设定初始位置
t.penup()  # 抬起画笔
t.pendown()  # 落笔

# 设置海龟的大小
t.pensize(10)

# 设置海龟的颜色
t.pencolor('purple')
t.pencolor('purple')
t.pencolor(1.0, 1.0, 1.0)
t.pencolor((1.0, 1.0, 1.0))

# 输入字符串
t.write('')

# 运动控制函数
# 绝对坐标 窗体中心为（0，0）
t.goto(100, 100)

# 相对坐标 海龟3D
t.fd(100)
t.bk(100)

# 曲线，根据左侧圆形半径r/右侧-r距离处绘制旋转角度为extent的弧形，注意是角度而不是弧度
t.circle(2, extent=4, step=4)

# 方向控制函数
# 绝对角坐标
t.seth(10)
t.seth(-10)

# 相对角坐标
t.left(12)
t.right(12)

# 隐藏 turtle 
t.hideturtle()

#结束并不自动退出
t.done()
```

`time`

```python
# time库的使用
import time as t

# 时间获取
t.time()      # 时间戳 浮点数
t.gmtime()    # 获取时间并转化为其他程序可处理的时间格式
t.ctime()     # 获取并将时间转化为易于读取的 字符串

# 时间格式化
ts = t.gmtime()
# 第一个参数提供格式化模板的字符串 第二个计算机内部时间类型变量 O 输出时间字符串
ty = t.strftime('%B=%b %A=%a %Y-%m-%d %H=%h %p:%M:%S', ts)  # 在%h时会变为月份。。
# 第一个依照模板的时间字符串 第二个参数提供第一个输入参数格式化模板的字符串 O 输出程序可处理时间格式
t.strptime(ty, '%B=%b %A=%a %Y-%m-%d %H=%h %p:%M:%S')

# 程序计时
t.sleep()        # 时间间隔设定
t.perf_counter()  # 测量时间 返回系统时间计数值 浮点数 需要差值计算

```

`random`

```python
# random库 注意伪随机数的意义:梅森螺旋算法产生的随机序列中的元素
import random as r

# 基本随机数函数 随机种子产生随机数的原理
r.seed()  # 随机数种子确定随机序列的产生 默认种子是第一次调用random库的系统时间float 重复率
r.random()  # 注意范围是0-1的float 而random得到的随机数的顺序与随机序列相同 可以复现

# 扩展随机数函数
# 在特定范围内找到随机数
r.randint(1, 20)  # 生成位于闭区间[1,20]的随机整数
r.uniform(1, 20)  # 生成位于闭区间[1,20]的随机float
i = input()
r.randrange(1, 20, i)  # 生成位于左闭右开区间[1,20)中的以i为步长分割得到的随机数列中抽取的一个随机整数
# 转化随机序列
seq = [1, 2, 3]  # 可以是其他类型的序列
r.choice(seq)  # 从seq中抽取一个随机元素
r.shuffle(seq)    # 将seq中的元素随机打乱，返回被打乱的seq 注意这不是副本了
# 特殊
r.getrandbits(i)  # 生成一个i比特长的二进制随机数转成的随机整数 注意的是一个数字

```

`pyinstaller`

```shell
# Pyinstaller 打包 编译 将源代码转化为可执行文件，在没有解释器的条件下进行运行。
# dist中生成独立的可执行文件
pyinstaller -F <name.py>
pyinstaller -onefile <name.py>
# dist中生成一系列的文件，整个文件夹相当于具有完整的功能，指定目录
pyinstaller -D <name.py> 
pyinstaller -onedir <name.py>
# 文件图标指定
pyinstaller -i <icon.ico> -F <name.py>
# 帮助
pyinstaller -h
# 清理打包过程中的临时文件
pyinstaller --clean

# 注意其实是可以像Linux一样多个指令同一行执行的
```

`jieba`

```python
# jieba module
import jieba
# 精确模式 不存在冗余单词
jieba.lcut('注意一定要是中文啊')  # 返回列表

# 搜索引擎模式 精确模式后对长词进一步切分以供搜索引擎使用
jieba.lcut_for_search('注意一定要是中文啊')

# 全模式 所有可能的中文词语 不同的切分组合方式
jieba.lcut('注意一定要是中文啊', cut_all=True)

# 增加新单词
jieba.add_word('阿里云')

```

`wordcloud`

```python
# wordcloud
import wordcloud
wordcloud.WordCloud()  # WordCloud是库wordcloud中代表文本词云的对象

# 词云绘制
# 空格进行文本分割 统计词频并对较少的词进行过滤
# 参数调整 词云输出效果参数配置
w = wordcloud.WordCloud(width=400, height=200,
                        \
                        min_font_size=1, max_font_size=20, font_step=1,
                        \
                        font_path='msyh.ttc'
                        )
# 参数调整 词的筛选
w2 = wordcloud.WordCloud(max_words=20, stop_words={'fu'})
# 参数调整 词云形状更换
from scipy.misc import imread
mk = imread('fff.png')
w3 = wordcloud.WordCloud(mask=mk, background_color='white')

w.generate('加载词')
w.to_file('wordcloud.jpg')  # 或者png也行

```

