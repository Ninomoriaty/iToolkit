module CCal where

-- 主要目的是实现所有的计算方程，主要是关注于其中出现可能的报错问题并使用Maybe(构造函数)来对这些问题进行解决。
-- 其中>>=就是monad。

data Exp = Con Int | Div Exp Exp | Add Exp Exp 
    deriving (Show, Eq)

-- normal edition
eval :: Exp -> Maybe Int
eval (Con i) = Just i
eval (Div e1 e2) = case (eval e1) of 
    Nothing -> Nothing
    Just i1 -> case (eval e2) of 
        Nothing -> Nothing
        Just i2 -> if i2 ==0 then Nothing else Just (i1 `div` i2)
eval (Add e1 e2) = case (eval e1) of
    Nothing -> Nothing
    Just i1 -> case (eval e2) of
        Nothing -> Nothing
        Just i2 -> Just (i1 + i2)

-- monad edition
m_eval1 :: Exp -> Maybe Int
m_eval1 (Con i) = return i
m_eval1 (Div e1 e2) = 
    m_eval1 e1 >>= (\i1 ->
        m_eval1 e2 >>= (\i2 ->
            if i2 == 0 then Nothing
                else return (i1 `div` i2)))
m_eval1 (Add e1 e2) = 
    m_eval1 e1 >>= \i1 ->
        m_eval1 e2 >>= \i2 -> 
            return (i1 + i2)

-- do edition
m_evalDO :: Exp -> Maybe Int
m_evalDO (Con i) = return i 
m_evalDO (Div e1 e2) = do 
    i1 <- m_eval1 e1 
    i2 <- m_eval1 e2
    if i2 == 0 then Nothing
        else return (i1 `div`i2)
m_evalDO (Add e1 e2) = do
    i1 <- m_eval1 e1 
    i2 <- m_eval1 e2
    return (i1 + i2)

run_m_eval :: Exp -> IO ()
run_m_eval exp = do
    case m_eval1 exp of
        Nothing -> putStrLn "Something Wrong!"
        Just i -> putStrLn $ show i
         
-- common formal
safeDiv :: Maybe Int -> Maybe Int -> Maybe Int
safeDiv x Nothing = Nothing
saftDiv Nothing y = Nothing
saftDiv (Just x) (Just y) = if y == 0 then Nothing else Just (div x y)

-- do edition
(//), (///) :: Maybe Int -> Maybe Int -> Maybe Int
x // y = x >>= (\a -> y >>= (\b -> if b == 0 then Nothing else Just (div a b)))
x /// y = do
    a <- x
    b <- y
    if b == 0 then Nothing else Just (div a b)

