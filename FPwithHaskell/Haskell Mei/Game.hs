--16336178 王成伟 生命科学学院
--module
module Game where

--import
import System.Random
import Test.QuickCheck

--definition
data Hand = Rock | Scissor | Paper
  deriving (Show,Enum,Eq)
instance Random Hand where
    random g = case randomR (0,2) g of
            (r, g') -> (toEnum r, g')
    randomR (a,b) g = case randomR (fromEnum a, fromEnum b) g of
            (r, g') -> (toEnum r, g')

--function
transCharHand :: Char -> Hand
transCharHand x
  | x == 'r' = Rock
  | x == 's' = Scissor
  | x == 'p' = Paper

transHandString :: Hand -> String
transHandString x
  | x == Rock = "石头"
  | x == Scissor = "剪刀"
  | x == Paper = "布"

getHand :: IO Hand
getHand = do
  x <- getChar
  return (transCharHand x)

win :: Hand -> Hand -> Bool
win Rock Scissor = True
win Scissor Paper = True
win Paper Rock = True
win _ _ = False

{-eq :: Hand -> Hand -> Bool
eq Rock Rock = True
eq Scissor Scissor = True
eq Paper Paper = True
eq _ _ = False-}

winner :: Int -> Int -> IO ()
winner n1 n2 = do
  if (n1 == 3 || n2 == 3)
    then do
      if n1 == 3 then do putStrLn ("算您赢了这轮。")
                 else do putStrLn ("哈哈，我赢了！")
    else do
      putStr ("请您出手 (R)石头, (S)剪刀, (P)布")
      player1 <- getHand
      empty <- getHand
      player2 <- randomRIO (Rock, Paper)
      putStrLn("您出了" ++ transHandString player1 ++ ",我出了" ++ (transHandString player2))
      if (win player1 player2)
        then do
                putStrLn ("您赢了这手")
                putStrLn ("我的得分: "++ show(n2) ++ "\n您的得分: "++ show(n1+1))
                winner (n1+1) n2
        else do
          if player1 == player2
            then do
                    putStrLn ("这一手平手")
                    putStrLn ("我的得分: "++ show(n2) ++ "\n您的得分: "++ show(n1))
                    winner n1 n2
            else do
                    putStrLn ("我赢了这手")
                    putStrLn ("我的得分: "++ show(n2+1) ++ "\n您的得分: "++ show(n1))
                    winner n1 (n2+1)

play :: IO()
play = winner 0 0
