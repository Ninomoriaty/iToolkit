module Lab3 where

--tips: nub union unions fromList toList
import Data.List

--T1
data Prop = Const Bool
    | Var Char
    | Not Prop
    | And Prop Prop
    | Or Prop Prop
    | Imply Prop Prop
        deriving Eq

instance Show Prop where
--    show :: Prop -> String
    show (Const True) = "True"
    show (Const False) = "False"
    show (Var char) = [char]
    show (Not exp1) = "~" ++ show exp1
    show (And exp1 exp2) = show exp1 ++ "&&" ++ show exp2
    show (Or exp1 exp2) = show exp1 ++ "||" ++ show exp2
    show (Imply exp1 exp2) = show exp1 ++ "=>" ++ show exp2

--T2
p1, p2, p3, p4 :: Prop
p1 = And (Var 'A') (Not (Var 'A'))
p2 = Or (Var 'A') (Not (Var 'A'))
p3 = Imply (Var 'A') (And (Var 'A') (Var 'B'))
p4 = Imply (Var 'p') (Imply (Var 'q')(Var 'p'))

--T3
type Subst = [(Char, Bool)]

sub1 = [('A', True),('B', False)]

eval :: Subst -> Prop -> Bool
eval sub (Const b) = b
eval sub (Var x) =  head [b | (a, b) <- sub, a == x]
eval sub (Not e1) = not (eval sub e1)
eval sub (And e1 e2) = eval sub e1 && eval sub e2 
eval sub (Or e1 e2) = eval sub e1 || eval sub e2 
eval sub (Imply e1 e2) = if (eval sub e1 == True) then eval sub e2 
                                                  else True

eval2 :: Prop -> Subst -> Bool
eval2 p sub = eval sub p

evalMaybe :: Subst -> Prop -> Maybe Bool

--T4
rmCopies :: (Eq a) => [a] -> [a]
rmCopies [] = []
rmCopies (x:xs) = x : rmCopies [ y | y <-xs, y/=x ]

varc :: Prop -> [Char]
varc (Const True) = ['T']
varc (Const False) = ['F']
varc (Var x) = [x]
varc (And e1 e2) = varc e1 ++ varc e2
varc (Not e1) = varc e1
varc (Or e1 e2) = varc e1 ++ varc e2
varc (Imply e1 e2) = varc e1 ++ varc e2

vars :: Prop -> [Char]
vars p = rmCopies (varc p)

allsubsts :: [Char] -> [Subst]
allsubsts ['T'] = [[('T', True)]]
allsubsts ['F'] = [[('F', False)]]
allsubsts [x] = [[(x, True)]] ++ [[(x, False)]]
allsubsts (x:xs) = [(x, True):s | s <- ss] ++ [(x, False):s | s <- ss]
    where
        ss = allsubsts xs

substs :: Prop -> [Subst]
substs p = allsubsts (vars p)

--T5
isTaut :: Prop -> Bool
isTaut p = and $ map (eval2 p) (substs p)


