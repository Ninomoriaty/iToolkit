--王成伟 16336178 生命科学学院
--Module
module Merge where

--Import
import Test.QuickCheck
import Data.List

--Function
merge :: Ord a => (a -> a -> Ording) [a] -> [a] -> [a]
merge f [] ys = ys
merge f xs [] = xs
merge f (x:xs) (y:ys)
    | 

mergeBy :: (a -> a -> Ordering) -> [a] -> [a] -> [a]
mergeBy _cmp [] ys = ys
mergeBy _cmp xs [] = xs
mergeBy cmp (allx@(x:xs)) (ally@(y:ys))
        -- Ordering derives Eq, Ord, so the comparison below is valid.
        -- Explanation left as an exercise for the reader.
        -- Someone please put this code out of its misery.
    | (x `cmp` y) <= EQ = x : mergeBy cmp xs ally
    | otherwise = y : mergeBy cmp allx ys

qSort :: [Integer] -> [Integer]
qSort [] = []
qSort (x:xs)
  = qSort [ y | y<-xs , y<=x] ++ [x] ++ qSort [ y | y<-xs , y>x]

mergesort ::  Ord a => [a] -> [a]
mergesort 
