-- 王成伟 Wang Chengwei 16336178  504430784@qq.com 生命科学学院 School of Life Science
--test for the module
{--}

--Module
module Newton_Raphson where

--import
import Test.QuickCheck

--Functions
squareroot2 :: Float -> Integer -> Float
squareroot2 x n
  | n == 0 = x
  | n >= 0 = (squareroot2 x (n-1) + 2/squareroot2 x (n-1))/2

squareroot :: Float -> Float -> Integer -> Float
squareroot r x n
  | n == 0 = x
  | n >= 0 = (squareroot r x (n-1) + r/squareroot r x (n-1))/2

{-sqrtSeq :: Float -> Float -> [Float]
sqrtSeq x r = squareroot x r n : sqrtSeq x r
  where
    n = toInteger (length (sqrtSeq x r))
  -}

sqrtSeq :: Float -> Float -> [Float]
sqrtSeq r x = x : sqrtSeq r (squareroot r x 1)

squareroot' :: Float -> Float -> Float -> Float
squareroot' r x epsilon
    = head [a |(a,b) <- zip (sqrtSeq r x) (0 : sqrtSeq r x), abs (a - b)< epsilon]

--test
{-
prop_sqrt' :: Float -> Float -> Float -> Bool
prop_sqrt' r x epsilon
  | r > 0 && epsilon > 0 && epsilon < 1 && x > 0 =(squareroot' r x epsilon)**2 - r < epsilon
  | otherwise = True

prop_sqrt :: Float -> Float -> Integer -> Bool
prop_sqrt r x n
  | r > 0 && n > 100 && x > 0 =(squareroot r x n)**2 - r < 1
  | otherwise = True

prop_sqrt2 :: Float -> Integer -> Bool
prop_sqrt2 x n
  | n > 100 && x > 0= (squareroot2 x n)**2 - 2 < 1
  | otherwise = True
-}
