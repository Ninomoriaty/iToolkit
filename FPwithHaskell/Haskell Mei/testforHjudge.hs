module Main where

mymax :: [Int] -> Int
mymax [] = 0
mymax xs = maximum xs

getIntxs :: IO [Int]
getIntxs = do
  x <- getLine
  let xs = map (\x -> read x :: Int) (words x)
  return xs

main :: IO ()
main = do
    xs <- getIntxs
    let y = mymax xs
    putStr (show y)
