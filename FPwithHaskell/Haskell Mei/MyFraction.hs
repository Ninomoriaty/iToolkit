-- Wang Chengwei 16336178 School of Life Science
--null

--module
module MyFraction where

--import
import Test.QuickCheck

--type
type Fraction = (Integer,Integer)

--1 functions
ratplus, ratminus, rattimes, ratdiv :: Fraction -> Fraction -> Fraction
ratfloor :: Fraction -> Integer
ratfloat :: Fraction -> Float
rateq :: Fraction -> Fraction -> Bool

neg :: Fraction -> Fraction
neg (a,b) =  (-a,b)

ratplus (x2,y2) (x1,y1) = (div (x2*y1+x1*y2) de, div (y1*y2) de)
  where
    de = gcd (x2*y1+x1*y2) (y2*y1)

ratminus (x2,y2) (x1,y1) = (div (x2*y1-x1*y2) de, div (y1*y2) de)
  where
    de = gcd (x2*y1-x1*y2) (y2*y1)

rattimes (x2,y2) (x1,y1) = (div (x2*x1) de, div (y1*y2) de)
  where
    de = gcd (x2*x1) (y1*y2)

ratdiv (x2,y2) (x1,y1) = (div (x2*y1) de, div (x1*y2) de)
  where
    de = gcd (x2*y1) (y2*x1)

ratfloor (x2,y2) = floor (ratfloat (x2,y2))

ratfloat (x2,y2) = fromInteger x2 / fromInteger y2

rateq (x2,y2) (x1,y1) = div x2 (gcd x2 y2) == div x1 (gcd x1 y1) && div y2 (gcd x2 y2) == div y1 (gcd x1 y1)

--2 operators
infix 6 <+>
(<+>) :: Fraction -> Fraction -> Fraction
(<+>) (x1,y1) (x2,y2) = ratplus (x1,y1) (x2,y2)

infix 6 <->
(<->) :: Fraction -> Fraction -> Fraction
(<->) (x1,y1) (x2,y2) = ratminus (x1,y1) (x2,y2)

infix 7 <-*->
(<-*->) :: Fraction -> Fraction -> Fraction
(<-*->) (x1,y1) (x2,y2) = rattimes (x1,y1) (x2,y2)


infix 7 </>
(</>) :: Fraction -> Fraction -> Fraction
(</>) (x1,y1) (x2,y2) = ratdiv (x1,y1) (x2,y2)

infix 4 <==>
(<==>) :: Fraction -> Fraction -> Bool
(<==>) (x1,y1) (x2,y2) = rateq (x1,y1) (x2,y2)

--3 property
prop_ratplus_unit :: Fraction -> Property
prop_ratplus_unit (x,y) = y > 0 ==> (x,y) <+> (0,1) <==> (x,y)

prop_ratminus :: Fraction -> Property
prop_ratminus (x,y) = y > 0 ==> (x,y) <-> (0,1) <==> (x,y)

prop_rattimes :: Fraction -> Property
prop_rattimes (x,y) = y > 0 ==> (x,y) <-*-> (0,1) <==> (0,1)

prop_ratdiv :: Fraction -> Property
prop_ratdiv (x,y) = y > 0 ==> (x,y) </> (1,1) <==> (x,y)
