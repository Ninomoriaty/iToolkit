module Triands where

triands :: Integer -> [(Integer, Integer, Integer)]
triands n = [(x,y,z) |  x <- [1..n], y <- [1..n], z <- [1..n],
            (fromInteger x)**2 + (fromInteger y)**2 == (fromInteger z)**2]

triands2 :: Integer -> [(Integer, Integer, Integer)]
triands2 n = [(x,y,z) |  x <- [1..n], y <- [1..n], z <- [1..n],
            (fromInteger x)**2 + (fromInteger y)**2 == (fromInteger z)**2
            && x <= y]
