{- module -}
module ToolkitForIris where

{- common used packages -}
-- check but don't submit
import Test.QuickCheck -- ghci quickCheck
import Test.HUnit -- testcase

-- about List 
import Data.List --
import 

--about Text Processing

{- Zh 基本概念解释 -}
-- 数据 数据是各种领域中的对象或信息中抽象出来的表示对应属性的数值化符号表示。例如：二进制文本数据，体温，GDP等。
-- 程序 计算机程序是通过指令的顺序，使计算机能按所要求的功能进行精确执行的指令集。而在haskell中程序是输入数据类型到输出数据类型的函数。例如：应用软件，操作系统。
-- 计算机程序设计 为了完成一个特定计算任务而设计和编写计算机可执行程序的过程
-- 算法 算法是任何良定义的计算过程，该过程提取某个值或集合作为输入，根据表达式(expression)进行转化形成某个值或集合的输出的计算步骤的一个序列。例如：Strassen算法，Bellman-Ford算法，Dijkstra算法，或者就像Haskell中定义的函数(function)即是算法的一种缩影。
-- 类型 类型是一系列同类(sort)的值(value)和运算的集合，这往往代表了对象具有的问题域(problem domain)或属性(attribute)。例如：Integer, Float, String。
-- 函数 函数是输入变量到输出变量的一种映射，而这种映射在计算机中往往以算法体现。例如：Haskell中的内置函数sum, take, drop。
-- 递归函数 接受自然数的有限元组并并返回一个单一自然数的偏函数。包括初始函数并闭合在复合、原始递归和μ算子下的最小的偏函数类。简单的意义上说一个函数由基例和递归模块组成且在内部直接或间接调用函数自身那么这个函数就是递归函数。
-- 高阶函数 将函数作为参数输入或输出的可能带有Curring化性质的函数属于高阶函数
-- 多态函数 函数的类型签名里包含类型变量/型别变数，即这个函数的某些参数可以是任意类型，那么这种函数就是多态函数
-- 重载 允许在同一作用域中的某个函数和运算符指定多个类型定义的函数、运算符或者类型，从而使得它们可以以相同的签名执行多种不同的功能
-- 自定义类型 在程序自带的标准库之外，用户通过构造函数自行定义的数据类型，在haskell中体现为type 或者 data 


{- functions repository -}
-- Syntax sugars

-- List operation

-- IO how to I/O


{- Algorithm -}
-- Prime: 1st must be a prime. And then try to filter out all non-prime of it. 
primes :: [Integer]
primes = filterPrime [2..] 
  where filterPrime (p:xs) = 
          p : filterPrime [x | x <- xs, x `mod` p /= 0]








