
--module
module CardPlayer where

--import
-- import Test.QuickCheck


--type
type Team = (Player,Player)
data Player = East | South | West | North | None
  deriving (Eq,Show,Ord)

data Suit = Spladespades | Hearts | Diamonds| Clubs
  deriving (Eq,Show,Ord)
type Value = Int
type Deck = (Suit, Value)

type Hand = ([Deck],Player)
type Hands = [Hand]

type Trick = [(Deck,Player)]

--functions
myTrick1 :: Trick
myTrick1 = [((Hearts,2),East),((Hearts,6),South),((Hearts,4),West),((Hearts,5),North)]

myTrick2 :: Trick
myTrick2 = [((Hearts,2),East),((Hearts,6),South),((Hearts,4),West),((Clubs,5),North)]

myTricks :: [Trick]
myTricks = [myTrick1, myTrick2]

myHands :: Hands
myHands = [([(Hearts,2),(Hearts,5)],East),([(Hearts,3)],South),([(Hearts,4)],West),([(Hearts,5)],North)]

teamNS :: Team
teamNS = (North, South)

teamEW :: Team
teamEW = (East, West)

teamNone :: Team
teamNone = (None,None)

winNT :: Trick -> [Player]
winNT trick = [snd step | step <- trick, snd (fst step) == maximum (map (snd.fst) trick)]

winT :: Suit -> Trick -> [Player]
winT suit trick
  | suit `elem` map (fst.fst) trick = winNT [step | step <- trick, (fst.fst) step == suit]
  | otherwise = winNT trick

checkPlay :: Hands -> Trick -> Bool
checkPlay hands trick
  | length hands == 1 =
          null [deck | deck <- [fst step | step <- trick, snd step == snd (head hands)],
          deck `notElem` fst (head hands)]
  | null [deck | deck <- [fst step | step <- trick, snd step == snd (head hands)],
          deck `notElem` fst (head hands)] && checkPlay (tail hands) trick
          && null [step | step <-trick, (fst.fst) step /= fst (fst (head trick))] = True
  | otherwise = False

winNS :: [Trick] -> Int
winNS tricks = length [ winner | winner <- map winNT tricks, North `elem` winner || South `elem` winner]

winEW :: [Trick] -> Int
winEW tricks = length [ winner | winner <- map winNT tricks,  East `elem` winner || West `elem` winner]

winnerNT :: [Trick] -> Team
winnerNT tricks
  | winEW tricks > winNS tricks = teamEW
  | winNS tricks > winEW tricks = teamNS
  | otherwise = teamNone

rmTrick :: Player -> Trick -> Trick
rmTrick player trick = [itemNext | itemNext <- trick, snd itemNext /= player]

checkPlays :: [Trick] -> Bool
checkPlays tricks = checkPlay (hands1 (concat tricks)) (concat tricks)
  where
    hands1 :: Trick -> Hands
    hands1 trick
      | length trick == 0 = []
      | otherwise = ([fst step | step <- trick, snd step == snd (head trick)], snd (head trick))
        : hands1 (rmTrick (snd (head trick)) trick)
