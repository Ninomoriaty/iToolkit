-- 王成伟 Wang Chengwei 16336178  504430784@qq.com 生命科学学院 School of Life Science
--test for the module
{-Here we don't use the method given by TA but some functions defined when learning
Picture.-}

--Module
module HaskellStore where

--import
import Test.QuickCheck
import Text.Printf

--type
type Picture = [String]

type Name = String
type Price = Float
type Amount = Float

type Items = [Item]
type Item = (Name, Amount, Price)

--function
titleRow :: String
titleRow = pushLeft "Name" ++ pushLeft "Amount"
        ++ pushLeft "Price" ++ pushLeft "Sum"

showPre :: Float -> String
showPre = printf"%.2f"

pushLeft :: String -> String
pushLeft xs
  | n - length xs <= 0 = xs
  | n - length xs /= 0 || xs == "" = pushLeft (xs ++ " ")
    where
      n = 12

fst3 :: (a, b, c) -> a
fst3 (a, _, _) = a

snd3 :: (a, b, c) -> b
snd3 (_, b, _) = b

third :: (a, b, c) -> c
third (_, _, c) = c

total1 :: Items -> Float
total1 items
  | null items = 0
  | length items == 1 = third (head items) * snd3 (head items)
  | otherwise = third (head items) * snd3 (head items) + total1 (tail items)

total2 :: Items -> Float
total2 items
  | null items = 0
  | otherwise = sum [snd3 item * third item | item <- items]

transItemsPic :: Items -> Picture
transItemsPic items = titleRow : [pushLeft (fst3 item) ++ pushLeft (showPre (snd3 item))
  ++ pushLeft (showPre (third item)) ++ pushLeft (showPre (third item * snd3 item)) | item <- items]
  ++ ["Total  "++ replicate 27 '.' ++ "  " ++ pushLeft (showPre (total2 items))]

printPicture :: Picture -> IO ()
printPicture = putStr . unlines

rmItems :: Item -> Items -> Items
rmItems item items = [itemNext | itemNext <- items, fst3 itemNext /= fst3 item]

(<+>) :: Items -> Items
(<+>) xs = [(fst3 (head xs), sum (map snd3 xs), third (head xs))]

addSameItems :: Items -> Items
addSameItems items
  | length items == 0 = []
addSameItems (item:items) = (<+>) (item : [ item2 | item2 <- items, fst3 item == fst3 item2])
                            ++ addSameItems (rmItems item items)

printItems :: Items -> IO()
printItems items = printPicture (transItemsPic (addSameItems items))

myItems :: Items
myItems = [("huaq2", 12, 12),("huaq2", 2, 12),("huaq1",3,23),("huaq1",12,23),("huaq2",2,12),("huaq3",12,2)]

--properties
prop_showPre :: Float -> Bool
prop_showPre x = length (showPre x) >= 2

prop_pushLeft :: String -> Bool
prop_pushLeft str = length (pushLeft str) >= 12

--之前提到过的精度问题，尝试更换Double解决
type Price' = Double
type Amount' = Double

type Items' = [Item']
type Item' = (Name, Amount', Price')

total1' :: Items' -> Double
total1' items
  | null items = 0
  | length items == 1 = third (head items) * snd3 (head items)
  | otherwise = third (head items) * snd3 (head items) + total1' (tail items)

total2' :: Items' -> Double
total2' items
  | null items = 0
  | otherwise = sum [snd3 item * third item | item <- items]

prop_total12 :: Items' -> Bool
prop_total12 items = total1' items == total2' items

prop_transItemPic :: Items -> Bool
prop_transItemPic items = length items + 2 == length (transItemsPic items)
