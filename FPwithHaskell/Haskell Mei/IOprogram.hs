--Wang Chengwei 16336178

--module
module IOprogram where

--import
import System.Random
import Test.QuickCheck

--IO Int: The type of actions that return an Int
--IO () :The type of purely side effecting actions that return no result value.

--Guess Num
getInt :: IO Int
getInt = do
  x <- getLine
  return (read x :: Int)

getIntxs :: IO [Int]
getIntxs = do
  x <- getLine
  let xs = map (\x -> read x :: Int) (words x)
  return xs

getInts :: Int -> IO [Int]
getInts n = do
  x <- getInt
  if n == 1
    then return [x]
    else
      do
        xs <- getInts (n-1)
        return ([x] ++ xs)

getIntsPositive ::  IO [Int]
getIntsPositive = do
  x <- getInt
  if x < 0
    then return []
    else
      do
        xs <- getIntsPositive
        return ([x] ++ xs)

getIntsPositiveSum ::  IO Int
getIntsPositiveSum = do
  x <- getInt
  if x < 0
    then return 0
    else
      do
        y <- getIntsPositiveSum
        return (x+y)

guessIt :: Int -> Int -> IO ()
guessIt x n = do
  y <- getInt
  if x == y
    then putStrLn ("Bingo and you have guessed "++ show(n) ++ "times")
    else do
      if x > y
        then do putStrLn ("Larger than yours and you have guessed "++ show(n) ++ "times")
                guessIt x (n+1)
        else do putStrLn ("Smaller than yours and you have guessed "++ show(n) ++ "times")
                guessIt x (n+1)

guessNum :: IO ()
guessNum = do
  x <- getInt
  guessIt x 1

guessRandomNum :: IO ()
guessRandomNum = do
  x <- randomRIO (0, 1000)
  guessIt x 1

--Rock
win :: Int -> Int -> Bool
win 0 1 = True
win 1 2 = True
win 2 0 = True
win _ _ = False

winner :: Int -> Int -> IO ()
winner n1 n2 = do
  if (n1 == 2 || n2 == 2)
    then do
      putStrLn ("Player1 Score:"++ show(n1) ++ "    Player2 Score:"++ show(n2))
      if n1 == 2 then do putStrLn ("You win")
                 else do putStrLn ("You lose")
    else do
      player1 <- getInt
      player2 <- randomRIO (0, 2)
      if (win player1 player2)
        then do putStrLn ("Player1 Score:"++ show(n1) ++ "    Player2 Score:"++ show(n2))
                winner (n1+1) n2
        else do
          if player1 == player2
            then do putStrLn ("Player1 Score:"++ show(n1) ++ "    Player2 Score:"++ show(n2))
                    winner n1 n2
            else do putStrLn ("Player1 Score:"++ show(n1) ++ "    Player2 Score:"++ show(n2))
                    winner n1 (n2+1)

--flipCoin

flipCoin :: IO Int
flipCoin =  do
  x <- randomRIO (0, 1)
  return x

getCoins :: Int -> IO [Int]
getCoins n = do
  x <- flipCoin
  if n == 1
    then return [x]
    else
      do
        xs <- getCoins (n-1)
        return ([x] ++ xs)

countIt :: [Int] -> Int
countIt xs = length [x | x <- xs, x == 1 ]

flipManyTimes :: Int -> IO Float
flipManyTimes n = do
  xs <- getCoins n
  let y = countIt xs
  return ((fromIntegral y)/(fromIntegral n))

flipAverageList :: Int -> Int -> IO [Float]
flipAverageList n m = do
  x <- flipManyTimes n
  if m == 1
    then return [x]
    else
      do
        xs <- flipAverageList n (m-1)
        return ([x] ++ xs)

flipAverage :: IO Float
flipAverage = do
    n <- getInt
    m <- getInt
    xs <- flipAverageList n m
    return ((sum xs)/(fromIntegral m))

-- Average
computeAverage :: IO ()
computeAverage = do
  xs <- readFile "input.txt"
  let ys = [(read x :: Int) | x <- (words xs)]
  let y = average ys
  writeFile "output.txt" (show y)

average :: [Int] -> Float
average xs = fromIntegral.sum xs / length xs
