--16336178 王成伟 生命科学学院
--null

--Module
module Test where

--import
import Test.QuickCheck

--1.非负整数最小公因子函数--辗转相除法
myGcd :: Integer -> Integer -> Integer
myGcd x y
  | x >= y = myGcdL x y
  | x <= y = myGcdL y x

myGcdL :: Integer -> Integer -> Integer
myGcdL x y
  | mod x y == 0 = y
  | mod x y /= 0 = myGcdL y (mod x y)

--1.非负整数最小公因子函数--辗转相减法
myGcdMinus :: Integer -> Integer -> Integer
myGcdMinus x y
  | abs x >= abs y = myGcdM (abs x) (abs y)
  | abs x <= abs y = myGcdM (abs y) (abs x)

myGcdM :: Integer -> Integer -> Integer
myGcdM x y
  | x - y == 0 = y
  | x /= 0 && y == 0 = x
  | x - y /= 0 && (x-y) >= y = myGcdM (x - y) y
  | x - y /= 0 && (x-y) <= y = myGcdM y (x-y)

--如果需要包含负数
myGcdNegative :: Integer -> Integer -> Integer
myGcdNegative x y
  | x >= 0 && y >= 0 = myGcdMinus x y
  | x <= 0 && y <= 0 = myGcdMinus (abs x) (abs y)
  | x <= 0 && y >= 0 = -myGcdMinus (abs x) y
  | y <= 0 && x >= 0 = -myGcdMinus (abs y) x

--简化版的负数 参考abs加成后的myGcdMinus

--Properties 其实比较怀疑gcd使用的是辗转相减法
{-prop_myGcd :: Integer -> Integer -> Bool
prop_myGcd x y = myGcd x y == gcd x y-}

prop_myGcdM :: Integer -> Integer -> Bool
prop_myGcdM x y = myGcdMinus x y == gcd x y

--2.阶乘函数
fac :: Integer -> Integer
fac n
  | n==0 = 1
  | n>0 = fac (n-1) * n
  | otherwise = error "fac only defined on natural numbers"

--3.阶乘累加函数
sumFacs :: Integer -> Integer
sumFacs n
  | n==0 = 1
  | n>0 = sumFacs (n-1) + fac n

--4.广义累加函数
sumFun :: (Integer -> Integer) -> Integer -> Integer
sumFun f n
  | n==0 = f 0
  | n>0 = sumFun f (n-1) + f n
