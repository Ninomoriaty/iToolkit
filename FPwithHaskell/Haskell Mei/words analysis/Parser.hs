--module
module Parser where

--import
import Data.List

--function
groupi :: [String] -> [[String]]
groupi [] = []
groupi xs = sort (group xs)

count :: [[String]] -> [(String,Int)]
count xss = [ (head xs, length xs) | xs <- xss]

refile2 :: [(String,Int)] -> String
refile2 [] = []
refile2 ((x,y):xs) = x ++ ": " ++ show y ++ " \n" ++ refile2 xs

rank :: [(String,Int)] -> [(String,Int)]
rank zs = sortBy (\(a1,b1) (a2,b2) -> compare b2 b1) zs

parserIO :: IO ()
parserIO = do
    xs <- readFile ("input.txt")
    let ys = refile2 (rank (count (groupi (words xs))))
    writeFile "output.txt" (ys)
