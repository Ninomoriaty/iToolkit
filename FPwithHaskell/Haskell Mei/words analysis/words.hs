--王成伟 16336178 生命科学学院 生物技术专业

--Module
module Main where

--Import
import Data.List
import Data.Char
import Data.String


--Function

toLowerStr :: String -> String
toLowerStr str = [ toLower x | x <- str]

groupSp :: [[String]] -> [[String]]
groupSp xss = [xs | xs <- xss , not (null (head xs)) ]

groupi :: [String] -> [[String]]
groupi [] = []
groupi xs = group (sort xs)

count :: [[String]] -> [(String,Int)]
count xss = [ (head xs, length xs) | xs <- xss]

rank :: [(String,Int)] -> [(String,Int)]
rank zs = sortBy (\(a1,b1) (a2,b2) -> compare b2 b1) zs

replaceSp :: String -> String
replaceSp [] = []
replaceSp [wd] = [wd]
replaceSp (wd1:wd2:ls)
  | (wd1 == '\37413' && wd2 == '\27290') || (wd1 == '\37413' && wd2 == '\27054') || wd1 == '’' || wd1 == '‘' 
        = '\'' : replaceSp ls
  | (wd1 == '.') || (wd1 == ',') || (wd1 == '\"') || (wd1 == '/') || (wd1 == '(') || (wd1 == ')')  = ' ' : replaceSp (wd2 :ls)
  | otherwise =  wd1 : replaceSp (wd2 :ls)

wordFilter :: [String] -> [String]
wordFilter [] = []
wordFilter (str:strs)
    | str == "warm-up:" = "warm-up" : wordFilter strs
    | elem '\'' str = (takeWhile (\x -> x /= '\'') str) : wordFilter strs
    | elem '-' str = str : wordFilter strs
    | null str = wordFilter strs
    | and (map (\x -> isAlpha x || isDigit x) str) = str : wordFilter strs
    | otherwise = filter (\x -> isAlpha x || isDigit x) str : wordFilter strs

string2listofpairs :: String ->[(String, Int)] 
string2listofpairs xs = rank $ count $ groupSp $ groupi $ wordFilter $ words $ replaceSp $ toLowerStr xs

formatting :: [(String, Int)] ->String 
formatting [] = []
formatting ((x,y):xs) = x ++ " " ++ show y ++ "\n" ++ formatting xs

main :: IO ()
main = do
   ls <- readFile "text.txt"
   let result = string2listofpairs ls
   let formatedResult = formatting result
   writeFile "answer.txt" (formatedResult)

--detect
format :: [(String, String)] -> String
format [] =[]
format ((x,y):xs) = x ++ ".." ++ y ++ "\n" ++ format xs

check :: IO ()
check = do
   ls1 <- readFile "answer.txt"
   ls2 <- readFile "ansb2.txt"
   let ys = [ (x, y) |(x, y) <- zip (lines ls1) (lines ls2), x /= y ]
   writeFile "checklist.txt" (format ys)
