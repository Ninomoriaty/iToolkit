
module Shape where

import Test.QuickCheck

--data Item = Itemize Name Amount Price

data Shape = Circle Float |
             Rectangle Float Float|
             Triangles Float Float Float
         deriving (Eq,Ord,Show,Read)

isRound :: Shape -> Bool
isRound (Circle _)      = True
isRound _ = False

perimeter :: Shape -> Float
perimeter (Circle r) = 2*pi*r
perimeter (Rectangle h w) = 2*(h+w)
perimeter (Triangles a b c) = a + b + c

isShapeRegular :: Shape -> Bool
isShapeRegular shape =
    case shape of
        (Triangles a b c) -> (a == b) && (b == c)
        (Circle r) -> True
        (Rectangle a b) -> a == b



data Season = Spring | Summer | Autumn | Winter
    deriving (Eq,Ord,Enum,Show,Read)

isHot :: Season -> Bool
isHot season =
    case season of
        Spring -> False
        Summer -> True
        Autumn -> False
        Winter -> False

nextSeason :: Season -> Season
nextSeason season =
    case season of
        Spring -> Summer
        Summer -> Autumn
        Autumn -> Winter
        Winter -> Spring
