--16336178 王成伟 生命科学学院
--使用说明
{-
--模拟方法和过程
模拟方法的实现过程如下：
1)dice首先模拟了一次投掷骰子的点数
2)dices然后模拟了同时投掷n个投掷骰子所得到的点数的列表
3)countIt对dices中提供的列表进行6点数的元素计数
4)manyDices形成进行count次dices中6的点数出现次数的列表
5)countThem对manyDices中的6的点数出现次数中大于y的元素计数
6)provement最终集合了上述步骤，对同时投掷n枚骰子中出现至少y次6的事件重复count次进行了
概率计算

--原理以及使用说明
在使用这个对Newton–Pepys problem进行求解的方式是：调用provement函数进行测试。

在provement函数中，provement n y count代表了n个正常的骰子独立投掷，至少出现y个6的
事件在进行count次实验后所得到的频率,当count足够大时（经过测试，大概10000次以上
是比较有准确的)，可以认为其为事件发生的概率，因此在对
1)provement 6 1 10000
2)provement 12 2 10000
3)provement 18 3 10000
进行相应的测试之后，对比其计算出的频率的大小可以大致地估计其概率大小，从而可以验证哪一种
情况发生的概率较大。

最终的结果是第一种情况出现的概率最大。

PS.当然，其实这就是一个概率论问题，在wiki上已经给出了概率解，因此我们不考虑实现这种较为
简单的概率运算，选择了重复试验的方法进行验证。
-}

--module
module NewtonPepysProblem where

--import
import System.Random
import Test.QuickCheck

--type


--function
dice :: IO Int
dice =  do
  x <- randomRIO (1, 6)
  return x

dices :: Int -> IO [Int]
dices n = do
  x <- dice
  if n == 1
    then return [x]
    else
      do
        xs <- dices (n-1)
        return ([x] ++ xs)

countIt :: [Int] -> Int
countIt xs = length [x | x <- xs, x == 6 ]

manyDices :: Int -> Int -> IO [Int]
manyDices n count = do
  xs <- dices n
  if count == 1
    then return ([countIt xs])
    else
      do
        counts <- manyDices n (count-1)
        return ([countIt xs] ++ counts)

countThem :: [Int] -> Int -> Int
countThem xs y = length [x | x <- xs, x >= y ]

provement :: Int -> Int -> Int -> IO Float
provement n y count = do
  xs <- manyDices n count
  let z = countThem xs y
  return ((fromIntegral z)/(fromIntegral count))
