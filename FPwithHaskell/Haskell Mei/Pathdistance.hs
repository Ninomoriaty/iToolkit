--Module
module PathDistance where

--Type
type Point = (Float, Float)

--Function
distance :: Point -> Point -> Float
distance (x, y) (a, b) = sqrt ((x - a)**2 + (y - b)**2)

pathDistance :: [Point] -> Float
pathDistance xs = sum [distance x y | (x, y) <- zip (init xs) (tail xs)]
