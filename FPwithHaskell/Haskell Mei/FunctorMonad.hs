module FunctorMonad where

import CCal
-- 课程记录 结合CCal进行阅读
-- Functor Part
fmapi :: (a -> b) -> Maybe a -> Maybe b
fmapi f Nothing = Nothing
fmapi f (Just x) = Just (f x)

fmapIO :: (a -> b) -> IO a -> IO b
fmapIO f k = do 
    x <- k
    return (f x)

fmapxs :: (a -> b) -> [a] -> [b]
fmapxs f [] = []
fmapxs f (x:xs) = f x : fmapxs f xs

--Monad参考CCal 通过这种机制可以将不同的函数连接起来，尝试提高每一个function的可复用性
-- a simple case about Monad
getInt :: IO Int
getInt = do
    x <- getLine
    return (read x :: Int)

doubleprint :: Int -> IO ()
doubleprint x = putStrLn (show (2*x))

combine :: IO ()
combine = getInt >>= doubleprint