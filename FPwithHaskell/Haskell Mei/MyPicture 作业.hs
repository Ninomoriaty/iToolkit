
--module
module MyPicture where

--import
import Data.Char
import Data.List
import Test.QuickCheck
import Pictures

-- Data
alphabet :: [Picture]
alphabet = [picA, picB, picC, picD, picE, picF, picG,
            picH, picI, picJ, picK, picL, picM, picN,
            picO, picP, picQ, picR, picS, picT, picU,
            picV, picW, picX, picY, picZ]

numbers :: [Picture]
numbers = [pic0, pic1, pic2, pic3, pic4, pic5, pic6, pic7, pic8, pic9]

-- Function
sideBySide :: [Picture] -> Picture
sideBySide (pic:pics)
  | length pics == 1 = pic `beside` (concat pics)
  | otherwise = pic `beside` (sideBySide pics)

figureOut :: Int -> Picture
figureOut int
  | int <= ord 'Z' && int >= ord 'A' = alphabet !! (int - ord 'A')
  | int <= ord '9' && int >= ord '0' = numbers !! (int - ord '0')

say :: String -> Picture
say str = sideBySide [figureOut (ord (toUpper ch)) | ch <- str, isUpper ch || isLower ch || isDigit ch]

sayIt :: String -> IO()
sayIt = printPicture.say

--Alphabet
picA, picB, picC, picD, picE, picF, picG :: Picture
picH, picI, picJ, picK, picL, picM, picN :: Picture
picO, picP, picQ, picR, picS, picT, picU :: Picture
picV, picW, picX, picY, picZ :: Picture
pic0, pic1, pic2, pic3, pic4, pic5, pic6, pic7, pic8, pic9 :: Picture

picA =
    ["    ##    ",
     "  #    #  ",
     " #      # ",
     " ######## ",
     " #      # ",
     " #      # ",
     " #      # "]

picB =
     [" #####   ",
      " #    #  ",
      " #   #   ",
      " ####### ",
      " #     # ",
      " #    #  ",
      " #####   "]

picC =
   ["    #### ",
    "  #      ",
    " #       ",
    " #       ",
    " #       ",
    "  #      ",
    "    #### "]

picD =
    [" #####   ",
     " #    #  ",
     " #     # ",
     " #     # ",
     " #     # ",
     " #    #  ",
     " ####   "]

picE =
   [" ####### ",
    " #       ",
    " #       ",
    " ####### ",
    " #       ",
    " #       ",
    " ####### "]

picF =
   [" ####### ",
    " #       ",
    " #       ",
    " ####### ",
    " #       ",
    " #       ",
    " #       "]

picG =
   ["   ###   ",
    "  #      ",
    " #       ",
    " #   ### ",
    " #     # ",
    "  #    # ",
    "   ###   "]

picH =
    [" #      # ",
     " #      # ",
     " #      # ",
     " ######## ",
     " #      # ",
     " #      # ",
     " #      # "]

picI =
   [" ######## ",
    "    ##    ",
    "    ##    ",
    "    ##    ",
    "    ##    ",
    "    ##    ",
    " ######## "]

picJ =
   [" ######## ",
    "    ##    ",
    "    ##    ",
    "    ##    ",
    "    ##    ",
    "    ##    ",
    " ####     "]

picK =
    [" #       #  ",
     " #     #    ",
     " #   #      ",
     " # #        ",
     " #   #      ",
     " #     #    ",
     " #       #  "]

picL =
   [" #          ",
    " #          ",
    " #          ",
    " #          ",
    " #          ",
    " #          ",
    " ########## "]

picM =
    [" #        # ",
     " ##       # ",
     " # #     ## ",
     " #  #   # # ",
     " #   # #  # ",
     " #    #   # ",
     " #    #   # "]

picN =
    [" ##       # ",
     " # #      # ",
     " #  #     # ",
     " #    #   # ",
     " #     #  # ",
     " #      # # ",
     " #        # "]

picO =
     ["    ####    ",
      "   #    #   ",
      " #        # ",
      " #        # ",
      " #        # ",
      "  #      #  ",
      "    ####    "]

picP =
      [" #########  ",
       " #        # ",
       " #        # ",
       " ########   ",
       " #          ",
       " #          ",
       " #          "]
picQ =
       ["    ####    ",
        "   #    #   ",
        " #        # ",
        " #        # ",
        " #     #  # ",
        "  #      #  ",
        "    ####  # "]
picR =
        [" #########  ",
         " #        # ",
         " #        # ",
         " ########   ",
         " #   #      ",
         " #     #    ",
         " #        # "]
picS =
         ["   #####    ",
          " #      #   ",
          "  #         ",
          "   # # #    ",
          "          # ",
          "  #       # ",
          "    #####   "]
picT =
          [" ########## ",
           "     #      ",
           "     #      ",
           "     #      ",
           "     #      ",
           "     #      ",
           "     #      "]
picU =
           [" #        # ",
            " #        # ",
            " #        # ",
            " #        # ",
            " #        # ",
            "  #      ## ",
            "   #####  # "]
picV =
            [" #       #  ",
             " #       #  ",
             " #       #  ",
             " #       #  ",
             "  #     #   ",
             "    # #     ",
             "     #      "]
picW =
             [" #         # ",
              " #         # ",
              " #    #    # ",
              " #    #    # ",
              "  #   #   #  ",
              "   # # # #   ",
              "    #   #    "]
picX =
              ["  #     #   ",
               "   #   #    ",
               "    # #     ",
               "    # #     ",
               "   #   #    ",
               "  #     #   ",
               " #        # "]
picY =
               [" #        # ",
                "  #      #  ",
                "    #   #   ",
                "      #     ",
                "      #     ",
                "      #     ",
                "      #     "]
picZ =
                [" ########## ",
                 "         #  ",
                 "       #    ",
                 "     #      ",
                 "   #        ",
                 " #          ",
                 " ########## "]
pic1 =
                 ["     11     ",
                  "    111     ",
                  "   1!11     ",
                  "     11     ",
                  "     11     ",
                  "     11     ",
                  " 1111111111 "]
pic2 =
                  ["   2222222  ",
                   "  2      22 ",
                   "       22   ",
                   "     22     ",
                   "   22       ",
                   " 22         ",
                   " 2222222222 "]
pic3 =
                   ["   333333   ",
                    "         33 ",
                    "       333  ",
                    "   3333     ",
                    "       333  ",
                    "         33 ",
                    " 33333333   "]
pic4 =
                    ["       4 4  ",
                     "      4  4  ",
                     "    4    4  ",
                     "   4     4  ",
                     " 4444444444 ",
                     "         4  ",
                     "         4  "]

pic5 =
                [" 555555555  ",
                 " 5          ",
                 " 5          ",
                 " 555555555  ",
                 "          5 ",
                 "          5 ",
                 " 555555555  "]
pic6 =
                 [" ########## ",
                  " #          ",
                  " #          ",
                  " ########## ",
                  " #        # ",
                  " #        # ",
                  " ########## "]
pic7 =
                  [" 7777777777 ",
                   "         77 ",
                   "        7   ",
                   "      7     ",
                   "    7       ",
                   "   7        ",
                   " 7          "]
pic8 =
                   ["   888888   ",
                    " 8        8 ",
                    " 8        8 ",
                    " 8888888888 ",
                    " 8        8 ",
                    " 8        8 ",
                    "   888888   "]

pic9 =
                   [" ########## ",
                    " #        # ",
                    " #        # ",
                    " ########## ",
                    "          # ",
                    "          # ",
                    " ########## "]

pic0 =
                   ["   000000   ",
                    " 0        0 ",
                    " 0        0 ",
                    " 0        0 ",
                    " 0        0 ",
                    " 0        0 ",
                    "   000000   "]
