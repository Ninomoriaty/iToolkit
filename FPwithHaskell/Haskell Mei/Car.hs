--王成伟 16336178 生命科学学院

-- module
module Car where

-- import
import Test.QuickCheck
import Data.List

-- function
test :: String
test = "1230197812061080       24596\n1230197210049047       11293\n1331197708184092       26387\n1331198509014173       21991\n1331198702174176       13778"

extract :: String -> [(Int,Int)]
extract [] = [] 
extract xs = (read (take 16 xs) :: Int, read (take 5 (drop 23 xs)) :: Int) : extract (drop 29 xs)       

refile1 :: [(Int,Int)] -> String
refile1 [] = []
refile1 ((x,y):xs) = show x ++ "       " ++ show y ++ " \n" ++ refile1 xs 

averagei :: [(Int,Int)] -> Float
averagei xs = fromIntegral (sum [ y | (x,y) <- xs]) / fromIntegral (length xs)

final :: IO ()
final = do
  xs <- readFile "bids_201711.txt"
  let zs = extract xs
  let os = sortBy (\(a1,b1) (a2,b2) -> compare b2 b1) zs
  let as = take 10 os
  let bs = 
        "最高成交价：" ++ show (snd (head os)) ++ 
        "\n最低成交价：" ++ show (snd (last os)) ++ 
        "\n平均成交价：" ++ show (averagei os) ++
        "\n总共有" ++ show (length os) ++ "参与竞价" ++
        "\n成交名单：\n"
        ++ (refile1 as)
  writeFile "bidResults.txt" (bs)
