###########################################
############### Introduction #################
###########################################



###########################################
############### Shell #################
###########################################
# Interact with shell in REPL
## Check and modify the workspace and directory
dir = pwd()  # current workspace
homedir()  # check the home directory
cd("./iToolkit/JuliaToolkits/BasicIntroduction/")  # change directory

## Enter shell mode
;  # Entry
Ctrl+C  # return to Julia REPL

## Cmd Type, Run shell command in REPL
cmd = `cat $dir/JuliaToolkits/BasicIntroduction/IOtest.txt`  # Cmd Type which is within ``; interpolation with $
typeof(cmd)
success(cmd)  # test if the process will process successfully
run(cmd)  # Process the command

## Pipeline like | in bash
cmd1 = `cat $dir/JuliaToolkits/BasicIntroduction/IOtest.txt`
cmd2 = `sort`
run(pipeline(cmd1, cmd2))

## Run in Parallel
cmd1 = `cat $dir/JuliaToolkits/BasicIntroduction/IOtest.txt`
cmd2 = `cat $dir/JuliaToolkits/BasicIntroduction/JuliaBasicIntroduction.jl`
run(cmd1 & cmd2)


###########################################
############### C #################
###########################################
#=
Lk
    - http://docs.julialang.org/en/latest/manual/calling-c-and-fortran-code/
Cc

=#
# pkg
using libc  # ccall
using Cpp
using Clang

# TODO: Learn C and complete this part
Ptr{}
primitive
unsafe_string()



###########################################
############### Python #################
###########################################
# pkg
using PyCall

## import python package
@pyimport math


# run python command
py"10*10"
math.sin(math.pi / 2)
