# Guideline Level
##################################
########## Introduction ##########
##################################
# Key Words: Class, Object, Variables, Basic Operations, Control Structures, Function
#= 
Ref
    - Julia 1.0 dynamic and high performance programming to build fast scientific applications
Ab
    - Every thing in Julia is an experssion which could return values.
    - strongly/dynamically typed, functinoal language.
    - case-sensitive
    - 即时(Just-In-Time，JIT)编译语言 Compile
Pc
=#


#########################################
########## Packages ##########
#########################################
# Lk: Official pkg lists: http://pkg.julialang.org
# Package managers 
## Tips: content within # Package Managers should be executed in Pkg package manager.
Pkg  # shortcut of pkg Mode: ]

## Check installed packages
status 

## initialise the environment
#init  # Create an empty Project.toml file

## pin package to certain version
#pin [pkg_name], [version]

## update and upgrade your packages
#up  # update all packages
#up [pkg_name]  # update the particular package

## add new packages
#add [pkg_name]

Pkg.clone("git@github.com:ericchiang/ANN.jl.git")  # Clone a pkg from GitHub

## delete packages
#rm [pkg_name]  # only remove the reference to package
#gc [pkg_name]  # remove the sources


# Packages
## Personalisation
### OhMyREPL: personalise the REPL
using OhMyREPL:repl  # alias for packages like matplotlib > mpl

## External Languages
### Cxx: use C++ PEPL
using Cxx

## Visualisation
### Lk: Visualisation.jl
using GR, Interact
using PyPlot
using Plots

## Data Management
### JLD2: show the data structure
using JLD2

## Other fields or Common-used
### Printf: Formatting numebers and strings
using Printf


# Modules
## Lk: Modules section in this document
import Statistics

# Scripts
include("test.jl")




#####################################
########## Help and Manual ##########
#####################################
# Help Mode
?  # ternary operator
#Mt: ?[search_problem_string]
apropos("println")  # find all places related/defined/used to "println"

@which esc  # look up the source code where and how a method is defined



# Shell Mode (Use Backspace to return julia prompt)
;


# Annotation
#=
 multi-line annotation
=#


# Write document(md+LaTeX)
"""
    is_even
    # Input
    # Output
    # Function
    ```math
        y = x * 2
    ```
"""
is_even
# Edit document by @doc macro
@doc is_even
@doc "new document" is_even



##########################################################################
########## Julia Commmand line (Read Evaluate Print Loop (REPL) ##########
##########################################################################
# Def
## LLVM just in time (JIT) compiler
code_llvm()  # show the LLVM code of functions
code_native()  # show the assembly code of functions
## REPL
## Julia VM
## garbage collector (GC).
GC.gc()  # release memory
GC.enable(true)  # In certain cases (such as real-time applications), disabling garbage collection(temporarily) with GC.enable(false) can be useful.


# Shortcuts in REPL
#= 
Ctrl+L Clean the screen
Ctrl+R Command History: repl_history.jl file in .julia\logs
Tab*2 autocomplete feature
=#

# Separate commands
var = rand(1)[1]; print(var); var2 = var * 2
# Tips: REPL will wait untill you enter syntactically correct command lines, even for multiline commands


# REPL/Julia VM commands
julia -v  # version
julia -h  # help
isinteractive()  # check whether the REPL is interactive
# julia -e "commands"  # but the string problems are not supported anymore


# Julia Scripts
## Execute jl scripts in REPL
# Mt: julia [file.jl] [arg1] [arg2] [arg3]

## Execute Julia scripts in jl scripts
include("test.jl")


# Benchmark and HPC （https://julialang.org/blog/2013/09/fast-numeric/）
## Runtime
@time sin(pi)
@elapsed sin(pi)  # only return the execution time
@timed sin(pi)  # store results in a tuple

## Allocated memory
@allocated sin(pi)

## Extra librarys
#using BenchmarkingTools
#@profile Lk: https://docs.julialang.org/en/latest/manual/profile/)
#using ProfileView Lk: https://github.com/timholy/ProfileView.jl


# Clean workspace
using InteractiveUtils
workspace()


# Copy and paste
clipboard("Julia")
var = clipboard()
print(var)

# Interact with shell in REPL
## Check and modify the workspace and directory
pwd()  # current workspace
homedir()  # check the home directory
cd("./iToolkit/JuliaToolkits/BasicIntroduction/")  # change directory

## Enter shell mode
;  # Entry
Ctrl+C  # return to Julia REPL

## Cmd Type, Run shell command in REPL
cmd = `cat ./JuliaToolkits/BasicIntroduction/IOtest.txt`  # Cmd Type which is within ``
typeof(cmd)
success(cmd)  # test if the process will process successfully
run(cmd)  # Process the command


###########################################
############### Variables #################
###########################################
# Create and Assign variables
var = 1
中文也是可以的 = 2  ## Unicode and ASCII supported
⌣ = 3  # emoji
ϵ = 2  # LaTeX
print(π+1)  # LLVM constant
1 + 1;
ans  # default variable in REPL for ; input


# Construct a data type
struct iComplex
    real::Float64
    imag::Float64
end

c = iComplex(1.0, 2.0)
c.real
c.imag


# Show/print objects
x = "Julia"
print(x)
println(x)  # print lines
printstyled(x, color=:blue)  # print with particular styles
## Show the variables with macro
@show "Hello world"
@show str1 # @show also shows the variable name
show("Julia")
display("Julia")


# Global/Local scope
## Create a local block
begin
    if x < 1
        x += 1
    end
end

## Compound expression
x = begin
    a = 5
    2 * a
end
x = (a = 5; 2 * a)  # The value of a compound expression is the value of the last sub-expression

## Specify global/local variable in local block
a = 10; b = 15
#while true
    local a = 0  # the local variable
    println(a)
    global a += 1  # indicate global variable a
    local a += 2  # indicate local variable a
    println("global: $(a), local $(a)")
    if a >= a
        println("Finally Done")
    end
#end

## let: Creat a local block binding for a variable
begin
    x = 2
    let
        local x = 1
        println("let block: $x")
    end
    println("begin block: $x")
end


anon = Array{Any}(undef, 10)
for i = 2:2:4
    println(i)
    anon[i] = ()-> println(i)
    i += 1  # although the Anonymous function is defined when i = 2, but however i = 3 when calling the annoymous function as i += 1
end

for i = 2:2:4
    let i = i
        anon[i] = ()-> println(i)  # Assign i in <for block> to i in <let block>, then the change of i in <for block> will not change the i in <let block>
    end
    i += 1
end



###########################################
########## Data Structures/Types ##########
###########################################
#=
Cc
    - Type Annotation
        Mt:The :: operator is, in fact, an assertion that affirms that the value on the left is of the type on the right
    - Automatic type promotion: promote arguments of mathematical operators and assignments to a common type
    - Type hierarchy
        - supertype:
            - Any: The supertype of all types
            - Nothing: No object can have this type, but it is a subtype of every other type. 
        - subtype: belong to the supertype
            - Union{}: The subtype of all types
    - Type unions
    - Parametric types and methods
        - Def:  a parametric <Type>; its elements can be of any arbitrary type T, written as <Type>{T, 1}
        - e.g. Array

Cat
    - data file formats: dlm csv hdf5 formats are supported

    - Abstract types
        - Def: Data types that group multiple subtypes
        - Ab
            - No concrete objects or values of their own
            - Collect subtypes
        - e.g. Signed Number Integer
    - Concrete Types
        - Def: 
        - Ab
            - Instantiate: represent the concrete instance
            - Concrete types have no subtypes
        - e.g Int64  Float64
    
=#

#==========================================
# Data Operations
==========================================#
# Save and Load data
@save "data.jld2"  JLD2.SINGLE_OBJECT_NAME
@load "data.jld2"  JLD2.SINGLE_OBJECT_NAME


# Check the data type
typeof(64)
isa("Julia", String)


# Check the max and min values of a Type
typemin(Int64)
typemax(Int64)


# Type annotation
x = 100
y = 1.0
x::Int64
y::Float64
x + y::Float64


# Type Conversion
Int64(1.0)
Bool(true)

convert(Int64, 1.0)

promote(1, 2.3, 2//5, true, im)  # convert values with different types to a common type (by automatic type promotion system in Julia)
promote_type(Int64, Float64)  # Check the common promotion type of different types
promote_rule(Int64, Float64)  # Check the promotion rules of different types


# Type hierarchy
supertype(Integer)
subtypes(Real)
InteractiveUtils.subtypes(Real)

Real <: Integer  # indicate subtype relationship
Integer <: Real

#==========================================
# Any Type and Nothing Type
==========================================#
x = "julia"
y = 2.3
x::Any
y::Any
nothing::Nothing  # The only value in this type is nothing.



#==========================================
# Bool
==========================================#
## Boolean values
true
false
Int(true)
Int(false)
## Boolean operators
true && false  # and, short-circuit Boolean evaluation
true & false  # and, non-short-circuit Boolean evaluation
true || false  # or, short-circuit Boolean evaluation
true | false  # or, non-short-circuit Boolean evaluation
!true  # not


## Create BitArray
arr_bit = trues(3)
arr_bit[2]


#==========================================
# Numbers
==========================================#
#=
Cc
    - Number: The supertype for all values of numeric types
    - 
=#
# Special precedence


# Arbitary-Precision Numbers
big(2.09)

# Binary representation
bitstring(3)


# Type: Number
Number(2.3)


# Infinity and NaN
Inf
-Inf
NaN  # not a number / not exist
0 / 0
Inf - Inf


# Int8-128
Integer(2)  # supertype of all signed int
Int(2)  # Int64 or Int32
Int64(0)
UInt64(2.0)  # Unsigned variants
BigInt(2.0)  # Arbitary-Precision Integers


# Float16-64
Float64(1.2e2)  # double precision
Float32(1.2f2)  # single precision
BigFloat(2.0)  # Arbitary-Precision Floats


# Rational and Complex numbers (Parametric Type)
Real(1.2)

Rational(1/2)
1//2
numerator(2//3)f
denominator(2//3)

Complex(1,2)
1 + 2im  # im means square root of -1 (i.e. i in maths)
real(1 + 2im)
imag(1 + 2im)
abs(1 + 2im)


# Basic Operators
var = -1
1 + 1
var += 1
1 - 1
var -= 1

1 * 1
1 / 1
div(10, 3)
rem(10, 3)

2 ^ 2
sqrt(2^2)

1 == 1
2 >= 1
2 <= 1

abs(-1)
round(2.6)
round(2.5)
ceil(2.3)
floor(2.6)


# Bitwise Operators



#==========================================
# String and Character
==========================================#
## Char
'c'
Int('a')  # Unicode code point
Char(97)  # Convert from Unicode code point
'\u4900'  # Use unicode in Char

isvalid(Char,0x3b1)

## String
str2 = "I am the α: the beginning"
str2[11]  # BoundError
str2[10:12]
"$str2 now"  # Call var in string

### Formatting Numbers and Strings
str2 = "content"
print("\"$(str2[2:5])\"")  # also workable for other functions or methods
@printf("insert str: %s \n", str2)
result = @sprintf("insert str: %s \n", str2)
@printf("insert str: %c \n", 'a')
@printf("%50s\n", "text right justified!")
@printf("%0s\n", "text left justified!")
@printf("a pointer: %p\n", 1e10)

num2 = 1e2
@printf("insert data: %d \n", num2)
@printf("insert rounded data: %.2f, %0.3f \n", pi, pi)
@printf("insert scientific format data: %.3e \n", num2)

## Escape Char
print("\t Julia \n Juno")

## Case switch
lowercase("ABSc")
uppercase("absC")

## Split
split("a, b, c, d", ", ")  # return a vector

## Replace
replace("testBlueRedYellow", "Blue" => "Red")

## Append
join(["dsf", "method"])

## Strip
strip("   remove redundant head and tail  ")
lstrip("   remove redundant head, not tail  ")
rstrip("   remove redundant tail, not head  ")


#==========================================
# Regular experssion(Regex)
==========================================#
#= 
- Ref-Perl syntax: http://www.regular-expressions.info/reference.html
- Pcp-Order: From right/end to left/start
=#
input = "john@.com; test@.com; "

visa = r"^(?:4[0-9]{12}(?:[0-9]{3})?)$"
pattern = r".+@.com;.+"
pattern_user = r"(.+)@.com;(.+)"

occursin(pattern, input)  # test whether the given regex matches the string

## RegexMatch object
match(pattern, input)  # return RegexMatch object
if occursin(pattern, input)
    println("------------ Match Test -------------")
    m = match(pattern, input)
    m_user = match(pattern_user, input)
    println(m.match)  # contains the entire substring that matches
    println(m.offset)  # show what position the matching begins
    println(m.offsets)  # show all beginning positions of the matchings
    println(m_user.captures)  # contains the captured substrings as a tuple
end

m = match(r"(ju|l)(i)?(a)", "Julia")
println(m.match) #> "lia"
println(m.captures) #> l - i - a
println(m.offset) #> 3
println(m.offsets)

## Search and Replace
pattern = r"[\w]{3,}"  # Substrings with 3-5 Characters
results = collect(m.match for m = eachmatch(pattern, "jd, sdfedfdfdf, abc"))  # eachmatch returns iterator(Type iter)
show(results)
replace("Julia", r"u[\w]*i" => "[replaced]")


#=========================================
Multi-Dimension Matrices / Collection Type
==========================================#
#=
Lk
    - Collection module in Julia
Cc
    - Collection Type:

=#


#==========================================#
# Range（1D）
range1 = 1:10  # UnitRange
range2 = 0:2:20  # StepRange: start:step:stop
range2 = range(0, 20, 10)  # start, end, length
eqa = range(0, step=10, length=5)
show(range2)

## Inspect
length(range(0, 20))
lastindex(range(0, 20))

## Select
ls[1]
ls[end]

## Slice
ls[1:3]
ls[1:end]

## Replace
arr[2:4] = [8,9,10]

## Split


#==========================================#
# Array(2-inf dims)
#=
Ab: 
    - Homogeneous
    - Ordered
-=#
## Vector(1D array)
vector = [1, 2, 3]
typeof(vector)
vector_fp = [i for i in 2:2:10]
"Julia"[1]  # String is an array of Char
zeros(2)
ones(2,2)
rand(Int64, 5)
repeat([1, 2, 3], inner = [2])

## Create Arrays(2D array)
emp_arr2 = []  # Empty array
jarr = Array{Int64, 2}[]  # Create an empty array with particular data type( with data annoation) and dimensions of vector/matrix

arr1 = Array{Int64}(undef, 5)  # Type generic, number, dimension
arr2 = Array{Int64}(undef, 5)
arr3 = Int64[100, 25, 232]  # Specify the type of array
arr4 = Any[100, 25, "ABC"]  # Default Any Type
arr5 = collect(1:3)  # Transfer from range

sizehint!(arr5, 10^5)  # indicate the final number of elements from the start for the performance

multi_1 = fill(1, (3,4,5))  # dimension 3*4*5
multi_arr = [(1, 2, 3), (1, 2, 3)]  # Create the vector with row vectors

dict_arr_pair = ["key1" => 12, "key2" => 2]  # Key-value pairs vector/array

## Check basic info of Array
eltype(arr2)  # Check the element type of array
length(arr2)  # Check the number of elements
ndims(arr2)  # Check the number of Dimensions of Array
size(arr2, 1)  # Check the number of elements in x dimension (usable for Matrix)
isempty((arr2))

## Append/Remove elements
append!(arr1, arr2)  # combine arrays method
push!(arr2, 42)  # add elements to the end
push!(jarr, [1 2; 3 4])  # jagged array
jarr2 = [[1, 2], [3, 4]]  # jagged array
pushfirst!(arr2, 12)  # add elements to the first element
pop!(arr2)  # remove and return the last element
popfirst!(arr2)  # remove and return the first element
splice!(b,8)  # remove an element at a certain index
join("123", "head")  # splice operators
join(["Ju", "Lia"])  # Combine elements

## Check wheter values is within arrays
in(42, arr2)

## Sort
sort(arr2)  # but not change the original array
sort!(arr2)  # change the original array

## Replace values in elements
arr2[2] = 5

## Copy arrays/matrices
arr_copy = arr2  # Direct assign with the same value pointer in memory
arr2[2] = 3
show(arr_copy)

arr_copy1 = copy(arr2)  # shallow copy of a matrix which refers to the original matrix
arr_copy2 = deepcopy(arr2)  # especially for recursive copy
arr2[2] = 3
show(arr_copy1)  # The value of shallow copy will change when corresponding elements in the original matrix is changed
show(arr_copy2)

## Unique Array
union([1, 2, 3, 3])

## Vector(1D Matrix)
vec = [1 2 3 4]
typeof(vec)
size(vec)

## Find elements



## Matrices(2D)
### Tips: a matrix is an alias for the two-dimensional case of Array
### Create Matrices
mat = [1.1 1.2 1.3; 2.1 2.2 2.3; 3.1 3.2 3.3] # spaces separate the elements within a dimension
mat_rand1 = rand(3, 5)  # random matrix with values between 0 and 1
mat_rand2 = rand(3, 5)

### Basic info of matrices
size(mat_rand)  # Check the row and column numbers of a matrix
size(mat_rand, 1)  # Check the number of row
size(mat_rand, 2)  # Check the number of column
size(mat_rand, 3)  # Check the number of elements in x dimensions
ndims(mat_rand)  # Check the dimension of matrix

### Slicing operations
mat[1, 2]  # Single value in particular row and column
mat[2:end, 3]  # from the 2nd elements in the 3rd column
mat[:, 3]  # the whole 3rd column
mat[3, 2:end]  # from the 2nd elements in the 3rd row
mat[3, :]  # the whole 3rd row
mat[2:3, 2:3]
mat[2:end, 2:end]
mat[:]  # in column order

### Concatenate
hcat(arr1, arr2)  # horizontal
vcat(arr1, arr2)  # vertical
cat(mat_rand1, mat_rand2; dims = 1)  # Concatenate matrices return a matrix with arbitary dims (SP.e.g. dims = 1: vcat, dims = 2: hcat)
mat_hcat = [mat_rand1 mat_rand2]
mat_vcat = [mat_rand1; mat_rand2]

### Reshape the dimensions of a matrix
#### Tips: first col then row
reshape(1:12, 3, 4)
reshape(1:12, (3, 4))
reshape(mat_rand1, 5, 3)

### Assign values to matrix
#### Tips: select particular elements with slicing methods
mat_modified = mat
mat_modified[1, 2] = 2.2
mat_modified[2, :] .= 0  # dot notation and = operation
mat_modified[2:3, 2:3] = mat_modified[1:2, 1:2]

### List comprehension and implicit loop
arr2 = Float64[x^2 for x in 1:4]
matrix_fp0 = [i for i in 1:10, j in 1:10]
matrix_fp1 = [(i,j) for i in 1:10, j in 1:10]
matrix_fp2 = Float64[(i,j) for i in 1:10, j in 1:10]  # annouce the data type


#==========================================#
# Set
#=
Def
    - 
    - btw, Set in Julia is more similar to the Set concept in Set theory
Ab
    - Heterogeneous
    - Unique values
    - Not ordered
=#
# Create sets
set = Set{Any}([1, 2, 3])
set_duplicate = Set([1, 2, 3, 3])  # Duplicates will be eliminated
set_empty = Set()
set_arr = Set([[1, 2, 3], [4, 5, 6]])


# Check if a value is in a set
in("test", set1)
2 in set1


# Add elements to a set
push!(set1, "test")



# Remove elements in a set



#==========================================#
# Tuple
#=
Def: Fixed-sized group of values, separated by commas
F
    - The tuple can store the multiple values to return as a single variable in a function 
    - Argument list of function is a tuple
Ab 
    - Heterogeneous
    - Immutable
    - Order
=#
# Create tuples
tup = (1, 2.0, "3", [4])
typeof(tup)  # check the type annotations of the elements

tup_empty = ()  # empty tuple
tup_type = (1, 2.0, "3", [4])::Tuple{Int64, Float64, String, Array}  # Specify the data type of elements (another kind of type annotation)
tup_type = tup::Tuple{Int64, Float64, String, Array}
tup_one = (1, )

# Slice tuples
tup[1]
tup[end]
tup[2:3]

# Unpack values from tuples
var_1, var_2 = tup
var_1
var_2


#==========================================#
# Dictionaries(Dict)
#=
Def
    - a collection of two-element tuples of the form (key, value)
Ab
    - Hash
    - Heterogeneous
    - Unique key with possible duplicate values
    - Not ordered
    - Mutable
=#
# Create Dictionaries
dict = Dict("key1" => 1, 3.2 => 2)
dict_type = Dict{String, Int64}("key1" => 12, "key2" => 2)  # Specify the data types of the keys and values
dict_key_charOrString = Dict{Symbol, Int64}(:key1 => 12, :key2 => 2)
dict_empty = Dict()
dict_empty_with_types = Dict{Symbol, Int64}()

dict_from_pairedTup = Dict([("key1", 1), (3.2, 2)])
dict_from_pairedArrs = Dict(zip(["key1", 3.2], [1, 2]))


# Basic info of a Dict
## length
length(dict)

## Show all the keys and values
keys(dict)
values(dict)

collect(values(dict))  # transfer the object to vector/array
[key for (key, value) in dict]  # use list comprehension to create arrays with keys or values
[value for (key, value) in dict]


# Extract the values with particular key
dict["key1"]
dict[3.2]
dict_key_charOrString[:key1]

get(dict, "key1", "Oops, key not found")  # return when the key is not found
get(dict, "keyerror_catcher", "Oops, key not found")

# Search keys/values in dictionarys
## Test if a key is present in Dict
haskey(dict, "key1")

if "key1" in keys(dict)
    print(" Value of key1: $(dict["key1"])")
else
    print("key1 is not a key in dict.")
end

# Add/Change key-value pairs
dict["key1"] = 2  # The data type of the values should be identical
dict["key2"] = 23  # Add new key-value pairs at head


# Remove key-value pairs
delete!(dict, "key2")



#=========================================
# Composite Types
==========================================#
#=
Pcp
    - A type once defined cannot be changed
    - an immutable type contains a mutable field such as an array, the contents of that field can be changed
Ab
    - In julia, func(obj, args) rather than object.func(args)
    - No classes since functions/methods belong to corresponding types
Lk
    - Julia built-in type hierarchy: type_hierarchy.jl
=#

# Type of Data Type
DataType()



# Define a data type (Composite type)
## Concrete Types
mutable struct typeName  # mutable means that objects of the type can be modified
    x::Float64
    y::Float64
    z::Float64
end

struct typeName2  # Default, immutable objects, enhance performance
    x::Float64
    y::Float64
    z::Float64
end

typeof(typeName)

### Default implicit constructors of object
nt = typeName(1, 2.2, 3)  # Create the object of the data type
nt = typeName(1.2::Float64, 2.2::Float64, 3.3::Float64)
nt2 = typeName2(1, 2, 3)
typeof(nt)

## Define outer constructors
typeName(x, y) = typeName(x, y, 0.0)
typeName(1, 2)

## Define inner constructors
### Tips: when you use inner constructors, there are no default constructors any more.
mutable struct inner_typeName
    x::Float64
    y::Float64
    z::Float64
    # inner constructors
    inner_typeName(x::Int64) = new(Float64(x), x*2, x*3)  # Basic inner constructor
    inner_typeName(x::T, y::T, z::T) where T <: Integer = new(x, y*20, z*30)  # innter constructor with parametric types
end

inner_typeName(2)
inner_typeName(2, 3, 4)
# inner_typeName(1, 2.4, 3.5), but if only one Int64 will cause errors

## Create collections of user-defined types
arr_nt = typeName[]
push!(arr_nt, nt)

## Define abstract Types
abstract type abstractTypeName
end

## Define type hierarchy relationship
mutable struct typeName3 <: abstractTypeName
    x::Float64
    y::Float64
    z::Float64
    t::String
end

nt3 = typeName3(3, 2, 1, "Axis")
### Multiple dispatch example
function multiple_dispatch_example(coordinate::typeName3, coordinate2::abstractTypeName)
    print("$(coordinate.x) $(coordinate2.t)")
end

multiple_dispatch_example(nt3, nt3)  # Since typeName3 is a subtype of abstractTypeName, nt3 can be utilised in this function. (typeName is also a natural subtype of typeName3)


# Check the fields of a type
fieldnames(typeName)
nt.y  # Check the state of the field
nt.y = 4  # Change the object values with field names
nt2.y = 2  # immutable


# Check methods/Constructors of a type
methods()  # Check all methods
methods(typeName)  # default implicit constructors
@which()

# Alias
aliasTypeName = typeName
aliasTypeName(1, 2, 3)


# Identical object: whether point to the same memory location
nt3 = typeName(1, 2.2, 3)
isequal(nt3, nt)  # mutable
isequal(nt2, nt2)# immutable 


# Type Union
unionTypeName = Union{typeName3, typeName}
isa(nt, unionTypeName)
isa(nt3, unionTypeName)


# Restrict the parameter type
mutable struct para_typeName{T <: Real}  # Arbitrary type parameter T, take elements with different types(Int64, Float64) within Real Type
    x::T
    y::T
    z::T
end

int_nt = para_typeName(1, 2, 3)
float_nt = para_typeName(1.1, 2.2, 3.3)

## Function and methods (also use parametric type)
add(x::T, y::T) where T <: Number = x + y



#==========================================#
# Data Frame
## Lk: http://juliadata.github.io/DataFrames.jl/latest/index.html.
## pkgs
using DataFrames
using DataArrays
using RDatasets  # data sets used in the R literature
using Missings  # represent non-exisiting value with Missing types (with value missing)
using CSV  # read csv file and store in dataframes

## constructing a DataFrame:
df = DataFrame()
df[:Col1] = 1:4
df[:Col2] = [exp(1), pi, sqrt(2), 42]
df[:Col3] = [true, false, true, false]
show(df)

df = DataFrame(Col1 = 1:4, Col2 = [e, pi, sqrt(2), 42],
Col3 = [true, false, true, false])

data = CSV.read(fname, delim = ';')
CSV.write("dataframe1.csv", df, delim = ';')
df = readtable()  # read from gzip

## Inspect DataFrame
describe(df)



#=========================================
# Modules
==========================================#
#=
Cc
    - Modules: librays/Julia packages will be contained in a module
        - SP. e.g.
            - Main Module: the start module of your working environment/current top-level module
            - Core Module: non-Julia sources used by Julia source to interface to the OS through APIs
Space
    - LOAD_PATH: the list of dirs where julia looks for module files
        - ~/.julia/config/startup.jl 
        - Mt-Add new path: push!(LOAD_PATH, "new/path/to/search")
=#
# Type of Modules
typeof(Base)


# Create a module
module ModuleName  # Convention: start with Uppercase letter
    # declare/export internal definition(types/functions/etc.) to make them visible for other modules
    export TypeName, plusTwice
    
    # inclue scripts
    ##include("test.jl")

    # Types in module
    mutable struct typeName
        x::Float64
        y::Float64
        z::Float64
    end

    # Functions in module
    function plusTwice(n::Int64, m::Int64 = 0)  
        n + m + m
    end

    # __init__(): execute when the module is first loaded
    function __init__()
        print("\nWelcome to Modules in Julia!\n")
    end
end

# import a module
## Tips: imported variables are read-only
import.Base.Statistics  # import librarys within module
import.ModuleName  # import all definitions from ModuleName
import ModuleName.plusTwice  # only import the function
# importall  # import all the exported names

# Use the function in particular module
Statistics.eval(2)
ModuleName.plusTwice(2, 3)

# Check the info of modules
names(Main)  # named types/fucntions/modules in Main Module
varinfo()  # lists these objects with their types in Main Module
varinfo(ModuleName)


###################################
########## Control Flows ##########
###################################
#=
Def:
- Conditioanl evalution: pieces of code are evaluated, depending on whether a Boolean expression is either true or false 
- Repeated evaluation:
- Exception handling and Scope revisited
=#

#==========================================
# Conditional evaluation
==========================================#
# if
if 1 > 2
    print("1 > 2")
elseif 3 > 2 > 1  # comparsion can be compounded
    print("2 > 1")
else
    print("2 = 1")
end

#==========================================#
# expr and ternary operators
a = 12
a > 1 ? a -= 1 : a += 1  # condition ? true_branch : false_branch 
a > 10 ? print("larger") : a <10 ? print("Smaller") : print("Equal")  # chained ternary operators
output = a > 10 ? "larger" : a <10 ? "Smaller" : "Equal"  # the results can be assigend to var
print(output)

#==========================================#
# Short-circuit evaluation
a >= 10 || error("n must be non-negative")  # a || b, b is not evaluated when a is true (since || is already true)
a == 10 && return 0  # a && b, b is not evaluated when a is false (since && is already false)


#==========================================
# Loop / Repeated Evaluation
==========================================#
#=
Tips: All collection type could be looped
=#

# for loop
## Variables
ls = rand(5)
num = 0
sum = 0
arr = [x^2 for x in 1:10]

## Local block
for num in ls  # Local variables
    sum += num
end
print(num)

## Multiple/Nested
for n in 1:10, m in 1:10  # nested for loop, each n with all m
    println(n * m)
end

## Indices
for i in 1:length(arr)
    println("$i-th, $(arr[i])")
end
for (i, val) in enumerate(arr)  # return both index and value in a tuple
    println("$i-th, $val")
end

## Range loop
for n = 0:9  # one liner for loop of numeric range
    n += 1
    println(n)
end

## Dictionary loop
for (key, value) in dict  # collect key-value pairs as tuple
    print("Key: $key, Value: $value\n")
end

for pair in dict
    print("Key: $(pair[1]), Value: $(pair[2])\n")
end

#==========================================#
# foreach
foreach(i -> println(i^2), 1:5)

#==========================================#
# while loop
## while
i = 0
while i <= 10 
    i += 1
    println(i)
end

## do while
i = 0
while true
    i += 1
    println(i)
    i <= 10 || break
end

#==========================================#
# Loop Control
# Break
for (i, val) in enumerate(arr)
    println("$i-th, $val")
    if i == 5
        break  # exit the upper loop
    end
end

# Continue
for (i, val) in enumerate(arr)
    println("$i-th, $val")
    if i == 1/2*(length(arr))
        println("Middle")
        continue  # pause this loop and continue the next loop
    end
end


#==========================================#
# Exception handling
## Data Type: Exception
### Error message
error("Warning: reminder")  # stop the program with ErrorException function and return the ERROR: error message


## Throw exception and rethrow
a = 20
if a in 1:10
    println("This is an acceptable code")
else
    throw(DomainError())
end

rethrow()

## Try
a = []
try
    pop!(a)
catch ex  # variable ex contains the exception from try block
    println(typeof(ex))
    showerror(stdout, ex)
end

t = try  # try is an expansion that can be assigned
        pop!(a)
    catch ex  # variable ex contains the exception from try block
        println(typeof(ex))
        showerror(stdout, ex)
        a = [1, 2]
    end
t

try
    global f = open("file1.txt") # returns an IOStream(<file file1.txt>)
    # operate on file f
catch ex
finally  # whether there is a error, this command block will be executed
    close(f)
end



###############################
########## Functions ##########
###############################
#=
Cc
=#
# Create a function
## Standard Method
function sumUP(vec)
    sum = 0
    for num in vec
        sum += num
    end
    return sum  # In general, the value of the last expression in the function is returned
end

## Compact one-line function syntax
is_even(x::Int) = x % 2 == 0  # var::Int is a type annotation

## Operators
typeof(+)
+(x, y)

## Arguments
### Type of arguments
function mult(x::Int, y::Int)  # positional arguments with type annotations
    println("x is $x and y is $y")
    if x == 1
    return y  # when return values the function will be ended.
    end
    x * y
end
result = mult(2, 3)

### multiple arguments
function moreargs(n, m, args...)  # ... ellipsis/splat operators to take a number of arguments as a tuple arguments
    println("arguments : $n $m $args")
end
moreargs(1, 2, 3, 4, [5, 6], (7, 8))

### Optional positional arguments
function plus(n::Int64, m::Int64 = 0)  
    n + m
end
plus(2)
plus(2, 2)
plus(2, 5)

### Optional keyword arguments
function mp(x, y; z, t) # After semi-colon, z and t are optinal keyword arguments
    x - y + z -t
end
mp(2, 5, z = 2, t = 3)

### Use functions as arguments
function twice_mp(f, x, y, z, t)
    result = f(x, y, z = z, t = t) * f(t, z, z = y, t = x)
    return result
end
twice_mp(mp, 2, 5, 2, 3)

# Returns
## Return values
function single(n::Int, m::Int)
    n + m
end
x = single(8, 2)  # Assign the values to vars

function multi(n::Int, m::Int)
    n*m, div(n,m), n%m  # return multiple values in a tuple
end
x, y, z = multi(8, 2)  # Assign the values to vars

## Return Functions
function refunc(f)
    return function (x, y, z, t)
        f(x, y, z = z, t = t) ^ 2
    end  
end
mp2 = refunc(mp)
mp2(2, 5, 2, 3)

function multi_refunc()
    return (x) -> x + 1, (x) -> x + 2
end
plus1, plus2 = multi_refunc()
(plus1, plus2) = multi_refunc()
plus1(2)
plus2(1)


# Assign function to a variable
func = sumUP
func(1:10)


# Anonymous functions
(x, y) -> x^2 + y^2  # lambda expression and stab character(->)
() -> print("Hello")
circle = (x, y) -> x^2 + y^2  # Name the Anonymous functions

function (x, y)
    x^2 + y^2
end

ans(2, 2)  # only once
circle(2, 3)


# First-class functions


# Closures
## Tips: functions closed to variables in local function groups
function closures()
    x = 0  # the varibale in the closures
    return () -> x = x + 1, () -> x = 0
end
closure1, closure2 = closures()
closure1()
closure1()
closure2()


# Curring function
## Tips: a func with multiple args -> a sequence of functions each with a single arg
function add(x)
    return function f(y)
        return x + y
    end
end

fib(n) = n < 2 ? n : fib(n-1) + fib(n-2)  # ternary operator + function: Calculate the nth Fibonacci number


# Recursive functions
function add_recursive_func(x)
    y = x*2
    function add2(x, y)
        x + y
    end
    add2(x, y)
end

add_recursive_func(2)


# Inspect a function internally
code_lowered(+, (Int, Int))
code_typed(+, (Int, Int))  # type-inferred form


# Generic functions <-> Methods + Multiple dispatch Mechaism
#= Def
- Generic functions: If we define a function without type annotations, it is generic; the Julia JIT compiler is ready to generate versions called methods for different argument types when needed.
- Method: a concrete version of a function for a specific combination of argument types
    - Space: Methods are stored in virtual method table (vtable) for a function
    - Mt-Multiple dispatch mechanism: when functions are called, Julia will look up in the vtable to find the methods that match the argument types
    - Tips: Note that only positional arguments are taken into account for multiple dispatch, and not keyword arguments.
    - Lk: Page 58 0 Julia 1.0 Programming Dynamic and high-performance programming to build fast scientific applications by Ivo Balbaert (z-lib.org)
- Overloading: Define a new method for the same function with different argument types
=#
# Help info of methods
methodswith(Array)  # show the methods defined for a certain type
@which(2 * 2)  # return the exact method and where in the source code that the method is defined
methods(methods)  # Show all methods of a function

# Create methods for Functions (Methods belong to functions, multiple dispatch)
foo(x::Int64, y::Float64) = x * y
foo(x::Char, y::String) = join([x, y])
foo(x, y::String) = println("$y is coming")
foo(1, 2.0)
foo('x', "test")
foo(12, "Winter")

# Use parametric type (T in this ) to specify the data type of function parameters
add(x::T, y::T) where T <: Number = x + y  # code before = is the parametric type definition and code after = is the function definition.
function para_float_plus(x::T, y::T) where T <: AbstractFloat  # Another explansion of the function
    x + y
end

para_float_plus(1.1, 2.2)  # only subtypes of AbstractFloat can be used as parameter as the parametric type defined.



############################################
########## Functional Programming ##########
############################################
# Broadcasting, Element-wise operators
## Tips: A function can be broadcast over all elements of an array (or matrix) by using the dot notation
arr1 = collect(1:3)
arr2 = collect(11:13)

arr2 .+ 2
arr2 .* 5  # acutally . is not needed for *
arr2 * 5
arr .* arr2

sin.(arr1)
broadcast_two_args(x, y) = x + y
broadcast_two_args.(10, arr1)  # apply x to each elements in y


# Map, filter
## Map
map(sin, arr1)
map(+, arr1, arr2)

map((x) -> x + 1, arr1)  # apply tempory Anonymous functions to map elements

map(arr1) do x  # create a function with argument x and pass it to map as 1st arguments
    if x > 1 return 1
    elseif x <= 1 return 0
    else return -1
    end
end

mapreduce((x) -> x * 2, +, 1:10) 

## Filter
filter(x -> x < 2, arr1)  # apply boolean functions to each elements in arrays and return values evaluted to be true


# List Comprehension
i = 0
arr_lc = []
for i = 1:10
    append!(arr_lc, i)
end

i = 0
arr_lc = [i for i = 1:10 ]  # list comprehension will also create local block like for loop



###########################################
########## Metaprogramming ##########
###########################################
#=
Cc
    - expression: an object that represents Julia code. 
        St
            - Nested expression
        Cpn
            - Symbol: operators, indicate access to variable
            - Literal: values, variable
    - homoiconicity:
        - Lk: Lisp, FP
    - abstract syntax tree (AST): a tree representation of the abstract syntactic structure of source code
        - Node: simple data structure of the type expression Expr
        - Lk: http:/​/​en.​wikipedia.​org/​wiki/​Abstract_​syntax_​tree.
    
Pcp
    - code is data, data is code
=#

#==========================================
# Syntax Tree
==========================================#
# Expressions and symbols
## TODO: check the info of generated function @generated
## Quote operators: block the code for expression evaluation (not evaluated unless eval)
expr = :(2 + 3; 4 + 5)  # treat the arguments as data, not as code
:expr  # symbol prevent evaluation of its content
typeof(expr)
typeof(:expr)

## Assign an expression
a = b = c = 3
expr1 = :(2 + a * b - c)

## Build expression from constructor
expr_Cpn = Expr(:call, -, Expr(:call, +, 2, Expr(:call, *, a, b)), c)
expr1 == expr_Cpn

## Standard quote expression
quote
    2 + 3
    4 + 5
end

## Show the expression syntax tree
expr1.head  # indicate the kind of expression
expr1.args  # shows the elements in AST
dump(expr1)  # show the syntax tree

## Evaluate the expressions
eval(expr1) == eval(expr_Cpn)
print(eval(expr1))

@show 2 + 3 # show the expression, processes and results

# Expression interpolation(evaluated when constructed)
"$expr1 is eight"  # use $ operator to interpolate expression
expr_var = :(a + 4)
expr_num = :($a + 4)
show(expr_var)
show(expr_num)


#==========================================
# Macros
==========================================#
#=
Cc
    - Macro: a custom program tranformation takes the input expressions and returns the modified expressions at parse time
        - inject code into the namespace
    - Function: takes the input values and returns the computed values at runtime
    - Domain Specific language(DSL)
Cpn-args
    - Expression:
    - Hygienic macro: 
Pcp
    - Declare the variables used in the macro as local, so as not to conflict with the outer variables
    - Use the escape function esc to make sure that an interpolated expression is not expanded, but instead is used literally
    - Don't call eval inside a macro (because it is likely that the variables you are evaluating don't even exist at that point)
=#
# Extra libraries
#using MacroTools


# Defining macros
macro macro_name(expr)
    quote
        if $expr == nothing
            println("ERROR: Nothing")
        else
            local t0 = time()
            local result = $(esc(expr))
            local t1 = time()
            print("elapsed time in seconds: ")
            @printf "%.3f" t1 - t0
            result
        end
    end
end

@macro_name :(2 + a * b - c)

# Expand macro expression
macroexpand(Main, :(@macro_name :(2 + a * b - c))) 

# Built-in macros
## libraries
using Test
@assert 1 == 3
@test 1 == 3


# Reflection capabilities




###########################################
################# I/O #####################
###########################################
#= 
Cc
    - I/O
    - TTY type: Teletype
        - stdin
        - stdout
Ab
    - Stream-oriented
=#
# pkg
using libuv
#==========================================
# System
=========================================#
# Check operating system KERNEL 
Sys.KERNEL  

Sys.iswindows()
Sys.isunix()
Sys.islinux()
Sys.isosx()

# 

#==========================================
# IO Types
=========================================#
# Type of IO
fieldnames(IO)
fieldnames(IOStream)

IO.types
IOStream.types


#==========================================
# Input
=========================================#
# End of stream
eof(stream)


# input stream
stdin
eachline(stdin)


# Open and read line-oriented files
dir_file = "./JuliaToolkits/BasicIntroduction/IOtest.txt"
dir = "./JuliaToolkits/BasicIntroduction"
file1 = open(dir_file)
open(dir_file, "w")  # open files with permissions

close(file1)

## open(func dir_file)
open(dir_file) do file
    # open and apply a function process to a file
    while(!eof(file))
        a = readline(file)
    end
end  # implicitly close(file)



# Read files
## Read one time
read(stdin, Char)  # read from standard input
read(dir_file, String)  # Specify the type of the content
read(dir_file, Char)  # only the first character

## Read all inputs until newline character \n
readline(dir_file)  # first line, Vector
readlines(dir_file)  # all lines, Vector

## Read all inputs into one string
readall(dir_file)

## Read all files in directory
readdir(dir)

for file in readdir(dir)
    # process file
end

## Read bytes
read(stdin, 3)  # read three bytes
readbytes()  # read a number of bytes and store in a vector

## Read data with data frames
### Common and Other method Lk: Type > DataFrame


#==========================================
# Output
=========================================#
# Output Stream
stdin


# Output one line in stdout
write(stdout, "Standard Output")  # the added number is the number of bytes in the output stream
write(stdout, "Standard Output");  # with ;, the bytes number will not be showed.


# Write the opened files
f1 = open(dir_file, "w")  # overwrite
write(f1, "overwrite") 
close(f1)

f1 = open(dir_file, "a")  # append
write(f1, "append\n") 
close(f1)



#==========================================
# TCP/IP, Network I/O
=========================================#
#=
Cc
    - Transmission Control Protocol / Internet Protocol (TCP/IP)

=#
# pkgs
using Sockets

# Julia TCP server
#=
server = Sockets.listen(8080)  # port 8080

while true
    conn = accept(server) @async begin
    try
        while true
            line = readline(conn)
            println(line) # output in server console
            write(conn,line)
        end
        catch ex
            print("connection ended with error $ex")
        end
    end # end coroutine block
end

# use netcat to access TCP server
nc localhost 8080
=#


#==========================================
# Datafiles and Databases
=========================================#
#=
Lk
    - Suppport DB: Memcache, FoundationDB, MongoDB, Redis, MySQL, SQLite, and PostgreSQL
    - https://github.com/JuliaDatabases
Cc
    - Open Database Connectivity(ODBC): a low-level protocol for establishing connections with the majority of databases and datasources 
    - Data Source Name (DSN)
        -  IODBC or Unix ODBC
=#
# csv files (Comma-separated files)
## Read
using DelimitedFiles
dataset = DelimitedFiles.readdlm([csv_file], ",", Float64, header = true)  # or other delimiter characters

## Write
writecsv
writedlm("datafilename.dat", data, ',')

### JSON files
using JSON

### XML and YAML
using LightXML
using YAML

### hdf5
using HDF5

### INI (Windows)
using INIFile

# ODBC
using ODBC
ODBC.DSN(database_name,user,password)
ODBC.disconnect!(dsn)
ODBC.query(dsn, "select * from titles")




##########################################################
## Tasks Management / Coroutines / Parallel operations ###
##########################################################
#= 
Cc
- Channel: 
- coroutines
=#

# Type Channel
function fib_producer(c::Channel)
    a, b = (0, 1)
    for i = 1:10
        put!(c, b)  # generate var b values into a channel c
        a, b = (b, a + b)
    end
end

## Create Channels
chnl = Channel(fib_producer)  # Create/Name the channel

## Create Tasks and asynchronous threads
chnl = @task fib_producer(c::Channel)  # @task macro: Create a task binded with channel (not executed)

fac(i::Integer) = (i > 1) ? i*fac(i - 1) : 1
c = Channel(0)  # create the channel with initial value 0
task = @async foreach(i -> put!(c,fac(i)), 1:5)  # @async start the task asynchronously # for each value in 1:5, it will be puted in the function fac and return to channel c by put!
bind(c,task)  # bind/assign task to the c channel
for i in c
    @show i  # show the values in channel c respectively
end


# Return multiple times, once for each take! call
take!(chnl)  # take the value from the channel chnl in Functions
for n in chnl
    println(n)
end


# Reflection
#=
Cc
    - Reflection: a running program can dynamically discover its own properties
=# 

@isdefined var  # check whether a variable is defined or not


# Parallel Computing
## Lk
#ClusterManager type (see http://docs.julialang.org/en/latest/manual/parallel-computing/)

## pkgs
using Distributed

## Parallel
#julia -p n program_name # starts REPL with n workers

@parallel
@distributed

## Check the parallel info
workers()  # check working processsors
nprocs()  # The number of available workers

myid()  # Each worker can get its own process ID
addprocs()  # add new processors
rmprocs()  # remove processors

## Distribution
#julia --machine-file machines_list script.jl  # machines is a file that contains the names of the computers you want to engage


## low level communication
rc = remotecall()  # give certain worker a function with arguments
fetch(rc)  # fetch the result/return from rc

remotecall_fetch()

@spawn
@spawnat

@everywhere

## parallel map: map-reduce problems
pmap()


###########################################
########## Others ##########
###########################################
# Date and Time
time()  # the number of seconds since epoch

using Dates
date = Date(2020,4,8)  # Create date objects
date_time = DateTime(2014,9,1,12,30,59,1)  # Create DateTime object
year(date)  # subtract the year in date object
month(date)  # subtract the month info in date object 
week(date)  # the number of week before the date in this year
day(date)  # subtract the date in the date object
dayofweek(date)  # check the day of week
daysinmonth(date)  # check the numebr of days in this month
dayofyear(date)  # check the numebr of days before date object in this year
isleapyear(date)  # check whether this year is leap year



###########################################
########## Performance Tips ##########
###########################################
#= 
Lk
    - http://docs.julialang.org/en/latest/manual/performance-tips/
Pcp
- Refrain from using global variables
- Use main() function to structure your code.
- Local and global should be relatively independent
- Type Stability
- Data size
- use named functions instead of annoymous fucntions
- sensible function volume
- use type annotation instead of type test within function or splat operator(...) for key arguments
- Use the macro @inbounds so that no array bounds checking occurs in expressions
- 
=#
