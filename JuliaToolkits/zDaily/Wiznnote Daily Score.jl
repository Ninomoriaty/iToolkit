###########################################
########## Wiznote Daily Score Calculator ##########
###########################################
# Introduction
#=
F: Calculate the Score Column in the Wiznote
=#

# Input scores from the Wiznote Score Column
float_sum_score = sum(map((x) -> x != "" ? parse(Float64, x) : x = 0, readlines("./JuliaToolkits/zDaily/Score")))
print("Total Score: $float_sum_score")