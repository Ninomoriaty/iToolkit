arr_file = readlines("IOtest.txt")
arr_words = arr_file
ls_escape = [" ", ",", "!"]
for escape_char in ls_escape
    global arr_words = collect(Base.Flatten([split(strip(x), escape_char) for x in  arr_words]))
end
for word in union(arr_words)
    isempty(word) ?
    continue : word_frequency = count((x) -> x == word, arr_words)
    println("$word, $word_frequency")
end
