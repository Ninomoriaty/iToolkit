## Guideline Level
#####################################
########## Introduction ##########
#####################################
# Key Words: 



#####################################
########## DataSets(Example) ##########
#####################################
# RDatasets
## pkg
using RDatasets  # example datasets from R like iris

## datasets
iris = dataset("datasets", "iris")


#####################################
########## PyPlot ##########
#####################################
#=

=#
# Pkg
using PyPlot

# Plot Methods
## Basic
x = 1:10
y = 2:2:20
plot(x, y)
title("Title")
title(L"$e^\sqrt{x}$ $LaTeX Title")
xlabel("x-axis")
ylabel("y-axis")
legend("Legend")
grid()
axis("equal")

## save the result
savefig("fig.png")


## log scale plot
semilogx(x,y)  # 
semilogy(x,y)



#####################################
########## Plots ##########
#####################################
#=
Lk
    - : http:/​/​docs.juliaplots.​org/​latest/​
Cc
    - a visualization interface and toolset that works with several backends, in particular GR (the default backend), PyPlot, and PlotyJS.
=#
# pkg
using Plots
## select a certain backend
gr()
pyplot()

# Plot Methods
## Basic Attributes
plot(rand(100))
subplot()
fill
line
xaxis
yaxis
zaxis

# Example
using PyPlot, StatPlots, RDatasets
iris = dataset("datasets", "iris")
@df iris scatter(:SepalLength, :SepalWidth, group = :Species, m = (0.5, [:+ :h :star7], 4), bg=RGB(1.0,1.0,1.0))



#####################################
########## StatPlots ##########
#####################################
#=
=#
# pkg
using StatPlots

