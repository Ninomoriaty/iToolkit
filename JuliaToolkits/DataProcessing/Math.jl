## Guideline Level
##################################
########## Introduction ##########
##################################
# Key Words: Matrix Calculation
#==========================================#
## Packages
### Tips: pkgs are placed in corresponding sections
# General
using LaTeXStrings  # amsmath
using Plots  # Visualisation


########################################
########## Basic Operations ##########
########################################
#==========================================#
## Check number attributes
isequal(1, 1)
1/3 == 1//3  # equaltiy between types
1/3 ≈ 1//3  # appoximately equal (although only mathematical equality is true)
@test 1 ≈ 1.1 atol=0.2  # test if appoximately equal and the tollerance is 0.2
1 < 2
2 > 1
1 <= 2
2 >= 1

iseven(0)
isodd(1)



# Random numbers
rand(100)

# Sum
arr2 = collect(1:10)
sum(arr2)



########################################
########## Primary Algebra ##########
########################################
#==========================================#
# Immutable Constant (Uppercase, Cammel)
# Tips: Constants can only be assigned a value once
# Assign Constants
const TEST = 6.67e-11 # gravitational constant in m3/kg s2

# System info
ARGS  # last output
VERSION  # version of julia
OS_NAME  #

# Math/Physical Constant
pi


#==========================================#
# Primary Calculation
1 == 1

1 + 2
1 - 2

1 * 2
1 / 2
2 \ 1
10 % 4  # rem
rem(10, 4)
11 ÷ 2

2 ^ 3





#==========================================#
# Primary Algebra
# Exp
cbrt(3)
exp(3)
log(exp(3))



# Triangle Algebra
sin(1/2*pi)
sinh(1/2*pi)
cos(pi)
tan(pi)




########################################
########## Algebra ##########
########################################
#=
Lk
    - math.mit.edu/linearalgebra: this website might provde julia codes for Introduction to Linear Algebra 
    - https://docs.julialang.org/en/v1/stdlib/LinearAlgebra/: Offical supports in Linear Algebra by pkg LinearAlgebra
Pcp
    - Code for Introduction to Linear Algebra learniing 
=#
#==========================================#
# pkgs
using Plots
using LinearAlgebra
#using ImmutableArrays  # type-stable and faster arrays, seemingly not usable


#==========================================#
# Vector
# Variables in this section
vec1 = [1, 2, 3]
vec2 = [5, 9, 2]


# Linear combination / Vector addition / Scalar  multiplication
2*vec1 + 4*vec2
2*vec1 - 4*vec2
0*vec1 + 0*vec2  # an idea for zero vector


# Visualise the linear combination(2-3D)
using Plots
function lcPlot(vec_visual_1, vec_visual_2, scalar1, scalar2)
    if length(vec_visual_1) == length(vec_visual_2)
        # linear combination
        vec_visual_lc = scalar1 * vec_visual_1 + scalar2 * vec_visual_2

        # Create the plot
        pic = plot(aspect_ratio = 1, palette = :Dark2_5)
        ## Generate the arrows
        for vec in [vec_visual_1, vec_visual_2, vec_visual_lc]
            args = []
            for i in 1:length(vec)
                args = push!(args, [0, vec[i]])
            end
            plot!(Tuple(args), arrow=true, linewidth=2)
        end

        # Display the plot
        return pic
    else
        print("ERROR: Dimensions of vectors are not equivalent.")
    end
end

lcPlot([4, 1], [-2, 2], 1, 1)
lcPlot([2, 1], [0, 1], 0, 0)
lcPlot([-1, 2, 3], [4, 2, 2], 2, 1)


# Dot product
## Tips: Perpendicular vectors have zero dot products.
sum(vec1 .* vec2)
LinearAlgebra.dot(vec1, vec2)


# length of a vector (||vec1|| not the length(vec1))
sqrt(LinearAlgebra.dot(vec1, vec1))



#==========================================#
# Matrix
# Variables in this section
mat = [1 2 3; 4 5 6; 7 8 9]
mat1 = [1.1 1.2 1.3; 2.1 2.2 2.3; 3.1 3.2 3.3]

# Variables used in this section
vec1 = [i for i in 1:3]
vec2 = [i for i in -1:1]
vec_col = [-1; 0; 1]


# SP. Matrix
## Identity matrix
mat_identity = Matrix(I, 3, 3)
mat_identity = Array(I, 3, 3)


# Manipulation
mat1 * mat1'

## Division
### Tips: X = inv(m1) * m2 LAPACKException or SingularException
M = [2 1; 1.5 0.5]
M / M
M \ M


#
tr(mat1)


# 
det(mat1)

# Transpose
mat1'
transpose(mat1)


# Inverse
# Tips: X = inv(m1) * m2 LAPACKException or SingularException
M * inv(M)


# 特征值
eigvals(mat)


# 特征向量
eigvecs(mat)



########################################
########## Calculus ##########
########################################



########################################
########## ODE ##########
########################################



########################################
########## PDE ##########
########################################



#######################################################
########## Probability Theory and Statistics ##########
#######################################################
#==========================================#
# Pkgs
using Statistics  # Most Statistics data sets
using Distributions  # Probability Theory Distributions
using GLM
using Optim  # Optimise 凸优化 运筹学


#==========================================#
# Set theory
#=
Lk
    - JuliaBasicIntroduction.jl > Collection Type > Set
Cc
    - Set: 
    - 
F
=#

# Variables in set theory section
set1 = ([1, 2, 3])
set2 = ([2, 3, 4])

# Set relationships (Venn)
### Union
union(set1, set2)

### Intersect
intersect(set1, set2)

### Mutually exclusive value in set1
setdiff(set1, set2)
setdiff(set2, set1)

### Check if set1 is a subset of set2
issubset(set1, set2)
issubset(([2,3]), set2)



########################################
####### Discrete Math and Logics #######
########################################
#=
Pcp
    - Code for Discrete Mathematics and its Application learniing 
=#
#==========================================#
# pkgs for discrete math
# Lk: https://www.juliapackages.com/c/discrete-math


#==========================================#
# Logic and Proofs
# Proposition
p = true
q = false


# Logical operators/Connectives
!p; ~p  # Negation
p & q  # Conjunction
p | q  # Disjunction / inclusive or
p ⊻ xor(p, p) #  XOR / exclusive or  (symbol: \xor)
p ⊼ q	# bitwise nand (not and)  (symbol: \nand)
p ⊽ q	# bitwise nor (not or)  (symbol: \nor)
p ? q : print("else")  # Conditional Statement(Lk: Control Flow > if)
p == q  # equivalent/biconditional statements/bi-implications

## Logical Shift
p >>> q
p >> q
p << q


# Check the Truth Table of logical operators
function createTruthTable(operator::Function, proposition::Union{Bool, Vector{Bool}})  # compound or atomic proposition
    truthTable = Array{Union{Bool, Missing}}(missing, length(proposition) + 1, length(proposition) * 2)
    if length(proposition) > 2
        print("ERROR: The number of propositions > 2")
    elseif length(proposition) == 1
        truthTable[:, length(proposition)] = [proposition, !proposition]
        truthTable[:, length(proposition) + 1] = map(operator, truthTable[:, 1])
        return truthTable
    else
        truthTable = Array{Union{Bool, Missing}}(missing, length(proposition) * 2, length(proposition) + 1)
        truthTable[:, 1:length(proposition)] = [p q; p !q; !p q; !p !q]  # TODO:The problem of this function which prohibit the combination of 1 and 2 arguments
        truthTable[:, length(proposition) + 1] = map(operator, truthTable[:, 1], truthTable[:, length(proposition)])
        return truthTable
    end
end

createTruthTable(!, p & (q | !q))
createTruthTable(==, Bool[p, q])


# Bitwise operations
bp = 101
bq = 110
bp & bq  # bitwise or
bp | bq  # bitwise and 
bp ⊻ bq  # bitwise xor (The first bit is 0 and it does not show in number string)


